<div class="page">
  <div class="page-header">
    <ol class="breadcrumb">
      <a href="<?php echo base_url('backend/admdashboard'); ?>" type="button" class="btn btn-round btn-info"><i class="icon md-home" aria-hidden="true"></i>Menu Dashboard</a>
        &nbsp;&nbsp;
    </ol>
    <br>
      <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="<?=base_url('backend/purchase/list_approval_finance')?>">Approval PR List</a></li>
    <li class="breadcrumb-item active">Detail Finance PR List</li>
  </ol>
    
    <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button><p><?php echo $this->session->flashdata('success'); ?></p>
        </div>
    <?php } ?>

  </div>
    <h3 class="panel-title" style="text-align: center; padding: 0px;"><b>Detail Finance PR List</b></h3>
      <div class="page-content">
        <div class="panel">
          <header class="panel-heading" style="text-align: right;">
            <h4><b>PR No.</b> <b style="color: #ff3300; text-decoration: underline;"><?=$pr_no;?></b></h4>
            </header>
          <div class="panel-body">
              <table  id="tableData" class="table table-hover dataTable table-striped w-full">
              <thead>
                <tr>
                  <th rowspan="2">No.</th>
                  <!-- <th rowspan="2">Id PR</th> -->
                  <th rowspan="2">PR No.</th>
                  <th rowspan="2">PO No.</th>
                  <th rowspan="2">Part No</th>
                  <th rowspan="2">Part Name</th>
                  <th rowspan="2">Purpose</th>
                  <th rowspan="2">Cost Center</th>
                  <th rowspan="2">Category Request</th>
                  <th rowspan="2">New Order Qty</th>
                  <th rowspan="2">UOM</th>
                  <th rowspan="2">Qty Current Balance</th>
                  <th rowspan="2">Date Current Balance</th>
                  <th rowspan="2">Unit Price</th>
                  <th rowspan="2">Total Amount</th>
                  <th rowspan="2">Supplier Name</th>
                  <th colspan="4" style="text-align: center; padding: 0px;">LAST ORDER</th>
                </tr>
                <tr>
                  <th>Date</th>
                <th>Qty</th>
                <th>Unit Price </th>
                <th>Supplier </th>
                </tr>
              </thead>
              <tbody>
               <?php $no=1; foreach ($list_fin as $val) { ?>
              <tr>
                  <td><?= $no++ ?></td>
                  <!-- <td><input type="text" id="id_pr" name="id_pr" value="<?=$val->id_pr;?>" readonly></td> -->
                  <td><?=$val->pr_no;?></td>
                  <td><?=$val->po_no;?></td>
                  <td><?=$val->part_no;?></td>
                  <td><?=$val->description;?></td>
                  <td><?=$val->purpose;?></td>
                  <td><?=$val->cost_center;?></td>
                  <td><?=$val->category_request;?></td>
                  <td><?=$val->new_order_qty;?></td>
                  <td><?=$val->uom;?></td>
                  <td><?=$val->qty_cb;?></td>
                  <td><?=$val->date_cb;?></td>
                  <td><?=$val->unit_price_pf;?></td>
                  <td><?=$val->total_amount;?></td>
                  <td><?=$val->supplier_name_pf;?></td>
                  <td><?=$val->date_pf;?></td>
                  <td><?=$val->qty_lo_pf;?></td>
                  <td><?=$val->unit_price_pf;?></td>
                  <td><?=$val->supplier_lo;?></td>
                 
            </tr>
              <?php } ?>
               </tbody>
            </table>
             <table id="tblSum" class="table table-hover dataTable table-striped w-full">
               <thead>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><b>TOTAL</b></td>
                  <td><?=$ttl_sum->ttl_amount?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
    </div>
</div>

<div class="modal fade" id="loader" aria-hidden="true" aria-labelledby="filterdata" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-center">
    <div class="modal-content">
      <div class="modal-header">
      </div>
       <div class="loader"></div>
        <h4 class="modal-title" style="text-align: center;">Loading....</h4>
  </div>
</div>

<style>
.swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
  font-family: Georgia, serif;
}
</style>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js" integrity="sha256-Ry2q7Rf2s2TWPC2ddAg7eLmm7Am6S52743VTZRx9ENw=" crossorigin="anonymous"></script>

<script type="text/javascript">
  $( document ).ready(function() {

  var currency = "<?=$currency->currency?>";

  $('#tableData').DataTable({
        "scrollX": true,
        "scrollY":true,
         columnDefs: [    
          {
              targets: 11,
              render: $.fn.dataTable.render.number( '.', ',', 0,currency )
          },
          {
              targets: 12,
              render: $.fn.dataTable.render.number( '.', ',', 0,currency )
          },
        ],
    });

  $('#tblSum').DataTable({
    paging: false,
    ordering: false,
    info: false,
    "searching": false,
    columnDefs: [    
          {
              targets: 20,
              render: $.fn.dataTable.render.number( '.', ',', 0,currency)
          },
          
        ],
  });
});

</script>
<script>
  $("#tbl").on("click", ".change", function () {
  var id = $(this).attr("data-bind");
  Swal.fire({
    title: "you want to Read Detail PR?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
      $.ajax({
          success: function(data) {
            window.location.href = '<?= base_url("backend/purchase/list_detail_my_pr/")?>'+id;
          }
        });
    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});




$("#tbl").on("click", ".approve", function () {
  var id = $(this).attr("data-bind");
  // console.log(id);
  Swal.fire({
    title: "Are You Sure Approve The PR?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
        $.ajax({
        url: "<?= base_url("backend/purchase/approve_pr_hod/")?>"+id,
        type:"DELETE",
        success: function(data){
          // console.log(data);
        // console.log("sukses");
        window.location.reload();
        },
        
      });

    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});


$("#tbl").on("click", ".decline", function () {
  var id = $(this).attr("data-bind");
  // console.log(id);
  Swal.fire({
    title: "Are You Sure Decline The PR?",
    type: 'question',
    icon: 'warning',
     html: '<div class="example-grid">'+
              '<label class="col-xl-12 col-md-3 form-control-label"><b>Reason Decline.</b><span class="required">*</span></label>'+
                  '<input id="reason" type="text" name="reason"/>'+
          '</div>',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    var reason = $('#reason').val();

    console.log(reason);
    if (result.value == true) {
        $.ajax({
        url: "<?= base_url("backend/purchase/decline_pr_hod/")?>"+id+'/'+reason,
        type:"DELETE",
        success: function(data){
        window.location.reload();
        },
        
      });

    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});

</script>