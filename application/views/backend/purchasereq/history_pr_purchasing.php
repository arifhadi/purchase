<div class="page">
  <div class="page-header">
    <ol class="breadcrumb">
      <a href="<?php echo base_url('backend/admdashboard'); ?>" type="button" class="btn btn-round btn-info"><i class="icon md-home" aria-hidden="true"></i>Menu Dashboard</a>
        &nbsp;&nbsp;
    </ol>
    <br>
    
    <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button><p><?php echo $this->session->flashdata('success'); ?></p>
        </div>
    <?php } ?>

  </div>
    <h3 class="panel-title" style="text-align: center; padding: 0px;"><b>HISTORY PR REQUEST PURCHASING</b></h3>
      <div class="page-content">
        <div class="panel">
          <div class="panel-body">
            <table  id="tbl"class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>PR No.</th>
                  <th>Name Request</th>
                  <th>Date Request</th>
                  <th>Status</th>
                  <th>Action</th>
                  <th>Urgency</th>
                  <th>Machine No</th>
                  <th>Tool No</th>
                  <th>Claimable</th>
                  <th>Expected Material Arrive</th>
                  <th>Department</th>
                  <th>Name HOD</th>
                  <th>Name Werehouse</th>
                </tr>
              </thead>
              <tbody>
              <?php $no=1; foreach ($historypur as $val) { ?>
              <tr>
               <td><?= $no++ ?></td>
               <td><?=$val->pr_no;?></td>
               <td><?=$val->first_name;?></td>
               <td><?=$val->create_at;?></td>

               <?php if($val->is_approve_purchasing==1)
               {
                if($val->is_reject_by_purchasing==1)
                {
                  ?><td style="color: #ff0000;"><i class="zmdi zmdi-close-circle"></i></td>
                  <?php
                }
                else{
                  ?><td style="color: #2ecc71;"><i class="zmdi zmdi-check-circle"></i></td><?php
                }
               }if($val->is_approve_purchasing==0)
               {
                  if($val->is_reject_by_purchasing==1)
                  {
                    ?><td style="color: #ff0000;"><i class="zmdi zmdi-close-circle"></i></td>
                  <?php
                  }
                  else{
                  ?><td style="color: #ff0000;">Need Purchasing Check</td><?php
                 }
               }
               ?>

                <td>
                     <button data-bind="<?=$val->pr_no;?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-success btn-xs change" title="View Detail"><i class="zmdi zmdi-file" aria-hidden="true"></i></button>
                </td>
                <td><?=$val->urgency;?></td>
                <td><?=$val->mc_no;?> - <?=$val->line_mc_no;?> </td>
                <td><?=$val->tool_no;?></td>
                <td><?=$val->claimable;?></td>
                <td><?=$val->expected_material_date_arrive;?></td>
                <td><?=$val->department;?></td>
                <td><?=$val->user_hod;?></td>
                <td><?=$val->user_werehouse;?></td>
              </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
    </div>
</div>

<style>
.swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
  font-family: Georgia, serif;
}
</style>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js" integrity="sha256-Ry2q7Rf2s2TWPC2ddAg7eLmm7Am6S52743VTZRx9ENw=" crossorigin="anonymous"></script>


<script>
    $("#tbl").on("click", ".change", function () {
  var id = $(this).attr("data-bind");
  Swal.fire({
    title: "you want to Read Detail PR?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
      $.ajax({
          success: function(data) {
            window.location.href = '<?= base_url("backend/purchase/list_detail_purchasing/")?>'+id;
          }
        });
    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});
</script>