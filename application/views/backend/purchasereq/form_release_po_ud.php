<div class="page">
  <div class="page-header">
    <ol class="breadcrumb">
      <a href="<?php echo base_url('backend/admdashboard'); ?>" type="button" class="btn btn-round btn-info"><i class="icon md-home" aria-hidden="true"></i>Menu Dashboard</a>
        &nbsp;&nbsp;
    </ol>
     <br>
    <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="<?=base_url('backend/purchase/list_data_all_pr')?>">All Request PR</a></li>
    <li class="breadcrumb-item active">Release PO PR</li>
  </ol>

    
    <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button><p><?php echo $this->session->flashdata('success'); ?></p>
        </div>
    <?php } ?>

  </div>

  <h3 class="panel-title" style="text-align: center; padding: 0px;"><b>Release PO PR</b></h3>
    <div class="page-content" style="padding: 0px 0px;">
        <div class="panel">
          <header class="panel-heading" style="text-align: right;">
            <h4><b>PR No.</b> <b style="color: #ff3300; text-decoration: underline;"><?=$pr_no;?></b></h4>
          </header>
          <div class="panel-body">
          <table  id="tableData" class="table table-hover dataTable table-striped w-full">
              <thead>
                <tr>
                  <th rowspan="2">No.</th>
                  <th rowspan="2">Id Pr</th>
                  <th rowspan="2">PR No.</th>
                  <th rowspan="2">PO No.</th>
                  <th rowspan="2">Description</th>
                  <th rowspan="2">Purpose</th>
                  <th rowspan="2">Cost Center</th>
                  <th rowspan="2">Category Request</th>
                  <th rowspan="2">New Order Qty</th>
                  <th rowspan="2">UOM</th>
                  <th rowspan="2">Qty Current Balance</th>
                  <th rowspan="2">Date Current Balance</th>
                  <th rowspan="2">Unit Price</th> 
                  <th rowspan="2">Currency</th>
                  <th rowspan="2">Total Ammount</th>
                  <th rowspan="2">Supplier Name</th>
                  <th colspan="4" style="text-align: center; padding: 0px;">LAST ORDER</th>
                </tr>
                <tr>
                  <th>Date</th>
                <th>Qty</th>
                <th>Unit Price </th>
                <th>Supplier </th>
                </tr>
              </thead>
            <tbody>
               <?php $no=1; foreach ($list_pur as $val) { ?>
              <tr>
                  <td><?= $no++ ?></td>
                  <td><?=$val->id_pr?></td>
                  <!-- <td><input type="text" id="id_pr" name="id_pr" value="<?=$val->id_pr;?>" readonly></td> -->
                  <td><?=$val->pr_no;?></td>
                  <td><?=$val->po_no;?></td>
                  <td><?=$val->description;?></td>
                  <td><?=$val->purpose;?></td>
                  <td><?=$val->cost_center;?></td>
                  <td><?=$val->category_request;?></td>
                  <td><?=$val->new_order_qty;?></td>
                  <td><?=$val->uom;?></td>
                  <td><?=$val->qty_cb;?></td>
                  <td><?=$val->date_cb;?></td>
                  <td><?=$val->unit_price_pf;?></td>
                  <td><?=$val->currency;?></td>
                  <td><?=$val->total_amount;?></td>
                  <td><?=$val->supplier_name_pf;?></td>
                  <td><?=$val->date_pf;?></td>
                  <td><?=$val->qty_lo_pf;?></td>
                  <td><?=$val->unit_price_lo;?></td>
                  <td><?=$val->supplier_lo;?></td>
            </tr>
              <?php } ?>
               </tbody>
            </table>
            <table id="tblSum" class="table table-hover dataTable table-striped w-full">
               <thead>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><b>TOTAL</b></td>
                  <td><?=$ttl_sum->ttl_amount?></td>
                </tr>
              </tbody>
            </table>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn-sm btn-success" type="button" onclick="approve()">Release&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;
          </div>

        </div>
    </div>


</div>



<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>

<div class="modal fade" id="loader" aria-hidden="true" aria-labelledby="filterdata" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-center">
    <div class="modal-content">
      <div class="modal-header">
      </div>
       <div class="loader"></div>
        <h4 class="modal-title" style="text-align: center;">Loading...</h4>
  </div>
</div>

<style>
.swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
  font-family: Georgia, serif;
}
</style>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js" integrity="sha256-Ry2q7Rf2s2TWPC2ddAg7eLmm7Am6S52743VTZRx9ENw=" crossorigin="anonymous"></script>

<script type="text/javascript">


$( document ).ready(function() {

  var currency = "<?=$currency?>";

  // console.log(currency);
  $('#tableData').DataTable({
        "scrollX": true,
        "scrollY":true,
         columnDefs: [    
          {
              targets: 12,
              render: $.fn.dataTable.render.number( '.', ',', 0,currency )
          },
          {
              targets: 14,
              render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
           {
              targets: 18,
              render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
        ],


    });

  // var currency

  $('#tblSum').DataTable({
    paging: false,
    ordering: false,
    info: false,
    "searching": false,
    columnDefs: [    
          {
              targets: 20,
              render: $.fn.dataTable.render.number( '.', ',', 0,'$')
          },
          
        ],
  });
});


</script>



<script>


function approve()
{
  var val = "<?=$pr_no?>";


  Swal.fire({
    title: "You Want To Release This PR?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
        $.ajax({
        method: 'POST',
        url: "<?= base_url("backend/purchase/action_release_po_ud/")?>",
        data: {'pr_no': val},
        cache: false,
        async : true,
        dataType : 'json',
        beforeSend:function()
        {
             $('#loader').modal('show');
        },
        success: function(data){
        // window.location.reload();
        $('#loader').modal('hide');
        window.location.href = '<?= base_url("backend/purchase/list_data_all_pr")?>';
        },
        error:function(data)
        {
          // window.location.reload();
          // $('#loader').modal('show');
           $('#loader').modal('hide');
          window.location.href = '<?= base_url("backend/purchase/list_data_all_pr")?>';
        }
      });

    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
}


</script>

