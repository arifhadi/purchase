<div class="page">
  <div class="page-header">
    <ol class="breadcrumb">
      <a href="<?php echo base_url('backend/admdashboard'); ?>" type="button" class="btn btn-round btn-info"><i class="icon md-home" aria-hidden="true"></i>Menu Dashboard</a>
      &nbsp;&nbsp;
      <a href="<?php echo base_url('backend/purchase/create_pr'); ?>" type="button" class="btn btn-round btn-danger"><i class="icon md-plus" aria-hidden="true"></i>Request PR</a>
    </ol>
    <br>
    
    <?php if ($this->session->flashdata('success')) { ?>
      <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button><p><?php echo $this->session->flashdata('success'); ?></p>
      </div>
    <?php } ?>
  </div>
  <div class="page-content">
    <div class="panel">
      <header class="panel-heading" style="text-align: right;">
       <h4><b>PR No.</b> <b style="color: #ff3300; text-decoration: underline;"><?=$pr_no;?></b></h4>
     </header>
     <div class="panel-body container-fluid" style="padding: 0px;">
      <!-- **Alert**-->
      <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button><p><?php echo $this->session->flashdata('success'); ?></p>
        </div>
      <?php }elseif($this->session->flashdata('error')){ ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button><p><?php echo $this->session->flashdata('error'); ?></p>
        </div>
      <?php } ?>

      <div class="panel">
        <div class="panel-body container-fluid" style="padding: 20px;">
          <div class="row row-lg">

            <!-- Button Action -->
            <div class="col-lg-10 form-group form-material">
             <div class="pearls row">
              <div class="pearl current col-4">
                <div class="pearl-icon" style="color: green; border-color: green;"><i class="icon md-check" aria-hidden="true"></i></div>
                <span class="pearl-title"><a href="#"> <button type="button" class="btn btn-secondary btn-sm active">&nbsp; General PR &nbsp;</button></span>
                </div>
                <?php if (!empty($edit_data_1st)) { ?>
                  <div class="pearl current col-4">
                    <div class="pearl-icon" style="color: green; border-color: green;"><i class="icon md-check" aria-hidden="true"></i></div>
                    <span class="pearl-title"><a href="#" class="btn btn-info btn-sm">&emsp; Detail PR &emsp;</a></span>
                  </div>
                <?php }else{ ?>
                  <div class="pearl col-4">
                    <div class="pearl-icon"><i class="icon md-check" aria-hidden="true"></i></div>
                    <span class="pearl-title"><a href="#" class="btn btn-info btn-sm">&emsp; Detail PR &emsp;</a></span>
                  </div>
                <?php } ?>

              </div>
            </div>
            <div class="col-lg-2 form-group form-material">
              <div class="pearls row">
                <div class="col-lg-5"><br><br>
                  <button type="Submit" class="btn btn-success btn-sm" form="login_validation">&emsp; ADD ITEM &emsp;</button>
                </div>
              </div>
            </div>
            <!-- End Button -->
            <!-- Example Horizontal Form -->

            <div class="col-md-12 col-lg-5">
              <div class="example-wrap">
                <div class="example">
                  <!-- <form class="form-horizontal"> -->
                    <?= form_open(base_url('backend/purchase/update_general_pr'),  'id="login_validation" enctype="multipart/form-data"') ?>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Part Name<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <select class="form-control" data-plugin="select2" id="description" name="description" data-placeholder="Select Description">
                           <option></option>
                            <?php foreach ($list_item as $val) { ?>
                                  <option value="<?=$val->id_item;?>"><?php echo "$val->item_no - $val->description"?></option>
                            <?php } ?>
                        </select>

                        </div>
                      </div>
                      <div hidden class="form-group row">
                        <label class="col-md-3 form-control-label"><b>PR_NO<b style="color: red;">*</b> : </b></label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="pr_no" value="<?=$pr_no;?>" required="required" />
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-md-3 form-control-label"><b>Purpose<b style="color: red;">*</b> : </b></label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="purpose" required="required" />
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-md-3 form-control-label"><b>Cost Center<b style="color: red;">*</b> : </b></label>
                        <div class="col-md-9">
                          <select class="form-control" required="required" data-plugin="select2" id="cost_center" name="cost_center" data-placeholder="Select Cost Center">
                           <option></option>
                           <?php foreach ($cost_center as $val) { ?>
                            <option value="<?=$val->cost_center;?>"><?=$val->cost_center;?>  -  ( <?=$val->dept;?> )</option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                  </div>
                </div>
              </div>

              <div class="col-md-12 col-lg-6">
                <div class="example-wrap">
                  <div class="example">

                   <div class="form-group row">
                    <label class="col-md-3 form-control-label"><b>Category Request<b style="color: red;">*</b> : </b></label>
                    <div class="col-md-9">
                     <select class="form-control" required="required" data-plugin="select2" id="category_request" name="category_request" data-placeholder="Select Category Request">
                       <option></option>
                       <?php foreach ($category_request as $val) { ?>
                        <option value="<?=$val->category_request;?>"><?=$val->code;?>  -  ( <?=$val->category_request;?> )</option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-3 form-control-label"><b>New Order Qty<b style="color: red;">*</b> : </b></label>
                  <div class="col-md-9">
                    <input type="number" class="form-control" name="new_order_qty" autocomplete="off" required="required"  />
                  </div>
                </div>


                <div class="form-group row">
                  <label class="col-md-3 form-control-label"><b>UOM<b style="color: red;">*</b> : </b></label>
                  <div class="col-md-9">
                    <select class="form-control" required="required" data-plugin="select2" id="uom" name="uom" data-placeholder="Select UOM">
                      <option></option>
                      <option>BAG</option>
                      <option>BAR</option>
                      <option>BOTTLE</option>
                      <option>BOX</option>
                      <option>CAN</option>
                      <option>CASE</option>
                      <option>CM</option>
                      <option>CNTR</option>
                      <option>CUBIC</option>
                      <option>DRUM</option>
                      <option>EA</option>
                      <option>GALLON</option>
                      <option>HOLES</option>
                      <option>JOB</option>
                      <option>KG</option>
                      <option>LENGTH</option>
                      <option>LG</option>
                      <option>LITRE</option>
                      <option>LOT</option>
                      <option>METER</option>
                      <option>MONTH</option>
                      <option>ONS</option>
                      <option>PACK</option>
                      <option>PAD</option>
                      <option>PAIL</option>
                      <option>PAIR</option>
                      <option>PAX</option>
                      <option>PERSON</option>
                      <option>REAM</option>
                      <option>ROLL</option>
                      <option>SACK</option>
                      <option>SAMPLE</option>
                      <option>SET</option>
                      <option>SHEET</option>
                      <option>TIME</option>
                      <option>TIN</option>
                      <option>TUBE</option>
                      <option>UNIT</option>
                      <option>VISIT</option>
                      <option>WAY</option>
                      <option>YEAR</option>
                    </select>
                  </div>
                </div>


            <!--   <div class="col-lg-2 form-group form-material">
                <div class="pearls row">
                  
                </div>
              </div> -->

            </div>
          </div>
        </div>

        <?php form_close() ?>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<div class="page-content">
  <div class="panel">
    <div class="panel-body">
      <table  id="tbl"class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
        <thead>
          <tr>
            <th>No.</th>
            <th>PR No.</th>
            <th>PO No.</th>
            <th>Part Name</th>
            <th>Purpose</th>
            <th>Cost Center</th>
            <th>Category Request</th>
            <th>New Order Qty</th>
            <th>UOM</th>
          </tr>
        </thead>
        <tbody>
         <?php $no=1; foreach ($edit_data as $val) { ?>
          <tr>
            <td><?= $no++ ?></td>
            <td><?=$val->pr_no;?></td>
            <td><?=$val->po_no;?></td>
            <td><?=$val->description;?></td>
            <td><?=$val->purpose;?></td>
            <td><?=$val->cost_center;?></td>
            <td><?=$val->category_request;?></td>
            <td><?=$val->new_order_qty;?></td>
            <td><?=$val->uom;?></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
    <div class="col-lg-10"><br>
      &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<button data-bind="<?=$pr_no;?>" onclick="finishPr()" type="button" data-toggle="tooltip" class="btn btn-success finish" title="FINISH PR">&emsp; FINISIH&emsp;</button>
      </div>
  </div>
</div>
</div>
</div>

<style>
.swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
  font-family: Georgia, serif;
}
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
  
</style>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js" integrity="sha256-Ry2q7Rf2s2TWPC2ddAg7eLmm7Am6S52743VTZRx9ENw=" crossorigin="anonymous"></script>
<div class="modal fade" id="loader" aria-hidden="true" aria-labelledby="filterdata" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-center">
    <div class="modal-content">
      <div class="modal-header">
      </div>
       <div class="loader"></div>
        <h4 class="modal-title" style="text-align: center;">Loading...</h4>
  </div>
</div>

<script>
  function finishPr()
  {
      var val = "<?=$pr_no?>";

      // console.log(val);
  Swal.fire({
    title: "you want to Send Your PR?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
        $.ajax({
        method: 'POST',
        url: "<?= base_url("backend/purchase/finish_request_pr/")?>",
        data: {'pr_no': val},
        cache: false,
        async : true,
        dataType : 'json',
        beforeSend:function()
        {
             $('#loader').modal('show');
        },
        success: function(data){
        $('#loader').modal('hide');
        window.location.href = '<?= base_url("backend/purchase/list_my_pr")?>';
        },
        error:function(data)
        {
          $('#loader').modal('hide');
          window.location.href = '<?= base_url("backend/purchase/list_my_pr")?>';
        }
      });

    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
  }
</script>