<!-- Page -->
<div class="page">
  <ol class="breadcrumb">
    <a href="<?php echo base_url('backend/purchase/list_data_item'); ?>" type="button" class="btn btn-round btn-warning"><i class="icon md-format-indent-increase" aria-hidden="true"></i>Item List</a>
  </ol>
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="<?=base_url('backend/purchase/list_data_item')?>">Item List</a></li>
    <li class="breadcrumb-item active">Create Item</li>
  </ol>
  <h4 style="text-align: left; color:#0000e6; font-weight: 900;"><b>&emsp; >>Create<< </b></h4>
  <?php if ($this->session->flashdata('success')) { ?>
  <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('success'); ?></p>
  </div>
  <?php }elseif($this->session->flashdata('error')){ ?>
  <div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('error'); ?></p>
  </div>
  <?php } ?>
  <div class="page-header" style="text-align: center; padding: 0px;">
    <h1 class="page-title">Form Create Item</h1>
  </div>
  <div class="page-content" style="padding: 0px;">
    <div class="panel">
      <div class="panel-body container-fluid" style="padding: 0px;">
        <div class="panel">
          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-md-12 col-lg-6">
                <!-- Example Horizontal Form -->
                <div class="example-wrap">
                  <div class="example">
                    <!-- <form class="form-horizontal"> -->
                    <?= form_open(base_url('backend/purchase/item_create'),  'id="login_validation" enctype="multipart/form-data"') ?>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Part No<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" placeholder="Part No"  autocomplete="off" name="item_no" />
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Part Name<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" placeholder="Part Name"  autocomplete="off" name="description" />
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Unit Price<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" id="unit_price" name="unit_price" placeholder="Unit Price" autocomplete="off" oninput="setRibuan()" />
                      </div>
                    </div>
                    


                  </div>
                </div>
                <!-- End Example Horizontal Form -->
              </div>
              <div class="col-md-12 col-lg-6">

                <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Currency<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <select class="form-control" required="required" data-plugin="select2" id="currency" name="currency" data-placeholder="Select Currency">
                          <option></option>
                          <option value="IDR">IDR</option>
                          <option value="USD">USD</option>
                          <option value="SGD">SGD</option>
                          <option value="JPY">JPY</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Supplier Name<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <select class="form-control" required="required" data-plugin="select2" id="supplier_name_pf" name="supplier_name_pf" data-placeholder="Select Supplier">
                          <option></option>
                          <?php foreach ($sup as $val) { ?>
                          <option value="<?php echo $val->code?>">
                            <?php echo "$val->code - $val->name" ?>
                          </option>
                          <?php } ?>
                        </select>

                      </div>
                    </div>

                  <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Incoterm<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" id="incoterm" name="incoterm" placeholder="Incoterm" autocomplete="off" />
                      </div>
                    </div>

              </div>
              <!-- Button Action -->
              <div class="col-lg-5 form-group form-material">
                <!-- <input type="text" class="form-control" placeholder=".col-lg-4"> -->
              </div>
              <div class="col-lg-5 form-group form-material">
                <button type="Submit" class="btn btn-success btn-sm">&emsp;&emsp;SAVE&emsp;&emsp;</button>
              </div>
              <div class="col-lg-2 form-group form-material">
              </div>
              <?php form_close() ?>
              <!-- Button Action -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function setRibuan()
  {

     var unit_price = document.getElementById('unit_price');
    var total_amount = document.getElementById('total_amount');

    unit_price.value = formatRupiah(unit_price.value);
    
    // total_amount.value = formatRupiah(total_amount.value);



  }
 function formatRupiah(angka, prefix)
    {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split    = number_string.split(','),
            sisa     = split[0].length % 3,
            rupiah     = split[0].substr(0, sisa),
            ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
            
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'IDR. ' + rupiah : '');
  }

</script>
<!-- End Page -->