<!-- Page -->
<div class="page">
  <ol class="breadcrumb">
    <a href="<?php echo base_url('backend/purchase/list_my_pr'); ?>" type="button" class="btn btn-round btn-warning"><i class="icon md-format-indent-increase" aria-hidden="true"></i>PR List</a>
  </ol>
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="<?=base_url('backend/purchase/list_my_pr')?>">PR List</a></li>
    <li class="breadcrumb-item active">Create Request PR</li>
  </ol>
  <h4 style="text-align: left; color:#0000e6; font-weight: 900;"><b>&emsp; >>Create<< </b></h4>
  <div class="page-header" style="text-align: center; padding: 0px;">
    <h1 class="page-title">Form Request PR</h1>
  </div>
  <div class="page-content">
    <div class="panel">
      <div class="panel-body container-fluid" style="padding: 0px;">
        <div class="panel">
          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-lg-10 form-group form-material">
                <div class="pearls row">
                  <div class="pearl col-4">
                    <div class="pearl-icon"><i class="icon md-check" aria-hidden="true"></i></div>
                    <span class="pearl-title"> <button type="button" class="btn btn-info btn-sm">&nbsp; General PR &nbsp;</button></span>
                  </div>
                  <div class="pearl col-4">
                    <div class="pearl-icon"><i class="icon md-check" aria-hidden="true"></i></div>
                    <span class="pearl-title"> <button type="button" class="btn btn-secondary btn-sm" disabled>&emsp; Detail PR &emsp;</button></span>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 form-group form-material">
                <div class="pearls row">
                  <div class="col-lg-3"><br><br>
                    <button type="Submit" class="btn btn-success btn-sm" form="login_validation">&emsp; NEXT &emsp;</button>
                  </div>
                </div>
              </div>

              <div class="col-md-12 col-lg-6">
                <!-- Example Horizontal Form -->
                <div class="example-wrap">
                  <div class="example">
                    <!-- <form class="form-horizontal"> -->
                    <?= form_open(base_url('backend/purchase/create_pr'),  'id="login_validation" enctype="multipart/form-data"') ?>

                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Urgency<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <select class="form-control" required data-plugin="select2" id="urgency" name="urgency" data-placeholder="Select Urgency">
                          <option></option>
                          <option>YES</option>
                          <option>NO</option>
                        </select>
                      </div>
                    </div>
                    
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Machine No</b> : </b></label>
                      <div class="col-md-9">
                       <select class="form-control" data-plugin="select2" id="mc_no" name="mc_no" data-placeholder="Select Machine">
                          <option></option>
                          <?php foreach ($machine as $val) { ?>
                          <option value="<?php echo $val->id_machine?>">
                            <?php echo "$val->mc_no - $val->line_mc_no" ?>
                          </option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Mold/Tooling No</b> : </b></label>
                      <div class="col-md-9">
                       <select class="form-control" data-plugin="select2" id="tool_no" name="tool_no" data-placeholder="Select Mold ">
                          <option></option>
                          <?php foreach ($tool as $val) { ?>
                          <option value="<?php echo $val->id?>">
                            <?php echo "$val->mc_no - $val->line_mc_no" ?>
                          </option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                  </div>
                </div>
                <!-- End Example Horizontal Form -->
              </div>
              <div class="col-md-12 col-lg-6">
                <!-- Example Horizontal Form -->
                <div class="example-wrap">
                  <div class="example">
                  <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Uncurrent Demand<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="radio" class="StatusPart" value="1" name="is_uncurrent_demand" id="Active" required>&ensp;<label class="badge badge-success" style="font-size: 12px;" for="Active" dreq>&emsp;Active&emsp;</label>&emsp;
                        <input type="radio" class="StatusPart" value="0" name="is_uncurrent_demand" id="Nonactive">&ensp;<label class="badge badge-danger" style="font-size: 12px;" for="Nonactive">Non-Active</label>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Claimable  (Yes/No)<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                       <select class="form-control" required data-plugin="select2" id="claimable" name="claimable" data-placeholder="Select Claimable">
                          <option></option>
                          <option>YES</option>
                          <option>NO</option>
                        </select>
                      </div>
                    </div>



                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Department<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                       <input type="text" class="form-control" name="department" autocomplete="off" required="required" value="<?=$department->department?>" readonly/>
                      </div>
                    </div>

                 

                  </div>
                </div>
                <!-- End Example Horizontal Form -->
              </div>
              <!-- Button Action -->
              <div class="col-lg-5 form-group form-material">
                <!-- <input type="text" class="form-control" placeholder=".col-lg-4"> -->
              </div>
         <!--      <div class="col-lg-5 form-group form-material">
                
                <button type="reset" class="btn btn-sm" style="background-color: #ff0066; color: white;">&nbsp;&nbsp;&nbsp;REFRESH&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button type="Submit" class="btn btn-success btn-sm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SAVE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
              </div> -->
              <div class="col-lg-2 form-group form-material">
                <!-- <input type="text" class="form-control" placeholder=".col-lg-4"> -->
              </div>
              <?php form_close() ?>
              <!-- Button Action -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Page -->
