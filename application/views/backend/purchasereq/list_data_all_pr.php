<div class="page">
  <div class="page-header">
    <ol class="breadcrumb">
      <a href="<?php echo base_url('backend/admdashboard'); ?>" type="button" class="btn btn-round btn-info"><i class="icon md-home" aria-hidden="true"></i>Menu Dashboard</a>
    </ol>
    <br>
    
    <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button><p><?php echo $this->session->flashdata('success'); ?></p>
        </div>
    <?php } ?>

  </div>
    <h3 class="panel-title" style="text-align: center; padding: 0px;"><b>All PR List</b></h3>
      <div class="page-content">
        <div class="panel">
          <div class="panel-body">
            <table  id="tbl" class="table table-hover dataTable table-striped w-full">
              <thead>
                <tr>
                  <th rowspan="2">No.</th>
                  <th rowspan="2">PR No.</th>
                  <th rowspan="2">Date Request</th>
                  <th rowspan="2">Name Requestor</th>
                  <th colspan="5" style="text-align: center; padding: 0px;">Status</th>
                  <th rowspan="2">Action</th>
                  <th rowspan="2">Urgency</th>
                  <th rowspan="2">Machine No</th>
                  <th rowspan="2">Tool No</th>
                  <th rowspan="2">Claimable</th>
                  <th rowspan="2">Expected Material Date Arrive</th>
                  <th rowspan="2">Department</th>
                </tr>
                <tr>
                  <th>Checked By HOD</th>
                  <th>Verified by Warehouse</th>
                  <th>Verified by Finance</th>
                  <th>Approved by Division Head</th>
                  <th>Approved by PD</th>
                </tr>
              </thead>
              <tbody>
              <?php $no=1; foreach ($allpr as $val) { ?>
              <tr>
               <td><?= $no++ ?></td>
               <td><?=$val->pr_no;?></td>
               <td><?=$val->create_at;?></td>
               <td><?=$val->first_name;?></td>

               <?php if($val->is_approve_hod==1)
               {
                if($val->is_reject_hod==1)
                {
                  ?><td style="color: #ff0000;"><i class="zmdi zmdi-close-circle"></i></td>
                  <?php
                }
                else{
                  ?><td style="color: #2ecc71;"><i class="zmdi zmdi-check-circle"></i></td><?php
                }
               }if($val->is_approve_hod==0)
               {
                  if($val->is_reject_hod==1)
                  { 
                    ?><td style="color: #ff0000;"><i class="zmdi zmdi-close-circle"></i></td>
                  <?php
                  }
                  else{
                  ?><td style="color: #ff0000;">Need HOD Check</td><?php
                 }
               }
               ?>


              <?php if($val->is_approve_werehouse==1)
               {
                if($val->is_reject_werehouse==1)
                {
                  ?><td style="color: #ff0000;"><i class="zmdi zmdi-close-circle"></i></td>
                  <?php
                }
                else{
                  ?><td style="color: #2ecc71;"><i class="zmdi zmdi-check-circle"></i></td><?php
                }
               }if($val->is_approve_werehouse==0)
               {
                  if($val->is_reject_werehouse==1)
                  {
                    ?><td style="color: #ff0000;"><i class="zmdi zmdi-close-circle"></i></td>
                  <?php
                  }
                  else{
                  ?><td style="color: #ff0000;">Need Werehouse Verified</td><?php
                 }
               }
               ?>

                <?php if($val->is_approve_finance==1)
               {
                if($val->is_reject_finance==1)
                {
                  ?><td style="color: #ff0000;"><i class="zmdi zmdi-close-circle"></i></td>
                  <?php
                }
                else{
                  ?><td style="color: #2ecc71;"><i class="zmdi zmdi-check-circle"></i></td><?php
                }
               }if($val->is_approve_finance==0)
               {
                  if($val->is_reject_finance==1)
                  {
                    ?><td style="color: #ff0000;"><i class="zmdi zmdi-close-circle"></i></td>
                  <?php
                  }
                  else{
                  ?><td style="color: #ff0000;">Need Finance Verified</td><?php
                 }
               }
               ?>

             <?php if($val->is_approve_division_head==1)
               {
                if($val->is_reject_division_head==1)
                {
                  ?><td style="color: #ff0000;"><i class="zmdi zmdi-close-circle"></i></td>
                  <?php
                }
                else{
                  ?><td style="color: #2ecc71;"><i class="zmdi zmdi-check-circle"></i></td><?php
                }
               }if($val->is_approve_division_head==0)
               {
                  if($val->is_reject_division_head==1)
                  {
                    ?><td style="color: #ff0000;"><i class="zmdi zmdi-close-circle"></i></td>
                  <?php
                  }
                  else{
                  ?><td style="color: #ff0000;">Need Division  Verified</td><?php
                 }
               }
               ?>

              <?php if($val->is_approve_by_pd==1)
               {
                if($val->is_reject_by_pd==1)
                {
                  ?><td style="color: #ff0000;"><i class="zmdi zmdi-close-circle"></i></td>
                  <?php
                }
                else{
                  ?><td style="color: #2ecc71;"><i class="zmdi zmdi-check-circle"></i></td><?php
                }
               }if($val->is_approve_by_pd==0)
               {
                  if($val->is_reject_by_pd==1)
                  {
                    ?><td style="color: #ff0000;"><i class="zmdi zmdi-close-circle"></i></td>
                  <?php
                  }
                  else{
                  ?><td style="color: #ff0000;">Need PD Verified</td><?php
                 }
               }
               ?>

                <td>
                     <button data-bind="<?=$val->pr_no;?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-info btn-xs change" title="View Detail"><i class="zmdi zmdi-file" aria-hidden="true"></i></button>
                    <?php
                      if($val->is_approve_hod==1)
                      {
                        if($val->is_approve_hod==1)
                        {
                          if($val->is_approve_werehouse==1)
                          {
                            if($val->is_approve_purchasing==1)
                            {
                              if($val->is_approve_finance==1)
                              {
                                if($val->is_approve_division_head==1)
                                {
                                  if($val->is_approve_by_pd==1)
                                  {
                                    ?>
                                    <button data-bind="<?=$val->pr_no;?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-secondary btn-xs print" title="Print"><i class="zmdi zmdi-print" aria-hidden="true"></i></button>
                                    <button data-bind="<?=$val->pr_no;?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-warning btn-xs pdf" title="Export To PDF"><i class="zmdi zmdi-collection-pdf" aria-hidden="true"></i></button>
                                    <?php
                                    if($val->is_release_po==0)
                                    {
                                      ?>
                                      <button data-bind="<?=$val->pr_no;?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-info btn-xs blink release" title="Release PO No"><i class="zmdi zmdi-collection-bookmark" aria-hidden="true"></i></button>
                                      <?php
                                    }
                                    else{
                                      echo "";
                                    }
                                  } 
                                } 
                              }
                            }
                          }
                        }
                      }
                     ?>

                     <?php
                      if($val->is_approve_purchasing==1)
                      {
                        if($val->is_uncurrent_demand==0)
                      {
                        echo "";
                      }
                      else{
                          
                          if(empty($val->po_no))
                          {
                            if($val->is_revision==0)
                            {
                              ?>
                                <button data-bind="<?=$val->pr_no;?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-success  btn-xs revision" title="Revision Purchasing Field"><i class="zmdi zmdi-edit" aria-hidden="true"></i></button>
                              <?php
                            }
                            else{
                              echo "";
                            }
                          }
                          else{
                            echo "";
                          }
                        }
                      }
                      else{

                        echo "";
                      }
                     ?>
                </td>
                <td><?=$val->urgency;?></td>
                <td><?=$val->mc_no;?> - <?=$val->line_mc_no;?> </td>
                <td><?=$val->tool_no;?></td>
                <td><?=$val->claimable;?></td>
                <td><?=$val->expected_material_date_arrive;?></td>
                <td><?=$val->department;?></td>
             </tr>
              <?php } ?>
               </tbody>
            </table>
          </div>
        </div>
    </div>
</div>

<style>
.swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
  font-family: Georgia, serif;
}

      .blink {
        background-color: #1c87c9;
        -webkit-border-radius: 60px;
        border-radius: 60px;
        border: none;
        color: #eeeeee;
        cursor: pointer;
        display: inline-block;
        font-family: sans-serif;
        font-size: 20px;
        padding: 5px 15px;
        text-align: center;
        text-decoration: none;
      }
      @keyframes glowing {
        0% {
          background-color: #2ba805;
          box-shadow: 0 0 5px #2ba805;
        }
        50% {
          background-color: #49e819;
          box-shadow: 0 0 20px #49e819;
        }
        100% {
          background-color: #2ba805;
          box-shadow: 0 0 5px #2ba805;
        }
      }
      .blink {
        animation: glowing 1300ms infinite;
      }

</style>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js" integrity="sha256-Ry2q7Rf2s2TWPC2ddAg7eLmm7Am6S52743VTZRx9ENw=" crossorigin="anonymous"></script>


<script>
  $( document ).ready(function() {

 
  $('#tbl').DataTable({
        "scrollX": true,
        "scrollY":true
    });

});

    $("#tbl").on("click", ".change", function () {
  var id = $(this).attr("data-bind");
  Swal.fire({
    title: "You Want To Read Detail PR?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
      $.ajax({
          success: function(data) {
            window.location.href = '<?= base_url("backend/purchase/list_detail_purchasing/")?>'+id;
          }
        });
    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});

  $("#tbl").on("click", ".print", function () {
  var id = $(this).attr("data-bind");
  Swal.fire({
    title: "You Want To Print The PR?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
      $.ajax({
          success: function(data) {
            window.location.href = '<?= base_url("backend/purchase/view_print_pr/")?>'+id;
          }
        });
    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});


    $("#tbl").on("click", ".pdf", function () {
  var id = $(this).attr("data-bind");
  Swal.fire({
    title: "You Want To Export PDF?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
      $.ajax({
          success: function(data) {
            window.location.href = '<?= base_url("backend/purchase/export_to_pdf/")?>'+id;
          }
        });
    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});

  $("#tbl").on("click", ".release", function () {
  var id = $(this).attr("data-bind");
  Swal.fire({
    title: "You Want To Release The PO PR?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
      $.ajax({
          success: function(data) {
            window.location.href = '<?= base_url("backend/purchase_order/release_po/")?>';
          }
        });
    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});


$("#tbl").on("click", ".revision", function () {
  var id = $(this).attr("data-bind");
  Swal.fire({
    title: "You Want To Revision The PR?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
      $.ajax({
          success: function(data) {
            window.location.href = '<?= base_url("backend/purchase/form_fill_revision_purchasing/")?>'+id;
          }
        });
    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});




</script>