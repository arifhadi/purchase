<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Daily Production</title>
    

          
    <table width="100%">
        <tr>
          <td width="50" align="center">
            <img src="<?php echo base_url('src/material/global/assets/images/kop.png');?>" width="60%">
          </td>
        </tr>
        </table> 
        <hr>
    <h2 align="center">PURCHASE REQUISITION</h2> 
</div>
</head>
    <body>
         <div class="form-row">
            <div class="col-2"></div>
                <div class="col-1"></div>
                <div class="col-1"></div>
                <div class="col-3"></div>
                <div class="col-1"><h5>PR NO.</h5></div>
                <div class="col-1"><h5>:<?=$pr_no;?></h5></div>
                <div class="col-1"><h5>PO No.</h5></div>
                <div class="col-1"><h5>:<?=$po_no;?></h5></div>
        </div>
        <div class="form-row">
                <div class="col-2"><h5>Date</h5></div>
                <div class="col-2"><h5>:<?=$date;?></h5></div>
                <div class="col-2"><h5></h5></div>
                <div class="col-1"><h5></h5></div>
                <div class="col-1"><h5></h5></div>
                <div class="col-4"><h5></h5></div>
              </div>
              <div class="form-row">
                <div class="col-2"><h5>Urgency (Yes/No)</h5></div>
                <div class="col-2">:<?=$urgency;?></div>
                <div class="col-2"><h5>Claimable  (Yes/No) </h5></div>
                <div class="col-2">: <?=$claimable;?></div>
                <div class="col-2"></div>
                <div class="col-2"></div>
              </div>

               <div class="form-row">

                <div class="col-2"><h5>Machine No.</h5></div>
                <div class="col-2">: <?=$mc_no;?> - <?=$line_mc_no;?></div>
                <div class="col-2"><h5>Expected Material Date Arrive </h5></div>
                <div class="col-2">:<?=$expected_material_date_arrive;?></div>
                <div class="col-2"></div>
                <div class="col-2"></div>
              </div>

                <div class="form-row">

                <div class="col-2"><h5>Mold / Tooling No.</h5></div>
                <div class="col-2">: ss</div>
                <div class="col-2"><h5>Department </h5></div>
                <div class="col-2">:<?=$department;?></div>
                <div class="col-2"></div>
                <div class="col-2"></div>

              </div>

        <table id="tbl_prz" border="1" class="table table-hover dataTable table-striped w-full"  >
        <thead>
            <tr>
                <th rowspan="2">No.</th>
                  <!-- <th rowspan="2">Id PR</th> -->
                  <th rowspan="2">PR No.</th>
                  <th rowspan="2">PO No.</th>
                  <th rowspan="2">Part Name</th>
                  <th rowspan="2">Purpose</th>
                  <th rowspan="2">Cost Center</th>
                  <th rowspan="2">Category Request</th>
                  <th rowspan="2">New Order Qty</th>
                  <th rowspan="2">UOM</th>
                  <th rowspan="2">Qty Current Balance</th>
                  <th rowspan="2">Date Current Balance</th>
                  <th rowspan="2">Unit Price</th> 
                  <th rowspan="2">Currency</th>
                  <th rowspan="2">Total Ammount</th>
                  <th rowspan="2">Supplier Name</th>
                  <th colspan="4" style="text-align: center; padding: 0px;">LAST ORDER</th>
            </tr>
            <tr>
                <th>Date</th>
                <th>Qty</th>
                <th>Unit Price </th>
                <th>Supplier </th>
            </tr>
        </thead>
        <tbody>
          <?php $no=1; foreach ($printpr as $val) { ?>
              <tr>
                  <td><?= $no++ ?></td>
                  <!-- <td><input type="text" id="id_pr" name="id_pr" value="<?=$val->id_pr;?>" readonly></td> -->
                  <td><?=$val->pr_no;?></td>
                  <td><?=$val->po_no;?></td>
                  <td><?=$val->desc_item;?></td>
                  <td><?=$val->purpose;?></td>
                  <td><?=$val->cost_center;?></td>
                  <td><?=$val->category_request;?></td>
                  <td><?=$val->new_order_qty;?></td>
                  <td><?=$val->uom;?></td>
                  <td><?=$val->qty_cb;?></td>
                  <td><?=$val->date_cb;?></td>
                  <td><?=$val->unit_price;?></td>
                  <td><?=$val->item_curr;?></td>
                  <td><?=$val->total_ammount_item;?></td>
                  <td><?=$val->sup_name;?></td>
                  <td><?=$val->date_order_lo;?></td>
                  <td><?=$val->qty_lo;?></td>
                  <td><?=$val->unit_price_new_lo;?></td>
                  <td><?=$val->sup_lo_his;?></td>
                 
            </tr>
              <?php } ?>
        </tbody>

        <!-- <tfoot>
            <tr>
                <th colspan="10" style="text-align: right; padding: 0px;"><b>TOTAL</b></th>
                <th colspan="6" style="text-align: left; padding: 0px;"><b><?=$ttl_sum->ttl_amount;?></b></th>
            </tr>
        </tfoot> -->
        </table>
         <table id="tblSum" class="table table-hover dataTable table-striped w-full">
               <thead>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><b>TOTAL</b></td>
                  <td><?=$ttl_sum->ttl_amount?></td>
                </tr>
              </tbody>
            </table> 

        <table id="tbl_prs" border="1" class="table table-hover dataTable table-striped w-full" >
                <thead>
                    <tr>
                        <th>Requested by</th>
                        <th>Checked by HOD</th>
                        <th>Verified by Warehouse</th>
                        <th>Verified by Finance</th>
                        <th>Approved by Division Head</th>
                        <th>Approved by PD</th>
                    </tr> 
                </thead>
            <tbody>
                <tr>
                    <td><img src="<?php echo base_url('src/material/global/assets/images/approved.png');?>" width="60%"></td>
                    <td><img src="<?php echo base_url('src/material/global/assets/images/approved.png');?>" width="60%"></td>
                    <td><img src="<?php echo base_url('src/material/global/assets/images/approved.png');?>" width="60%"></td>
                    <td><img src="<?php echo base_url('src/material/global/assets/images/approved.png');?>" width="45%"></td>
                    <td><img src="<?php echo base_url('src/material/global/assets/images/approved.png');?>" width="60%"></td>
                    <td><img src="<?php echo base_url('src/material/global/assets/images/approved.png');?>" width="60%"></td>
                </tr>
            </tbody>
        <tfoot>
            <tr>
                <th>Name : <?=$signature->first_name?></th>
                <th>Name : <?=$signature->user_hod?> </th>
                <th>Name : <?=$signature->user_werehouse?> </th>
                <th>Name : <?=$signature->user_finance?></th>
                <th>Name : <?=$signature->user_division_head?></th>
                <th>Name : <?=$signature->user_pd?></th>
            </tr>
            <tr>
                <th>Date : <?=$signature->create_at?> </th>
                <th>Date : <?=$signature->date_approve_hod?> </th>
                <th>Date : <?=$signature->date_approve_werehouse?></th>
                <th>Date : <?=$signature->date_approve_finance?></th>
                <th>Date : <?=$signature->date_approve_division_head?></th>
                <th>Date : <?=$signature->date_approve_by_pd?></th>
            </tr>
        </tfoot>

        </table>
    </body>

<script>
    
    $(document).ready(function () {
    
     $('#tbl_prz').DataTable({
         paging: false,
        ordering: false,
        info: false,
        "searching": false,
         columnDefs: [    
          {
              targets: 11,
              render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
          {
              targets: 12,
              render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
          {
              targets: 13,
              render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
        ],
    });
    $('#tblSum').DataTable({
    paging: false,
    ordering: false,
    info: false,
    "searching": false,
    columnDefs: [    
          {
              targets: 5,
              render: $.fn.dataTable.render.number( '.', ',', 0," ")
          },
          
        ],
  });


    });


</script>
</html>