<!-- Page -->
<div class="page">
  <ol class="breadcrumb">
    <a href="<?php echo base_url('backend/purchase/list_data_supplier'); ?>" type="button" class="btn btn-round btn-warning"><i class="icon md-format-indent-increase" aria-hidden="true"></i>Supplier List</a>
  </ol>
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="<?=base_url('backend/purchase/list_data_supplier')?>">Supplier List</a></li>
    <li class="breadcrumb-item active">Create Supplier</li>
  </ol>
  <h4 style="text-align: left; color:#0000e6; font-weight: 900;"><b>&emsp; >>Create<< </b></h4>
  <?php if ($this->session->flashdata('success')) { ?>
  <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('success'); ?></p>
  </div>
  <?php }elseif($this->session->flashdata('error')){ ?>
  <div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('error'); ?></p>
  </div>
  <?php } ?>
  <div class="page-header" style="text-align: center; padding: 0px;">
    <h1 class="page-title">Form Create Supplier</h1>
  </div>
  <div class="page-content" style="padding: 0px;">
    <div class="panel">
      <div class="panel-body container-fluid" style="padding: 0px;">
        <div class="panel">
          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-md-12 col-lg-6">
                <div class="example-wrap">
                  <div class="example">
                    <!-- <form class="form-horizontal"> -->
                    <?= form_open(base_url('backend/purchase/create_data_supplier'),  'id="login_validation" enctype="multipart/form-data"') ?>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Name<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" name="supplier_name" placeholder="Supplier Name" autocomplete="off" />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Address<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <textarea class="maxlength-textarea form-control" data-plugin="maxlength" data-placement="bottom-right-inside" maxlength="200" name="address" rows="3" placeholder="Location"></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Address 2<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <textarea class="maxlength-textarea form-control" data-plugin="maxlength" data-placement="bottom-right-inside" maxlength="200" name="address_2" rows="3" placeholder="Address 2"></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Telpon<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" name="telpon" placeholder="Telpon" autocomplete="off" />
                      </div>
                    </div>
                       <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Email<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" name="email" placeholder="Email" autocomplete="off" />
                      </div>
                    </div>

                  </div>
                </div>
              </div>
              <div class="col-md-12 col-lg-6">

                <div class="example-wrap">
                  <div class="example">
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Loc Code<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" name="loc_code" placeholder="Loc Code" autocomplete="off" />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Payment Term<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" name="payment_term" placeholder="Payment Term" autocomplete="off" />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Type Of Bussines<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" name="type_of_bussines" placeholder="Type Of Bussines" autocomplete="off" />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Attn<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" name="attn" placeholder="Attn" autocomplete="off" />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Type Of Purchase<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" name="type_of_purchase" placeholder="Type Of Purchase" autocomplete="off" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-5 form-group form-material">
              
              </div>
              <div class="col-lg-5 form-group form-material">
                <button type="Submit" class="btn btn-success btn-sm">&emsp;&emsp;SAVE&emsp;&emsp;</button>
              </div>
              <div class="col-lg-2 form-group form-material">
              </div>
              <?php form_close() ?>
              <!-- Button Action -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Page -->