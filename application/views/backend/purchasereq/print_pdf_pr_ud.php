<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Daily Production</title>
    <table width="100%">
        <tr>
          <td width="200" align="center">
            <img src="<?php echo base_url('src/material/global/assets/images/kop.png');?>" width="200%">
          </td>
        </tr>
        </table> 
        <hr>
    
</div>
</head>
    <body>
      <div class="document active">
  <div class="spreadSheetGroup">
<h2 style="font-size:100%;" align="center">PURCHASE REQUISITION</h2> 
    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:50%">
          </td>
          <td style="width:12.5%">
            <strong><b>PR NO.</b></strong><br>
          </td>
          <td style="width:12.5%">
           : <?=$pr_no;?><br>
          </td>
           <td style="width:12.5%">
            <b>PO No</b><br>
          </td>
          <td style="width:12.5%">
           :<?=$po_no;?> <br>
          </td>
        </tr>
      </tbody>
    </table>



    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:25%">
          <b>Date</b><br>
          <b>Urgency</b><br>
          <b>Machine No. </b><br>
          <b>Mold / Tooling No.</b><br>
          </td>
          <td style="width:25%">
            :<?=$date;?><br>
            :<?=$urgency;?><br>
            :<?=$mc_no;?> - <?=$line_mc_no;?><br>
            :<?=$gih_tool_no?><br>
          </td>
          <td style="width:25%">
            <b>Claimable (Yes/No)</b><br>
            <b>Expected Material Date Arrive</b><br>
            <b>Department</b><br>
          </td>
          <td style="width:25%">
            :<?=$claimable;?><br>
            :<?=$expected_material_date_arrive;?><br>
            :<?=$department;?><br>
          </td>
        </tr>
      </tbody>
    </table>

    <table id="tbl_prz" border="1" class="table table-hover dataTable table-striped w-full">
      <thead>
              <tr>
                <th rowspan="2">No.</th>
                  <!-- <th rowspan="2">Id PR</th> -->
                  <th rowspan="2">PR No.</th>
                  <th rowspan="2">PO No.</th>
                  <th rowspan="2">Part Name</th>
                  <th rowspan="2">Purpose</th>
                  <th rowspan="2">Cost Center</th>
                  <th rowspan="2">Category Request</th>
                  <th rowspan="2">New Order Qty</th>
                  <th rowspan="2">UOM</th>
                  <th rowspan="2">Qty Current Balance</th>
                  <th rowspan="2">Date Current Balance</th>
                  <th rowspan="2">Unit Price</th> 
                  <th rowspan="2">Currency</th>
                  <th rowspan="2">Total Ammount</th>
                  <th rowspan="2">Supplier Name</th>
                  <th colspan="4" style="text-align: center; padding: 0px;">LAST ORDER</th>
            </tr>
            <tr>
                <th>Date</th>
                <th>Qty</th>
                <th>Unit Price </th>
                <th>Supplier </th>
            </tr>
      </thead>
      <tbody>
          <?php $no=1; foreach ($printpr as $val) { ?>
              <tr>
                   <td><?= $no++ ?></td>
                  <td><?=$val->pr_no;?></td>
                  <td><?=$val->po_no;?></td>
                  <td><?=$val->description;?></td>
                  <td><?=$val->purpose;?></td>
                  <td><?=$val->cost_center;?></td>
                  <td><?=$val->category_request;?></td>
                  <td><?=$val->new_order_qty;?></td>
                  <td><?=$val->uom;?></td>
                  <td><?=$val->qty_cb;?></td>
                  <td><?=$val->date_cb;?></td>
                  <td><?=$val->unit_price_pf;?></td>
                  <td><?=$val->currency;?></td>
                  <td><?=$val->total_amount;?></td>
                  <td><?=$val->supplier_name_pf;?></td>
                  <td><?=$val->date_pf;?></td>
                  <td><?=$val->qty_lo_pf;?></td>
                  <td><?=$val->unit_price_lo;?></td>
                  <td><?=$val->supplier_lo;?></td>
                 
            </tr>
              <?php } ?>
      </tbody>
      <tfoot>
            <tr>
                <th colspan="13" style="text-align: center; padding: 0px;">TOTAL</th>
                <th colspan="6" style="text-align: left; padding: 0px;"><?=$ttl_sum->ttl_amount?></th>
            </tr>
        </tfoot>
    </table>
       <table id="tbl_prs" border="1" class="table table-hover dataTable table-striped w-full" >
                <thead>
                    <tr>
                        <th>Requested by</th>
                        <th>Checked by HOD</th>
                        <th>Verified by Warehouse</th>
                        <th>Verified by Finance</th>
                        <th>Approved by Division Head</th>
                        <th>Approved by PD</th>
                    </tr> 
                </thead>
            <tbody>
                <tr>
                    <td><img src="<?php echo base_url('src/material/global/assets/images/approved.png');?>" width="10%"></td>
                    <td><img src="<?php echo base_url('src/material/global/assets/images/approved.png');?>" width="10%"></td>
                    <td><img src="<?php echo base_url('src/material/global/assets/images/approved.png');?>" width="10%"></td>
                    <td><img src="<?php echo base_url('src/material/global/assets/images/approved.png');?>" width="10%"></td>
                    <td><img src="<?php echo base_url('src/material/global/assets/images/approved.png');?>" width="10%"></td>
                    <td><img src="<?php echo base_url('src/material/global/assets/images/approved.png');?>" width="10%"></td>
                </tr>
            </tbody>
        <tfoot>
            <tr>
                <th>Name : <?=$signature->first_name?></th>
                <th>Name : <?=$signature->user_hod?> </th>
                <th>Name : <?=$signature->user_werehouse?> </th>
                <th>Name : <?=$signature->user_finance?></th>
                <th>Name : <?=$signature->user_division_head?></th>
                <th>Name : <?=$signature->user_pd?></th>
            </tr>
            <tr>
                <th>Date : <?=$signature->create_at?> </th>
                <th>Date : <?=$signature->date_approve_hod?> </th>
                <th>Date : <?=$signature->date_approve_werehouse?></th>
                <th>Date : <?=$signature->date_approve_finance?></th>
                <th>Date : <?=$signature->date_approve_division_head?></th>
                <th>Date : <?=$signature->date_approve_by_pd?></th>
            </tr>
        </tfoot>
        </table>
  </div>
</div>
    </body>
  <footer>
     <p style="font-size:70%;"> <em>Generated By System : https://purchasing.asiagalaxy.com</em></p>
  </footer>
<style type="text/css">
  /* Housekeeping */
body{
  font-size:12px;
}
.spreadSheetGroup{
    /*font:0.75em/1.5 sans-serif;
    font-size:14px;
  */
    color:#333;
    background-color:#fff;
    padding:1em;
}

/* Tables */
.spreadSheetGroup table{
    width:100%;
    margin-bottom:1em;
    border-collapse: collapse;
}
.spreadSheetGroup .proposedWork th{
    background-color:#eee;
}
.tableBorder th{
  background-color:#eee;
}
.spreadSheetGroup th,
.spreadSheetGroup tbody td{
    padding:0.5em;

}
.spreadSheetGroup tfoot td{
    padding:0.5em;

}
.spreadSheetGroup td:focus { 
  border:1px solid #fff;
  -webkit-box-shadow:inset 0px 0px 0px 2px #5292F7;
  -moz-box-shadow:inset 0px 0px 0px 2px #5292F7;
  box-shadow:inset 0px 0px 0px 2px #5292F7;
  outline: none;
}
.spreadSheetGroup .spreadSheetTitle{ 
  font-weight: bold;
}
.spreadSheetGroup tr td{
  text-align:center;
}
/*
.spreadSheetGroup tr td:nth-child(2){
  text-align:left;
  width:100%;
}
*/

/*
.documentArea.active tr td.calculation{
  background-color:#fafafa;
  text-align:right;
  cursor: not-allowed;
}
*/
.spreadSheetGroup .calculation::before, .spreadSheetGroup .groupTotal::before{
  /*content: "$";*/
}
.spreadSheetGroup .trAdd{
  background-color: #007bff !important;
  color:#fff;
  font-weight:800;
  cursor: pointer;
}
.spreadSheetGroup .tdDelete{
  background-color: #eee;
  color:#888;
  font-weight:800;
  cursor: pointer;
}
.spreadSheetGroup .tdDelete:hover{
  background-color: #df5640;
  color:#fff;
  border-color: #ce3118;
}
.documentControls{
  text-align:right;
}
.spreadSheetTitle span{
  padding-right:10px;
}

.spreadSheetTitle a{
  font-weight: normal;
  padding: 0 12px;
}
.spreadSheetTitle a:hover, .spreadSheetTitle a:focus, .spreadSheetTitle a:active{
  text-decoration:none;
}
.spreadSheetGroup .groupTotal{
  text-align:right;
}



table.style1 tr td:first-child{
  font-weight:bold;
  white-space:nowrap;
  text-align:right;
}
table.style1 tr td:last-child{
  border-bottom:1px solid #000;
}



table.proposedWork td,
table.proposedWork th,
table.exclusions td,
table.exclusions th{
  border:1px solid #000;
}
table.proposedWork thead th, table.exclusions thead th{
  font-weight:bold;
}
table.proposedWork td,
table.proposedWork th:first-child,
table.exclusions th, table.exclusions td{
  text-align:left;
  vertical-align:top;
}
table.proposedWork td.description{
  width:20%;
}

table.proposedWork td.amountColumn, table.proposedWork th.amountColumn,
table.proposedWork td:last-child, table.proposedWork th:last-child{
  text-align:center;
  vertical-align:top;
  white-space:nowrap;
}

.amount:before, .total:before{
  content: "$";
}
table.proposedWork tfoot td:first-child{
  border:none;
  text-align:right;
}
table.proposedWork tfoot tr:last-child td{
  font-size:16px;
  font-weight:bold;
}

table.style1 tr td:last-child{
  width:100%;
}
table.style1 td:last-child{
  text-align:left;
}
td.tdDelete{
  width:1%;
}

table.coResponse td{text-align:left}
table.shipToFrom td, table.shipToFrom th{text-align:left}

.docEdit{border:0 !important}

.tableBorder td, .tableBorder th{
  border:1px solid #000;
}
.tableBorder th, .tableBorder td{text-align:center}

table.proposedWork td, table.proposedWork th{text-align:center}
table.proposedWork td.description{text-align:left}
</style>
<script>
    
    $(document).ready(function () {
    
     $('#tbl_prz').DataTable({
         paging: false,
        ordering: false,
        info: false,
        "searching": false,
         columnDefs: [    
          {
              targets: 8,
              render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
          {
              targets: 10,
              render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
        ],
    });
    $('#tblSum').DataTable({
    paging: false,
    ordering: false,
    info: false,
    "searching": false,
    columnDefs: [    
          {
              targets: 31,
              render: $.fn.dataTable.render.number( '.', ',', 0," ")
          },
          
        ],
  });


    });


</script>
</html>