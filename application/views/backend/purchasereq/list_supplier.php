<div class="page">
  <div class="page-header" style="padding: 20px 10px;">
    <ol class="breadcrumb">
      <a href="<?php echo base_url('backend/purchase/create_data_supplier'); ?>" class="btn btn-round btn-danger"><i class="icon md-plus" aria-hidden="true"></i>&nbsp; Create &nbsp;</a>&emsp;&emsp;
      <!-- <button type="button" class="btn btn-success btn-round" data-target="#import-sup" data-toggle="modal"><i class="icon md-upload" aria-hidden="true"></i>Import Excel</button>&emsp;&emsp; -->
    </ol><br>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
      <li class="breadcrumb-item active">Supplier List</li>
    </ol>
  </div>
  <?php if ($this->session->flashdata('success')) { ?>
  <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('success'); ?></p>
  </div>
  <?php }elseif($this->session->flashdata('error')){ ?>
  <div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('error'); ?></p>
  </div>
  <?php } ?>
  <h3 class="panel-title" style="text-align: center; padding: 0px;"><b>Supplier List</b></h3>
  <div class="page-content" style="padding: 0px 0px;">
    <div class="panel"><br>
      <div class="panel-body">
        <table id="datatable" class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
          <thead>
            <tr>
              <th>No.</th>
              <th>Code</th>
              <th>Name</th>
              <th>Loc Code</th>
              <th>Payment Term</th>
              <th>Type Of Business</th>
              <th>Address 1</th>
              <th>Address 2</th>
              <th>Telpon</th>
              <th>Email</th>
              <th>Attn</th>
              <th>Status</th>
              <th>F1/GK</th>
              <th>Type Of Purchase</th>
              <th style="text-align: center;">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach ($sup as $val) { ?>
              <tr>
                <td><?= $no++ ?></td>
                <td><?=$val->code;?></td>
                <td><?=$val->name;?></td>
                <td><?=$val->loc_code;?></td>
                <td><?=$val->payment_term;?></td>
                <td><?=$val->type_of_bussines;?></td>
                <td><?=$val->address;?></td>
                <td><?=$val->address_2;?></td>
                <td><?=$val->telpon;?></td>
                <td><?=$val->email;?></td>
                <td><?=$val->attn;?></td>
                 <td>
                <div class="btn-group btn-group-xs" aria-label="Extra-small button group" role="group">
                  <?php if ($val->status=='A') {
                    echo "<button type='submit' data-bind='$val->code' class='btn btn-success nonactive'>Active</button>";
                  }
                  else {
                    echo "<button type='submit' data-bind='$val->code' class='btn btn-danger desc'>Non-Active</button>";
                  }
                  
                   ?>
                  
                </div>
              </td> 
                <td><?=$val->f1_gk;?></td>
                <td><?=$val->type_of_purchase;?></td>
                <td style="text-align: center;">
                  <button data-bind="<?=$val->code;?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-success btn-xs change" title="Change"><i class="icon md-edit" aria-hidden="true"></i></button>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->



<!-- Modal -->
<div class="modal fade" id="import-sup" aria-hidden="true" aria-labelledby="filterdata" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-center">
    <div class="modal-content">
      <?= form_open(base_url('backend/purchase/import_excel_supplier'),  'id="login_validation" enctype="multipart/form-data"') ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
    <h4 class="modal-title" style="text-align: center;">Upload File Here</h4>
      <div class="modal-body">
        <div class="example-grid">
          <div class="row">
            <div class="col-md-9">
              <a href="<?php echo base_url('backend/purchase/template_excel/Template_Cost_Center.xlsx') ?>">Download Template.xlsx</a>
              <div class="input-group input-group-file" data-plugin="inputGroupFile">
                <input type="text" class="form-control" readonly="">
                <span class="input-group-append">
                  <span class="btn btn-success btn-file waves-effect waves-light waves-round">
                    <i class="icon md-upload" aria-hidden="true"></i>
                    <!-- <input type="file" name="file_picture" multiple=""> -->
                    <input type="file" class="form-control" name="file" accept=".xls, .xlsx" required>
                  </span>
                </span>
              </div>
            </div>
            <div class="col-lg-6">
              <span class="text-secondary">file format : <b style="color:red;">.xls, xlsx</b></span>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="Submit" class="btn btn-success btn-sm">&emsp;&nbsp; IMPORT &emsp;&nbsp;</button>&emsp;
        <?php form_close() ?>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL -->
<style>
.swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
  font-family: Georgia, serif;
}
</style>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js" integrity="sha256-Ry2q7Rf2s2TWPC2ddAg7eLmm7Am6S52743VTZRx9ENw=" crossorigin="anonymous"></script>
<script type="text/javascript">//On-Progress

$("#datatable").on("click", ".desc", function () {
  var code    = $(this).attr("data-bind");

  Swal.fire({
    title: "You Want To Active The Supplier ?",
    type: 'question',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#d33',
    confirmButtonClass: "btn-success",
    confirmButtonText: " Yes !",
    cancelButtonText: "Cancel",
    customClass: 'swal2-overflow',
  }).then(function(result) {
    if (result.value == undefined){
      swal.fire("Cancelled", "Close Action", "error");
    }else{
      $.ajax({
        url: '<?= base_url("/backend/purchase/active_supplier/")?>'+code,
        type: 'DELETE',
        error: function() {
          alert('Something is wrong');
        },
        success: function(data) {
          // alert(data)
          var res = data;
          if (res == 1) {
            // console.log(data)
            swal.fire("Success", "<b>Supplier is Active !</b>", "success");

           window.location.reload();
          }else {
           window.location.reload();
          }
        }
      });
    }
    swall.closeModal();
  });
});



$("#datatable").on("click", ".change", function () {
      var id = $(this).attr("data-bind");
      Swal.fire({
    title: "Are You Sure Want To Change The Data",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    var reason = $('#reason').val();
    if (result.value == true) {
        $.ajax({
        success: function(data){
          window.location.href = '<?= base_url("backend/purchase/update_data_supplier/")?>'+id;
        },
        
      });

    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});




  $("#datatable").on("click", ".nonactive", function () {
  var id = $(this).attr("data-bind");
  // console.log(id);
  Swal.fire({
    title: "<h2 style='color:red; font-sty'><b>Are You Sure Want to Non Active Supplier?</b></h2>",
    type: 'question',
    icon: 'warning',
     html: '<div class="example-grid">'+
              '<label class="col-xl-12 col-md-3 form-control-label"><b>Reason Non Active Supplier</b><span class="required">*</span></label>'+
                  '<input id="reason" type="text" name="reason"/>'+
          '</div>',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    var reason = $('#reason').val();
    if (result.value == true) {
        $.ajax({
        url: "<?= base_url("backend/purchase/non_active_supplier/")?>"+id+'/'+reason,
        type:"DELETE",
        success: function(data){
        window.location.reload();
        },
        
      });

    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});
</script>


