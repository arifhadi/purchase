<div class="page">
  <div class="page-header">
    <ol class="breadcrumb">
      <a href="<?php echo base_url('backend/admdashboard'); ?>" type="button" class="btn btn-round btn-info"><i class="icon md-home" aria-hidden="true"></i>Menu Dashboard</a>
        &nbsp;&nbsp;
    </ol>
     <br>
    <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="<?=base_url('backend/purchase/list_my_pr')?>">PR List</a></li>
    <li class="breadcrumb-item active">Detail PR List</li>
  </ol>

    
    <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button><p><?php echo $this->session->flashdata('success'); ?></p>
        </div>
    <?php } ?>

  </div>

  <h3 class="panel-title" style="text-align: center; padding: 0px;"><b>My Detail PR List</b></h3>
    <div class="page-content">
        <div class="panel">
          <header class="panel-heading" style="text-align: right;">
       <h4><b>Current Status Your PR : </b><br> 
         <?php
         if(!empty($detial_pr->reason_reject_hod))
         {
           echo "<b style='color: #ff3300; text-decoration: underline;'><p class='blink_failed'>$detial_pr->reason_reject_hod</p>";
         }
         else if(!empty($detial_pr->reason_reject_werehouse)){
          echo "<b style='color: #ff3300; text-decoration: underline;'><p class='blink_failed'>$detial_pr->reason_reject_werehouse</p>";
         }
         else if(!empty($detial_pr->reason_reject_finance))
         {
            echo "<b style='color: #ff3300; text-decoration: underline;'><p class='blink_failed'>$detial_pr->reason_reject_finance</p>";
         }
         else if(!empty($detial_pr->reason_reject_divison_head))
         {
            echo "<b style='color: #ff3300; text-decoration: underline;'><p class='blink_failed'>$detial_pr->reason_reject_divison_head</p>";
         }
         else if(!empty($detial_pr->reason_reject_by_pd))
         {
          echo "<b style='color: #ff3300; text-decoration: underline;'><p class='blink_failed'>$detial_pr->reason_reject_by_pd</p>";
         }
         else if(!empty($detial_pr->reason_reject_by_purchasing))
         {
          echo "<b style='color: #ff3300; text-decoration: underline;'><p class='blink_failed'>$detial_pr->reason_reject_by_purchasing</p>";
         }
         else if($detial_pr->is_approve_by_pd==1)
         {
            echo "<b style='color: #27ae60; text-decoration: underline;'><p class='blink'>Your PR Finish Approved By PD</p>";
         }
         else{
          echo "<b style='color: #ff3300; text-decoration: underline;'>Waiting To Verified";
         }
         ?>
       </b></h4>
      </header>
          <div class="panel-body">
            <table  id="tblS"class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>PR No.</th>
                  <th>PO No.</th>
                  <th>Part No</th>
                  <th>Part Name</th>
                  <th>Purpose</th>
                  <th>Cost Center</th>
                  <th>Category Request</th>
                  <th>New Order Qty</th>
                  <th>UOM</th>
                </tr>
              </thead>
              <tbody>
               <?php $no=1; foreach ($edit_data as $val) { ?>
              <tr>
                  <td><?= $no++ ?></td>
                  <td><?=$val->pr_no;?></td>
                  <td><?=$val->po_no;?></td>
                  <td><?=$val->item_no;?></td>
                  <td><?=$val->desc_item;?></td>
                  <td><?=$val->purpose;?></td>
                  <td><?=$val->cost_center;?></td>
                  <td><?=$val->category_request;?></td>
                  <td><?=$val->new_order_qty;?></td>
                  <td><?=$val->uom;?></td>
            </tr>
              <?php } ?>
               </tbody>
            </table>
          </div>
        </div>
    </div>


</div>

<style>
.swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
  font-family: Georgia, serif;
}
</style>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js" integrity="sha256-Ry2q7Rf2s2TWPC2ddAg7eLmm7Am6S52743VTZRx9ENw=" crossorigin="anonymous"></script>

<style>
            .blink {
                animation: blinker 3s linear infinite;
                color: #27ae60;
                font-family: sans-serif;
            }
            @keyframes blinker {
                50% {
                    opacity: 0;
                }
            }

            .blink_failed {
                animation: blinker 3s linear infinite;
                color: #ff3300;
                font-family: sans-serif;
            }
            @keyframes blinker {
                50% {
                    opacity: 0;
                }
            }
        </style>
<script>
    $("#tbl").on("click", ".change", function () {
  var id = $(this).attr("data-bind");
  Swal.fire({
    title: "you want to Read Detail PR?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
      $.ajax({
          success: function(data) {
            // window.location.href = '<?= base_url("backend/bom_injection/actual_planning_update/")?>'+id;
          }
        });
    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});
</script>