<div class="page">
  <div class="page-header">
    <ol class="breadcrumb">
      <a href="<?php echo base_url('backend/admdashboard'); ?>" type="button" class="btn btn-round btn-info"><i class="icon md-home" aria-hidden="true"></i>Menu Dashboard</a>
        &nbsp;&nbsp;
    </ol>
     <br>
    <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="<?=base_url('backend/purchase/list_data_all_pr')?>">All List PR</a></li>
    <li class="breadcrumb-item active">Revision Purchasing Field</li>
  </ol>

    
    <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button><p><?php echo $this->session->flashdata('success'); ?></p>
        </div>
    <?php } ?>

  </div>

  <h3 class="panel-title" style="text-align: center; padding: 0px;"><b>Revision Purchasing Field</b></h3>
    <div class="page-content" style="padding: 0px 0px;">
        <div class="panel">
          <header class="panel-heading" style="text-align: right;">
            <h4><b>PR No.</b> <b style="color: #ff3300; text-decoration: underline;"><?=$pr_no;?></b></h4>
            </header>
          <div class="panel-body">
          <table  id="tableData" class="table table-hover dataTable table-striped w-full">
              <thead>
                <tr>
                  <th rowspan="2">No.</th>
                  <!-- <th rowspan="2">Id PR</th> -->
                  <th rowspan="2">PR No.</th>
                  <th rowspan="2">PO No.</th>
                  <th rowspan="2">Part Name</th>
                  <th rowspan="2">Purpose</th>
                  <th rowspan="2">Cost Center</th>
                  <th rowspan="2">Category Request</th>
                  <th rowspan="2">New Order Qty</th>
                  <th rowspan="2">UOM</th>
                  <th rowspan="2">Qty Current Balance</th>
                  <th rowspan="2">Date Current Balance</th>
                  <th rowspan="2">Unit Price</th> 
                  <th rowspan="2">Currency</th>
                  <th rowspan="2">Total Ammount</th>
                  <th rowspan="2">Supplier Name</th>
                  <th colspan="4" style="text-align: center; padding: 0px;">LAST ORDER</th>
                  <th rowspan="2">Action</th>
                </tr>
                <tr>
                  <th>Date</th>
                <th>Qty</th>
                <th>Unit Price </th>
                <th>Supplier </th>
                </tr>
              </thead>
            <tbody>
               <?php $no=1; foreach ($list_pur as $val) { ?>
              <tr>
                  <td><?= $no++ ?></td>
                  <!-- <td><input type="text" id="id_pr" name="id_pr" value="<?=$val->id_pr;?>" readonly></td> -->
                  <td><?=$val->pr_no;?></td>
                  <td><?=$val->po_no;?></td>
                  <td><?=$val->description;?></td>
                  <td><?=$val->purpose;?></td>
                  <td><?=$val->cost_center;?></td>
                  <td><?=$val->category_request;?></td>
                  <td><?=$val->new_order_qty;?></td>
                  <td><?=$val->uom;?></td>
                  <td><?=$val->qty_cb;?></td>
                  <td><?=$val->date_cb;?></td>
                  <td><?=$val->unit_price_pf;?></td>
                  <td><?=$val->currency;?></td>
                  <td><?=$val->total_amount;?></td>
                  <td><?=$val->supplier_name_pf;?></td>
                  <td><?=$val->date_pf;?></td>
                  <td><?=$val->qty_lo_pf;?></td>
                  <td><?=$val->unit_price_lo;?></td>
                  <td><?=$val->supplier_lo;?></td>
                  <td>
                      <button data-bind="<?=$val->id_pr;?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-primary btn-xs fill"><i class="zmdi zmdi-comment-alt-text" aria-hidden="true"></i></button>
                  </td>
            </tr>
              <?php } ?>
               </tbody>
            </table>
            <table id="tblSum" class="table table-hover dataTable table-striped w-full">
               <thead>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><b>TOTAL</b></td>
                  <td><?=$ttl_sum->ttl_amount?></td>
                </tr>
              </tbody>
            </table>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn-sm btn-success" type="button" onclick="approve()">FINISH &nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;
            
         
          </div>

        </div>
    </div>


</div>

<div class="modal fade" id="filldata" aria-hidden="true" aria-labelledby="filldata" role="dialog">
  <div class="modal-dialog modal-simple modal-center" style="max-width: 85%;" role="document">
    <div class="modal-content">
      <?= form_open(base_url('backend/purchase/update_revision_fill_purchasing'),  'id="login_validation" enctype="multipart/form-data"') ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="icon md-close-circle" aria-hidden="true" style="color: red;"></i>
        </button>
      </div>
      <h4 class="modal-title" style="text-align: center; color: #005ce6;"><b>Fill The Revision Purchasing</b></h4>
      <h5 class="modal-title" style="text-align: center; color: #005ce6;" id="lbltipAddedComment"></h5>
      <div class="modal-body">
      <div class="example-grid">
          <div class="row">
            <div class="col-lg-4">
              <div class="form-group row">
                <label class="col-md-2 form-control-label"><b>Unit Price<b style="color: red;">*</b>  </b></label>
                   <div class="col-md-9">
                      <table>
                          <tbody>
                            <tr>
                              <td style="width: 100%;">
                                 <input type="text" class="form-control" id="unit_price" name="unit_price" autocomplete="off" required="required" oninput="setRibuan()"  />
                              </td>
                              <td>
                                <select class="form-control" data-plugin="select2" id="currency" name="currency" data-placeholder="Select Currency">
                          <option value="IDR">IDR</option>
                          <option value="USD">USD</option>
                          <option value="SGD">SGD</option>
                          <option value="JPY">JPY</option>
                        </select>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                   </div>
              </div>
            </div>
            <div class="form-group row">
                <label class="col-md-2 form-control-label"><b>Total Amount<b style="color: red;">*</b>  </b></label>
                   <div class="col-md-9">
                     <input type="text" class="form-control" id="total_amount" name="total_amount" autocomplete="off" required="required" oninput="setRibuan()"/>
                   </div>
              </div>
             <div class="form-group row">
                <label class="col-md-3 form-control-label"><b>Supplier Name<b style="color: red;">*</b> </b></label>
                   <div class="col-md-9">
                    <!--  <input type="text" class="form-control" name="supplier_name_pf" autocomplete="off" required="required" /> -->
                    <select class="form-control" required="required"data-plugin="select2" id="supplier_name_pf" name="supplier_name_pf" data-placeholder="Select Supplier">
                          <option></option>
                          <?php foreach ($sup as $val) { ?>
                          <option value="<?php echo $val->code?>">
                            <?php echo "$val->code - $val->name" ?>
                          </option>
                          <?php } ?>
                        </select>
                   </div>
              </div>

            <div class="col-lg-4 FinishCls" style="text-align: center;display: none;">
              <div class="col-md-12">
                <h5 style="text-align: left;">Finish</h5>
                <input type="time" id="IDFinish" class="form-control" name="finish" required placeholder="--:-- --" readonly>
              </div>
            </div>
          </div>
        </div>
        <div class="example-grid">
          <div class="row">
            <div class="col-lg-4">
              <div class="form-group row">
                <label class="col-md-2 form-control-label"><b>Date Last Order<b style="color: red;">*</b>  </b></label>
                   <div class="col-md-9">
                     <input type="datetime-local" id="date_pf" class="form-control" name="date_pf" autocomplete="off" required="required" />
                   </div>
              </div>
            </div>
            <div class="form-group row">
                <label class="col-md-2 form-control-label"><b>Qty Last Order<b style="color: red;">*</b>  </b></label>
                   <div class="col-md-9">
                     <input type="text" class="form-control" name="qty_lo_pf" autocomplete="off" required="required" />
                   </div>
              </div>
             <div class="form-group row">
                <label class="col-md-3 form-control-label"><b>Unit Price Last Order<b style="color: red;">*</b> </b></label>
                   <div class="col-md-9">
                     <input type="text" class="form-control" name="unit_price_lo" autocomplete="off" required="required" />
                   </div>
              </div>
          </div>
        </div>
         <div class="example-grid">
          <div class="row">
            <div class="col-lg-4"> 
              <div class="form-group row">
                <label class="col-md-2 form-control-label"><b>Supplier Name Last Order<b style="color: red;">*</b>  </b></label>
                   <div class="col-md-9">
                     <!-- <input type="text" class="form-control" name="supplier_lo" autocomplete="off" required="required" /> -->

                     <select class="form-control" required="required" data-plugin="select2" id="supplier_lo" name="supplier_lo" data-placeholder="Select Supplier">
                          <option></option>
                          <?php foreach ($sup as $val) { ?>
                          <option value="<?php echo $val->code?>">
                            <?php echo "$val->code - $val->name" ?>
                          </option>
                          <?php } ?>
                        </select>
                        

                   </div>
              </div>
              <input hidden  type="text" class="form-control" name="id_pr" autocomplete="off" id="id_pr" required="required" />
              <input hidden type="text" class="form-control" name="pr_no" autocomplete="off" id="pr_no" required="required" value="<?=$pr_no?>"/>
              <input hidden  type="text" class="form-control" name="new_order_qty" autocomplete="off" id="new_order_qty" required="required"/>

            </div>
            <div class="col-lg-4 FinishCls" style="text-align: center;display: none;">
              <div class="col-md-12">
                <h5 style="text-align: left;">Finish</h5>
                <input type="time" id="IDFinish" class="form-control" name="finish" required placeholder="--:-- --" readonly>
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="example-grid">
          <div class="row">
            <div class="col-lg-4">
              <div class="form-group row form-material">
                <div class="col-xl-12 col-md-9">
                  <div class="d-flex flex-column">
                   
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form-group row form-material">
                <div class="col-xl-12 col-md-9">
                  <div class="d-flex flex-column">
                  
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form-group row form-material">
                <div class="col-xl-12 col-md-9">
                  <div class="d-flex flex-column">
                   
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="col-lg-4">
          <button type="Submit" class="btn btn-success btn-sm">&ensp;&ensp; Submit &ensp;&ensp;</button>
        </div>
        <div class="col-lg-3">
        </div>
        <?php form_close() ?>
      </div>
    </div>
  </div>
</div>

<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>

<div class="modal fade" id="loader" aria-hidden="true" aria-labelledby="filterdata" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-center">
    <div class="modal-content">
      <div class="modal-header">
      </div>
       <div class="loader"></div>
        <h4 class="modal-title" style="text-align: center;">Loading...</h4>
  </div>
</div>

<style>
.swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
  font-family: Georgia, serif;
}
</style>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js" integrity="sha256-Ry2q7Rf2s2TWPC2ddAg7eLmm7Am6S52743VTZRx9ENw=" crossorigin="anonymous"></script>

<script type="text/javascript">


$( document ).ready(function() {

  var currency = "<?=$currency?>";

  // console.log(currency);
  $('#tableData').DataTable({
        "scrollX": true,
        "scrollY":true,
         columnDefs: [    
          {
              targets: 11,
              render: $.fn.dataTable.render.number( '.', ',', 0,currency )
          },
          {
              targets: 13,
              render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
           {
              targets: 17,
              render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
        ],


    });

  // var currency

  $('#tblSum').DataTable({
    paging: false,
    ordering: false,
    info: false,
    "searching": false,
    columnDefs: [    
          {
              targets: 20,
              render: $.fn.dataTable.render.number( '.', ',', 0,'$')
          },
          
        ],
  });
});





function setRibuan()
  {

     var unit_price = document.getElementById('unit_price');
    var total_amount = document.getElementById('total_amount');

    unit_price.value = formatRupiah(unit_price.value);
    
    countTotalAmmount();
    total_amount.value = formatRupiah(total_amount.value);



  }
 function formatRupiah(angka, prefix)
    {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split    = number_string.split(','),
            sisa     = split[0].length % 3,
            rupiah     = split[0].substr(0, sisa),
            ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
            
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'IDR. ' + rupiah : '');
  }

function countTotalAmmount()
{
  var new_order_qty = parseInt(document.getElementById("new_order_qty").value);
  var unit_price = document.getElementById("unit_price").value;
  var replaceUnit = unit_price.replace(".","").replace(".","").replace(".","").replace(".","").replace(".","");
  // console.log(replaceUnit);
  var countTtl = new_order_qty * parseInt(replaceUnit);

  // console.log(countTtl);
  document.getElementById("total_amount").value = parseInt(countTtl);


}


 
</script>



<script>
 $("#tableData").on("click", ".fill", function () {
  var id = $(this).attr("data-bind");
   var val = "<?=$pr_no?>";
  Swal.fire({
    title: "You Want To Fill The PR?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
     
      $.ajax({
        method: 'POST',
        url: "<?= base_url("backend/purchase/get_all_data_to_revision/")?>",
        data: {'id': id},
        cache: false,
        async : true,
        dataType : 'json',
        success: function(data){
           $("#filldata").modal();
           document.getElementById("id_pr").value = id;
          $('input[name="new_order_qty"]').val(data.new_order_qty);
          $('input[name="unit_price"]').val(data.unit_price_pf); 
          $('input[name="total_amount"]').val(data.total_amount);
          $('input[name="qty_lo_pf"]').val(data.qty_lo_pf);
          $('input[name="unit_price_lo"]').val(data.unit_price_lo); 
          $('input[name="date_pf"]').val(data.date_pf);
          setRibuan();

           document.getElementById('lbltipAddedComment').innerHTML = data.description;

        }
      });

    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});

function approve()
{
  var val = "<?=$pr_no?>";
  Swal.fire({
    title: "You Want To Finish Revision This PR?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
        $.ajax({
        method: 'POST',
        url: "<?= base_url("backend/purchase/approve_pr_purchasing/")?>"+val,
        data: {'pr_no': val},
        cache: false,
        async : true,
        dataType : 'json',
        beforeSend:function()
        {
             $('#loader').modal('show');
        },
        success: function(data){
        // window.location.reload();
        $('#loader').modal('hide');
        window.location.href = '<?= base_url("backend/purchase/list_apporval_purchasing")?>';
        },
        error:function(data)
        {
          // window.location.reload();
          // $('#loader').modal('show');
           $('#loader').modal('hide');
          window.location.href = '<?= base_url("backend/purchase/list_apporval_purchasing")?>';
        }
      });

    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
}


</script>

