<div class="page">
  <div class="page-header">
    <ol class="breadcrumb">
      <a href="<?php echo base_url('backend/admdashboard'); ?>" type="button" class="btn btn-round btn-info"><i class="icon md-home" aria-hidden="true"></i>Menu Dashboard</a>
        &nbsp;&nbsp;
    </ol>
     <br>
    <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="<?=base_url('backend/purchase/list_approval_werehouse')?>">List Approval Werehouse</a></li>
    <li class="breadcrumb-item active">Detail PR List</li>
  </ol>

    
    <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button><p><?php echo $this->session->flashdata('success'); ?></p>
        </div>
    <?php } ?>

  </div>

  <h3 class="panel-title" style="text-align: center; padding: 0px;"><b>Detail PR List To Fill</b></h3>
    <div class="page-content">
        <div class="panel">
          
          <div class="panel-body">
            <table  id="tblS"class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>PR No.</th>
                  <th>Part Name</th>
                  <th>Purpose</th>
                  <th>Cost Center</th>
                  <th>Category Request</th>
                  <th>New Order Qty</th>
                  <th>UOM</th>
                  <th>Qty Current Balance</th>
                  <th>Date Current Balance</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
               <?php $no=1; foreach ($list_were as $val) { ?>
              <tr>
                  <td><?= $no++ ?></td>
                  <td><?=$val->pr_no;?></td>
                  <td><?=$val->description;?></td>
                  <td><?=$val->purpose;?></td>
                  <td><?=$val->cost_center;?></td>
                  <td><?=$val->category_request;?></td>
                  <td><?=$val->new_order_qty;?></td>
                  <td><?=$val->uom;?></td>
                  <td><?=$val->qty_cb;?></td>
                  <td><?=$val->date_cb;?></td>
                  <td>
                     <?php 
                      if(isset($val->qty_cb))
                      {
                        ?> 
                          <label>Complate</label>
                        <?php
                      } else{
                        ?> 
                        <button data-bind="<?=$val->id_pr;?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-success btn-xs fill" title="Fill The Qty"><i class="zmdi zmdi-comment-alt-text" aria-hidden="true"></i></button>
                        <?php
                      }
                     ?>
                  </td>
            </tr>
              <?php } ?>
               </tbody>
            </table>
            

            <?php 
              if(isset($qty_cb->qty_cb))
              {
                ?> 
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn-sm btn-success" type="button" onclick="approve()"><i class="zmdi zmdi-check" aria-hidden="true"></i> Approve &nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php
              }
              else{
                ?>
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button disabled class="btn-sm btn-success" type="Submit"><i class="zmdi zmdi-check" aria-hidden="true"></i> Approve &nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php
              }?>
            
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn-sm btn-danger" onclick="decline()" type="button"><i class="zmdi zmdi-close" aria-hidden="true" ></i> Decline &nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;
          </div>

        </div>
    </div>


</div>

<style>
.swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
  font-family: Georgia, serif;
}
</style>

<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>

<div class="modal fade" id="loader" aria-hidden="true" aria-labelledby="filterdata" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-center">
    <div class="modal-content">
      <div class="modal-header">
      </div>
       <div class="loader"></div>
        <h4 class="modal-title" style="text-align: center;">Loading...</h4>
  </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js" integrity="sha256-Ry2q7Rf2s2TWPC2ddAg7eLmm7Am6S52743VTZRx9ENw=" crossorigin="anonymous"></script>


<script>
    $("#tblS").on("click", ".fill", function () {
  var id = $(this).attr("data-bind");
  Swal.fire({
    title: "you want to Fill The Qty Current Ballance?",
    type: 'question',
    icon: 'warning',
    html: '<div class="example-grid">'+
              '<label class="col-xl-12 col-md-3 form-control-label"><b>Qty Balance</b><span class="required">*</span></label>'+
                  '<input id="qty_cb" type="number" name="qty_cb"/>'+
          '</div>',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    var qty_cb = $('#qty_cb').val();
    if (result.value == true) {
      $.ajax({
        url: "<?= base_url("backend/purchase/create_qty_cb_werehouse/")?>"+id+'/'+qty_cb,
        type:"POST",
        success: function(data){
        window.location.reload();
        },
        
      });
    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});

function approve()
{
  var val = "<?=$pr_no?>";
  Swal.fire({
    title: "you want to Approve This PR?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
        $.ajax({
        method: 'POST',
        url: "<?= base_url("backend/purchase/approve_pr_werehouse/")?>"+val,
        data: {'pr_no': val},
        cache: false,
        async : true,
        dataType : 'json',
        beforeSend:function()
        {
             $('#loader').modal('show');
        },
        success: function(data){
        // window.location.reload();
        $('#loader').modal('hide');
        window.location.href = '<?= base_url("backend/purchase/list_approval_werehouse")?>';
        },
        error:function(data)
        {
          // window.location.reload();
          // $('#loader').modal('show');
          window.location.href = '<?= base_url("backend/purchase/list_approval_werehouse")?>';
        }
      });

    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
}

function decline()
{   
    var val = "<?=$pr_no?>";
    Swal.fire({
    title: "Are You Sure Decline The PR?",
    type: 'question',
    icon: 'warning',
    html: '<div class="example-grid">'+
              '<label class="col-xl-12 col-md-3 form-control-label"><b>Reason Decline.</b><span class="required">*</span></label>'+
                  '<input id="reason" type="text" name="reason"/>'+
          '</div>',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    var reason = $('#reason').val();
    if (result.value == true) {
        $.ajax({
        url: "<?= base_url("backend/purchase/decline_pr_werehouse/")?>"+val+'/'+reason,
        type:"DELETE",
        success: function(data){
        // window.location.reload();
        window.location.href = '<?= base_url("backend/purchase/list_approval_werehouse")?>';
        },
        error:function(data)
        {
          // window.location.reload();
          // $('#loader').modal('show');
          window.location.href = '<?= base_url("backend/purchase/list_approval_werehouse")?>';
        }
        
      });

    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
}

</script>

