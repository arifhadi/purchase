<div class="page">
  <div class="page-header">
    <ol class="breadcrumb">
      <a href="<?php echo base_url('backend/admdashboard'); ?>" type="button" class="btn btn-round btn-info"><i class="icon md-home" aria-hidden="true"></i>Menu Dashboard</a>
        &nbsp;&nbsp;
        <a href="<?php echo base_url('backend/purchase/list_history_pr_hod'); ?>" type="button" class="btn btn-round btn-danger">History PR Request</a>
    </ol>
    <br>
    
<?php if ($this->session->flashdata('success')) { ?>
  <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('success'); ?></p>
  </div>
  <?php }elseif($this->session->flashdata('error')){ ?>
  <div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('error'); ?></p>
  </div>
  <?php } ?>

  </div>
    <h3 class="panel-title" style="text-align: center; padding: 0px;"><b>Approval HOD PR List</b></h3>
      <div class="page-content">
        <div class="panel">
          <div class="panel-body">
            <table  id="tbl"class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>PR No</th>
                  <th>Name Request</th>
                  <th>Date Request</th>
                  <th>View</th>
                  <th>Urgency</th>
                  <th>Machine No</th>
                  <th>Tool No</th>
                  <th>Claimable</th>
                  <th>Department</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              <?php $no=1; foreach ($approval_hod as $val) { ?>
              <tr>
               <td><?= $no++ ?></td>
               <td><?=$val->pr_no;?></td>
               <td><?=$val->first_name;?></td>
               <td><?=$val->create_at;?></td>
                <td>
                <button data-bind="<?=$val->pr_no;?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-success btn-xs change" title="View Detail"><i class="zmdi zmdi-file" aria-hidden="true"></i></button>
                </td>
                <td><?=$val->urgency;?></td>
                <td><?=$val->mc_no;?> - <?=$val->line_mc_no;?> </td>
                <td><?=$val->tool_no;?></td>
                <td><?=$val->claimable;?></td>
                <td><?=$val->department;?></td>
                <td>
                <button data-bind="<?=$val->pr_no;?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-success btn-xs approve" title="Approve"><i class="zmdi zmdi-check-circle" aria-hidden="true"></i></button>
                <button data-bind="<?=$val->pr_no;?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-danger btn-xs decline" title="Decline"><i class="zmdi zmdi-close-circle" aria-hidden="true"></i></button>
                </td>
              </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
    </div>
</div>

<div class="modal fade" id="loader" aria-hidden="true" aria-labelledby="filterdata" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-center">
    <div class="modal-content">
      <div class="modal-header">
      </div>
       <div class="loader"></div>
        <h4 class="modal-title" style="text-align: center;">Loading ...</h4>
  </div>
</div>

<style>
.swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
  font-family: Georgia, serif;
}
</style>

<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>

<div class="modal fade" id="loader" aria-hidden="true" aria-labelledby="filterdata" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-center">
    <div class="modal-content">
      <div class="modal-header">
      </div>
       <div class="loader"></div>
        <h4 class="modal-title" style="text-align: center;">Loading...</h4>
  </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js" integrity="sha256-Ry2q7Rf2s2TWPC2ddAg7eLmm7Am6S52743VTZRx9ENw=" crossorigin="anonymous"></script>


<script>
  $("#tbl").on("click", ".change", function () {
  var id = $(this).attr("data-bind");
  Swal.fire({
    title: "you want to Read Detail PR?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
      $.ajax({
          success: function(data) {
            window.location.href = '<?= base_url("backend/purchase/list_detail_my_pr_hod/")?>'+id;
          }
        });
    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});


$("#tbl").on("click", ".approve", function () {
  var id = $(this).attr("data-bind");
  // console.log(id);
  Swal.fire({
    title: "Are You Sure Approve The PR?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
        $.ajax({
        url: "<?= base_url("backend/purchase/approve_pr_hod/")?>"+id,
        type:"DELETE",
         beforeSend:function()
        {
             $('#loader').modal('show');
        },
        success: function(data){
        $('#loader').modal('hide');
        window.location.reload();
        },
        error:function(data)
        {
          // window.location.reload();
          // $('#loader').modal('show');
           $('#loader').modal('hide');
           window.location.reload();
          
        }
        
      });

    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});


$("#tbl").on("click", ".decline", function () {
  var id = $(this).attr("data-bind");
  // console.log(id);
  Swal.fire({
    title: "Are You Sure Decline The PR?",
    type: 'question',
    icon: 'warning',
     html: '<div class="example-grid">'+
              '<label class="col-xl-12 col-md-3 form-control-label"><b>Reason Decline.</b><span class="required">*</span></label>'+
                  '<input id="reason" type="text" name="reason"/>'+
          '</div>',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    var reason = $('#reason').val();
    if (result.value == true) {

      // console.log(reason);
        $.ajax({
        url: "<?= base_url("backend/purchase/decline_pr_hod/")?>"+id,
        type:"POST",
        data:{'reason':reason},
        dataType:'json',
        success: function(data){
        window.location.reload();
        },
        
      });

    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});
</script>