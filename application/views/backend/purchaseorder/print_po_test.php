<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Daily Production</title>
    <table width="100%">
        <tr>
          <td width="200" align="center">
            <img src="<?php echo base_url('src/assets/images/file_head_latter/'.$edit_data->file_head_latter); ?>" width="200%">
          </td>
        </tr>
        </table> 
        <hr>
    <h2 style="font-size:100%;" align="center">PURCHASE ORDER</h2> 
</div>
</head>
    <body>
      <div class="document active">
  <div class="spreadSheetGroup">

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th>SUPPLIER DETAILS : </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:50%">
            <b><?=$name_sup?></b><br/>
            <?=$address?><br/>
          <?=$address_2?> <br/>
          Email : <?=$email?><br/>
          Phone : <?=$telpon?><br/>
          Attn : <?=$attn?><br/>
          <br>
          <br>
          <br>
          </td>
          <td style="width:25%">
            <strong><b>PO.NO</b></strong><br>
             <strong>PO Date</strong><br>
             <b>Payment Terms</b><br>
             <b>Incoterms</b><br>
            <small>Bank Charges</small><br>
            <small>Remarks</small><br>
            <small>***</small><br>
            <small>Ref No.</small><br>
            <small>***</small><br>
          </td>
          <td style="width:25%">
           : <?=$po_no;?><br>
           : <?php echo date('Y-m-d');?><br>
           : <?=$payment_term?><br>
           : <?=$incoterm?><br>
           : <small>OUR</small><br>
           <br>
           <br>
           <br>
           <br>
          </td>
        </tr>
      </tbody>
    </table>



    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th>SHIP TO ADDRESS :</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:50%">
          <b>PT.GALAKSI INVESTASI HARAPAN</b><br/>
          Panbil Industrial Estate, Factory B3 Lot 3,Jl.Ahmad Yani Muka Kuning,29548</b><br/>
          Indonesia <br/>
          Email : juniy@asiagalaxy.com<br/>
          Phone : +62 778 - 371713 (Hunting)<br/>
          Fax : <br/>
          </td>
          <td style="width:50%">
          </td>
        </tr>
      </tbody>
    </table>

    <table class="proposedWork" width="100%" style="margin-top:20px">
      <thead>
         <tr>
        <th>No</th>
        <th>PR No</th>
        <th>Part No</th>
        <th>Part Name</th>
        <th>UOM</th>
        <th>Currency</th>
        <th>Unit Price</th>
        <th>Qty</th>
        <th>Total Price</th>
         </tr>
        <!-- <th class="amountColumn">Grand Total</th> -->
      </thead>
      <tbody>
         <?php $no=1; foreach ($data_po as $val) { ?>
              <tr>
                  <td><?= $no++ ?></td>
                  <td><?=$val->pr_no;?></td>
                  <td><?=$val->part_no;?></td>
                  <td><?=$val->description;?></td>
                  <td><?=$val->uom;?></td>
                  <td><?=$val->currency;?></td>
                  <td><?=$val->unit_price_ttl;?></td>
                  <td><?=$val->new_order_qty;?></td>
                  <td><?=$val->total_amount;?></td>
                 
            </tr>
              <?php } ?>
      </tbody>
      <tfoot>
            <tr>
                <th  colspan="7" style="text-align: center; padding: 0px;">GRAND TOTAL</th>
                <th><?=$grand_total_qty?></th>
                <th><?=$grand_total?></th>
            </tr>
        </tfoot>
    </table>
   <!--  <table id="tblSum" border="1" class="table table-hover dataTable table-striped w-full">
              <tbody>
                <tr>
                  <td ><b></b></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td colspan="10"><?=$grand_total_qty?></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><?=$grand_total?></td>
                </tr>
              </tbody>
            </table>  -->

    
    
        <p style="font-size:70%;">============================================================================</p>
        <p style="font-size:80%;">Note : </p>
        <p style="font-size:80%;">•Operations hours for RECEIVING GOODS & MATERIAL (Monday-Friday): 08:00am-11.30pm-16:00pm.
        <p style="font-size:80%;">•PO NO must be stated in Delivery Order and Invoice.
        <p style="font-size:80%;">•Purchase Order (PO),Delivery Order(DO) must be attached when sending the INVOICE.



    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:50%">
            Approved By<br/>
            <!-- <img src="<?php echo base_url('src/material/global/assets/images/approved.png');?>" width="20%"><br/> -->
            <br>
            <br>
            <br>
            <br>
            <b>Phuspitaa Naaidu Ramlo</b>
          </td>
          <td style="width:50%">
          
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
    </body>
  <footer>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
     <p style="font-size:70%;"> <em>Generated By System : https://purchasing.asiagalaxy.com</em></p>
  </footer>
<style type="text/css">
  /* Housekeeping */
body{
  font-size:12px;
}
.spreadSheetGroup{
    /*font:0.75em/1.5 sans-serif;
    font-size:14px;
  */
    color:#333;
    background-color:#fff;
    padding:1em;
}

/* Tables */
.spreadSheetGroup table{
    width:100%;
    margin-bottom:1em;
    border-collapse: collapse;
}
.spreadSheetGroup .proposedWork th{
    background-color:#eee;
}
.tableBorder th{
  background-color:#eee;
}
.spreadSheetGroup th,
.spreadSheetGroup tbody td{
    padding:0.5em;

}
.spreadSheetGroup tfoot td{
    padding:0.5em;

}
.spreadSheetGroup td:focus { 
  border:1px solid #fff;
  -webkit-box-shadow:inset 0px 0px 0px 2px #5292F7;
  -moz-box-shadow:inset 0px 0px 0px 2px #5292F7;
  box-shadow:inset 0px 0px 0px 2px #5292F7;
  outline: none;
}
.spreadSheetGroup .spreadSheetTitle{ 
  font-weight: bold;
}
.spreadSheetGroup tr td{
  text-align:center;
}
/*
.spreadSheetGroup tr td:nth-child(2){
  text-align:left;
  width:100%;
}
*/

/*
.documentArea.active tr td.calculation{
  background-color:#fafafa;
  text-align:right;
  cursor: not-allowed;
}
*/
.spreadSheetGroup .calculation::before, .spreadSheetGroup .groupTotal::before{
  /*content: "$";*/
}
.spreadSheetGroup .trAdd{
  background-color: #007bff !important;
  color:#fff;
  font-weight:800;
  cursor: pointer;
}
.spreadSheetGroup .tdDelete{
  background-color: #eee;
  color:#888;
  font-weight:800;
  cursor: pointer;
}
.spreadSheetGroup .tdDelete:hover{
  background-color: #df5640;
  color:#fff;
  border-color: #ce3118;
}
.documentControls{
  text-align:right;
}
.spreadSheetTitle span{
  padding-right:10px;
}

.spreadSheetTitle a{
  font-weight: normal;
  padding: 0 12px;
}
.spreadSheetTitle a:hover, .spreadSheetTitle a:focus, .spreadSheetTitle a:active{
  text-decoration:none;
}
.spreadSheetGroup .groupTotal{
  text-align:right;
}



table.style1 tr td:first-child{
  font-weight:bold;
  white-space:nowrap;
  text-align:right;
}
table.style1 tr td:last-child{
  border-bottom:1px solid #000;
}



table.proposedWork td,
table.proposedWork th,
table.exclusions td,
table.exclusions th{
  border:1px solid #000;
}
table.proposedWork thead th, table.exclusions thead th{
  font-weight:bold;
}
table.proposedWork td,
table.proposedWork th:first-child,
table.exclusions th, table.exclusions td{
  text-align:left;
  vertical-align:top;
}
table.proposedWork td.description{
  width:20%;
}

table.proposedWork td.amountColumn, table.proposedWork th.amountColumn,
table.proposedWork td:last-child, table.proposedWork th:last-child{
  text-align:center;
  vertical-align:top;
  white-space:nowrap;
}

.amount:before, .total:before{
  content: "$";
}
table.proposedWork tfoot td:first-child{
  border:none;
  text-align:right;
}
table.proposedWork tfoot tr:last-child td{
  font-size:16px;
  font-weight:bold;
}

table.style1 tr td:last-child{
  width:100%;
}
table.style1 td:last-child{
  text-align:left;
}
td.tdDelete{
  width:1%;
}

table.coResponse td{text-align:left}
table.shipToFrom td, table.shipToFrom th{text-align:left}

.docEdit{border:0 !important}

.tableBorder td, .tableBorder th{
  border:1px solid #000;
}
.tableBorder th, .tableBorder td{text-align:center}

table.proposedWork td, table.proposedWork th{text-align:center}
table.proposedWork td.description{text-align:left}
</style>
<script>
    
    $(document).ready(function () {
    
     $('#tbl_prz').DataTable({
         paging: false,
        ordering: false,
        info: false,
        "searching": false,
         columnDefs: [    
          {
              targets: 8,
              render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
          {
              targets: 10,
              render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
        ],
    });
    $('#tblSum').DataTable({
    paging: false,
    ordering: false,
    info: false,
    "searching": false,
    columnDefs: [    
          {
              targets: 31,
              render: $.fn.dataTable.render.number( '.', ',', 0," ")
          },
          
        ],
  });


    });


</script>
</html>