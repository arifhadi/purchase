<div class="page">
  <div class="page-header" style="padding: 20px 10px;">
    <ol class="breadcrumb">
      <a href="<?php echo base_url('backend/admdashboard'); ?>" class="btn btn-round btn-info"><i class="icon md-home" aria-hidden="true"></i>Menu Dashboard</a>&emsp;&emsp;
      <a href="<?php echo base_url('backend/purchase_order/release_po'); ?>" class="btn btn-round btn-warning"><i class="icon md-plus" aria-hidden="true"></i>&nbsp; Release PO &nbsp;</a>&emsp;&emsp;
      <a href="<?php echo base_url('backend/purchase_order/list_po_unrelease'); ?>" class="btn btn-round btn-primary"><i class="zmdi zmdi-format-list-bulleted" aria-hidden="true"> List PR Unrelease</i>&nbsp;  &nbsp;<span class="badge"><?=$ttl_po_unrelease?></span></a>&emsp;&emsp;

    </ol><br>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
      <li class="breadcrumb-item active">PO List</li>
    </ol>
  </div>
  <?php if ($this->session->flashdata('success')) { ?>
  <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('success'); ?></p>
  </div>
  <?php }elseif($this->session->flashdata('error')){ ?>
  <div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('error'); ?></p>
  </div>
  <?php } ?>
  <h3 class="panel-title" style="text-align: center; padding: 0px;"><b>PO Release List</b></h3>
  <div class="page-content" style="padding: 0px 0px;">
    <div class="panel"><br>
      <div class="panel-body">
        <table  id="tableData" class="table table-hover dataTable table-striped w-full">
          <thead>
            <tr>
              <th>No.</th>
              <th>PO Date</th>
              <th>Department</th>
              <th>PO No</th>
              <th>PR No</th>
              <th>Date Request</th>
              <th>Date Approve PR</th>
              <th>Cost Center</th>
              <th>Purpose</th>
              <th>Part Name</th>
              <th>Category Request</th>
              <th>User Request</th>
              <th>Vendor Code</th>
              <th>Vendor Loc Code</th>
              <th>Vendor Name</th>
              <th>Payment Term</th>
              <th>Qty</th>
              <th>Unit Price</th>
              <th>Total Amount</th>
              <th>Currency</th>
              <th>Incoterm</th>
              <th style="text-align: center;">Action</th>


            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach ($po as $val) { ?>
              <tr>
                <td><?= $no++ ?></td>
                <td><?=$val->po_date;?></td>
                <td><?=$val->department;?></td>
                <td><?=$val->po_no;?></td>
                <td><?=$val->pr_no;?></td>
                <td><?=$val->date_request;?></td>
                <td><?=$val->date_pr_approve;?></td>
                <td><?=$val->cost_center;?></td>
                <td><?=$val->purpose;?></td>
                <td><?=$val->description;?></td>
                <td><?=$val->category_request;?></td>
                <td><?=$val->user_req;?></td>
                <td><?=$val->supplier_code;?></td>
                <td><?=$val->loc_code;?></td>
                <td><?=$val->name_sup;?></td>
                <td><?=$val->payment_term;?></td>
                <td><?=$val->new_order_qty;?></td>
                <td><?=$val->unit_price_ttl;?></td>
                <td><?=$val->total_amount;?></td>
                <td><?=$val->currency;?></td>
                <td><?=$val->incoterm;?></td>
                <td>
                  <button data-bind="<?=$val->po_no;?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-info btn-xs print" title="Print"><i class="zmdi zmdi-print" aria-hidden="true"></i></button>
                  <button data-bind="<?=$val->po_no;?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-warning btn-xs pdf" title="Export To PDF"><i class="zmdi zmdi-collection-pdf" aria-hidden="true"></i></button>
                  <button data-bind="<?=$val->po_no;?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-primary btn-xs excel" title="Export To Excel"><i class="zmdi zmdi-collection-text" aria-hidden="true"></i></button>
                </td>

              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->



<!-- Modal -->
<div class="modal fade" id="import-cost" aria-hidden="true" aria-labelledby="filterdata" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-center">
    <div class="modal-content">
      <?= form_open(base_url('backend/purchase/import_excel_cost_center'),  'id="login_validation" enctype="multipart/form-data"') ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
    <h4 class="modal-title" style="text-align: center;">Upload File Here</h4>
      <div class="modal-body">
        <div class="example-grid">
          <div class="row">
            <div class="col-md-9">
              <a href="<?php echo base_url('backend/purchase/template_excel/Template_Cost_Center.xlsx') ?>">Download Template.xlsx</a>
              <div class="input-group input-group-file" data-plugin="inputGroupFile">
                <input type="text" class="form-control" readonly="">
                <span class="input-group-append">
                  <span class="btn btn-success btn-file waves-effect waves-light waves-round">
                    <i class="icon md-upload" aria-hidden="true"></i>
                    <!-- <input type="file" name="file_picture" multiple=""> -->
                    <input type="file" class="form-control" name="file" accept=".xls, .xlsx" required>
                  </span>
                </span>
              </div>
            </div>
            <div class="col-lg-6">
              <span class="text-secondary">file format : <b style="color:red;">.xls, xlsx</b></span>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="Submit" class="btn btn-success btn-sm">&emsp;&nbsp; IMPORT &emsp;&nbsp;</button>&emsp;
        <?php form_close() ?>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL -->
<style type="text/css">
  
  .swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
  font-family: Georgia, serif;
}

</style>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js" integrity="sha256-Ry2q7Rf2s2TWPC2ddAg7eLmm7Am6S52743VTZRx9ENw=" crossorigin="anonymous"></script>
<script type="text/javascript">//On-Progress


$( document ).ready(function() {

  $('#tableData').DataTable({
        "scrollX": true,
        "scrollY":true,
         columnDefs: [    
          {
              targets: 17,
             render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
          {
              targets: 18,
             render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
        ],


    });


});

  $("#tableData").on("click", ".print", function () {
  var id = $(this).attr("data-bind");
  Swal.fire({
    title: "You Want To Print The PO ?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
      $.ajax({
          success: function(data) {
            window.location.href = '<?= base_url("backend/purchase_order/view_print_po/")?>'+id;
          }
        });
    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});

   $("#tableData").on("click", ".excel", function () {
  var id = $(this).attr("data-bind");
  Swal.fire({
    title: "You Want To Export Excel?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
      $.ajax({
          success: function(data) {
            window.location.href = '<?= base_url("backend/purchase_order/export_to_excel_po/")?>'+id;
          }
        });
    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});

 $("#tableData").on("click", ".pdf", function () {
  var id = $(this).attr("data-bind");
  Swal.fire({
    title: "You Want To Export PDF?",
    type: 'question',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#66bb6a',
    cancelButtonColor: '#ef5350',
    confirmButtonText: 'Yes, Confirm!'
  }).then((result) => {
    if (result.value == true) {
      $.ajax({
          success: function(data) {
            window.location.href = '<?= base_url("backend/purchase_order/export_to_pdf_po/")?>'+id;
          }
        });
    }else if(result.value == undefined){
      Swal.fire(
        'Cancelled !',
        'Your Cancel Action',
        'error'
      )
    }
  })
});

  $(".change").click(function(){
    var id = $(this).attr("data-bind");
    swal({
      title: "you want to change the data?",
      text: "",
      type: "info",
      showCancelButton: true,
      confirmButtonClass: "btn-info",
      confirmButtonText: "Yes",
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          success: function(data) {
              window.location.href = '<?= base_url("backend/purchase/item_update/")?>'+id;
          }
        });
      } else {
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
    });
  });
</script>


