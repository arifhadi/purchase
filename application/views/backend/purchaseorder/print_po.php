<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Daily Production</title>
    

          
    <table width="100%">
        <tr>
          <td width="50" align="center">
            <img src="<?php echo base_url('src/assets/images/file_head_latter/'.$edit_data->file_head_latter); ?>" width="60%">
          </td>
        </tr>
        </table> 
        <hr>
    <h2 align="center">PURCHASE ORDER</h2> 
</div>
</head>
    <body>

      <div class="form-row">
            <div class="col-2"></div>
                <div class="col-1"></div>
                <div class="col-1"></div>
                <div class="col-2"></div>
                <div class="col-1"><h5>PO Date.</h5></div>
                <div class="col-2"><h5>: <?php echo date('Y-m-d');?></h5></div>
                <div class="col-1"><h5>PO No.</h5></div>
                <div class="col-1"><h5>:<?=$po_no?></h5></div>
        </div>

        <div class="form-row">
            <div class="col-2"></div>
                <div class="col-1"></div>
                <div class="col-1"></div>
                <div class="col-2"></div>
                <div class="col-1"><h5>PAYMENT TERMS</h5></div>
                <div class="col-2"><h5>: <?=$payment_term?></h5></div>
                <div class="col-1"><h5>INCOTERMS</h5></div>
                <div class="col-1"><h5>:<?=$incoterm?></h5></div>
        </div>



        <div class="form-row">
                <div class="col-2"><h5>SUPPLIER DETAILS</h5></div>
                <div class="col-3"><h5>:</h5></div>
                <div class="col-2"><h5>SHIP TO ADDRESS</h5></div>
                <div class="col-2"><h5></h5></div>
                <div class="col-2"><h5></h5></div>
                <div class="col-1"><h5></h5></div>
              </div>

              


              <div class="form-row">
                <div class="col-2"><h5><b><?=$name_sup?></b></h5></div>
                <div class="col-3"></div>
                <div class="col-3"><h5><b>PT.GALAKSI INVESTASI HARAPAN</b></h5></div>
                <div class="col-1"></div>
                <div class="col-2"></div>
                <div class="col-1"></div>
              </div>

               <div class="form-row">

                <div class="col-2"><h5><b><?=$address?></b></h5></div>
                <div class="col-3"> </div>
              <div class="col-5"><h5>Panbil Industrial Estate, Factory B3 Lot 3,Jl.Ahmad Yani Muka Kuning,29548</h5></div>
                <div class="col-1"></div>
                <div class="col-1"></div>
                <div class="col-1"></div>
              </div>

                <div class="form-row">

                <div class="col-2"><h5><b><?=$address_2?></b></h5></div>
                <div class="col-3"></div>
                <div class="col-2"><h5>Indonesia</h5></div>
                <div class="col-2"></div>
                <div class="col-2"></div>
                <div class="col-1"></div>

              </div>


              <div class="form-row">

                <div class="col-2"><h5>Email</h5></div>
                <div class="col-3">:<?=$email?></div>
                <div class="col-2"><h5>Email</h5></div>
                <div class="col-2">: juniy@asiagalaxy.com</div>
                <div class="col-2"></div>
                <div class="col-1"></div>

              </div>


               <div class="form-row">

                <div class="col-2"><h5>Phone</h5></div>
                <div class="col-3">: <?=$telpon?></div>
                <div class="col-2"><h5>Phone </h5></div>
                <div class="col-2">: +62 778 - 371713 (Hunting)</div>
                <div class="col-2"></div>
                <div class="col-1"></div>

              </div>

               <div class="form-row">

                <div class="col-2"><h5>Attn</h5></div>
                <div class="col-3">:<?=$attn?> </div>
                <div class="col-2"><h5>Fax </h5></div>
                <div class="col-2">:</div>
                <div class="col-2"></div>
                <div class="col-1"></div>

              </div>
              <br>

              <div class="form-row">
                <div class="col-2"><h5><b>Remarks</b></h5></div>
                <div class="col-3">: </div>
                <div class="col-2"><h5></h5></div>
                <div class="col-2"></div>
                <div class="col-2"></div>
                <div class="col-1"></div>

              </div>

              <div class="form-row">
                <div class="col-2"><h5>***</h5></div>
                <div class="col-3"></div>
                <div class="col-2"><h5></h5></div>
                <div class="col-2"></div>
                <div class="col-2"></div>
                <div class="col-1"></div>

              </div>


               <div class="form-row">
                <div class="col-2"><b>Ref No.</b></div>
                <div class="col-3">: </div>
                <div class="col-2"><h5></h5></div>
                <div class="col-2"></div>
                <div class="col-2"></div>
                <div class="col-1"></div>

              </div>

              <div class="form-row">
                <div class="col-2"><h5>***</h5></div>
                <div class="col-3"></div>
                <div class="col-2"><h5></h5></div>
                <div class="col-2"></div>
                <div class="col-2"></div>
                <div class="col-1"></div>

              </div>


               <div class="form-row">
                <div class="col-2"><b>Bank Charges.</b></div>
                <div class="col-3">: OUR </div>
                <div class="col-2"><h5></h5></div>
                <div class="col-2"></div>
                <div class="col-2"></div>
                <div class="col-1"></div>
              </div>

              <br>


        <table id="tbl_prz" border="1" class="table table-hover dataTable table-striped w-full"  >
        <thead>
            <tr>
                <th>No.</th>
                  <th>PR No.</th>
                  <th>Part No</th>
                  <th>Part Name</th>
                  <th>Cost Center</th>
                  <th>Category Request</th>
                  <th>UOM</th>
                  <th>Currency</th>
                  <th>Unit Price</th> 
                  <th>Qty</th>
                  <th>Total Price</th>
            </tr>
        </thead>
        <tbody>
     <?php $no=1; foreach ($data_po as $val) { ?>
              <tr>
                  <td><?= $no++ ?></td>
                  <td><?=$val->pr_no;?></td>
                  <td><?=$val->part_no;?></td>
                  <td><?=$val->description;?></td>
                  <td><?=$val->cost_center;?></td>
                  <td><?=$val->category_request;?></td>
                  <td><?=$val->uom;?></td>
                  <td><?=$val->currency;?></td>
                  <td><?=$val->unit_price_ttl;?></td>
                  <td><?=$val->new_order_qty;?></td>
                  <td><?=$val->total_amount;?></td>
                 
            </tr>
              <?php } ?>
        </tbody>

        </table>



         <table id="tblSum" class="table table-hover dataTable table-striped w-full">
               <thead>
                <tr>
                <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><b>GRAND TOTAL</b></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><?=$grand_total_qty?></td>
                  <td><?=$grand_total?></td>
                </tr>
              </tbody>
            </table> 
             <p style="font-size:70%;">============================================================================</p>
        <p style="font-size:80%;">Note : </p>
        <p style="font-size:80%;">•Operations hours for RECEIVING GOODS & MATERIAL (Monday-Friday): 08:00am-11.30pm-16:00pm.
        <p style="font-size:80%;">•PO NO must be stated in Delivery Order and Invoice.
        <p style="font-size:80%;">•Purchase Order (PO),Delivery Order(DO) must be attached when sending the INVOICE.

        <table id="tbl_prs" class="cell-border compact stripe" >
                <thead>
                    <tr>
                        <th style="text-align: center; padding: 0px;">Approved By</th>
                    </tr> 
                </thead>
            <tbody>
                <tr>
                    <td><img src="<?php echo base_url('src/material/global/assets/images/approved.png');?>" width="100%"></td>
                </tr>
            </tbody>
        <tfoot>
            <tr>
                <th style="text-align: center; padding: 0px;">Phuspitaa Naaidu Ramlo</th>
            </tr>
        </tfoot>

        </table>
    </body>

<script>
    
    $(document).ready(function () {
    
     $('#tbl_prz').DataTable({
         paging: false,
        ordering: false,
        info: false,
        "searching": false,
         columnDefs: [    
          {
              targets: 8,
              render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
          {
              targets: 10,
              render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
        ],
    });
    $('#tblSum').DataTable({
    paging: false,
    ordering: false,
    info: false,
    "searching": false,
    columnDefs: [    
          {
              targets: 31,
              render: $.fn.dataTable.render.number( '.', ',', 0," ")
          },
          
        ],
  });


    });


</script>
</html>