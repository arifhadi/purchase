<!-- Page -->
<div class="page">
  <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
    <li class="breadcrumb-item active">Change Head Latter</li>
    </ol>
  <div class="page-header" style="text-align: left; padding: 0px;">
    <h5 style="color:#0052cc;">&emsp;<b><< CHANGE >></b></h5>
    <h1 style="margin-top: 0px; text-align: center;" class="page-title">Change Head Latter</h1>
  </div>
  <div class="page-content">
    <div class="panel">
      <div class="panel-body container-fluid" style="padding: 0px;">
        <!-- **Alert**-->
        <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button><p><?php echo $this->session->flashdata('success'); ?></p>
        </div>
        <?php }elseif($this->session->flashdata('error')){ ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button><p><?php echo $this->session->flashdata('error'); ?></p>
        </div>
        <?php } ?>
       
        <div class="panel">
          <div class="panel-body container-fluid" style="padding: 20px;">
            <div class="row row-lg">
              <div class="col-md-12 col-lg-5">
                <div class="example-wrap">
                  <div class="example">
                    <!-- <form class="form-horizontal"> -->
                    <?= form_open(base_url('backend/purchase_order/save_head_latter'),  'id="login_validation" enctype="multipart/form-data"') ?>

                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Image Head Latter</b></label>
                      <div class="col-md-9">
                        <?php if (!empty($edit_data)) {
                          {
                            ?> 
                            <img id="myImg3" src="<?php echo base_url('src/assets/images/file_head_latter/'.$edit_data->file_head_latter); ?>" alt="Paris" width="200" height="200" onclick="viewImagesCon1()">
                            <?php
                          }
                        }else{ ?>
                        <img src="<?php echo base_url('src/assets/images/profile/avatar2.jpg'); ?>" alt="Paris" width="200" height="200" class="profil">
                        <?php } ?>

                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label">&nbsp;</label>
                      <div class="col-md-6">
                        <div class="input-group input-group-file" data-plugin="inputGroupFile">
                          <input  value="<?=$edit_data->file_head_latter?>" type="text" class="form-control" readonly="">
                          <span class="input-group-append">
                            <span class="btn btn-success btn-file">
                              <i class="icon md-upload" aria-hidden="true"></i>
                              <input type="file" name="file_head_latter" multiple="">
                              <input type="text" hidden value="<?=$edit_data->file_head_latter?>" name="old_file_head_latter">
                            </span>
                          </span>
                        </div>
                      </div>
                    </div>

                     <button type="Submit" class="btn btn-success btn-sm" form="login_validation">&emsp; SAVE &emsp;</button>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-lg-6">
                <div class="example-wrap">
                  <div class="example">
                  
                  </div>
                </div>
              </div>
              <?php form_close() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
   
  </div>
</div>
<!-- End Page -->




<style>
  body {font-family: Arial, Helvetica, sans-serif;}




  #myImg3 {
    border-radius: 100px;
    cursor: pointer;
    transition: 0.3s;
    border: 5px solid #0099ff;
    padding: 3px;
    /*border-radius: 100px;*/
  }

  #myImg3:hover {opacity: 0.7;}

 

  /* The Modal (background) */
  .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    /*z-index: 1;*/ /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
  }

  /* Modal Content (image) */
  .modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
  }

  /* Caption of Modal Image */
  #caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
  }

  /* Add Animation */
  .modal-content, #caption {  
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
  }

  @-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
  }

  @keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
  }

  /* The Close Button */
  .close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
  }

  .close:hover,
  .close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
  }

  /* 100% Image Width on Smaller Screens */
  @media only screen and (max-width: 700px){
    .modal-content {
      width: 100%;
    }
  }
  </style>
    <!-- The Modal -->
    <div id="myModal" class="modal">
      <span class="close" id="close" onclick="closeModal()">&times;</span>
      <img class="modal-content" id="img01">
    </div>

    <!-- End Page -->


    <script>
      // Get the modal
      var modal = document.getElementById("myModal");

      // Get the image and insert it inside the modal - use its "alt" text as a caption
      var img = document.getElementById("myImg3");
      var modalImg = document.getElementById("img01");


      function viewImagesCon1()
      {
         modal.style.display = "block";
        modalImg.src = img.src;
      }

     

      img.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
      }

     


      // Get the <span> element that closes the modal
      var span = document.getElementsByClassName("close")[0];

      var click = document.getElementById("close");
      // consol
      // When the user clicks on <span> (x), close the modal


      span.onclick = function() { 
        modal.style.display = "none";
        
      }

      function closeModal()
      {
        modal.style.display = "none";
      }

      function openInNewTab(url) {
            window.open(url, '_blank').focus();
        }

    </script>

