<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Daily Production</title>
    <table width="50%">
        <tr>
          <td width="50" align="center">
            <img src="<?php echo base_url('src/material/global/assets/images/kop.png');?>" width="50%">
          </td>
        </tr>
        </table> 
        
</div>
</head>
    <body>
      <div class="document active">
  <div class="spreadSheetGroup">
    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:50%">
          </td>
          <td style="width:25%">
          <b><h2 align="center">PURCHASE ORDER</h2> </b><br/>
          </td>
          <td style="width:25%">
         
          </td>
        </tr>
      </tbody>
    </table>



     <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:50%">
          </td>
          <td style="width:25%">
          <b>PO Date. : <?php echo date('Y-m-d');?></b><br/>
          <b>PAYMENT TERMS</b>: <?=$payment_term?><br/>
          </td>
          <td style="width:25%">
          <b>PO No. : <?=$po_no;?></b><br/>
          <b>INCOTERMS</b>: <?=$incoterm?><br/>
          </td>
        </tr>
      </tbody>
    </table>



    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th>SUPPLIER DETAILS</th>
          <th>SHIP TO ADDRESS</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:50%">
            <b><?=$name_sup?></b><br/>
            <?=$address?><br/>
          <?=$address_2?> <br/>
          Email : <?=$email?><br/>
          Phone : <?=$telpon?><br/>
          Attn : <?=$attn?><br/>
          </td>
          <td style="width:50%">
          <b>PT.GALAKSI INVESTASI HARAPAN</b><br/>
          Panbil Industrial Estate, Factory B3 Lot 3,Jl.Ahmad Yani Muka Kuning,29548</b><br/>
          Indonesia <br/>
          Email : juniy@asiagalaxy.com<br/>
          Phone : +62 778 - 371713 (Hunting)<br/>
          Fax : <br/>
          </td>
        </tr>
      </tbody>
    </table>

      <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:50%">
            <b>Remarks</b><br/>
            ***<br/>
            <b>Ref No</b> <br/>
          ***<br/>
          <b>Bank Charges : OUR</b><br/>
          <br/>
          </td>
          <td style="width:50%">
          
          </td>
        </tr>
      </tbody>
    </table>

    
    
    
    <table class="proposedWork" width="100%" style="margin-top:20px">
      <thead>
         <tr>
        <th>No</th>
        <th>PR No</th>
        <th>Part No</th>
        <th>Part Name</th>
        <th>UOM</th>
        <th>Currency</th>
        <th>Unit Price</th>
        <th>Qty</th>
        <th>Total Price</th>
         </tr>
        <!-- <th class="amountColumn">Grand Total</th> -->
      </thead>
      <tbody>
         <?php $no=1; foreach ($data_po as $val) { ?>
              <tr>
                  <td><?= $no++ ?></td>
                  <td><?=$val->pr_no;?></td>
                  <td><?=$val->part_no;?></td>
                  <td><?=$val->description;?></td>
                  <td><?=$val->uom;?></td>
                  <td><?=$val->currency;?></td>
                  <td><?=$val->unit_price_ttl;?></td>
                  <td><?=$val->new_order_qty;?></td>
                  <td><?=$val->total_amount;?></td>
                 
            </tr>
              <?php } ?>
      </tbody>
    </table>
    <table id="tblSum" class="table table-hover dataTable table-striped w-full">
               <thead>
                <tr>
                <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><b>GRAND TOTAL</b></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><?=$grand_total_qty?></td>
                  <td><?=$grand_total?></td>
                </tr>
              </tbody>
            </table> 

    
    
        <p style="font-size:70%;">============================================================================</p>
        <p style="font-size:80%;">Note : </p>
        <p style="font-size:80%;">•Operations hours for RECEIVING GOODS & MATERIAL (Monday-Friday): 08:00am-11.30pm-16:00pm.
        <p style="font-size:80%;">•PO NO must be stated in Delivery Order and Invoice.
        <p style="font-size:80%;">•Purchase Order (PO),Delivery Order(DO) must be attached when sending the INVOICE.



          <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:50%">
            <b> Approved By</b><br/>
            <img src="<?php echo base_url('src/material/global/assets/images/approved.png');?>" width="15%"><br/>
            <br>
            <br>
            <br>
            <b>Phuspitaa Naaidu Ramlo</b>
          </td>
          <td style="width:50%">
          
          </td>
        </tr>
      </tbody>
    </table>


  </div>
</div>
    </body>
<style type="text/css">
  /* Housekeeping */
body{
  font-size:12px;
}
.spreadSheetGroup{
    /*font:0.75em/1.5 sans-serif;
    font-size:14px;
  */
    color:#333;
    background-color:#fff;
    padding:1em;
}

/* Tables */
.spreadSheetGroup table{
    width:100%;
    margin-bottom:1em;
    border-collapse: collapse;
}
.spreadSheetGroup .proposedWork th{
    background-color:#eee;
}
.tableBorder th{
  background-color:#eee;
}
.spreadSheetGroup th,
.spreadSheetGroup tbody td{
    padding:0.5em;

}
.spreadSheetGroup tfoot td{
    padding:0.5em;

}
.spreadSheetGroup td:focus { 
  border:1px solid #fff;
  -webkit-box-shadow:inset 0px 0px 0px 2px #5292F7;
  -moz-box-shadow:inset 0px 0px 0px 2px #5292F7;
  box-shadow:inset 0px 0px 0px 2px #5292F7;
  outline: none;
}
.spreadSheetGroup .spreadSheetTitle{ 
  font-weight: bold;
}
.spreadSheetGroup tr td{
  text-align:center;
}
/*
.spreadSheetGroup tr td:nth-child(2){
  text-align:left;
  width:100%;
}
*/

/*
.documentArea.active tr td.calculation{
  background-color:#fafafa;
  text-align:right;
  cursor: not-allowed;
}
*/
.spreadSheetGroup .calculation::before, .spreadSheetGroup .groupTotal::before{
  /*content: "$";*/
}
.spreadSheetGroup .trAdd{
  background-color: #007bff !important;
  color:#fff;
  font-weight:800;
  cursor: pointer;
}
.spreadSheetGroup .tdDelete{
  background-color: #eee;
  color:#888;
  font-weight:800;
  cursor: pointer;
}
.spreadSheetGroup .tdDelete:hover{
  background-color: #df5640;
  color:#fff;
  border-color: #ce3118;
}
.documentControls{
  text-align:right;
}
.spreadSheetTitle span{
  padding-right:10px;
}

.spreadSheetTitle a{
  font-weight: normal;
  padding: 0 12px;
}
.spreadSheetTitle a:hover, .spreadSheetTitle a:focus, .spreadSheetTitle a:active{
  text-decoration:none;
}
.spreadSheetGroup .groupTotal{
  text-align:right;
}



table.style1 tr td:first-child{
  font-weight:bold;
  white-space:nowrap;
  text-align:right;
}
table.style1 tr td:last-child{
  border-bottom:1px solid #000;
}



table.proposedWork td,
table.proposedWork th,
table.exclusions td,
table.exclusions th{
  border:1px solid #000;
}
table.proposedWork thead th, table.exclusions thead th{
  font-weight:bold;
}
table.proposedWork td,
table.proposedWork th:first-child,
table.exclusions th, table.exclusions td{
  text-align:left;
  vertical-align:top;
}
table.proposedWork td.description{
  width:20%;
}

table.proposedWork td.amountColumn, table.proposedWork th.amountColumn,
table.proposedWork td:last-child, table.proposedWork th:last-child{
  text-align:center;
  vertical-align:top;
  white-space:nowrap;
}

.amount:before, .total:before{
  content: "$";
}
table.proposedWork tfoot td:first-child{
  border:none;
  text-align:right;
}
table.proposedWork tfoot tr:last-child td{
  font-size:16px;
  font-weight:bold;
}

table.style1 tr td:last-child{
  width:100%;
}
table.style1 td:last-child{
  text-align:left;
}
td.tdDelete{
  width:1%;
}

table.coResponse td{text-align:left}
table.shipToFrom td, table.shipToFrom th{text-align:left}

.docEdit{border:0 !important}

.tableBorder td, .tableBorder th{
  border:1px solid #000;
}
.tableBorder th, .tableBorder td{text-align:center}

table.proposedWork td, table.proposedWork th{text-align:center}
table.proposedWork td.description{text-align:left}
</style>
<script>
    
    $(document).ready(function () {
    
     $('#tbl_prz').DataTable({
         paging: false,
        ordering: false,
        info: false,
        "searching": false,
         columnDefs: [    
          {
              targets: 8,
              render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
          {
              targets: 10,
              render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
        ],
    });
    $('#tblSum').DataTable({
    paging: false,
    ordering: false,
    info: false,
    "searching": false,
    columnDefs: [    
          {
              targets: 31,
              render: $.fn.dataTable.render.number( '.', ',', 0," ")
          },
          
        ],
  });


    });


</script>
</html>