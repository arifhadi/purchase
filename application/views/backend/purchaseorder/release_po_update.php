<!-- Page -->
<div class="page">
  <ol class="breadcrumb">
     <a href="<?php echo base_url('backend/purchase_order/list_po_release'); ?>" type="button" class="btn btn-round btn-warning"><i class="icon md-format-indent-increase" aria-hidden="true"></i>PO List</a>
  </ol>
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="<?=base_url('backend/purchase_order/list_po_release')?>">PO List</a></li>
    <li class="breadcrumb-item active">PO Release</li>
  </ol>
  <div class="page-header" style="text-align: left; padding: 0px;">
    <h5 style="color:#0052cc;">&emsp;<b><< CREATE >></b></h5>
    <h1 style="margin-top: 0px; text-align: center;" class="page-title">PO Release</h1>
  </div>
  <div class="page-content">
    <div class="page-content container-fluid" style="padding: 0px;">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel">
                 <header class="panel-heading" style="text-align: right;">
                  <h4><b>PO No.</b> <b style="color: #ff3300; text-decoration: underline;"><?=$po_no?></b></h4>
                </header>
                 <?php if ($this->session->flashdata('info')) { ?>
                    <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button><?= $this->session->flashdata('info'); ?>
                    </div>
                <?php } ?>
                    <div class="panel-body">
                    <h4 align="center"><b>Form Add PR To Release PO</b></h4>
                        <?php echo form_open_multipart('backend/purchase_order/update_po_no'); ?>
                            <div class="row row-lg-12">
                                <div class="col-md-12 col-lg-12">
                                    <div class="example-wrap">
                                        <div class="example">
                                            <div class="form-group row">
                                                <label class="col-md-6 col-form-label control-label" align='center'>PR No</label>
                                                <div class="col-md-4">
                                                    <select class="form-control" required="required" data-plugin="select2" id="pr_no" name="pr_no" data-placeholder="Select PR No">
                                                      <option></option>
                                                        <?php foreach ($pr_list as $val) { ?>
                                                          <option value="<?=$val->pr_no;?>"><?=$val->pr_no;?>  -  ( <?=$val->description;?> ) - (<?=$val->supplier_code;?>)</option>
                                                        <?php } ?>
                                                      </select>
                                                </div>
                                            </div>
                                            <div hidden class="form-group row">
                                                <label class="col-md-6 col-form-label control-label" align='center'>PO No</label>
                                                <div class="col-md-4">
                                                    <input type="text" required="required" class="form-control" name="po_no" placeholder="PO No" autocomplete="off" value="<?=$po_no?>"/>
                                                </div>
                                            </div>
                                            <div hidden class="form-group row">
                                                <label class="col-md-6 col-form-label control-label" align='center'>PO Temps</label>
                                                <div class="col-md-4">
                                                    <input type="text" required="required" class="form-control" name="po_temps" placeholder="PO Temps" autocomplete="off" value="<?=$po_no_temps?>"/>
                                                </div>
                                            </div>
                                            <div class="card-body" id="body" align="center">
                                                <button type="submit" class="btn btn-success btn-round btn-md">
                                                    <span class=" text">&emsp;Save&emsp;</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- End Panel Form -->
            </div>
            <r
            <div class="col-lg-6">
                <!-- Panel Extended -->
                <div class="panel">
                    <div class="panel-body">
                        <h4 align="center"><b>List PO</b></h4><br>
                        <table   id="tableData" class="table table-hover dataTable table-striped w-full">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>PO No</th>
                                    <th>PR No</th>
                                    <th>Qty</th>
                                    <th>Unit Price</th>
                                    <th>Total Amount</th>
                                    <th hidden>Id Item</th>
                                    <!-- <th>Actions</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($edit_data as $value) { ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $value->po_no ?></td>
                                        <td><?= $value->pr_no ?></td>
                                        <td><?=$value->new_order_qty;?></td>
                                        <td><?=$value->unit_price_ttl;?></td>
                                        <td><?=$value->total_amount;?></td>
                                        <td hidden><?=$value->description;?></td>
                                    <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>


<!-- End Page -->
<style>
  .example{
    margin-top: 0px;
  }

  div.dataTables_wrapper {
    width: 1200px;
    margin: 0 auto;
  }
</style>



<style>
.swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
  font-family: Georgia, serif;
}
</style>
<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>

<div class="modal fade" id="loader" aria-hidden="true" aria-labelledby="filterdata" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-center">
    <div class="modal-content">
      <div class="modal-header">
      </div>
       <div class="loader"></div>
        <h4 class="modal-title" style="text-align: center;">Loading...</h4>
  </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js" integrity="sha256-Ry2q7Rf2s2TWPC2ddAg7eLmm7Am6S52743VTZRx9ENw=" crossorigin="anonymous"></script>


<script>

$( document ).ready(function() {

  $('#tableData').DataTable({
        "scrollX": true,
        "scrollY":true,
        "paging":false,
        "searching": false,
         columnDefs: [    
          {
              targets: 17,
             render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
          {
              targets: 18,
             render: $.fn.dataTable.render.number( '.', ',', 0," " )
          },
        ],


    });


});



</script>



