<div class="page">
  <div class="page-header" style="padding: 20px 10px;">
    <ol class="breadcrumb">
      <a href="<?php echo base_url('backend/planner'); ?>" class="btn btn-round btn-info"><i class="icon md-home" aria-hidden="true"></i>Menu Planner</a>&emsp;&emsp;
      <a href="<?php echo base_url('backend/supplier/supplier_add'); ?>" class="btn btn-round btn-danger"><i class="icon md-plus" aria-hidden="true"></i>&nbsp; Create &nbsp;</a>&emsp;&emsp;
       <button type="button" class="btn btn-success btn-round" data-target="#supplier" data-toggle="modal"><i class="icon md-upload" aria-hidden="true"></i>Import Excel</button>&emsp;&emsp;
    </ol><br>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
      <!-- <li class="breadcrumb-item"><a href="<?=base_url('backend/planner')?>">Menu Planner</a></li>
      <li class="breadcrumb-item"><a href="<?=base_url('backend/planner/injection_molding')?>">Menu Injection Molding</a></li>
      <li class="breadcrumb-item"><a href="<?=base_url('backend/bom_injection')?>">Menu BOM Master</a></li> -->
      <li class="breadcrumb-item active">Supplier List</li>
    </ol>
  </div>
  <?php if ($this->session->flashdata('success')) { ?>
  <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('success'); ?></p>
  </div>
  <?php }elseif($this->session->flashdata('error')){ ?>
  <div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('error'); ?></p>
  </div>
  <?php } ?>
  <h3 class="panel-title" style="text-align: center; padding: 0px;"><b>Supplier List</b></h3>
  <div class="page-content" style="padding: 0px 0px;">
    <div class="panel"><br>
      <div class="panel-body">
        <table id="datatable" class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
          <thead>
            <tr>
              <th>No.</th>
              <th>Code</th>
              <th>Name</th>
              <th>Address</th>
              <th style="text-align: center;">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach ($supplier as $val) { ?>
              <tr>
                <td><?= $no++ ?></td>
                <td><?=$val->code;?></td>
                <td><?=$val->name;?></td>
                <td><?=$val->address;?></td>
                <td style="text-align: center;">
                  <button data-bind="<?=$val->code;?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-success btn-xs change" title="Change"><i class="icon md-edit" aria-hidden="true"></i></button>
                  <button data-bind=" " type="button" data-toggle="tooltip" class="btn btn-floating btn-danger btn-xs nodelete" title="Delete"><i class="icon md-delete" aria-hidden="true"></i></button>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>




<!-- Modal -->
<div class="modal fade" id="supplier" aria-hidden="true" aria-labelledby="filterdata" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-center">
    <div class="modal-content">
      <?= form_open(base_url('backend/bom_injection/import_excel_suplier'),  'id="login_validation" enctype="multipart/form-data"') ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
    <h4 class="modal-title" style="text-align: center;">Upload File Here</h4>
      <div class="modal-body">
        <div class="example-grid">
          <div class="row">
            <div class="col-md-9">
              <a href="<?php echo base_url('backend/bom_injection/template_brand/Template_Supplier.xlsx') ?>">Download Template.xlsx</a>
              <div class="input-group input-group-file" data-plugin="inputGroupFile">
                <input type="text" class="form-control" readonly="">
                <span class="input-group-append">
                  <span class="btn btn-success btn-file waves-effect waves-light waves-round">
                    <i class="icon md-upload" aria-hidden="true"></i>
                    <input type="file" class="form-control" name="file" accept=".xls, .xlsx" required>
                  </span>
                </span>
              </div>
            </div>
            <div class="col-lg-6">
              <span class="text-secondary">file format : <b style="color:red;">.xls, xlsx</b></span>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="Submit" class="btn btn-success btn-sm">&emsp;&nbsp; IMPORT &emsp;&nbsp;</button>&emsp;
        <?php form_close() ?>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL -->

<script type="text/javascript">//On-Progress
  $(".change").click(function(){
    var id = $(this).attr("data-bind");
    swal({
      title: "you want to change the data?",
      text: "",
      type: "info",
      showCancelButton: true,
      confirmButtonClass: "btn-info",
      confirmButtonText: "Yes",
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          // url: '<?= base_url("backend/main_menu/change_masterdata/")?>'+id,
          type: 'DELETE',
          error: function() {
            alert('Something is wrong');
          },
          success: function(data) {
            // $("#"+id).remove();
              // swal("Deleted!", "Your imaginary file has been deleted.", "success");
              window.location.href = '<?= base_url("backend/supplier/supplier_edit/")?>'+id;
          }
        });
      } else {
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
    });
  });
</script>


