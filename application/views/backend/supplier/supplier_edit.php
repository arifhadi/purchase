<div class="page">
  <ol class="breadcrumb">
    <a href="<?php echo base_url('backend/supplier'); ?>" type="button" class="btn btn-round btn-warning"><i class="icon md-format-indent-increase" aria-hidden="true"></i>Supplier List</a>
  </ol>
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="<?=base_url('backend/supplier')?>">Supplier List</a></li>
    <li class="breadcrumb-item active">Edit Supplier</li>
  </ol>
  <h4 style="text-align: left; color:#0000e6; font-weight: 900;"><b>&emsp; >>Update<< </b></h4>
  <?php if ($this->session->flashdata('success')) { ?>
  <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('success'); ?></p>
  </div>
  <?php }elseif($this->session->flashdata('error')){ ?>
  <div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('error'); ?></p>
  </div>
  <?php } ?>
  <div class="page-header" style="text-align: center; padding: 0px;">
    <h1 class="page-title">Form Edit Supplier</h1>
  </div>
  <div class="page-content" style="padding: 0px;">
    <div class="panel">
      <div class="panel-body container-fluid" style="padding: 0px;">
        <div class="panel">
          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-md-12 col-lg-6">
                <!-- Example Horizontal Form -->
                <div class="example-wrap">
                  <div class="example">
                    <!-- <form class="form-horizontal"> -->
                    <?= form_open(base_url('backend/supplier/supplier_edit'),  'id="login_validation" enctype="multipart/form-data"') ?>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Supplier Number<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" name="code" value="<?=$supplier->code;?>" placeholder="Auto Generate" autocomplete="off" readonly />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Name<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" name="supplier_name" value="<?=$supplier->name;?>" placeholder="Supplier Name" autocomplete="off" />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Address<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <textarea class="maxlength-textarea form-control" data-plugin="maxlength" data-placement="bottom-right-inside" maxlength="200" name="address" rows="3" placeholder="Location"><?=$supplier->address;?></textarea>
                      </div>
                    </div>
                       <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Telpon<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" value="<?=$supplier->telpon;?>"  name="telpon" placeholder="Telpon" autocomplete="off" />
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End Example Horizontal Form -->
              </div>
              <div class="col-md-12 col-lg-6">
              </div>
              <!-- Button Action -->
              <div class="col-lg-5 form-group form-material">
                <!-- <input type="text" class="form-control" placeholder=".col-lg-4"> -->
              </div>
              <div class="col-lg-5 form-group form-material">
                <button type="Submit" class="btn btn-success btn-sm">&emsp;&emsp;UPDATE&emsp;&emsp;</button>
              </div>
              <div class="col-lg-2 form-group form-material">
              </div>
              <?php form_close() ?>
              <!-- Button Action -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Page