<!-- Page -->
<div class="page">
  <div class="page-content container-fluid">
    <header>
      <h1 class="gradient-text"><b>PURCHASE SYSTEM</b></h1><br>
    </header>
    <div class="row" data-plugin="matchHeight" data-by-row="true">
      
     <!-- <?php if(check_permission_view(ID_GROUP,'read','supplier')) { ?>
      <div class="col-xl-4 col-md-6">
        <a href="<?php echo base_url('backend/purchase/list_data_supplier') ?>">
          <div class="card card-shadow menu" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-assignment-account grey-600 font-size-24 vertical-align-bottom mr-5"></i> <b>Master Data - Supplier</b>
                </div>
              </div>
              <svg id="Layer_35" enable-background="new 0 0 64 64" height="140px" viewBox="0 0 54 64" width="350px" xmlns="http://www.w3.org/2000/svg"><path d="m47.094 15.943c.628-.461 1.305-.859 2.033-1.165l-6.158-3.387c.011.203.031.404.031.609 0 .551-.054 1.089-.132 1.619z"/><path d="m21.132 13.618c-.078-.529-.132-1.067-.132-1.618 0-.205.02-.405.031-.608l-6.157 3.386c.727.306 1.404.703 2.032 1.164z"/><path d="m44 34c-1.105 0-2 .895-2 2v-2c0-1.105-.895-2-2-2s-2 .895-2 2c0-1.105-.895-2-2-2s-2 .895-2 2v-12c0-1.105-.895-2-2-2s-2 .895-2 2v19.014c0 .621-.145 1.233-.422 1.789l-.578 1.155h-1s-.587-2.934-1.125-5.623c-.509-2.545-2.744-4.377-5.34-4.377-.848 0-1.535.687-1.535 1.534v.001c0 .303.09.599.258.852l1.029 1.544c.469.703.768 1.507.873 2.345l.692 5.534c.098.786.351 1.545.744 2.233l3.404 6.999v3h-2v4h20v-4h-2v-3l1.8-2.4c.779-1.039 1.2-2.302 1.2-3.6v-13c0-1.105-.895-2-2-2z"/><path d="m6.208 30.391c1.337 1.005 2.993 1.609 4.792 1.609s3.455-.604 4.792-1.609c-1.125-1.492-2.895-2.391-4.792-2.391s-3.667.899-4.792 2.391z"/><circle cx="11" cy="23" r="3"/><path d="m11 16c-4.411 0-8 3.589-8 8 0 1.893.664 3.633 1.768 5.004.792-.986 1.796-1.763 2.931-2.278-1.034-.917-1.699-2.239-1.699-3.726 0-2.757 2.243-5 5-5s5 2.243 5 5c0 1.487-.665 2.809-1.699 3.726 1.135.516 2.139 1.292 2.931 2.278 1.104-1.371 1.768-3.111 1.768-5.004 0-4.411-3.589-8-8-8z"/><circle cx="53" cy="23" r="3"/><path d="m57.792 30.391c-1.125-1.492-2.895-2.391-4.792-2.391s-3.667.899-4.792 2.391c1.337 1.005 2.993 1.609 4.792 1.609s3.455-.604 4.792-1.609z"/><path d="m48 23c0-2.757 2.243-5 5-5s5 2.243 5 5c0 1.487-.665 2.809-1.699 3.726 1.135.516 2.139 1.292 2.931 2.278 1.104-1.371 1.768-3.111 1.768-5.004 0-4.411-3.589-8-8-8s-8 3.589-8 8c0 1.893.664 3.633 1.768 5.004.792-.986 1.796-1.763 2.931-2.278-1.034-.917-1.699-2.239-1.699-3.726z"/><path d="m9 49.184v-13.184h-2v13.184c-1.161.414-2 1.514-2 2.816 0 1.654 1.346 3 3 3s3-1.346 3-3c0-1.302-.839-2.402-2-2.816z"/><path d="m14 36c-1.654 0-3 1.346-3 3 0 1.302.839 2.402 2 2.816v13.184h2v-13.184c1.161-.414 2-1.514 2-2.816 0-1.654-1.346-3-3-3z"/><path d="m53 49.184v-13.184h-2v13.184c-1.161.414-2 1.514-2 2.816 0 1.654 1.346 3 3 3s3-1.346 3-3c0-1.302-.839-2.402-2-2.816z"/><path d="m58 36c-1.654 0-3 1.346-3 3 0 1.302.839 2.402 2 2.816v13.184h2v-13.184c1.161-.414 2-1.514 2-2.816 0-1.654-1.346-3-3-3z"/><path d="m28.686 13.715c-1.026-.916-1.686-2.234-1.686-3.715 0-2.757 2.243-5 5-5s5 2.243 5 5c0 1.481-.66 2.799-1.686 3.715 1.658.752 3.021 2.059 3.845 3.718 1.15-1.512 1.841-3.391 1.841-5.433 0-4.963-4.037-9-9-9s-9 4.037-9 9c0 2.042.691 3.921 1.841 5.433.824-1.659 2.187-2.966 3.845-3.718z"/><path d="m26.362 19.005c.623.503 1.317.92 2.06 1.244.651-1.326 2.003-2.249 3.578-2.249s2.927.923 3.579 2.249c.743-.325 1.436-.741 2.06-1.244-.839-2.38-3.067-4.005-5.639-4.005s-4.801 1.625-5.638 4.005z"/><circle cx="32" cy="10" r="3"/></svg>
            </div>
          </div>
        </a>
      </div> 
      <?php } ?> -->
       <?php if(check_permission_view(ID_GROUP,'read','requestor')) { ?>
      <div class="page-content" style="padding: 0px 0px;width: 100%">
        <div class="panel"><br>
          <div class="panel-body">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3 class="panel-title">Let's get started PURCHASING SYSTEM</h3>
                </div>
                <br>
                <div class="panel-body">
                  Welcome Back, <?=USER_NAME;?> :) <br>
                   <a href="<?=base_url('backend/purchase/list_my_pr')?>"><b><u>Click Here</b></u><span class="sr-only">(current)</span></a> To Request PR
                </div>
              </div>
          </div>
        </div>
      </div>
      <?php } ?>


      <?php if(check_permission_view(ID_GROUP,'read','approval')) { ?>
        <div class="col-xl-2 col-md-3">
          <a href="<?php echo base_url('backend/employee') ?>">
            <div class="card card-shadow menu" id="widgetLineareaOne">
              <div class="card-block p-20 pt-10">
                <h5 class="card-title"><b>Total All Request PR</b></h5>
                <div class="clearfix">
                  <div class="grey-800 float-center py-10">
                   <!-- <p>0</p> -->
                   <label style="font-size: 30px;">&emsp;0</label>
                 </div>
               </div>

             </div>
           </div>
         </a>
       </div>



       <div class="col-xl-2 col-md-3">
          <a href="<?php echo base_url('backend/approval') ?>">
            <div class="card card-shadow menu" id="widgetLineareaOne">
              <div class="card-block p-20 pt-10">
                <h5 class="card-title"><b>Total Request PR Today</b></h5>
                <div class="clearfix">
                  <div class="grey-800 float-center py-10">
                   <!-- <p>0</p> -->
                   <label style="font-size: 30px;">&emsp;0</label>
                 </div>
               </div>

             </div>
           </div>
         </a>
       </div>

       <div class="col-xl-2 col-md-3">
          <a href="<?php echo base_url('backend/approval') ?>">
            <div class="card card-shadow menu" id="widgetLineareaOne">
              <div class="card-block p-20 pt-10">
                <h5 class="card-title"><b>Total Request PR Done Approve</b></h5>
                <div class="clearfix">
                  <div class="grey-800 float-center py-10">
                   <!-- <p>0</p> -->
                   <label style="font-size: 30px;">&emsp;0</label>
                 </div>
               </div>

             </div>
           </div>
         </a>
       </div>


        <div class="col-xl-2 col-md-3">
          <a href="<?php echo base_url('backend/approval') ?>">
            <div class="card card-shadow menu" id="widgetLineareaOne">
              <div class="card-block p-20 pt-10">
                <h5 class="card-title"><b>Total Supplier</b></h5>
                <div class="clearfix">
                  <div class="grey-800 float-center py-10">
                   <!-- <p>0</p> -->
                   <label style="font-size: 30px;">&emsp;0</label>
                 </div>
               </div>

             </div>
           </div>
         </a>
       </div>


        <div class="col-xl-2 col-md-3">
          <a href="<?php echo base_url('backend/approval') ?>">
            <div class="card card-shadow menu" id="widgetLineareaOne">
              <div class="card-block p-20 pt-10">
                <h5 class="card-title"><b>Total Category Request</b></h5>
                <div class="clearfix">
                  <div class="grey-800 float-center py-10">
                   <!-- <p>0</p> -->
                   <label style="font-size: 30px;">&emsp;0</label>
                 </div>
               </div>

             </div>
           </div>
         </a>
       </div>

        <div class="col-xl-2 col-md-3">
          <a href="<?php echo base_url('backend/approval') ?>">
            <div class="card card-shadow menu" id="widgetLineareaOne">
              <div class="card-block p-20 pt-10">
                <h5 class="card-title"><b>Total Cost Center</b></h5>
                <div class="clearfix">
                  <div class="grey-800 float-center py-10">
                   <!-- <p>0</p> -->
                   <label style="font-size: 30px;">&emsp;0</label>
                 </div>
               </div>

             </div>
           </div>
         </a>
       </div>


<?php } ?>


<!--     <?php if(check_permission_view(ID_GROUP,'read','supplier')) { ?>
      <div class="col-xl-4 col-md-6">
        <a href="<?php echo base_url('backend/purchase/list_data_item') ?>">
          <div class="card card-shadow menu" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-assignment-account grey-600 font-size-24 vertical-align-bottom mr-5"></i> <b>Master Data - Item</b>
                </div>
              </div>
              <svg id="Layer_35" enable-background="new 0 0 64 64" height="140px" viewBox="0 0 24 24" width="350px" xmlns="http://www.w3.org/2000/svg"><path d="M18 12.08V10H20V12.08C19.67 12.03 19.34 12 19 12C18.66 12 18.33 12.03 18 12.08M9.5 11C9.22 11 9 11.22 9 11.5V13H15V11.5C15 11.22 14.78 11 14.5 11H9.5M6 19V10H4V21H12.3C12.11 20.37 12 19.7 12 19H6M21 9H3V3H21V9M19 5H5V7H19V5M23.8 20.4C23.9 20.4 23.9 20.5 23.8 20.6L22.8 22.3C22.7 22.4 22.6 22.4 22.5 22.4L21.3 22C21 22.2 20.8 22.3 20.5 22.5L20.3 23.8C20.3 23.9 20.2 24 20.1 24H18.1C18 24 17.9 23.9 17.8 23.8L17.6 22.5C17.3 22.4 17 22.2 16.8 22L15.6 22.5C15.5 22.5 15.4 22.5 15.3 22.4L14.3 20.7C14.2 20.6 14.3 20.5 14.4 20.4L15.5 19.6V18.6L14.4 17.8C14.3 17.7 14.3 17.6 14.3 17.5L15.3 15.8C15.4 15.7 15.5 15.7 15.6 15.7L16.8 16.2C17.1 16 17.3 15.9 17.6 15.7L17.8 14.4C17.8 14.3 17.9 14.2 18.1 14.2H20.1C20.2 14.2 20.3 14.3 20.3 14.4L20.5 15.7C20.8 15.8 21.1 16 21.4 16.2L22.6 15.7C22.7 15.7 22.9 15.7 22.9 15.8L23.9 17.5C24 17.6 23.9 17.7 23.8 17.8L22.7 18.6V19.6L23.8 20.4M20.5 19C20.5 18.2 19.8 17.5 19 17.5S17.5 18.2 17.5 19 18.2 20.5 19 20.5 20.5 19.8 20.5 19Z"/></svg>
            </div>
          </div>
        </a>
      </div> 
      <?php } ?> -->

<!--      <div class="col-xl-4 col-md-6">
        <a href="<?php echo base_url('backend/currency/menu_currency') ?>">
          <div class="card card-shadow menu" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon zmdi zmdi-money grey-600 font-size-24 vertical-align-bottom mr-5"></i> <b>Currency</b>
                </div>
              </div>

                         <svg height="116pt" width="300pt" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M3,6H21V18H3V6M12,9A3,3 0 0,1 15,12A3,3 0 0,1 12,15A3,3 0 0,1 9,12A3,3 0 0,1 12,9M7,8A2,2 0 0,1 5,10V14A2,2 0 0,1 7,16H17A2,2 0 0,1 19,14V10A2,2 0 0,1 17,8H7Z" />
                         </svg>
          </div>
          </div>
        </a>
      </div>  -->



      <!-- <?php if(check_permission_view(ID_GROUP,'read','mastercost')) { ?>
      <div class="col-xl-4 col-md-6">
        <a href="<?php echo base_url('backend/purchase/list_cost_center') ?>">
          <div class="card card-shadow menu" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-assignment-account grey-600 font-size-24 vertical-align-bottom mr-5"></i> <b>Master Data - Cost Center</b>
                </div>
              </div>
              <svg height="116pt" viewBox="0 0 456 596" width="300pt" xmlns="http://www.w3.org/2000/svg"><path d="m496 298.113281c0-24.808593-16.648438-46.921875-40.480469-53.808593l-63.519531-18.320313v-16.105469c8.457031-6.789062 15.144531-15.679687 19.289062-25.878906h4.710938c17.648438 0 32-14.351562 32-32v-56c0-52.9375-43.0625-96-96-96-35.441406 0-66.382812 19.351562-83.007812 48h-268.992188v448h384v-160h112zm-91.390625-51.839843-15.824219 36.917968-24.816406-19.847656 22.34375-22.34375zm-28.609375-17.585938-24 24-24-24v-9.398438c7.414062 3.015626 15.511719 4.710938 24 4.710938s16.585938-1.695312 24-4.710938zm-58.320312 12.304688 22.34375 22.34375-24.816407 19.847656-15.824219-36.917969zm82.320312-80.992188c0 26.472656-21.527344 48-48 48s-48-21.527344-48-48v-28.472656c0-10.765625 8.761719-19.527344 19.527344-19.527344 2.105468 0 4.175781.335938 6.175781 1l22.296875 7.433594 22.296875-7.433594c1.992187-.664062 4.0625-1 6.175781-1 10.765625 0 19.527344 8.761719 19.527344 19.527344zm16 8h-.550781c.328125-2.625.550781-5.289062.550781-8v-24c8.824219 0 16 7.175781 16 16s-7.175781 16-16 16zm-64-152c44.113281 0 80 35.886719 80 80v28.449219c-4.726562-2.753907-10.144531-4.449219-16-4.449219h-2.089844c-4.8125-13.921875-17.910156-24-33.445312-24-3.832032 0-7.609375.617188-11.242188 1.832031l-17.222656 5.734375-17.230469-5.742187c-3.625-1.207031-7.410156-1.824219-11.242187-1.824219-15.535156 0-28.632813 10.078125-33.449219 24h-2.078125c-5.855469 0-11.273438 1.695312-16 4.449219v-28.449219c0-44.113281 35.886719-80 80-80zm-63.449219 152h-.550781c-8.824219 0-16-7.175781-16-16s7.175781-16 16-16v24c0 2.710938.222656 5.375.550781 8zm79.449219 312h-352v-416h245.601562c-3.5625 10.023438-5.601562 20.769531-5.601562 32v56c0 17.648438 14.351562 32 32 32h4.710938c4.144531 10.199219 10.832031 19.089844 19.289062 25.878906v16.105469l-63.519531 18.320313c-23.832031 6.878906-40.480469 29-40.480469 53.808593v37.886719h160zm112-160h-32v-40h-16v40h-160v-40h-16v40h-32v-21.886719c0-17.714843 11.886719-33.511719 28.910156-38.433593l30.984375-8.945313 24.890625 58.074219 43.214844-34.5625 43.207031 34.570312 24.890625-58.074218 30.980469 8.945312c17.035156 4.90625 28.921875 20.710938 28.921875 38.425781zm0 0"/><path d="m192 464h160v-112h-160zm16-96h128v80h-128zm0 0"/><path d="m32 352h16v16h-16zm0 0"/><path d="m64 352h112v16h-112zm0 0"/><path d="m32 384h144v16h-144zm0 0"/><path d="m32 416h144v16h-144zm0 0"/><path d="m32 448h112v16h-112zm0 0"/><path d="m240 80h-208v128h208zm-16 112h-176v-96h176zm0 0"/><path d="m32 224h208v16h-208zm0 0"/><path d="m32 256h160v16h-160zm0 0"/><path d="m32 288h160v16h-160zm0 0"/></svg>
            </div>
          </div>
        </a>
      </div> 
      <?php } ?>

      <?php if(check_permission_view(ID_GROUP,'read','mastercategory')) { ?>
      <div class="col-xl-4 col-md-6">
        <a href="<?php echo base_url('backend/purchase/list_category_request') ?>">
          <div class="card card-shadow menu" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-assignment-account grey-600 font-size-24 vertical-align-bottom mr-5"></i> <b>Master Data - Category Request</b>
                </div>
              </div>
              <svg height="116pt" viewBox="0 0 456 596" width="300pt" xmlns="http://www.w3.org/2000/svg"><path d="m496 298.113281c0-24.808593-16.648438-46.921875-40.480469-53.808593l-63.519531-18.320313v-16.105469c8.457031-6.789062 15.144531-15.679687 19.289062-25.878906h4.710938c17.648438 0 32-14.351562 32-32v-56c0-52.9375-43.0625-96-96-96-35.441406 0-66.382812 19.351562-83.007812 48h-268.992188v448h384v-160h112zm-91.390625-51.839843-15.824219 36.917968-24.816406-19.847656 22.34375-22.34375zm-28.609375-17.585938-24 24-24-24v-9.398438c7.414062 3.015626 15.511719 4.710938 24 4.710938s16.585938-1.695312 24-4.710938zm-58.320312 12.304688 22.34375 22.34375-24.816407 19.847656-15.824219-36.917969zm82.320312-80.992188c0 26.472656-21.527344 48-48 48s-48-21.527344-48-48v-28.472656c0-10.765625 8.761719-19.527344 19.527344-19.527344 2.105468 0 4.175781.335938 6.175781 1l22.296875 7.433594 22.296875-7.433594c1.992187-.664062 4.0625-1 6.175781-1 10.765625 0 19.527344 8.761719 19.527344 19.527344zm16 8h-.550781c.328125-2.625.550781-5.289062.550781-8v-24c8.824219 0 16 7.175781 16 16s-7.175781 16-16 16zm-64-152c44.113281 0 80 35.886719 80 80v28.449219c-4.726562-2.753907-10.144531-4.449219-16-4.449219h-2.089844c-4.8125-13.921875-17.910156-24-33.445312-24-3.832032 0-7.609375.617188-11.242188 1.832031l-17.222656 5.734375-17.230469-5.742187c-3.625-1.207031-7.410156-1.824219-11.242187-1.824219-15.535156 0-28.632813 10.078125-33.449219 24h-2.078125c-5.855469 0-11.273438 1.695312-16 4.449219v-28.449219c0-44.113281 35.886719-80 80-80zm-63.449219 152h-.550781c-8.824219 0-16-7.175781-16-16s7.175781-16 16-16v24c0 2.710938.222656 5.375.550781 8zm79.449219 312h-352v-416h245.601562c-3.5625 10.023438-5.601562 20.769531-5.601562 32v56c0 17.648438 14.351562 32 32 32h4.710938c4.144531 10.199219 10.832031 19.089844 19.289062 25.878906v16.105469l-63.519531 18.320313c-23.832031 6.878906-40.480469 29-40.480469 53.808593v37.886719h160zm112-160h-32v-40h-16v40h-160v-40h-16v40h-32v-21.886719c0-17.714843 11.886719-33.511719 28.910156-38.433593l30.984375-8.945313 24.890625 58.074219 43.214844-34.5625 43.207031 34.570312 24.890625-58.074218 30.980469 8.945312c17.035156 4.90625 28.921875 20.710938 28.921875 38.425781zm0 0"/><path d="m192 464h160v-112h-160zm16-96h128v80h-128zm0 0"/><path d="m32 352h16v16h-16zm0 0"/><path d="m64 352h112v16h-112zm0 0"/><path d="m32 384h144v16h-144zm0 0"/><path d="m32 416h144v16h-144zm0 0"/><path d="m32 448h112v16h-112zm0 0"/><path d="m240 80h-208v128h208zm-16 112h-176v-96h176zm0 0"/><path d="m32 224h208v16h-208zm0 0"/><path d="m32 256h160v16h-160zm0 0"/><path d="m32 288h160v16h-160zm0 0"/></svg>
            </div>
          </div>
        </a>
      </div> 
      <?php } ?>

      <?php if(check_permission_view(ID_GROUP,'read','#approvalhod')) { ?>
      <div class="col-xl-4 col-md-6">
        <a href="<?php echo base_url('backend/purchase/list_approval_hod') ?>">
          <div class="card card-shadow menu" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-assignment-account grey-600 font-size-24 vertical-align-bottom mr-5"></i> <b>Approval Request PR - (HOD)</b>
                </div>
    
              </div>
             
              <svg id="Layer_35" enable-background="new 0 0 64 64" height="140px" viewBox="0 0 54 64" width="350px" xmlns="http://www.w3.org/2000/svg"><path d="m47.094 15.943c.628-.461 1.305-.859 2.033-1.165l-6.158-3.387c.011.203.031.404.031.609 0 .551-.054 1.089-.132 1.619z"/><path d="m21.132 13.618c-.078-.529-.132-1.067-.132-1.618 0-.205.02-.405.031-.608l-6.157 3.386c.727.306 1.404.703 2.032 1.164z"/><path d="m44 34c-1.105 0-2 .895-2 2v-2c0-1.105-.895-2-2-2s-2 .895-2 2c0-1.105-.895-2-2-2s-2 .895-2 2v-12c0-1.105-.895-2-2-2s-2 .895-2 2v19.014c0 .621-.145 1.233-.422 1.789l-.578 1.155h-1s-.587-2.934-1.125-5.623c-.509-2.545-2.744-4.377-5.34-4.377-.848 0-1.535.687-1.535 1.534v.001c0 .303.09.599.258.852l1.029 1.544c.469.703.768 1.507.873 2.345l.692 5.534c.098.786.351 1.545.744 2.233l3.404 6.999v3h-2v4h20v-4h-2v-3l1.8-2.4c.779-1.039 1.2-2.302 1.2-3.6v-13c0-1.105-.895-2-2-2z"/><path d="m6.208 30.391c1.337 1.005 2.993 1.609 4.792 1.609s3.455-.604 4.792-1.609c-1.125-1.492-2.895-2.391-4.792-2.391s-3.667.899-4.792 2.391z"/><circle cx="11" cy="23" r="3"/><path d="m11 16c-4.411 0-8 3.589-8 8 0 1.893.664 3.633 1.768 5.004.792-.986 1.796-1.763 2.931-2.278-1.034-.917-1.699-2.239-1.699-3.726 0-2.757 2.243-5 5-5s5 2.243 5 5c0 1.487-.665 2.809-1.699 3.726 1.135.516 2.139 1.292 2.931 2.278 1.104-1.371 1.768-3.111 1.768-5.004 0-4.411-3.589-8-8-8z"/><circle cx="53" cy="23" r="3"/><path d="m57.792 30.391c-1.125-1.492-2.895-2.391-4.792-2.391s-3.667.899-4.792 2.391c1.337 1.005 2.993 1.609 4.792 1.609s3.455-.604 4.792-1.609z"/><path d="m48 23c0-2.757 2.243-5 5-5s5 2.243 5 5c0 1.487-.665 2.809-1.699 3.726 1.135.516 2.139 1.292 2.931 2.278 1.104-1.371 1.768-3.111 1.768-5.004 0-4.411-3.589-8-8-8s-8 3.589-8 8c0 1.893.664 3.633 1.768 5.004.792-.986 1.796-1.763 2.931-2.278-1.034-.917-1.699-2.239-1.699-3.726z"/><path d="m9 49.184v-13.184h-2v13.184c-1.161.414-2 1.514-2 2.816 0 1.654 1.346 3 3 3s3-1.346 3-3c0-1.302-.839-2.402-2-2.816z"/><path d="m14 36c-1.654 0-3 1.346-3 3 0 1.302.839 2.402 2 2.816v13.184h2v-13.184c1.161-.414 2-1.514 2-2.816 0-1.654-1.346-3-3-3z"/><path d="m53 49.184v-13.184h-2v13.184c-1.161.414-2 1.514-2 2.816 0 1.654 1.346 3 3 3s3-1.346 3-3c0-1.302-.839-2.402-2-2.816z"/><path d="m58 36c-1.654 0-3 1.346-3 3 0 1.302.839 2.402 2 2.816v13.184h2v-13.184c1.161-.414 2-1.514 2-2.816 0-1.654-1.346-3-3-3z"/><path d="m28.686 13.715c-1.026-.916-1.686-2.234-1.686-3.715 0-2.757 2.243-5 5-5s5 2.243 5 5c0 1.481-.66 2.799-1.686 3.715 1.658.752 3.021 2.059 3.845 3.718 1.15-1.512 1.841-3.391 1.841-5.433 0-4.963-4.037-9-9-9s-9 4.037-9 9c0 2.042.691 3.921 1.841 5.433.824-1.659 2.187-2.966 3.845-3.718z"/><path d="m26.362 19.005c.623.503 1.317.92 2.06 1.244.651-1.326 2.003-2.249 3.578-2.249s2.927.923 3.579 2.249c.743-.325 1.436-.741 2.06-1.244-.839-2.38-3.067-4.005-5.639-4.005s-4.801 1.625-5.638 4.005z"/><circle cx="32" cy="10" r="3"/></svg>
            </div>
          </div>
        </a>
      </div> 
       <?php } ?>

        <?php if(check_permission_view(ID_GROUP,'read','#approvalwere')) { ?>
      <div class="col-xl-4 col-md-6">
        <a href="<?php echo base_url('backend/purchase/list_approval_werehouse') ?>">
          <div class="card card-shadow menu" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-assignment-account grey-600 font-size-24 vertical-align-bottom mr-5"></i> <b>Approval Request PR - (WEREHOUSE)</b>
                </div>
               
              </div>
              
              <svg id="Layer_35" enable-background="new 0 0 64 64" height="140px" viewBox="0 0 54 64" width="350px" xmlns="http://www.w3.org/2000/svg"><path d="m47.094 15.943c.628-.461 1.305-.859 2.033-1.165l-6.158-3.387c.011.203.031.404.031.609 0 .551-.054 1.089-.132 1.619z"/><path d="m21.132 13.618c-.078-.529-.132-1.067-.132-1.618 0-.205.02-.405.031-.608l-6.157 3.386c.727.306 1.404.703 2.032 1.164z"/><path d="m44 34c-1.105 0-2 .895-2 2v-2c0-1.105-.895-2-2-2s-2 .895-2 2c0-1.105-.895-2-2-2s-2 .895-2 2v-12c0-1.105-.895-2-2-2s-2 .895-2 2v19.014c0 .621-.145 1.233-.422 1.789l-.578 1.155h-1s-.587-2.934-1.125-5.623c-.509-2.545-2.744-4.377-5.34-4.377-.848 0-1.535.687-1.535 1.534v.001c0 .303.09.599.258.852l1.029 1.544c.469.703.768 1.507.873 2.345l.692 5.534c.098.786.351 1.545.744 2.233l3.404 6.999v3h-2v4h20v-4h-2v-3l1.8-2.4c.779-1.039 1.2-2.302 1.2-3.6v-13c0-1.105-.895-2-2-2z"/><path d="m6.208 30.391c1.337 1.005 2.993 1.609 4.792 1.609s3.455-.604 4.792-1.609c-1.125-1.492-2.895-2.391-4.792-2.391s-3.667.899-4.792 2.391z"/><circle cx="11" cy="23" r="3"/><path d="m11 16c-4.411 0-8 3.589-8 8 0 1.893.664 3.633 1.768 5.004.792-.986 1.796-1.763 2.931-2.278-1.034-.917-1.699-2.239-1.699-3.726 0-2.757 2.243-5 5-5s5 2.243 5 5c0 1.487-.665 2.809-1.699 3.726 1.135.516 2.139 1.292 2.931 2.278 1.104-1.371 1.768-3.111 1.768-5.004 0-4.411-3.589-8-8-8z"/><circle cx="53" cy="23" r="3"/><path d="m57.792 30.391c-1.125-1.492-2.895-2.391-4.792-2.391s-3.667.899-4.792 2.391c1.337 1.005 2.993 1.609 4.792 1.609s3.455-.604 4.792-1.609z"/><path d="m48 23c0-2.757 2.243-5 5-5s5 2.243 5 5c0 1.487-.665 2.809-1.699 3.726 1.135.516 2.139 1.292 2.931 2.278 1.104-1.371 1.768-3.111 1.768-5.004 0-4.411-3.589-8-8-8s-8 3.589-8 8c0 1.893.664 3.633 1.768 5.004.792-.986 1.796-1.763 2.931-2.278-1.034-.917-1.699-2.239-1.699-3.726z"/><path d="m9 49.184v-13.184h-2v13.184c-1.161.414-2 1.514-2 2.816 0 1.654 1.346 3 3 3s3-1.346 3-3c0-1.302-.839-2.402-2-2.816z"/><path d="m14 36c-1.654 0-3 1.346-3 3 0 1.302.839 2.402 2 2.816v13.184h2v-13.184c1.161-.414 2-1.514 2-2.816 0-1.654-1.346-3-3-3z"/><path d="m53 49.184v-13.184h-2v13.184c-1.161.414-2 1.514-2 2.816 0 1.654 1.346 3 3 3s3-1.346 3-3c0-1.302-.839-2.402-2-2.816z"/><path d="m58 36c-1.654 0-3 1.346-3 3 0 1.302.839 2.402 2 2.816v13.184h2v-13.184c1.161-.414 2-1.514 2-2.816 0-1.654-1.346-3-3-3z"/><path d="m28.686 13.715c-1.026-.916-1.686-2.234-1.686-3.715 0-2.757 2.243-5 5-5s5 2.243 5 5c0 1.481-.66 2.799-1.686 3.715 1.658.752 3.021 2.059 3.845 3.718 1.15-1.512 1.841-3.391 1.841-5.433 0-4.963-4.037-9-9-9s-9 4.037-9 9c0 2.042.691 3.921 1.841 5.433.824-1.659 2.187-2.966 3.845-3.718z"/><path d="m26.362 19.005c.623.503 1.317.92 2.06 1.244.651-1.326 2.003-2.249 3.578-2.249s2.927.923 3.579 2.249c.743-.325 1.436-.741 2.06-1.244-.839-2.38-3.067-4.005-5.639-4.005s-4.801 1.625-5.638 4.005z"/><circle cx="32" cy="10" r="3"/></svg>
            </div>
          </div>
        </a>
       
      </div> 
       <?php } ?>

        <?php if(check_permission_view(ID_GROUP,'read','#approvalpur')) { ?>
      <div class="col-xl-4 col-md-6">
        <a href="<?php echo base_url('backend/purchase/list_apporval_purchasing') ?>">
          <div class="card card-shadow menu" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-assignment-account grey-600 font-size-24 vertical-align-bottom mr-5"></i> <b>Approval Request PR - (PURCHASING)</b>
                </div>
              </div>
              <svg id="Layer_35" enable-background="new 0 0 64 64" height="140px" viewBox="0 0 24 24" width="350px" xmlns="http://www.w3.org/2000/svg"><path d="M14 12.8C13.5 12.31 12.78 12 12 12C10.34 12 9 13.34 9 15C9 16.31 9.84 17.41 11 17.82C11.07 15.67 12.27 13.8 14 12.8M11.09 19H5V5H16.17L19 7.83V12.35C19.75 12.61 20.42 13 21 13.54V7L17 3H5C3.89 3 3 3.9 3 5V19C3 20.1 3.89 21 5 21H11.81C11.46 20.39 11.21 19.72 11.09 19M6 10H15V6H6V10M15.75 21L13 18L14.16 16.84L15.75 18.43L19.34 14.84L20.5 16.25L15.75 21"/></svg>
            </div>
          </div>
        </a>
      </div> 
       <?php } ?>

      <?php if(check_permission_view(ID_GROUP,'read','#approvalfin')) { ?>
      <div class="col-xl-4 col-md-6">
        <a href="<?php echo base_url('backend/purchase/list_approval_finance') ?>">
          <div class="card card-shadow menu" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-assignment-account grey-600 font-size-24 vertical-align-bottom mr-5"></i> <b>Approval Request PR - (FINANCE)</b>
                </div>
              </div>
         <svg id="Layer_35" enable-background="new 0 0 64 64" height="140px" viewBox="0 0 24 24" width="350px" xmlns="http://www.w3.org/2000/svg"><path d="M14 12.8C13.5 12.31 12.78 12 12 12C10.34 12 9 13.34 9 15C9 16.31 9.84 17.41 11 17.82C11.07 15.67 12.27 13.8 14 12.8M11.09 19H5V5H16.17L19 7.83V12.35C19.75 12.61 20.42 13 21 13.54V7L17 3H5C3.89 3 3 3.9 3 5V19C3 20.1 3.89 21 5 21H11.81C11.46 20.39 11.21 19.72 11.09 19M6 10H15V6H6V10M15.75 21L13 18L14.16 16.84L15.75 18.43L19.34 14.84L20.5 16.25L15.75 21"/></svg>
            </div>
          </div>
        </a>
      </div> 
       <?php } ?>

        <?php if(check_permission_view(ID_GROUP,'read','#approvaldir')) { ?>
      <div class="col-xl-4 col-md-6">
        <a href="<?php echo base_url('backend/purchase/list_approval_director') ?>">
          <div class="card card-shadow menu" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-assignment-account grey-600 font-size-24 vertical-align-bottom mr-5"></i> <b>Approval Request PR - (HEAD DIVISION)</b>
                </div>
              </div>
             <svg id="Layer_35" enable-background="new 0 0 64 64" height="140px" viewBox="0 0 24 24" width="350px" xmlns="http://www.w3.org/2000/svg"><path d="M14 12.8C13.5 12.31 12.78 12 12 12C10.34 12 9 13.34 9 15C9 16.31 9.84 17.41 11 17.82C11.07 15.67 12.27 13.8 14 12.8M11.09 19H5V5H16.17L19 7.83V12.35C19.75 12.61 20.42 13 21 13.54V7L17 3H5C3.89 3 3 3.9 3 5V19C3 20.1 3.89 21 5 21H11.81C11.46 20.39 11.21 19.72 11.09 19M6 10H15V6H6V10M15.75 21L13 18L14.16 16.84L15.75 18.43L19.34 14.84L20.5 16.25L15.75 21"/></svg>
            </div>
          </div>
        </a>
      </div> 
       <?php } ?>

       <?php if(check_permission_view(ID_GROUP,'read','#approvalpd')) { ?>
      <div class="col-xl-4 col-md-6">
        <a href="<?php echo base_url('backend/purchase/list_approval_pd') ?>">
          <div class="card card-shadow menu" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-assignment-account grey-600 font-size-24 vertical-align-bottom mr-5"></i> <b>Approval Request PR - (PRESIDENT DIRECTOR)</b>
                </div>
              </div>
              <svg id="Layer_35" enable-background="new 0 0 64 64" height="140px" viewBox="0 0 24 24" width="350px" xmlns="http://www.w3.org/2000/svg"><path d="M14 12.8C13.5 12.31 12.78 12 12 12C10.34 12 9 13.34 9 15C9 16.31 9.84 17.41 11 17.82C11.07 15.67 12.27 13.8 14 12.8M11.09 19H5V5H16.17L19 7.83V12.35C19.75 12.61 20.42 13 21 13.54V7L17 3H5C3.89 3 3 3.9 3 5V19C3 20.1 3.89 21 5 21H11.81C11.46 20.39 11.21 19.72 11.09 19M6 10H15V6H6V10M15.75 21L13 18L14.16 16.84L15.75 18.43L19.34 14.84L20.5 16.25L15.75 21"/></svg>
            </div>
          </div>
        </a>
      </div> 
       <?php } ?>


      <?php if(check_permission_view(ID_GROUP,'read','masterpr')) { ?>
      <div class="col-xl-4 col-md-6">
        <a href="<?php echo base_url('backend/purchase/list_data_all_pr');?>">
          <div class="card card-shadow menu" id="widgetLineareaTwo">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-widgets grey-600 font-size-24 vertical-align-bottom mr-5"></i><b>Master Data Purchase Requisition</b>
                </div>
              </div>
                <svg id="Layer_35" enable-background="new 0 0 64 64" height="140px" viewBox="0 0 24 24" width="350px" xmlns="http://www.w3.org/2000/svg"><path d="M15 23C13.9 23 13 22.11 13 21V12C13 10.9 13.9 10 15 10H19L23 14V21C23 22.11 22.11 23 21 23H15M15 21H21V14.83L18.17 12H15V21M19 3C20.1 3 21 3.9 21 5V9.17L19.83 8H19V5H17V7H7V5H5V19H11V21H5C3.9 21 3 20.1 3 19V5C3 3.9 3.9 3 5 3H9.18C9.6 1.84 10.7 1 12 1C13.3 1 14.4 1.84 14.82 3H19M12 3C11.45 3 11 3.45 11 4C11 4.55 11.45 5 12 5C12.55 5 13 4.55 13 4C13 3.45 12.55 3 12 3Z"/></svg>
            </div>
          </div>
        </a>
      </div>
       <?php } ?>

      <div class="col-xl-4 col-md-6">
        <a href="<?php echo base_url('backend/purchase/list_my_pr');?>">
          <div class="card card-shadow menu" id="widgetLineareaTwo">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-widgets grey-600 font-size-24 vertical-align-bottom mr-5"></i><b>My Purchase Requisition</b>
                </div>
              </div>
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 145 145" width="310px" height="160" viewBox="0 0 300 500" style="enable-background:new 0 0 490 490;" xml:space="preserve">
                  <g>
                  <g id="XMLID_45_">
                  <g>
                    <polygon style="fill:#FFFFFF;" points="430,110 430,480 60,480 60,10 330,10 330,110      "/>
                      <polygon style="fill:#AFB6BB;" points="430,110 330,110 330,10       "/>
                  </g>
                  <g>
                      <path style="fill:#231F20;" d="M439.976,110c-0.001-2.602-0.992-5.159-2.904-7.071l-100-100c-1.912-1.913-4.47-2.904-7.071-2.904
                            V0H60c-5.522,0-10,4.477-10,10v470c0,5.523,4.478,10,10,10h370c5.522,0,10-4.477,10-10V110H439.976z M340,34.142L405.857,100H340
                            V34.142z M70,470V20h250v90c0,5.523,4.478,10,10,10h90v350H70z"/>
                      <path style="fill:#231F20;" d="M125,180.858l-17.929-17.929l-14.143,14.143l25,25C119.882,204.024,122.44,205,125,205
                              s5.118-0.976,7.071-2.929l50-50l-14.143-14.143L125,180.858z"/>
                      <rect x="200" y="150" style="fill:#231F20;" width="50" height="20"/>
                      <rect x="200" y="180" style="fill:#231F20;" width="110" height="20"/>
                      <rect x="265" y="150" style="fill:#231F20;" width="130" height="20"/>
                      <path style="fill:#231F20;" d="M125,260.858l-17.929-17.929l-14.143,14.143l25,25C119.882,284.024,122.44,285,125,285
                              s5.118-0.976,7.071-2.929l50-50l-14.143-14.143L125,260.858z"/>
                      <rect x="200" y="230" style="fill:#231F20;" width="50" height="20"/>
                      <rect x="200" y="260" style="fill:#231F20;" width="110" height="20"/>
                      <rect x="265" y="230" style="fill:#231F20;" width="130" height="20"/>
                      <path style="fill:#231F20;" d="M125,340.858l-17.929-17.929l-14.143,14.143l25,25C119.882,364.024,122.44,365,125,365
                              s5.118-0.976,7.071-2.929l50-50l-14.143-14.143L125,340.858z"/>
                      <rect x="200" y="310" style="fill:#231F20;" width="50" height="20"/>
                      <rect x="200" y="340" style="fill:#231F20;" width="110" height="20"/>
                      <rect x="265" y="310" style="fill:#231F20;" width="130" height="20"/>
                      <path style="fill:#231F20;" d="M125,420.858l-17.929-17.929l-14.143,14.143l25,25C119.882,444.024,122.44,445,125,445
                              s5.118-0.976,7.071-2.929l50-50l-14.143-14.143L125,420.858z"/>
                      <rect x="200" y="390" style="fill:#231F20;" width="50" height="20"/>
                      <rect x="200" y="420" style="fill:#231F20;" width="110" height="20"/>
                      <rect x="265" y="390" style="fill:#231F20;" width="130" height="20"/>
                      <rect x="95" y="45" style="fill:#231F20;" width="50" height="20"/>
                      <rect x="95" y="75" style="fill:#231F20;" width="105" height="20"/>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
          </div>
        </a>
      </div>

      <?php if(check_permission_view(ID_GROUP,'read','releasepo')) { ?>
      <div class="col-xl-4 col-md-6">
        <a href="<?php echo base_url('backend/purchase_order/list_po_release');?>">
          <div class="card card-shadow menu" id="widgetLineareaTwo">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-widgets grey-600 font-size-24 vertical-align-bottom mr-5"></i><b>Release PO</b>
                </div>
              </div>
                <svg id="Layer_35" enable-background="new 0 0 64 64" height="140px" viewBox="0 0 24 24" width="350px" xmlns="http://www.w3.org/2000/svg"><path d="M15 3H5C3.89 3 3 3.89 3 5V10.82C5.55 8.37 9.59 8.4 12.1 10.9C14.63 13.44 14.63 17.56 12.1 20.1C11.74 20.45 11.35 20.74 10.94 21H19C20.11 21 21 20.11 21 19V9L15 3M14 10V4.5L19.5 10H14M7.5 11C5 11 3 13 3 15.5C3 16.38 3.25 17.21 3.69 17.9L.61 21L2 22.39L5.12 19.32C5.81 19.75 6.63 20 7.5 20C10 20 12 18 12 15.5S10 11 7.5 11M7.5 18C6.12 18 5 16.88 5 15.5S6.12 13 7.5 13 10 14.12 10 15.5 8.88 18 7.5 18Z"/></svg>
            </div>
          </div>
        </a>
      </div>
       <?php } ?>


      <?php if(check_permission_view(ID_GROUP,'read','releasepo')) { ?>
      <div class="col-xl-4 col-md-6">
        <a href="<?php echo base_url('backend/purchase_order/save_head_latter');?>">
          <div class="card card-shadow menu" id="widgetLineareaTwo">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-widgets grey-600 font-size-24 vertical-align-bottom mr-5"></i><b>Change Head Latter</b>
                </div>
              </div>
                <svg id="Layer_35" enable-background="new 0 0 64 64" height="140px" viewBox="0 0 24 24" width="350px" xmlns="http://www.w3.org/2000/svg"><path d="M12,8A4,4 0 0,1 16,12A4,4 0 0,1 12,16A4,4 0 0,1 8,12A4,4 0 0,1 12,8M12,10A2,2 0 0,0 10,12A2,2 0 0,0 12,14A2,2 0 0,0 14,12A2,2 0 0,0 12,10M10,22C9.75,22 9.54,21.82 9.5,21.58L9.13,18.93C8.5,18.68 7.96,18.34 7.44,17.94L4.95,18.95C4.73,19.03 4.46,18.95 4.34,18.73L2.34,15.27C2.21,15.05 2.27,14.78 2.46,14.63L4.57,12.97L4.5,12L4.57,11L2.46,9.37C2.27,9.22 2.21,8.95 2.34,8.73L4.34,5.27C4.46,5.05 4.73,4.96 4.95,5.05L7.44,6.05C7.96,5.66 8.5,5.32 9.13,5.07L9.5,2.42C9.54,2.18 9.75,2 10,2H14C14.25,2 14.46,2.18 14.5,2.42L14.87,5.07C15.5,5.32 16.04,5.66 16.56,6.05L19.05,5.05C19.27,4.96 19.54,5.05 19.66,5.27L21.66,8.73C21.79,8.95 21.73,9.22 21.54,9.37L19.43,11L19.5,12L19.43,13L21.54,14.63C21.73,14.78 21.79,15.05 21.66,15.27L19.66,18.73C19.54,18.95 19.27,19.04 19.05,18.95L16.56,17.95C16.04,18.34 15.5,18.68 14.87,18.93L14.5,21.58C14.46,21.82 14.25,22 14,22H10M11.25,4L10.88,6.61C9.68,6.86 8.62,7.5 7.85,8.39L5.44,7.35L4.69,8.65L6.8,10.2C6.4,11.37 6.4,12.64 6.8,13.8L4.68,15.36L5.43,16.66L7.86,15.62C8.63,16.5 9.68,17.14 10.87,17.38L11.24,20H12.76L13.13,17.39C14.32,17.14 15.37,16.5 16.14,15.62L18.57,16.66L19.32,15.36L17.2,13.81C17.6,12.64 17.6,11.37 17.2,10.2L19.31,8.65L18.56,7.35L16.15,8.39C15.38,7.5 14.32,6.86 13.12,6.62L12.75,4H11.25Z"/></svg>
            </div>
          </div>
        </a>
      </div>
       <?php } ?>


       <?php if(check_permission_view(ID_GROUP,'read','#autorization')) { ?>
       <div class="col-xl-4 col-md-6">
          <a href="<?php echo base_url('backend/user');?>">
            <div class="card card-shadow menu" id="widgetLineareaThree">
              <div class="card-block p-20 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts-list-alt grey-600 font-size-24 vertical-align-bottom mr-5"></i>        <b>User Autorization</b>
                  </div>
                </div>
                <svg enable-background="new 0 0 24 24" width="310px" height="170" viewBox="0 0 8 28" width="212" xmlns="http://www.w3.org/2000/svg">
                  <circle cx="9" cy="5" r="5"/><path d="m11.534 20.8c-.521-.902-.417-2.013.203-2.8-.62-.787-.724-1.897-.203-2.8l.809-1.4c.445-.771 1.275-1.25 2.166-1.25.122 0 .242.009.361.026.033-.082.075-.159.116-.237-.54-.213-1.123-.339-1.736-.339h-8.5c-2.619 0-4.75 2.131-4.75 4.75v3.5c0 .414.336.75.75.75h10.899z"/><path d="m21.703 18.469c.02-.155.047-.309.047-.469 0-.161-.028-.314-.047-.469l.901-.682c.201-.152.257-.43.131-.649l-.809-1.4c-.126-.218-.395-.309-.627-.211l-1.037.437c-.253-.193-.522-.363-.819-.487l-.138-1.101c-.032-.25-.244-.438-.496-.438h-1.617c-.252 0-.465.188-.496.438l-.138 1.101c-.297.124-.567.295-.819.487l-1.037-.437c-.232-.098-.501-.008-.627.211l-.809 1.4c-.126.218-.07.496.131.649l.901.682c-.02.155-.047.309-.047.469 0 .161.028.314.047.469l-.901.682c-.201.152-.257.43-.131.649l.809 1.401c.126.218.395.309.627.211l1.037-.438c.253.193.522.363.819.487l.138 1.101c.031.25.243.438.495.438h1.617c.252 0 .465-.188.496-.438l.138-1.101c.297-.124.567-.295.819-.487l1.037.437c.232.098.501.008.627-.211l.809-1.401c.126-.218.07-.496-.131-.649zm-3.703 1.531c-1.105 0-2-.895-2-2s.895-2 2-2 2 .895 2 2-.895 2-2 2z"/>
                </svg>
              </div>
            </div>
          </a>
      </div>
      <?php } ?> -->
     
       
    </div>
  </div>
</div>
<!-- End Page -->

<style>
  .gradient-text {
  /* Fallback: Set a background color. */
  background-color: red;
  /* Create the gradient. */
  background-image: linear-gradient(to right, #360033, #0b8793);
  /* Set the background size and repeat properties. */
  background-size: 73%;
  background-repeat: repeat;
  /* Use the text as a mask for the background. */
  /* This will show the gradient as a text color rather than element bg. */
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  -moz-background-clip: text;
  -moz-text-fill-color: transparent;
  }
  h1 {
  font-family: "Archivo Black", sans-serif;
  font-weight: normal;
  font-size: 2em;
  text-align: center;
  margin-bottom: 0;
  margin-bottom: -0.25em;
  }
  .menu {
  border-radius: 15px;
  }
</style>