<!-- Page -->
<div class="page">
  <div class="page-content container-fluid">
    <ol class="breadcrumb">
      <a href="<?php echo base_url('backend/admdashboard'); ?>" type="button" class="btn btn-round btn-info"><i class="icon md-home" aria-hidden="true"></i>Menu Dashboard</a>
        &nbsp;&nbsp;
    </ol>
    <header>
      <h1 class="gradient-text"><b>MENU CURRENCY</b></h1><br>
    </header>
    <div class="row" data-plugin="matchHeight" data-by-row="true">
      

   <!--    <?php if(check_permission_view(ID_GROUP,'read','mastercost')) { ?> -->
      <div class="col-xl-4 col-md-6">
        <!-- Widget Linearea One-->
        <a href="<?php echo base_url('backend/currency/list_master_data_currency') ?>">
          <div class="card card-shadow menu" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-assignment-account grey-600 font-size-24 vertical-align-bottom mr-5"></i> <b>Master Data - Currency</b>
                </div>
              </div>
              <svg height="116pt" viewBox="0 0 24 24" width="300pt" xmlns="http://www.w3.org/2000/svg"><path d="M13.09 20C13.21 20.72 13.46 21.39 13.81 22H6C4.89 22 4 21.11 4 20V4C4 2.9 4.89 2 6 2H14L20 8V13.09C19.67 13.04 19.34 13 19 13C18.66 13 18.33 13.04 18 13.09V9H13V4H6V20H13.09M23 17L20 14.5V16H16V18H20V19.5L23 17M18 18.5L15 21L18 23.5V22H22V20H18V18.5Z"/></svg>
            </div>
          </div>
        </a>
        <!-- End Widget Linearea One -->
      </div> 
      <!-- <?php } ?> -->

      <?php if(check_permission_view(ID_GROUP,'read','mastercategory')) { ?>
      <div class="col-xl-4 col-md-6">
        <!-- Widget Linearea One-->
        <a href="<?php echo base_url('backend/purchase/list_category_request') ?>">
          <div class="card card-shadow menu" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon zmdi zmdi-money-box grey-600 font-size-24 vertical-align-bottom mr-5"></i> <b>Kurs Currency BI</b>
                </div>
                <!-- <span class="float-right grey-700 font-size-30">1,253</span> -->
              </div>
              <!-- <div class="ct-chart h-50"></div> -->
              <!-- <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 349.667 349.667" style="enable-background:new 0 0 349.667 349.667;" xml:space="preserve" width="350px" height="150px"> -->
              <svg height="116pt" viewBox="0 0 24 24" width="300pt" xmlns="http://www.w3.org/2000/svg"><path d="M3 6V18H13.32C13.1 17.33 13 16.66 13 16H7C7 14.9 6.11 14 5 14V10C6.11 10 7 9.11 7 8H17C17 9.11 17.9 10 19 10V10.06C19.67 10.06 20.34 10.18 21 10.4V6H3M12 9C10.3 9.03 9 10.3 9 12S10.3 14.94 12 15C12.38 15 12.77 14.92 13.14 14.77C13.41 13.67 13.86 12.63 14.97 11.61C14.85 10.28 13.59 8.97 12 9M19 11L21.25 13.25L19 15.5V14C17.15 14 15.94 15.96 16.76 17.62L15.67 18.71C13.91 16.05 15.81 12.5 19 12.5V11M19 22L16.75 19.75L19 17.5V19C20.85 19 22.06 17.04 21.24 15.38L22.33 14.29C24.09 16.95 22.19 20.5 19 20.5V2"/></svg>
            </div>
          </div>
        </a>
        <!-- End Widget Linearea One -->
      </div> 
      <?php } ?>

   





 

     
       
    </div>
  </div>
</div>
<!-- End Page -->

<style>
  .gradient-text {
  /* Fallback: Set a background color. */
  background-color: red;
  /* Create the gradient. */
  background-image: linear-gradient(to right, #360033, #0b8793);
  /* Set the background size and repeat properties. */
  background-size: 73%;
  background-repeat: repeat;
  /* Use the text as a mask for the background. */
  /* This will show the gradient as a text color rather than element bg. */
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  -moz-background-clip: text;
  -moz-text-fill-color: transparent;
  }
  h1 {
  font-family: "Archivo Black", sans-serif;
  font-weight: normal;
  font-size: 2em;
  text-align: center;
  margin-bottom: 0;
  margin-bottom: -0.25em;
  }
  .menu {
  border-radius: 15px;
  }
</style>