<!-- Page -->
<div class="page">
  <ol class="breadcrumb">
    <a href="<?php echo base_url('backend/purchase/list_master_data_currency'); ?>" type="button" class="btn btn-round btn-warning"><i class="icon md-format-indent-increase" aria-hidden="true"></i>Currency List</a>
  </ol>
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="<?=base_url('backend/currency/list_master_data_currency')?>">Currency List</a></li>
    <li class="breadcrumb-item active">Create Currency</li>
  </ol>
  <h4 style="text-align: left; color:#0000e6; font-weight: 900;"><b>&emsp; >>Create<< </b></h4>
  <?php if ($this->session->flashdata('success')) { ?>
  <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('success'); ?></p>
  </div>
  <?php }elseif($this->session->flashdata('error')){ ?>
  <div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('error'); ?></p>
  </div>
  <?php } ?>
  <div class="page-header" style="text-align: center; padding: 0px;">
    <h1 class="page-title">Form Create Currency</h1>
  </div>
  <div class="page-content" style="padding: 0px;">
    <div class="panel">
      <div class="panel-body container-fluid" style="padding: 0px;">
        <div class="panel">
          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-md-12 col-lg-6">
                <!-- Example Horizontal Form -->
                <div class="example-wrap">
                  <div class="example">
                    <!-- <form class="form-horizontal"> -->
                    <?= form_open(base_url('backend/currency/create_master_currency'),  'id="login_validation" enctype="multipart/form-data"') ?>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Code<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" name="code" placeholder="Code Currency" autocomplete="off" />
                      </div>
                    </div>
                     <?php

                     $hariIni        = new DateTime();
                     $formatToday = $hariIni->format('Y-m-d');

                     // echo $formatToday;
                    $url ="https://www.bi.go.id/biwebservice/wskursbi.asmx/getSubKursLokal3?mts=usd&startdate=$formatToday&enddate=$formatToday";
                    // echo $url;
                    $tem = file_get_contents($url);
                    $string = explode(" ", $tem);

                    // $pris = print_r($string[3]);
                    // print_r($string[314]);
                    $stringArr = substr($string[314],19,5);
                    $stringHargaJual = substr($string[322], 19,5);
                    echo "<input type='text' required='required' value='$stringArr' class='form-control' id='harga_beli' name='usd' placeholder='Code Currency' autocomplete='off' />";

                    echo "<input type='text' required='required' value='$stringHargaJual' class='form-control' id='harga_jual' name='usd' placeholder='Code Currency' autocomplete='off' />";

                    // echo "$stringArr";
                    // echo "<pre id='usd'>".print_r($string[314],true)."</pre><br>";
                    // print_r($string[322]);

                    ?>

                    <button type="button" onclick="countTit()">ss</button>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Country<b style="color: red;">*</b> : </b></label>
                      <div class="col-md-9">
                        <select class="form-control" data-plugin="select2" id="country" name="country" data-placeholder="Select Country">
                          <option></option>
                        </select>
                      </div>
                    </div>

                  </div>
                </div>
                <!-- End Example Horizontal Form -->
              </div>
              <div class="col-md-12 col-lg-6">
              </div>
              <!-- Button Action -->
              <div class="col-lg-5 form-group form-material">
                <!-- <input type="text" class="form-control" placeholder=".col-lg-4"> -->
              </div>
              <div class="col-lg-5 form-group form-material">
                <button type="Submit" class="btn btn-success btn-sm">&emsp;&emsp;SAVE&emsp;&emsp;</button>
              </div>
              <div class="col-lg-2 form-group form-material">
              </div>
              <?php form_close() ?>
              <!-- Button Action -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  $( document ).ready(function() {

    const selectCountry = document.querySelector('#country');
    fetch('https://restcountries.com/v3.1/all').then(res=>{
      return res.json();
    }).then(data=>{
      var countryOutput = "";
      data.forEach(country=>{
        countryOutput += `<option>${country.name.common}</option>`;

      })
      selectCountry.innerHTML = countryOutput; 
    }).catch(err=>{

      console.log(err);
    })

    // var data = file_get_contents('');

    // console.log(data);

    // $.ajax('create_currency.php', { url: 'https://www.bi.go.id/biwebservice/wskursbi.asmx/getSubKursLokal3?mts=usd&startdate=2023-01-04&enddate=2023-01-05' }, function(data) {
    // // document.getElementById('somediv').innerHTML = data;     
    // console.log("sss");   
    //   });


    //  fetch('https://www.bi.go.id/biwebservice/wskursbi.asmx/getSubKursJisdor4?startDate=2023-01-03').then(res=>{
     
    //   console.log(res);
    // }).then(data=>{
    //   console.log(data);
    // }).catch(err=>{

      
    // })

    //  fetch('https://www.bi.go.id/biwebservice/wskursbi.asmx/getSubKursJisdor4?startDate=2023-01-03').then(res=>{
    //   // return res.json();
    //   console.log(res);
    // }).then(data=>{
      
    // }).catch(err=>{

    //   console.log(err);
    // })

    // console.log("")


  //    $.ajax({
  //               type: "GET",
  //               url: "https://www.bi.go.id/biwebservice/wskursbi.asmx/getSubKursJisdor4?startDate=2023-01-03",
  //               crossDomain: true,
  //               contentType:"application/x-www-form-urlencoded; charset=UTF-8",
  //               dataType: "xml", 
  //               // cache: false,
  //               // Content-Type: "text/xml; charset=utf-8",
  //               // contentType: 'text/xml; charset=utf-8',
  //               // Content-Length: "length",
  //               // headers: {'Access-Control-Allow-Origin': 'https://www.bi.go.id'},
  //               success: function (result, status, xhr) {
  //                 console.log(result);
  //               },
  //               error: function (xhr, status, error) {
  //                 console.log(xhr);
  //                   // alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
  //               }
  // });



  });

  function countTit()
  {
    var hargaJualUsd = document.getElementById("harga_jual").value;
    var hargaBeliUsd = document.getElementById("harga_beli").value;

    var countMidPrice = (parseFloat(hargaJualUsd) + parseFloat(hargaBeliUsd)) / 2;

    console.log(countMidPrice);
    // console.log(ttt);

  }

</script>
<!-- End Page -->