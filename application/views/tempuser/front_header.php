
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Purchase System</title>
    
    <link rel="apple-touch-icon" href="<?php echo base_url('src/material/global/assets/images/logo-no-background.png');?>">
    <link rel="shortcut icon" href="<?php echo base_url('src/material/global/assets/images/logo-no-background.png');?>">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo base_url('src/material/global/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('src/material/global/css/bootstrap-extend.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('src/material/global/assets/css/site.min.css');?>">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/animsition/animsition.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/asscrollable/asScrollable.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/switchery/switchery.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/intro-js/introjs.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/slidepanel/slidePanel.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/flag-icon-css/flag-icon.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/waves/waves.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/chartist/chartist.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/jvectormap/jquery-jvectormap.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/assets/examples/css/dashboard/v1.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/assets/examples/css/forms/layouts.css');?>">
    
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/select2/select2.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/bootstrap-select/bootstrap-select.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/icheck/icheck.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/switchery/switchery.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/asrange/asRange.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/ionrangeslider/ionrangeslider.min.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/asspinner/asSpinner.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/clockpicker/clockpicker.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/ascolorpicker/asColorPicker.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/bootstrap-touchspin/bootstrap-touchspin.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/jquery-labelauty/jquery-labelauty.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/bootstrap-maxlength/bootstrap-maxlength.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/timepicker/jquery-timepicker.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/jquery-strength/jquery-strength.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/multi-select/multi-select.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/typeahead-js/typeahead.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/assets/examples/css/forms/advanced.css');?>">
      
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/jquery-wizard/jquery-wizard.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/formvalidation/formValidation.css');?>">

      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/jsgrid/jsgrid.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/assets/examples/css/tables/jsgrid.css');?>">

      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/webui-popover/webui-popover.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/toolbar/toolbar.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/assets/examples/css/uikit/modals.css');?>">
      
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/nestable/nestable.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/tasklist/tasklist.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/sortable/sortable.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/tablesaw/tablesaw.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/bootstrap-table/bootstrap-table.css');?>">
      
      <!-- chart -->
      <script src="<?php echo base_url('src/material/global/js/Chart.js');?>"></script>
      <!-- End Chart -->
      
      <!-- Alert -->
      <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" /> -->
      <script src="<?php echo base_url('src/material/global/js/jquery.min.js');?>"></script>
      <script src="<?php echo base_url('src/material/global/js/sweetalert.js');?>"></script>
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/css/sweetalert.css');?>" />
      <!-- End Alert -->

      <!-- Datatables -->
       <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/datatables.net-select-bs4/dataTables.select.bootstrap4.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('src/material/global/assets/examples/css/tables/datatable.css');?>">
      <!-- end -->
    
    <!-- button -->
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/vendor/ladda/ladda.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('src/material/global/assets/examples/css/uikit/buttons.css');?>">

    <!-- end button -->
    <!-- Fonts -->
    <link rel="stylesheet" href="<?php echo base_url('src/material/global/fonts/material-design/material-design.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('src/material/global/fonts/brand-icons/brand-icons.min.css');?>">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    
    <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!-- Moment -->
    <script src="<?php echo base_url('src/material/global/moment/jquery.min.js');?>" crossorigin="anonymous"></script>
    <script src="<?php echo base_url('src/material/global/moment/moment.min.js');?>" crossorigin="anonymous"></script>
    <!-- Moment -->
    <!-- Scripts -->
    <script src="<?php echo base_url('src/material/global/vendor/breakpoints/breakpoints.js');?>"></script>
    <script>
      Breakpoints();
    </script>
  </head>
  <body class="animsition dashboard">


     
    <div class="site-menubar" style="opacity: -5;">
      <div class="site-menubar-body">
        <div>
          <div>
            <ul class="site-menu" data-plugin="menu">

            </ul>      
          </div>
        </div>
      </div>
    </div>


