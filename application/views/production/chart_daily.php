<div class="page">
  <div class="page-header" style="padding: 20px 10px;">
    <!-- <ol class="breadcrumb">
      <a href="<?php echo base_url('backend/planner'); ?>" class="btn btn-round btn-info"><i class="icon md-home" aria-hidden="true"></i>Menu Planner</a>&emsp;&emsp;
      <a href="<?php echo base_url('backend/planner/create_target_daily'); ?>" class="btn btn-round btn-danger"><i class="icon md-plus" aria-hidden="true"></i>&nbsp; Create &nbsp;</a>&emsp;&emsp;
    </ol><br> -->
<!--     <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
      <li class="breadcrumb-item"><a href="<?=base_url('backend/planner')?>">Menu Planner</a></li>
      <li class="breadcrumb-item"><a href="<?=base_url('backend/planner/injection_molding')?>">Menu Injection Molding</a></li>
      <li class="breadcrumb-item active">Daily Production List</li>
    </ol> -->
  </div>

  <h3 class="panel-title" style="text-align: center; padding: 0px;"><b>Daily Production Chart</b></h3>
  <div class="page-content" style="padding: 0px 0px;">
    <div class="panel"><br>
      <div class="panel-body">
          <select class="form-control" data-plugin="select2" id="parts" name="part_no" data-placeholder="Select Part">
              <option>Choose Year</option>
              <?php foreach ($year as $val) { ?>
              <option value="<?php echo $val->date_report?>">
              <?php echo "$val->date_report" ?>
             </option>
             <?php } ?>
           </select>
        <!-- <div id="myDIV" style="display: none;"> -->
          <div id="myDIV" class="loader" style="display: none;">
            
          </div>

          <div id="chart" style="display: none;">
              <canvas id="myChart"></canvas>
           <div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->

<script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
<div class="modal fade" id="loader" aria-hidden="true" aria-labelledby="filterdata" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-center">
    <div class="modal-content">
      <div class="modal-header">
      </div>
       <div class="loader"></div>
        <h4 class="modal-title" style="text-align: center;">Loading...</h4>
  </div>
</div>

<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}



</style>

<!-- END MODAL -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script type="text/javascript">//On-Progress



  


$( document ).ready(function() {

let person = {
  name:'test',
  age:25
};

function increaseAge(obj)
{
    obj.age +=1;
    obj = {name:'Jane',age:22,status:true};
    console.log(obj.status=false);
}

increaseAge(person);

console.log(person);

var dataLabel = ['Januari','Februari','Maret','April','Mei','Juni','July','Agustus','September','Oktober','November','Desember'];

var x = document.getElementById("myDIV");
var canvaDiv = document.getElementById("chart");

var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: dataLabel,
        datasets: [
          {
            label: "Inisiate Good",
            // backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(0, 211, 253)',
            data: [0, 10, 5, 2, 20, 30, 45],
          },
          {
            label: "Inisiate Reject",
            // backgroundColor: 'rgb(0, 211, 253)',
            // borderColor: 'rgb(0, 211, 253)',
            borderColor: 'rgb(255, 99, 132)',
            data: [0, 10, 5, 2, 20, 30, 45],
          },

          {
            label: "Inisiate Total Output",
            // backgroundColor: 'rgb(0, 211, 253)',
            borderColor: 'rgb(0, 255, 60)',
            data: [0, 10, 5, 2, 20, 30, 45],
          },
          // {
          //   label: "Inisiate Target",
          //   // backgroundColor: 'rgb(0, 211, 253)',
          //   borderColor: 'rgb(247, 255, 0)',
          //   data: [0, 10, 5, 2, 20, 30, 45],
          // }
        ]
    },




   
});

  $(function () {
    $("#parts").change(function() {
      var val = $(this).val();

      chart.clear();
      // chart.reset();
      // $('#myChart').remove();
      $.ajax({
        method: 'POST',
        url: "<?= base_url("frontend/production/get_value_chart")?>",
        data: {'id': val},
        cache: false,
        async : true,
        dataType : 'json',
        beforeSend: function() {
            x.style.display = "block";
          
        },
        success: function(data){

          // console.log(data[0].reject);
          canvaDiv.style.display = "block";
          chart.data.labels = dataLabel;
          chart.data.datasets[0].label = "Good";
          chart.data.datasets[0].data[0] = data[0].good;
          chart.data.datasets[0].data[1] = data[1].good;
          chart.data.datasets[0].data[2] = data[2].good;
          chart.data.datasets[0].data[3] = data[3].good;
          chart.data.datasets[0].data[4] = data[4].good;
          chart.data.datasets[0].data[5] = data[5].good;
          chart.data.datasets[0].data[6] = data[6].good;
          chart.data.datasets[0].data[7] = data[7].good;
          chart.data.datasets[0].data[8] = data[8].good;
          chart.data.datasets[0].data[9] = data[9].good;
          chart.data.datasets[0].data[10] = data[10].good;
          chart.data.datasets[0].data[11] = data[11].good;


          chart.data.labels = dataLabel;
          chart.data.datasets[1].label = "Reject";
          chart.data.datasets[1].data[0] = data[0].tot_reject;
          chart.data.datasets[1].data[1] = data[1].tot_reject;
          chart.data.datasets[1].data[2] = data[2].tot_reject;
          chart.data.datasets[1].data[3] = data[3].tot_reject;
          chart.data.datasets[1].data[4] = data[4].tot_reject;
          chart.data.datasets[1].data[5] = data[5].tot_reject;
          chart.data.datasets[1].data[6] = data[6].tot_reject;
          chart.data.datasets[1].data[7] = data[7].tot_reject;
          chart.data.datasets[1].data[8] = data[8].tot_reject;
          chart.data.datasets[1].data[9] = data[9].tot_reject;
          chart.data.datasets[1].data[10] = data[10].tot_reject;
          chart.data.datasets[1].data[11] = data[11].tot_reject;

          chart.data.labels = dataLabel;
          chart.data.datasets[2].label = "Total Output";
          chart.data.datasets[2].data[0] = data[0].output_tot;
          chart.data.datasets[2].data[1] = data[1].output_tot;
          chart.data.datasets[2].data[2] = data[2].output_tot;
          chart.data.datasets[2].data[3] = data[3].output_tot;
          chart.data.datasets[2].data[4] = data[4].output_tot;
          chart.data.datasets[2].data[5] = data[5].output_tot;
          chart.data.datasets[2].data[6] = data[6].output_tot;
          chart.data.datasets[2].data[7] = data[7].output_tot;
          chart.data.datasets[2].data[8] = data[8].output_tot;
          chart.data.datasets[2].data[9] = data[9].output_tot;
          chart.data.datasets[2].data[10] = data[10].output_tot;
          chart.data.datasets[2].data[11] = data[11].output_tot;


          // chart.data.labels = dataLabel;
          // chart.data.datasets[3].label = "Target";
          // chart.data.datasets[3].data[0] = data[0].target;
          // chart.data.datasets[3].data[1] = data[1].target;
          // chart.data.datasets[3].data[2] = data[2].target;
          // chart.data.datasets[3].data[3] = data[3].target;
          // chart.data.datasets[3].data[4] = data[4].target;
          // chart.data.datasets[3].data[5] = data[5].target;
          // chart.data.datasets[3].data[6] = data[6].target;
          // chart.data.datasets[3].data[7] = data[7].target;
          // chart.data.datasets[3].data[8] = data[8].target;
          // chart.data.datasets[3].data[9] = data[9].target;
          // chart.data.datasets[3].data[10] = data[10].target;
          // chart.data.datasets[3].data[11] = data[11].target;




          chart.update();



        },
        complete:function(res)
        {

            x.style.display = "none";
        },
 
      });

       
  });
 });






});

  


</script>


