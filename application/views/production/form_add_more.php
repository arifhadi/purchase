 <!-- Page -->
<div class="page">
  <ol class="breadcrumb">
    <a href="<?php echo base_url('dashboard'); ?>" class="btn btn-round btn-warning"><i class="icon md-undo" aria-hidden="true"></i>Dashboard</a>&nbsp;&nbsp;
  </ol>
  <div class="page-header" style="text-align: center; padding: 0px;">
    <h1 class="page-title gradient-text">Productivity Output</h1>
  </div>

  <div class="page-content" style="padding: 0px 20px;">
    <?php if ($this->session->flashdata('success')) { ?>
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button><p><?php echo $this->session->flashdata('success'); ?></p>
    </div>
    <?php } ?>
  </div>

  <div class="page-content container-fluid" style="padding: 0px;">
    <div class="row">

      <div class="col-lg-2">
        <div class=""><!-- panel -->
          <div class="panel-body">
            
          </div>
        </div>
      </div>

      <div class="col-lg-8">
        <!-- **** Button Edit **** -->
        <div class="page-content" style="padding: 0px 20px;">
          <?php $no=1; foreach ($result_master_data as $val) { ?>
            <button type="button" name="add_more" data-bind='<?=$val->id_master?>' id="add_more" class="btn btn-floating btn-success btn-sm waves-effect waves-light waves-round edit"><?= $no++ ?></button>
          <?php } ?>
          <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-light waves-round"><?= $no++ ?></button>
        </div>
        <!-- **** End Button Edit **** -->
        <div class="panel">
          <div class="panel-body">
            <?= form_open(base_url('frontend/production/save_form'),  'id="login_validation" enctype="multipart/form-data"') ?>
            <div class="row row-lg-12">
              <div class="col-md-12 col-lg-12">
                <div class="example-wrap">
                  <div class="example">

                    <div class="form-group row">
                      <label class="col-md-2 form-control-label"><b>Date Report <b style="color: red;">*</b>:</b></label>
                      <div class="col-md-3">
                        <input id="DReport" type="date" class="form-control" name="date_report" required="required" placeholder="" autocomplete="off" style="background-color: #efc15d;" value="<?=date("Y-m-d", strtotime($master_data->date_report))?>" readonly/>
                      </div>
                      <div class="col-md-3">
                       <h5>Your Peformance Today : <b style="color: #004d99;"><?=$peformance?>%</b></h5>
                      </div>
                      <div class="col-md-3">
                        <h5>Grade : <b style="color: #004d99;"><?=$grade?></b></h5>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-2 form-control-label"><b>Shift  <b style="color: red;">*</b>:</b></label>
                      <div class="col-md-3">
                        <select class="form-control" id="IDShift" data-plugin="select2" name="shift" data-placeholder="-select-" onchange="Shift(this);" required disabled>
                          <option></option>
                          <?php if ($master_data->shift_start >= '06:00' && $master_data->shift_start <= '14:00') {
                            echo "<option selected='selected'>morning</option>";
                            }elseif ($master_data->shift_start >= '14:00' && $master_data->shift_start <= '22:00') {
                              echo "<option selected='selected'>afternoon</option>";
                            }elseif ($master_data->shift_start >= '22:00' && $master_data->shift_start <= '06:00') {
                              echo "<option selected='selected'>night</option>";
                            }else{
                              echo "<option selected='selected'>custom</option>";
                            }
                          ?>
                        </select>
                      </div>
                      <div class="col-md-2">
                        <input id="ShiftStart" type="time" class="form-control" name="shift_start" required="required" placeholder="--:--" autocomplete="off" value="<?=$master_data->shift_start?>" readonly/>
                      </div>
                      <div class="col-md-2">
                        <input id="ShiftFinish" type="time" class="form-control" name="shift_finish" required="required" placeholder="--:--" autocomplete="off" value="<?=$master_data->shift_finish?>" readonly/>
                      </div>
                    </div>

                    <div class="form-group row">
                     <label class="col-md-2 form-control-label"><b>Employee No</b><b style="color: red;">*</b> : </label>
                      <div class="col-md-3">
                       <select class="form-control" required="required" data-plugin="select2" id="UserOpt" name="employee_no" data-placeholder="xxxx" disabled>
                        <?php foreach ($data_opt as $val) { ?>
                          <option value=""></option>
                          <option class="nameOption" <?php if($employee_id == $val->employee_no){ echo 'selected="selected"'; } ?> value="<?php echo $val->employee_no?>">
                           <?php echo "$val->employee_no" ?>
                          </option>
                        <?php } ?>
                       </select>
                      </div>
                      <div class="col-md-4">
                        <input type="text" class="form-control" value="<?= $master_data->name_emp;?>" required placeholder="xxx" readonly>
                      </div>
                    </div>

                    <?= form_open(base_url('frontend/production/save_more_page'),  'id="login_validation" enctype="multipart/form-data"') ?>

                    <input type="text" class="form-control" value="<?=$master_data->id_group_master?>" name="id_group_master" required placeholder="xxx" readonly hidden>

                    <div class="form-group row">
                      <label class="col-md-2 form-control-label"><b>Time</b><b style="color: red;">*</b> : </label>
                      <div class="col-md-3">
                        <b>Date Start :</b> <input id="DStart" type="datetime-local" class="form-control" name="date_start" required="required" placeholder="" autocomplete="off" onchange="Formula();return false;" style="background-color: #efc15d;" autofocus="autofocus"/>
                      </div>
                      <div class="col-md-3">
                        <b>Date Finish :</b> <input id="DFinish" type="datetime-local" class="form-control" name="date_finish" required="required" placeholder="" autocomplete="off" onchange="Formula();return false;" style="background-color: #efc15d;"/>
                      </div>
                      <div class="col-md-3">
                        <table>
                          <tbody>
                          <tr>
                            <td>
                            <b>Hours</b><input id="IDHours" type="text" class="form-control readonly" name="t_hours" required placeholder="xx">
                            </td>
                            <td>
                              <b>Minute</b><input id="IDMinute" type="text" class="form-control readonly" name="t_minute" required placeholder="xx">
                            </td>
                            <td>
                              <input id="IDSecond" type="text" class="form-control readonly" name="t_second" required placeholder="xx" hidden>
                            </td>
                          </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <input id="IDStart" type="time" class="form-control" name="time_start" required="required" placeholder="--:--" autocomplete="off" hidden/>
                    <input id="IDFinish" type="time" class="form-control" name="time_finish" required="required" placeholder="--:--" autocomplete="off" hidden/>

                    <div class="form-group row">
                      <label class="col-md-2 form-control-label"><br><b>Machine No</b><b style="color: red;">*</b> : </label>
                      <div class="col-md-3">
                        <b>&nbsp;</b><select class="form-control" required="required"data-plugin="select2" id="MachineNo" name="machine_no"data-placeholder="-select-">
                          <option value=""></option>
                          <?php foreach ($datamachine as $val) { ?>
                          <option class="MachnieOption" value="<?php echo $val->id_machine?>"> 
                            <?php echo "$val->line_mc_no$val->mc_no" ?>
                          </option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-3">
                        <input type="text" class="form-control readonly" name="mc_no" required placeholder="xxx" hidden>
                        <b>Brand : </b><input type="text" class="form-control readonly" name="brand" required placeholder="xxx">
                      </div>
                      <div class="col-md-3">
                        <b>Tonnage : </b><input type="text" class="form-control readonly" name="tonnage" required placeholder="xxx">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-2 form-control-label"><br><b>Part Name</b><b style="color: red;">*</b> : </label>
                      <div class="col-md-3">
                        <b>&nbsp;</b><select onchange="partName(this);" class="form-control" required="required" data-plugin="select2" id="PartName" name="part_name" data-placeholder="-select-" >
                          <option value=""></option>
                          <?php foreach ($datatooling as $val) { ?>
                          <option value="<?php echo $val->gih_tool_no?>"> 
                            <?php echo " $val->tooling_name - $val->tooling_no " ?>
                          </option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-6">
                        <input id="TLNameNo" type="text" class="form-control readonly" name="tl_name_no" required placeholder="xxx" hidden>
                        <table>
                          <tbody>
                          <tr>
                            <td>
                            <b>No of Cavities</b> <input id="NoCavities" type="text" class="form-control readonly" name="no_of_cavities" required placeholder="xxx">
                            </td>
                            <td>
                              <b>Cycle time/pcs</b> <input id="CycleTime" type="text" class="form-control readonly" name="cycle_time_pcs" required placeholder="xxx">
                            </td>
                            <td>
                              <b>&nbsp;Part Weight </b> <input id="PartWeight" type="text" class="form-control readonly" name="part_weight" required placeholder="xxx">
                            </td>
                            <td>
                              <b>Runner Weight</b> <input id="RunnerWeight" type="text" class="form-control readonly" name="runner_weight" required placeholder="xxx">
                            </td>
                          </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-2 form-control-label"><b>Target (pcs)</b><b style="color: red;">*</b> : </label>
                      <div class="col-md-3">
                        <!-- <input id="Target" type="text" class="form-control" name="target" required placeholder="xxx" readonly> -->
                        <input id="Target" type="text" class="form-control readonly"  name="target" autocomplete="off" required placeholder="xxx" style="background-color: #eee;" />
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-2 form-control-label"><br><b>Output</b><b style="color: red;">*</b> : </label>
                      <div class="col-md-3">
                        <b> Good (pcs): </b> <input id="IDGood" type="text" class="form-control input-text" name="good" required placeholder="xxx" onkeypress="return hanyaAngka(event)" pattern="[0-9]*" inputmode="numeric"><p id="Hgood"></p>
                      </div>
                      <div class="col-md-3">
                        <b> Reject (pcs): </b> <input id="IDReject" type="text" class="form-control input-text" name="reject" required placeholder="xxx" onkeypress="return hanyaAngka(event)" pattern="[0-9]*" inputmode="numeric"><p id="HReject"></p>
                      </div>
                      <div class="col-md-3">
                        <b> Total (pcs): </b> <input id="IDtotalOutput" type="text" class="form-control" name="tot_output" required placeholder="xxx" onkeypress="return hanyaAngka(event)" readonly><p id="HtotalOutput"></p>
                      </div>
                    </div>

                    <div class="form-group row" id="billdesc">
                      <label class="col-md-3 form-control-label"><b>Peformance of the Day</b><b style="color: red;">*</b> : </label>
                      <div class="col-md-5 TextPeformance">
                        <h3>%</h3>
                      </div>
                      <input id="IDPeformance" type="text" class="form-control" name="peformance_day" required hidden>
                    </div>

                    <div class="form-group row form row">
                      <label class="col-md-2 form-control-label"><br><b>Target (g): </b></label>
                      <div class="col-md-9">
                        <table>
                          <tbody>
                          <tr>
                            <td>
                              <b>Lumps</b> <input id="TarLumps" type="text" class="form-control" name="tar_lumps" value="0" required placeholder="grams" readonly>
                            </td>
                            <td>
                              <b>Runner</b> <input id="TarRunner" type="text" class="form-control" name="tar_runner" required placeholder="grams" readonly>
                            </td>
                            <td>
                              <b>Prod Qty</b> <input id="TarProdQty" type="text" class="form-control" name="tar_prod_qty" required placeholder="grams" readonly>
                            </td>
                            <td>
                              <b></b><h1>:</h1></b>
                            </td>
                            <td>
                              <b>Total</b> <input id="TotalTarget" type="text" class="form-control" name="tot_target" required placeholder="xxx" readonly>
                            </td>
                          </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div class="form-group row form row">
                      <label class="col-md-2 form-control-label"><b>Actual (g): </b></label>
                      <div class="col-md-9">
                        <table>
                          <tbody>
                          <tr>
                            <td>
                              <input id="ActLumps" type="text" class="form-control input-text" name="act_lumps" required placeholder="grams">
                            </td>
                            <td>
                              <input id="ActRunner" type="text" class="form-control input-text" name="act_runner" required placeholder="grams">
                            </td>
                            <td>
                              <input id="ActProdQty" type="text" class="form-control" name="act_prod_qty" required placeholder="grams" readonly>
                            </td>
                            <td>
                              <h1>:</h1></b>
                            </td>
                            <td>
                              <input id="TotalActual" type="text" class="form-control" name="tot_actual" required placeholder="xxx" readonly>
                            </td>
                          </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div class="form-group row form row">
                      <label class="col-md-2 form-control-label"><b>Discrepancy (g): </b></label>
                      <div class="col-md-9">
                        <table>
                          <tbody>
                          <tr>
                            <td>
                              <input id="DissLumps" type="text" class="form-control" name="diss_lumps" required placeholder="grams" readonly>
                            </td>
                            <td>
                              <input id="DissRunner" type="text" class="form-control" name="diss_runner" required placeholder="grams" readonly>
                            </td>
                            <td>
                              <input id="DissProdQty" type="text" class="form-control" name="diss_prod_qty" required placeholder="grams" readonly>
                            </td>
                            <td>
                              <h1>:</h1></b>
                            </td>
                            <td>
                              <input id="TotalDiss" type="text" class="form-control" name="tot_dissrapancy" required placeholder="xxx" readonly>
                            </td>
                          </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>

      <div class="col-lg-2">
        <div class=""><!-- panel -->
          <div class="panel-body">

            <div class="row row-lg-12">
              <div class="col-md-12 col-lg-12">
                <div class="example-wrap">
                  <div class="example">
                    <div class="form-group row">
                      <div class="col-md-12">
                        <button type="Submit" class="btn btn-success ladda-button waves-effect waves-light waves-round btn-md addmore" data-style="expand-left" data-plugin="ladda" data-type="progress"><i class="icon md-plus" aria-hidden="true"></i>Add More</button>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-12 TextGrade">
                        <button type="button" class="btn btn-warning waves-effect waves-light waves-round finish"><i class="icon md-check" aria-hidden="true"></i>&nbsp; Finish &nbsp;&nbsp;</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <?php form_close() ?>

 <!--  <div id="AddNewPage">
      
  </div> -->
    </div>
  </div>
</div><br><br>
<!-- End Page -->
<style>
  .gradient-text {
    /* Fallback: Set a background color. */
    background-color: red;
    /* Create the gradient. */
    background-image: linear-gradient(to right, #ffaa00, #0b8793);
    /* Set the background size and repeat properties. */
    background-size: 73%;
    background-repeat: repeat;
    /* Use the text as a mask for the background. */
    /* This will show the gradient as a text color rather than element bg. */
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent; 
    -moz-background-clip: text;
    -moz-text-fill-color: transparent;
  }
</style>
<style>
  .select2-search input { background-color: #ccf5ff; }
  .select2-results { background-color: #ccf5ff; }
  .select2-selection{background-color:#99ebff !important;}
  .input-text{background-color:#99ebff;}
</style>
<style>
  .readonly{background-color: #eee;}
</style>

<script>
  $(".readonly").on('keydown paste focus mousedown', function(e){
    if(e.keyCode != 9) // ignore tab
    e.preventDefault();
  });
</script>

<script>
  function hanyaAngka(event) {
    var angka = (event.which) ? event.which : event.keyCode
    if (angka != 46 && angka > 31 && (angka < 48 || angka > 57))
      return false;
    return true;
  }

  (function($) {
    $.fn.inputFilter = function(inputFilter) {
      return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
          this.value = "";
        }
      });
    };
  });
  $("#intTextBox").inputFilter(function(value) {
    return /^-?\d*$/.test(value); });
</script>

<script>
  $('#MachineNo').attr("disabled", true);
  $('#PartName').attr("disabled", true);
  $('#IDGood').attr("readonly", true);
  $('#IDReject').attr("readonly", true);
  $('#ActLumps').attr("readonly", true);
  $('#ActRunner').attr("readonly", true);
</script>

<script>
  $(function () {
        $("#UserOpt").change(function() {
          var val = $(this).val();
          console.log(val);
          // $('input[name="name_opt"]').val(val);
          $.ajax({
          method: 'POST',
          url: "<?= base_url("frontend/production/get_data_opt/")?>",
          data: {'id': val},
          cache: false,
          async : true,
          dataType : 'json',
          success: function(data){
            // console.log(data);
            // var html = '';
            // var i;
            $('input[name="name_opt"]').val(data.optname);
          }
        });
        });
      });

  $(function () {
        $("#MachineNo").change(function() {
          var val = $(this).val();
          console.log(val);
          $.ajax({
          method: 'POST',
          url: "<?= base_url("frontend/production/get_data_mc/")?>",
          data: {'id': val},
          cache: false,
          async : true,
          dataType : 'json',
          success: function(data){
            $('input[name="mc_no"]').val(data.line_mc_no+""+data.mc_no);
            $('input[name="brand"]').val(data.brand);
            $('input[name="tonnage"]').val(data.tonnage);
            $('#PartName').attr("disabled", false);
            $('#PartName').focus();//After keyin Machine No
          }
          });
        });
    });  
</script>

<script>
  // var DateNow = ((new Date().getDate() < 10)?"0":"") + new Date().getDate() +"/"+(((new Date().getMonth()+1) < 10)?"0":"") + (new Date().getMonth()+1) +"/"+ new Date().getFullYear();
  $('#DFinish').attr("readonly", true);
  var TimeStart     ="";
  var TimeFinish    ="";
  var second        ="";
  var mcyclepcs     ="";
  var noofcavities  ="";
  var Target        ="";
  var TotOutput     ="";
  var TotAllOutput  ="";

  function Formula()
  {
    DateStart  = document.getElementById('DStart').value;
    DateFinish = document.getElementById('DFinish').value;
    $('#DFinish').attr("min", DateStart);

    if ($.trim(DateStart)){
      $('#DFinish').attr("readonly", false);
      $('#DFinish').focus();//After keyin Employee No
    }

    if ($.trim(DateFinish)) {
      $('#MachineNo').attr("disabled", false);
      $('#MachineNo').focus();//After keyin DateFinish
    }

    TimeStart  = moment(DateStart).format("HH:mm");
    TimeFinish = moment(DateFinish).format("HH:mm");
    
    $('#DateStart').val(DateStart);
    $('#DateFinish').val(DateFinish);
    $('#IDStart').val(TimeStart);
    $('#IDFinish').val(TimeFinish);

    DateStart1   = moment(DateStart, 'YYYY-MM-DD hh:mm:ss');
    DateFinish2  = moment(DateFinish, 'YYYY-MM-DD hh:mm:ss');

    var hoursDiff = DateFinish2.diff(DateStart1, 'hours');
    console.log('Hours:' + hoursDiff);
    
    var minutesDiff = DateFinish2.diff(DateStart1, 'minutes');
    console.log('Minutes:' + minutesDiff);
    
    var secondsDiff = DateFinish2.diff(DateStart1, 'seconds');
    console.log('Seconds:' + secondsDiff);

    var totminute = hoursDiff*60;
    var minutetot = minutesDiff - totminute;

    $('#IDHours').val(hoursDiff);
    $('#IDMinute').val(minutetot);
    $('#IDSecond').val(secondsDiff);
    
    second = secondsDiff;

    if (isNaN(Target)) {//Part Name -> Target
      $('#Target').val(null);
    }else{
      Target = second/mcyclepcs*noofcavities;
      $('#Target').val(Math.round(Target));
    }

    $('#IDGood').val(null);//clear Good
    $('#IDReject').val(null);//clear Reject
    $('#IDtotalOutput').val(null);//clear Total Output
    $('#Hgood').html("");//clear % Good
    $('#HReject').html("");//clear % Reject
    $('#HtotalOutput').html("");//clear % Total Output
    $('#IDPeformance').val(null);//clear peformance day
    $('.TextPeformance').html("<h3>%</h3>");//clear peformance day

    $('#TarRunner').val(null);//Target Runner
    $('#TarProdQty').val(null);//Target ProdQty
    $('#TotalTarget').val(null);//Total Target

    $('#ActLumps').val(null);//Actual Lumps
    $('#ActRunner').val(null);//Actual Runner
    $('#ActProdQty').val(null);//Actual ProdQty
    $('#TotalActual').val(null);//Total Actual

    $('#DissLumps').val(null);//Discrepancy Lumps
    $('#DissRunner').val(null);//Discrepancy Runner
    $('#DissProdQty').val(null);//Discrepancy ProdQty
    $('#TotalDiss').val(null);//Total Discrepancy
  }

    // function start(ev, object)
    // {
    //   TimeStart = object.value;
    // }

    // function finish(ev, object)
    // {
    //  TimeFinish = object.value;          
    //     var timeStart = new Date("01/01/2021 " + TimeStart).getHours();
    //     var timeEnd   = new Date("01/01/2021 " + TimeFinish).getHours();

    //     var hourDiff = timeEnd - timeStart;
    //     if (hourDiff < 0) {
    //         hourDiff = 24 + hourDiff;
    //         minute    = hourDiff*60;
    //         second   = hourDiff*3600;
    //         // console.log(minute, 'if');
    //     }else{
    //      minute   = hourDiff*60;
    //      second   = hourDiff*3600;
    //      // console.log(minute, 'else');
    //     }
    //     $('#IDHours').val(hourDiff);
    //     $('#IDMinute').val(minute);
    //     $('#IDSecond').val(second);
    // }

  function partName(selectObject) 
  {
    var val = selectObject.value;
    $.ajax({
      method: 'POST',
      url: "<?= base_url("frontend/production/data_tooling/")?>",
      data: {'id': val},
      cache: false,
      async : true,
      dataType : 'json',
        success: function(data){
          console.log(data.no_of_cavities);
          $('#TLNameNo').val(data.tooling_name+" - "+data.tooling_no);
          $('#NoCavities').val(data.no_of_cavities);
          $('#CycleTime').val(data.mold_cycle_pcs);
          $('#PartWeight').val(data.part_weight);
          $('#RunnerWeight').val(data.runner_weight);

          noofcavities  = data.no_of_cavities;
          mcyclepcs     = data.mold_cycle_pcs;
          pweight       = data.part_weight;
          rweight       = data.runner_weight;

          Target = second/mcyclepcs*noofcavities;
          if (isNaN(Target)) {
            $('#Target').val(null);  
          }else{
            $('#Target').val(Math.round(Target));  
          }
          $('#IDLumps').val("0");
          // $('#IDRunner').val(data.runner_weight);

          $('#IDGood').val(null);//clear Good
          $('#IDReject').val(null);//clear Reject
          $('#IDtotalOutput').val(null);//clear Total Output
          $('#Hgood').html("");//clear % Good
          $('#HReject').html("");//clear % Reject
          $('#HtotalOutput').html("");//clear % Total Output
          $('#IDPeformance').val(null);//clear peformance day
          $('.TextPeformance').html("<h3>%</h3>");//clear peformance day

          $('#TarRunner').val(null);//Target Runner
          $('#TarProdQty').val(null);//Target ProdQty
          $('#TotalTarget').val(null);//Total Target

          $('#ActLumps').val(null);//Actual Lumps
          $('#ActRunner').val(null);//Actual Runner
          $('#ActProdQty').val(null);//Actual ProdQty
          $('#TotalActual').val(null);//Total Actual

          $('#DissLumps').val(null);//Discrepancy Lumps
          $('#DissRunner').val(null);//Discrepancy Runner
          $('#DissProdQty').val(null);//Discrepancy ProdQty
          $('#TotalDiss').val(null);//Total Discrepancy

          $('#IDGood').attr("readonly", false);
          $('#IDReject').attr("readonly", false);
          $('#IDGood').focus();//After keyin Part Name
        }
    }); 
  }

  var ValGood   ="";
  var ValReject ="";
  var PerGood   ="";
  $("#IDGood").keyup(function(){
    ValGood    = this.value;
    PerGood  = ValGood/Math.round(Target)*100;
    $('#Hgood').html("<h4><b style='color:#004d99'>" + Math.round(PerGood) + "%</b></h4>");
    if (!$.trim(PerGood)){   
      $('#IDReject').focus();//After keyin Good
    }
  });

  var TRunner     ='';
  var TarProdQty  ='';
  var TotProdQty  ='';
  $("#IDReject").keyup(function(){
    ValReject   = this.value;
    var PerReject = ValReject/Math.round(Target)*100;
    $('#HReject').html("<h4><b style='color:#004d99'>" + Math.round(PerReject) + "%</b></h4>");
    TotOutput    = parseInt(ValGood)+parseInt(ValReject);
    TotAllOutput = TotOutput/Math.round(Target)*100;
    // console.log(noofcavities,'total')
    $('#IDtotalOutput').val(TotOutput);
    $('#HtotalOutput').html("<h4><b style='color:#004d99'>" + Math.round(TotAllOutput) + "%</b></h4>");
    $('#IDPeformance').val(Math.round(PerGood));
    $('.TextPeformance').html("<h2><b style='color:#004d99'>" + Math.round(PerGood) + "%</b></h2>");
    // ** Tabel Material **
    // ##**Target**
    TRunner     = TotOutput/noofcavities*rweight;//Target Runner
    $('#TarRunner').val(TRunner);
    TarProdQty  = TotOutput*pweight;//Target Prod Qty
    $('#TarProdQty').val(Math.round(TarProdQty));
    var TotalTarget = TRunner+Math.round(TarProdQty);//Target Total
    $('#TotalTarget').val(TotalTarget);
    // ##**End Target**

    $('#ActProdQty').val(Math.round(TarProdQty));// ##**Actual**

    TotProdQty = TarProdQty - TarProdQty;
    $('#DissProdQty').val(TotProdQty);// ##**Disscrapancy **
    // ** End Tabel Material **

    $('#ActLumps').attr("readonly", false);
    $('#ActRunner').attr("readonly", false);
  });

  var ValActLumps  ='';
  var ValActRunner ='';
  var TotActLumps  ='';//ActLumps
  var TotActRunner ='';//Total Act Runner
  $("#ActLumps").keyup(function(){//Actual Lumps
    ValActLumps    = this.value;
    TotActLumps    = ValActLumps;
    $('#DissLumps').val(TotActLumps);//value input Disscrapancy

    var TotAct  = parseInt(ValActLumps)+parseInt(ValActRunner)+parseInt(Math.round(TarProdQty));
    $('#TotalActual').val(TotAct);//Total Actual

    var TotDiss = parseInt(TotActLumps)+parseInt(TotActRunner)+parseInt(Math.round(TotProdQty));
    $('#TotalDiss').val(TotDiss);//Total Discrepancy
  });

  $("#ActRunner").keyup(function(){//Actual Runner
    ValActRunner = this.value;
    TotActRunner = TRunner-ValActRunner;
    $('#DissRunner').val(TotActRunner);//Discrepancy Runner

    var TotAct  = parseInt(ValActLumps)+parseInt(ValActRunner)+parseInt(Math.round(TarProdQty));
    $('#TotalActual').val(TotAct);//Total Actual

    var TotDiss = parseInt(TotActLumps)+parseInt(TotActRunner)+parseInt(Math.round(TotProdQty));
    console.log(TotDiss,'Total Diss')
    $('#TotalDiss').val(TotDiss);
  });
</script>

<script type="text/javascript">//Acceptance
  $(".finish").click(function(){
    var id = $(this).attr("data-bind");
    swal({
      title: "are you sure it's finished ?",
      text: "",
      type: "info",
      showCancelButton: true,
      confirmButtonClass: "btn-warning",
      confirmButtonText: "Yes",
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          // url: '<?= base_url("backend/main_menu/change_masterdata/")?>'+id,
          type: 'DELETE',
          error: function() {
            alert('Something is wrong');
          },
          success: function(data) {
            $("#"+id).remove();
              // swal("Deleted!", "Your imaginary file has been deleted.", "success");
              window.location.href = '<?= base_url("frontend/production")?>';
          }
        });
      } else {
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
    });
  });
</script>

<script type="text/javascript">//On-Progress
  $(".edit").click(function(){
    var id = $(this).attr("data-bind");
    var idgroup = <?=$master_data->id_group_master?>;
    swal({
      title: "you want to change the data?",
      text: "",
      type: "info",
      showCancelButton: true,
      confirmButtonClass: "btn-info",
      confirmButtonText: "Yes",
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          // url: '<?= base_url("backend/main_menu/change_masterdata/")?>'+id,
          type: 'DELETE',
          error: function() {
            alert('Something is wrong');
          },
          success: function(data) {
            // $("#"+id).remove();
              // swal("Deleted!", "Your imaginary file has been deleted.", "success");
              window.location.href = '<?= base_url("frontend/production/edit_form/")?>'+idgroup+'/'+id;
          }
        });
      } else {
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
    });
  });
</script>
