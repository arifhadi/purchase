<style>
  .gradient-text {
    /* Fallback: Set a background color. */
    background-color: red;
    /* Create the gradient. */
    background-image: linear-gradient(to right, #ffaa00, #0b8793);
    /* Set the background size and repeat properties. */
    background-size: 73%;
    background-repeat: repeat;
    /* Use the text as a mask for the background. */
    /* This will show the gradient as a text color rather than element bg. */
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    -moz-background-clip: text;
    -moz-text-fill-color: transparent;
  }
</style>
<!-- Page -->
<div class="page" style="max-width: 1800px;">
  <ol class="breadcrumb">
    <a href="<?php echo base_url('frontend/production/report_peformance'); ?>" class="btn btn-round btn-warning"><i class="icon md-undo" aria-hidden="true"></i>Dashboard</a>&nbsp;&nbsp;
  </ol>
  <h4 style="text-align: left; color:#0000e6; font-weight: 900;"><b>&emsp; >>Display<< </b></h4>
  <div class="page-header" style="text-align: center; padding: 0px;">
    <h1 class="page-title gradient-text">Display Productivity Output</h1>
  </div>

  <div class="page-content" style="padding: 0px 20px;">
    <?php if ($this->session->flashdata('success')) { ?>
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button><p><?php echo $this->session->flashdata('success'); ?></p>
    </div>
    <?php } ?>
  </div>
  
  <div class="page-content" style="padding: 0px 20px;">
    <div class="panel">
     <div class="panel-body container-fluid" style="padding: 0px 20px;">
       <div class="row row-lg">

         <div class="col-md-12 col-lg-5">
           <div class="example-wrap">
             <div class="example">

              <div class="form-group row">
                <label class="col-md-2 form-control-label"><b>Date Report <b style="color: red;">*</b>:</b></label>
                <div class="col-md-4">
                  <input id="DReport" type="date" class="form-control" name="date_report" required="required" placeholder="" autocomplete="off" style="background-color: #efc15d;" value="<?=date("Y-m-d", strtotime($edit_data->date_report))?>" readonly/>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 form-control-label"><b>Shift  <b style="color: red;">*</b>:</b></label>
                <div class="col-md-4">
                  <select class="form-control" id="IDShift" data-plugin="select2" name="shift" data-placeholder="-select-" onchange="Shift(this);" required disabled>
                    <option></option>
                    <?php if ($edit_data->shift_start >= '06:00' && $edit_data->shift_start <= '14:00') {
                      echo "<option selected='selected'>morning</option>";
                      }elseif ($edit_data->shift_start >= '14:00' && $edit_data->shift_start <= '22:00') {
                        echo "<option selected='selected'>afternoon</option>";
                      }elseif ($edit_data->shift_start >= '22:00' && $edit_data->shift_start <= '06:00') {
                        echo "<option selected='selected'>night</option>";
                      }else{
                        echo "<option selected='selected'>custom</option>";
                      }
                    ?>
                  </select>
                </div>
                <div class="col-md-3">
                  <input id="ShiftStart" type="time" class="form-control" name="shift_start" required="required" placeholder="--:--" autocomplete="off" value="<?=$edit_data->shift_start?>" readonly/>
                </div>
                <div class="col-md-3">
                  <input id="ShiftFinish" type="time" class="form-control" name="shift_finish" required="required" placeholder="--:--" autocomplete="off" value="<?=$edit_data->shift_finish?>" readonly/>
                </div>
              </div>

               <div class="form-group row">
                 <label class="col-md-2 form-control-label"><b>Employee No</b><b style="color: red;">*</b> : </label>
                  <div class="col-md-4">
                   <style>
                    .select2-search input { background-color: #ccf5ff; }
                    .select2-results { background-color: #ccf5ff; }
                    .select2-selection{background-color:#99ebff !important;}
                    .input-text{background-color:#99ebff;}
                   </style>
                   <select class="form-control" required="required" data-plugin="select2" id="UserOpt" name="employee_no" data-placeholder="xxxx" disabled>
                    <?php foreach ($data_opt as $val) { ?>
                      <option value=""></option>
                      <option class="nameOption" <?php if($employee_id == $val->employee_no){ echo 'selected="selected"'; } ?> value="<?php echo $val->employee_no?>">
                       <?php echo "$val->employee_no" ?>
                      </option>
                    <?php } ?>
                   </select>
                  </div>
                  <div class="col-md-4">
                    <input type="text" class="form-control" value="<?= $edit_data->name_emp;?>" required placeholder="xxx" readonly>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-12 col-lg-4">
            <!-- Example Horizontal Form -->
            <div class="example-wrap">
              <div class="example">

                <div class="form-group row">
                 <label class="col-md-1 form-control-label">&nbsp;</label>
                  <div class="col-md-5">
                  </div>
                </div>

                <div class="form-group row">
                 <label class="col-md-1 form-control-label">&nbsp;</label>
                  <div class="col-md-6">
                   <h4>Your Peformance Today :</h4>
                  </div>
                  <div class="col-md-4">
                    <h3 style="color: #004d99;"><?=$peformance?>%</h3>
                  </div>
                </div>

                <div class="form-group row">
                 <label class="col-md-1 form-control-label">&nbsp;</label>
                  <div class="col-md-6">
                    <h4>Grade :</h4>
                  </div>
                  <div class="col-md-4">
                    <h3 style="color: #004d99;"><?=$grade?></h3>
                  </div>
                </div>

              </div>
            </div>
            <!-- End Example Horizontal Form -->
          </div>

        </div>
      </div>
    </div>
  </div>

  <!-- **** Button Edit **** -->
  <div class="page-content" style="padding: 0px 20px;">
    <?php $no=1; foreach ($result_master_data as $val) { ?>
      <?php if ($edit_data->id_master == $val->id_master ) { ?>
        <a href="<?= base_url('frontend/production/display/'.$val->id_group_master.'/'.$val->id_master) ?>" name="add_more" id="add_more" class="btn btn-floating btn-warning btn-md waves-effect waves-light waves-round"><?= $no++ ?></a>
      <?php }else{ ?>
        <a href="<?= base_url('frontend/production/display/'.$val->id_group_master.'/'.$val->id_master) ?>" name="add_more" id="add_more" class="btn btn-floating btn-secondary btn-md waves-effect waves-light waves-round"><?= $no++ ?></a>
      <?php } ?>
    <?php } ?>
  </div><br>
  <!-- **** End Button Edit **** -->

  <div class="page-content" style="padding: 0px 20px;">
    <div class="panel">
      <div class="panel-body container-fluid" style="padding: 0px 20px;">
        <div class="row row-lg">
          <div class="col-md-12 col-lg-7">
            <!-- Example Horizontal Form -->
            <div class="example-wrap">
              <div class="example">

              <input type="text" class="form-control" value="<?=$edit_data->id_group_master?>" name="id_group" required placeholder="xxx" readonly hidden>
              <input type="text" class="form-control" value="<?=$edit_data->id_master?>" name="id_master" required placeholder="xxx" readonly hidden>
              <input type="text" class="form-control" value="<?=$employee_id?>" name="employee_id" required placeholder="xxx" readonly hidden>

                <div class="form-group row">
                  <label class="col-md-2 form-control-label"><b>Time</b><b style="color: red;">*</b> : </label>
                  <div class="col-md-3">
                    <b>Date Start :</b> <input id="DStart" type="datetime-local" class="form-control" name="date_start" value="<?=date("Y-m-d\TH:i:s", strtotime($edit_data->date_start))?>" required="required" placeholder="" autocomplete="off" onchange="Formula();return false;" style="background-color: #efc15d;" autofocus="autofocus" disabled/>
                  </div>
                  <div class="col-md-3">
                    <b>Date Finish :</b> <input id="DFinish" type="datetime-local" class="form-control" name="date_finish" value="<?=date("Y-m-d\TH:i:s", strtotime($edit_data->date_finish))?>" required="required" placeholder="" autocomplete="off" onchange="Formula();return false;" style="background-color: #efc15d;" disabled/>
                  </div>
                    <div class="col-md-3">
                      <table>
                        <tbody>
                          <tr>
                            <td>
                            <b>Hours</b><input id="IDHours" type="text" class="form-control readonly" name="t_hours" value="<?=$edit_data->t_hours?>" required placeholder="xx">
                            </td>
                            <td>
                              <b>Minute</b><input id="IDMinute" type="text" class="form-control readonly" name="t_minute" value="<?=$edit_data->t_minute?>" required placeholder="xx">
                            </td>
                            <td>
                              <input id="IDSecond" type="text" class="form-control readonly" name="t_second" value="<?=$edit_data->t_second?>" required placeholder="xx" hidden>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-2 form-control-label"><br><b>Machine No</b><b style="color: red;">*</b> : </label>
                    <div class="col-md-3">
                    <b>&nbsp;</b><select class="form-control" required="required"data-plugin="select2" id="MachnieNo" name="machine_no"data-placeholder="xxx" disabled>
                        <option value=""></option>
                        <?php foreach ($datamachine as $val) { ?>
                          <option class="MachnieOption" <?php if($edit_data->id_machine == $val->id_machine){ echo 'selected="selected"'; } ?> value="<?php echo $val->id_machine?>">
                            <?php echo "$val->line_mc_no$val->mc_no" ?>
                          </option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-md-3">
                      <b>Brand : </b><input type="text" class="form-control readonly" name="brand" value="<?=$edit_data->brand?>" required placeholder="xxx">
                    </div>
                    <div class="col-md-3">
                      <b>Tonnage : </b><input type="text" class="form-control readonly" name="tonnage" value="<?=$edit_data->tonnage?>" required placeholder="xxx">
                    </div>
                  </div>

                    <input id="DateStart" type="datetime-local" class="form-control" name="date_start" required="required" placeholder="" autocomplete="off" value="<?=date("Y-m-d\TH:i:s", strtotime($edit_data->date_start))?>" hidden/>
                    <input id="DateFinish" type="datetime-local" class="form-control" name="date_finish" required="required" placeholder="" autocomplete="off" value="<?=date("Y-m-d\TH:i:s", strtotime($edit_data->date_finish))?>" hidden/>
                    <input id="IDStart" type="time" class="form-control" name="time_start" value="<?=$edit_data->time_start?>" required="required" placeholder="--:--" autocomplete="off" hidden/>
                    <input id="IDFinish" type="time" class="form-control" name="time_finish" value="<?=$edit_data->time_finish?>" required="required" placeholder="--:--" autocomplete="off" hidden/>

                    <div class="form-group row">
                      <label class="col-md-2 form-control-label"><b>&nbsp;</b></label>
                      <div class="col-md-3">
                      </div>
                      <div class="col-md-6">
                        <table>
                          <tbody>
                          <tr>
                            <td>
                            <b>Hours</b><input id="IDHours" type="text" class="form-control readonly" name="t_hours" value="<?=$edit_data->t_hours?>" required placeholder="xx">
                            </td>
                            <td>
                              <b>Minute</b><input id="IDMinute" type="text" class="form-control readonly" name="t_minute" value="<?=$edit_data->t_minute?>" required placeholder="xx">
                            </td>
                            <td>
                              <b>Second</b><input id="IDSecond" type="text" class="form-control readonly" name="t_second" value="<?=$edit_data->t_second?>" required placeholder="xx">
                            </td>
                          </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-2 form-control-label"><br><b>Part Name</b><b style="color: red;">*</b> : </label>
                      <div class="col-md-3">
                        <b>&nbsp;</b><select onchange="partName(this);" class="form-control" required="required" data-plugin="select2" id="PartName" name="part_name" data-placeholder="Part Name" disabled>
                          <option value=""></option>
                          <?php foreach ($datatooling as $val) { ?>
                          <option <?php if($edit_data->part_name == $val->gih_tool_no){ echo 'selected="selected"'; } ?> value="<?php echo $val->gih_tool_no?>"> 
                            <?php echo " $val->tooling_name - $val->tooling_no " ?>
                          </option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-7">
                        <table>
                          <tbody>
                          <tr>
                            <td>
                            <b>No of Cavities</b> <input id="NoCavities" type="text" class="form-control readonly" name="no_of_cavities" value="<?=$edit_data->no_of_cavities?>" required placeholder="xxx">
                            </td>
                            <td>
                              <b>Cycle time/pcs</b> <input id="CycleTime" type="text" class="form-control readonly" name="cycle_time_pcs" value="<?=$edit_data->cycle_time_pcs?>" required placeholder="xxx">
                            </td>
                            <td>
                              <b>&nbsp;Part Weight</b> <input id="PartWeight" type="text" class="form-control readonly" name="part_weight" value="<?=$edit_data->part_weight?>" required placeholder="xxx" >
                            </td>
                            <td>
                              <b>Runner Weight</b> <input id="RunnerWeight" type="text" class="form-control readonly" name="runner_weight" value="<?=$edit_data->runner_weight?>" required placeholder="xxx">
                            </td>
                          </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-2 form-control-label"><b>Target (pcs)</b><b style="color: red;">*</b> : </label>
                      <div class="col-md-3">
                        <input id="Target" type="text" class="form-control readonly" name="target" value="<?=$edit_data->target?>" required placeholder="xxx">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-2 form-control-label"><br><b>Output</b><b style="color: red;">*</b> : </label>
                      <div class="col-md-3">
                        <b> Good (pcs): </b> <input id="IDGood" type="text" class="form-control input-text" name="good" value="<?=$edit_data->good?>" required placeholder="xxx" onkeypress="return hanyaAngka(event)" pattern="[0-9]*" inputmode="numeric" readonly><p id="Hgood"></p>
                      </div>
                      <div class="col-md-3">
                        <b> Reject (pcs): </b> <input id="IDReject" type="text" class="form-control input-text" name="reject" value="<?=$edit_data->reject?>" required placeholder="xxx" onkeypress="return hanyaAngka(event)" pattern="[0-9]*" inputmode="numeric" readonly><p id="HReject"></p>
                      </div>
                      <div class="col-md-3">
                        <b> Total (pcs): </b> <input id="IDtotalOutput" type="text" class="form-control" name="tot_output" value="<?=$edit_data->tot_output?>" required placeholder="xxx" onkeypress="return hanyaAngka(event)" readonly><p id="HtotalOutput"></p>
                      </div>
                    </div>

                  </div>
                </div>
                <!-- End Example Horizontal Form -->
              </div>
              <div class="col-md-12 col-lg-5">
                <!-- Example Horizontal Form -->
                <div class="example-wrap">
                  <div class="example">

                    <div class="form-group row" id="billdesc">
                      <label class="col-md-4 form-control-label"><b>Peformance of the Day</b><b style="color: red;">*</b> : </label>
                      <div class="col-md-5 TextPeformance">
                        <h3 style="color:#004d99;"><?=$edit_data->peformance_day?>%</h3>
                      </div>
                      <input id="IDPeformance" type="text" class="form-control" name="peformance_day" value="<?=$edit_data->peformance_day?>" required hidden>
                    </div>

                    <div class="form-group row form row">
                      <label class="col-md-2 form-control-label"><br><b>Target (g): </b></label>
                      <div class="col-md-9">
                        <table>
                          <tbody>
                          <tr>
                            <td>
                              <b>Lumps</b> <input id="TarLumps" type="text" class="form-control" name="tar_lumps" value="0" required placeholder="grams" readonly>
                            </td>
                            <td>
                              <b>Runner</b> <input id="TarRunner" type="text" class="form-control" name="tar_runner" value="<?=$edit_data->tar_runner?>" required placeholder="grams" readonly>
                            </td>
                            <td>
                              <b>Prod Qty</b> <input id="TarProdQty" type="text" class="form-control" name="tar_prod_qty" value="<?=$edit_data->tar_prod_qty?>" required placeholder="grams" readonly>
                            </td>
                            <td>
                              <b></b><h1>:</h1></b>
                            </td>
                            <td>
                              <b>Total</b> <input id="TotalTarget" type="text" class="form-control" name="tot_target" value="<?=$edit_data->tot_target?>" required placeholder="xxx" readonly>
                            </td>
                          </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div class="form-group row form row">
                      <label class="col-md-2 form-control-label"><b>Actual (g): </b></label>
                      <div class="col-md-9">
                        <table>
                          <tbody>
                          <tr>
                            <td>
                              <input id="ActLumps" type="text" class="form-control input-text" name="act_lumps" value="<?=$edit_data->act_lumps?>" required placeholder="grams" readonly>
                            </td>
                            <td>
                              <input id="ActRunner" type="text" class="form-control input-text" name="act_runner" value="<?=$edit_data->act_runner?>" required placeholder="grams" readonly>
                            </td>
                            <td>
                              <input id="ActProdQty" type="text" class="form-control" name="act_prod_qty" value="<?=$edit_data->act_prod_qty?>" required placeholder="grams" readonly>
                            </td>
                            <td>
                              <h1>:</h1></b>
                            </td>
                            <td>
                              <input id="TotalActual" type="text" class="form-control" name="tot_actual" value="<?=$edit_data->tot_actual?>" required placeholder="xxx" readonly>
                            </td>
                          </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div class="form-group row form row">
                      <label class="col-md-2 form-control-label"><b>Discrepancy (g): </b></label>
                      <div class="col-md-9">
                        <table>
                          <tbody>
                          <tr>
                            <td>
                              <input id="DissLumps" type="text" class="form-control" name="diss_lumps" value="<?=$edit_data->diss_lumps?>" required placeholder="grams" readonly>
                            </td>
                            <td>
                              <input id="DissRunner" type="text" class="form-control" name="diss_runner" value="<?=$edit_data->diss_runner?>" required placeholder="grams" readonly>
                            </td>
                            <td>
                              <input id="DissProdQty" type="text" class="form-control" name="diss_prod_qty" value="<?=$edit_data->diss_prod_qty?>" required placeholder="grams" readonly>
                            </td>
                            <td>
                              <h1>:</h1></b>
                            </td>
                            <td>
                              <input id="TotalDiss" type="text" class="form-control" name="tot_dissrapancy" value="<?=$edit_data->tot_dissrapancy?>" required placeholder="xxx" readonly>
                            </td>
                          </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                  </div>
                </div>
                <!-- End Example Horizontal Form -->
              </div>
              <!-- Button Action -->
              <!-- Button Action -->
            </div>
          </div>
        </div>
      </div>

 <!--  <div id="AddNewPage">
      
  </div> -->

</div>
<!-- End Page -->

<style>
  .readonly{background-color: #eee;}
</style>

<script>
  $(".readonly").on('keydown paste focus mousedown', function(e){
    if(e.keyCode != 9) // ignore tab
    e.preventDefault();
  });
</script>

<script>
  function hanyaAngka(event) {
    var angka = (event.which) ? event.which : event.keyCode
    if (angka != 46 && angka > 31 && (angka < 48 || angka > 57))
      return false;
    return true;
  }

  (function($) {
    $.fn.inputFilter = function(inputFilter) {
      return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
          this.value = "";
        }
      });
    };
  });
  $("#intTextBox").inputFilter(function(value) {
    return /^-?\d*$/.test(value); });
</script>

<script>
  $(function () {
    $("#UserOpt").change(function() {
      var val = $(this).val();
      console.log(val);
      // $('input[name="name_opt"]').val(val);
      $.ajax({
        method: 'POST',
        url: "<?= base_url("frontend/production/get_data_opt/")?>",
        data: {'id': val},
        cache: false,
        async : true,
        dataType : 'json',
          success: function(data){
            // console.log(data);
            // var html = '';
            // var i;
            $('input[name="name_opt"]').val(data.optname);
          }
      });
    });
  });

  $(function () {
    $("#MachnieNo").change(function() {
      var val = $(this).val();
      console.log(val);
      $.ajax({
        method: 'POST',
        url: "<?= base_url("frontend/production/get_data_mc/")?>",
        data: {'id': val},
        cache: false,
        async : true,
        dataType : 'json',
          success: function(data){
             $('input[name="brand"]').val(data.brand);
             $('input[name="tonnage"]').val(data.tonnage);
          }
      });
    });
  });  
</script>

<script>
  // var DateNow = ((new Date().getDate() < 10)?"0":"") + new Date().getDate() +"/"+(((new Date().getMonth()+1) < 10)?"0":"") + (new Date().getMonth()+1) +"/"+ new Date().getFullYear();
  var TimeStart     ="";
  var TimeFinish    ="";
  var second        ="";
  var mcyclepcs     = <?=$edit_data->cycle_time_pcs?>;
  var noofcavities  = <?=$edit_data->no_of_cavities?>;
  var Target        ="";
  var TotOutput     ="";
  var TotAllOutput  ="";

  function Formula()
  {
    DateStart  = document.getElementById('DStart').value;
    DateFinish = document.getElementById('DFinish').value;
    $('#DFinish').attr("min", DateStart);

    if (!$.trim(DateStart)){
      $('#DFinish').attr("readonly", true);
    }
    else{  
      $('#DFinish').attr("readonly", false);
    }
    TimeStart  = moment(DateStart).format("HH:mm");
    TimeFinish = moment(DateFinish).format("HH:mm");
    
    $('#DateStart').val(DateStart);
    $('#DateFinish').val(DateFinish);
    $('#IDStart').val(TimeStart);
    $('#IDFinish').val(TimeFinish);

    DateStart1   = moment(DateStart, 'YYYY-MM-DD hh:mm:ss');
    DateFinish2  = moment(DateFinish, 'YYYY-MM-DD hh:mm:ss');

    var hoursDiff = DateFinish2.diff(DateStart1, 'hours');
    console.log('Hours:' + hoursDiff);
    
    var minutesDiff = DateFinish2.diff(DateStart1, 'minutes');
    console.log('Minutes:' + minutesDiff);
    
    var secondsDiff = DateFinish2.diff(DateStart1, 'seconds');
    console.log('Seconds:' + secondsDiff);

    var totminute = hoursDiff*60;
    var minutetot = minutesDiff - totminute;

    $('#IDHours').val(hoursDiff);
    $('#IDMinute').val(minutetot);
    $('#IDSecond').val(secondsDiff);
    
    second = secondsDiff;

    if (isNaN(Target)) {//Part Name -> Target
      $('#Target').val(null);
    }else{
      Target = second/mcyclepcs*noofcavities;
      $('#Target').val(Math.round(Target));
    }

    $('#IDGood').val(null);//clear Good
    $('#IDReject').val(null);//clear Reject
    $('#IDtotalOutput').val(null);//clear Total Output
    $('#Hgood').html("");//clear % Good
    $('#HReject').html("");//clear % Reject
    $('#HtotalOutput').html("");//clear % Total Output
    $('#IDPeformance').val(null);//clear peformance day
    $('.TextPeformance').html("<h3>%</h3>");//clear peformance day

    $('#TarRunner').val(null);//Target Runner
    $('#TarProdQty').val(null);//Target ProdQty
    $('#TotalTarget').val(null);//Total Target

    $('#ActLumps').val(null);//Actual Lumps
    $('#ActRunner').val(null);//Actual Runner
    $('#ActProdQty').val(null);//Actual ProdQty
    $('#TotalActual').val(null);//Total Actual

    $('#DissLumps').val(null);//Discrepancy Lumps
    $('#DissRunner').val(null);//Discrepancy Runner
    $('#DissProdQty').val(null);//Discrepancy ProdQty
    $('#TotalDiss').val(null);//Total Discrepancy
  }

    // function start(ev, object)
    // {
    //   TimeStart = object.value;
    // }

    // function finish(ev, object)
    // {
    //  TimeFinish = object.value;          
    //     var timeStart = new Date("01/01/2021 " + TimeStart).getHours();
    //     var timeEnd   = new Date("01/01/2021 " + TimeFinish).getHours();

    //     var hourDiff = timeEnd - timeStart;
    //     if (hourDiff < 0) {
    //         hourDiff = 24 + hourDiff;
    //         minute    = hourDiff*60;
    //         second   = hourDiff*3600;
    //         // console.log(minute, 'if');
    //     }else{
    //      minute   = hourDiff*60;
    //      second   = hourDiff*3600;
    //      // console.log(minute, 'else');
    //     }
    //     $('#IDHours').val(hourDiff);
    //     $('#IDMinute').val(minute);
    //     $('#IDSecond').val(second);
    // }

  function partName(selectObject) 
  {
    var val = selectObject.value;
    $.ajax({
      method: 'POST',
      url: "<?= base_url("frontend/production/data_tooling/")?>",
      data: {'id': val},
      cache: false,
      async : true,
      dataType : 'json',
        success: function(data){
          console.log(data.no_of_cavities);
          $('#NoCavities').val(data.no_of_cavities);
          $('#CycleTime').val(data.mold_cycle_pcs);
          $('#PartWeight').val(data.part_weight);
          $('#RunnerWeight').val(data.runner_weight);

          noofcavities  = data.no_of_cavities;
          mcyclepcs     = data.mold_cycle_pcs;
          pweight       = data.part_weight;
          rweight       = data.runner_weight;
            
          if (second=="") {
            second = <?=$edit_data->t_second?>;
          }
          // Target = second/mcyclepcs*noofcavities;
          // console.log(Target,'Target Part')
          // $('#Target').val(Math.round(Target));
          
          if (isNaN(Target)) {//Part Name -> Target
            $('#Target').val(null);
          }else{
            Target = second/mcyclepcs*noofcavities;
            $('#Target').val(Math.round(Target));
          }
            
          $('#IDLumps').val("0");
          // $('#IDRunner').val(data.runner_weight);

          $('#IDGood').val(null);//clear Good
          $('#IDReject').val(null);//clear Reject
          $('#IDtotalOutput').val(null);//clear Total Output
          $('#Hgood').html("");//clear % Good
          $('#HReject').html("");//clear % Reject
          $('#HtotalOutput').html("");//clear % Total Output
          $('#IDPeformance').val(null);//clear peformance day
          $('.TextPeformance').html("<h3>%</h3>");//clear peformance day

          $('#TarRunner').val(null);//Target Runner
          $('#TarProdQty').val(null);//Target ProdQty
          $('#TotalTarget').val(null);//Total Target

          $('#ActLumps').val(null);//Actual Lumps
          $('#ActRunner').val(null);//Actual Runner
          $('#ActProdQty').val(null);//Actual ProdQty
          $('#TotalActual').val(null);//Total Actual

          $('#DissLumps').val(null);//Discrepancy Lumps
          $('#DissRunner').val(null);//Discrepancy Runner
          $('#DissProdQty').val(null);//Discrepancy ProdQty
          $('#TotalDiss').val(null);//Total Discrepancy 
        }
    }); 
  }

  var ValGood   ="";
  var ValReject ="";
  var PerGood  = <?=$edit_data->good/$edit_data->target?>*100;
  $('#Hgood').html("<h4><b style='color:#004d99'>"+ Math.round(PerGood) +"%</b></g4>");
  $("#IDGood").keyup(function(){
    ValGood    = this.value;
    if (Target=="") {
      Target = <?=$edit_data->target?>;
      console.log(Target,'Target Good')
    }
    PerGood  = ValGood/Target*100;
    $('#Hgood').html("<h4><b style='color:#004d99'>" + Math.round(PerGood) + "%</b></h4>");

    $('#IDReject').val(null);//clear Reject
    $('#IDtotalOutput').val(null);//clear Total Output

    $('#TarRunner').val(null);//Target Runner
    $('#TarProdQty').val(null);//Target ProdQty
    $('#TotalTarget').val(null);//Total Target

    $('#ActLumps').val(null);//Actual Lumps
    $('#ActRunner').val(null);//Actual Runner
    $('#ActProdQty').val(null);//Actual ProdQty
    $('#TotalActual').val(null);//Total Actual

    $('#DissLumps').val(null);//Discrepancy Lumps
    $('#DissRunner').val(null);//Discrepancy Runner
    $('#DissProdQty').val(null);//Discrepancy ProdQty
    $('#TotalDiss').val(null);//Total Discrepancy
  });

  var TRunner     ='';
  var TarProdQty  ='';
  var rweight     ='';
  var pweight     ='';
  var TotProdQty  ='';
  var PerReject   = <?=$edit_data->reject/$edit_data->target?>*100;
  $('#HReject').html("<h4><b style='color:#004d99'>"+ Math.round(PerReject) +"%</b></h4>");

  var TotAllOutput = <?=$edit_data->tot_output/$edit_data->target?>*100;
  $('#HtotalOutput').html("<h4><b style='color:#004d99'>" + Math.round(TotAllOutput) + "%</b></h4>");

  $("#IDReject").keyup(function(){
    ValReject   = this.value;
    if (Target=="") {
      Target = <?=$edit_data->target?>;
    }
    var PerReject = ValReject/Target*100;
    $('#HReject').html("<h4><b style='color:#004d99'>" + Math.round(PerReject) + "%</b></h4>");
    if (ValGood=="") {
      ValGood = <?=$edit_data->good?>;
    }
    TotOutput    = parseInt(ValGood)+parseInt(ValReject);
    TotAllOutput = TotOutput/Target*100;
    $('#IDtotalOutput').val(TotOutput);
    $('#HtotalOutput').html("<h4><b style='color:#004d99'>" + Math.round(TotAllOutput) + "%</b></h4>");
    $('#IDPeformance').val(Math.round(PerGood));
    $('.TextPeformance').html("<h3><b style='color:#004d99'>" + Math.round(PerGood) + "%</b></h3>");
    // ** Tabel Material **
    // ##**Target**
    if (rweight=="" || pweight =="") {
      rweight = <?=$edit_data->runner_weight?>;
      pweight = <?=$edit_data->part_weight?>;
    }
    TRunner     = TotOutput/noofcavities*rweight;//Target Runner
    $('#TarRunner').val(TRunner);
    TarProdQty  = TotOutput*pweight;//Target Prod Qty
    $('#TarProdQty').val(Math.round(TarProdQty));
    var TotalTarget = TRunner+TarProdQty;//Target Total
    $('#TotalTarget').val(Math.round(TotalTarget));
    // ##**End Target**

    $('#ActProdQty').val(Math.round(TarProdQty));// ##**Actual**

    TotProdQty = TarProdQty - TarProdQty;
    $('#DissProdQty').val(TotProdQty);// ##**Disscrapancy **

    // clear Actual Lumps & Runner
    $('#ActLumps').val(null);
    $('#ActRunner').val(null);
    $('#DissLumps').val(null);
    $('#DissRunner').val(null);
    // ** End Tabel Material **
  });

  var ValActLumps  ='';
  var ValActRunner ='';
  var TotActLumps  ='';//ActLumps
  var TotDissRunner ='';//Total Act Runner
  $("#ActLumps").keyup(function(){//Actual Lumps
    ValActLumps    = this.value;
    TotActLumps    = ValActLumps;
    $('#DissLumps').val(TotActLumps);//value input Disscrapancy

    if (ValActRunner=="" || TarProdQty=="" || TotDissRunner=="") {
      ValActRunner  = <?=$edit_data->act_runner?>;
      TarProdQty    = <?=$edit_data->tar_prod_qty?>;
      TotDissRunner  = <?=$edit_data->diss_runner?>;
    }
    var TotAct  = parseInt(ValActLumps)+parseInt(ValActRunner)+parseInt(Math.round(TarProdQty));
    $('#TotalActual').val(TotAct);//Total Actual

    var TotDiss = parseInt(TotActLumps)+parseInt(TotDissRunner)+parseInt(Math.round(TotProdQty));
    console.log(TotDiss,'Total Diss')
    $('#TotalDiss').val(TotDiss);//Total Discrepancy
  });

  $("#ActRunner").keyup(function(){//Actual Runner
    ValActRunner  = this.value;
    if (TRunner=="" || ValActLumps=="" || TarProdQty=="" || TotActLumps=="") {
      TRunner     = <?=$edit_data->tar_runner?>;
      ValActLumps = <?=$edit_data->act_lumps?>;
      TarProdQty  = <?=$edit_data->tar_prod_qty?>;
      TotActLumps = <?=$edit_data->act_lumps?>;
    }
    TotDissRunner = TRunner-ValActRunner;
    $('#DissRunner').val(TotDissRunner);//Discrepancy Runner

    var TotAct  = parseInt(ValActLumps)+parseInt(ValActRunner)+parseInt(Math.round(TarProdQty));
    $('#TotalActual').val(TotAct);//Total Actual
    console.log(TarProdQty,'Total Diss')
    var TotDiss = parseInt(TotActLumps)+parseInt(TotDissRunner)+parseInt(Math.round(TotProdQty));
    $('#TotalDiss').val(TotDiss);
  });
</script>

<script type="text/javascript">//Acceptance
  $(".finish").click(function(){
    var id = $(this).attr("data-bind");
    swal({
      title: "are you sure it's finished ?",
      text: "",
      type: "info",
      showCancelButton: true,
      confirmButtonClass: "btn-warning",
      confirmButtonText: "Yes",
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          // url: '<?= base_url("backend/main_menu/change_masterdata/")?>'+id,
          type: 'DELETE',
          error: function() {
            alert('Something is wrong');
          },
          success: function(data) {
            $("#"+id).remove();
              // swal("Deleted!", "Your imaginary file has been deleted.", "success");
              window.location.href = '<?= base_url("frontend/production/finish")?>';
          }
        });
      } else {
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
    });
  });
</script>
