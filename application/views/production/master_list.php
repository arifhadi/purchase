<br>
<div class="page">
  <div class="page-header" style="padding: 0px;">
    <ol class="breadcrumb">
      <a href="<?php echo base_url('dashboard'); ?>" class="btn btn-round btn-info"><i class="icon md-home" aria-hidden="true"></i>Dashboard</a>
    </ol>
  </div>
  <h3 class="panel-title" style="text-align: center; padding: 0px;"><b>YOUR PRODUCTIVITY OUTPUT</b></h3><br>
      <div class="page-content" style="padding: 0px;">
        <div class="panel"><br>
          <div style="text-align: left;">
            <div class="px-4 bg-light ">
          <marquee class="py-3"><b>Lates Updated : <?=$last_update[0]->last?></b></marquee>
        </div>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn-sm btn-primary" data-target="#filterdata" data-toggle="modal" type="button"><i class="icon md-filter-list" aria-hidden="true"></i> Filter &nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;
             <a href="<?php echo base_url('frontend/production/daily_chart'); ?>" class="btn btn-warning"><i class="zmdi zmdi-chart" aria-hidden="true"></i>&nbsp;&nbsp;View Chart</a>
          </div>
          <div class="panel-body">
            <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
              <?php if (!empty($data_emp)) { ?>
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Time Start</th>
                    <th>Time Finish</th>
                    <th>Machine</th>
                    <th>Part Name</th>
                    <th>Good</th>
                    <th>Reject</th>
                    <th>Total Output</th>
                    <th>Target</th>
                    <th>Peformance</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php $no=1; foreach ($data_emp as $val) { ?>
                  <tr>
                    <td><?= $no++ ?></td>
                     <td><?= $val->name_emp ?></td>
                    <td><?= $val->date_report ?></td>
                    <td><?= $val->start_time ?></td>
                    <td><?= $val->finish_time ?></td>
                    <td><?= $val->brand."-".$val->tonnage ?></td>
                    <td><?= $val->part_name ?></td>
                    <td><?= $val->tot_good ?></td>
                    <td><?=$val->tot_reject?></td>
                    <td><?=$val->output_tot?></td>
                    <td><?=$val->target?></td>
                    <!-- <td><?=$val->p_avarge?>%</td> -->
                    <?php if ($val->peformance >= '95') {
                      echo"<td style='color: green'>".$val->peformance."%</td>";
                    }elseif ($val->peformance >= '90' && $val->peformance <= '94' ) {
                       echo"<td>".$val->peformance."%</td>";
                    }elseif ($val->peformance >= '80' && $val->peformance <= '89') {
                       echo"<td style='color: red'>".$val->peformance."%</td>";
                    }elseif ($val->peformance <= '79') {
                       echo"<td style='color: red'><b>".$val->peformance."%</b></td>";
                    } ?>
                    <td>
                        
                      <a href="<?php echo base_url('frontend/production/display/'.$val->id_group_master.'/'.$val->id_master);?>" data-toggle="tooltip" class="btn btn-floating btn-info btn-xs" title="Display"><i class="icon md-assignment-check" aria-hidden="true"></i></a>

                      <button data-bind="<?=$val->id_group_master?>" type="button" data-toggle="tooltip" class="btn btn-floating btn-success btn-xs change" title="Change"><i class="icon md-edit" aria-hidden="true"></i></button>

                    </td>
                  </tr>
                <?php } ?>
                </tbody>
                <tfoot>
            <tr>
                <th colspan="7"><b>SUM</b></th>
                <th><b><?=$data_sum[0]->good?></b></th>
                <th><b><?=$data_sum[0]->tot_reject?></b></th>
                <th><b><?=$data_sum[0]->output_tot?></b></th>
                 <th colspan="3"><b><?=$data_sum[0]->target?></b></th>
            </tr>
        </tfoot>
              <?php }else{?><!-- //jika data kosong -->
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Time Start</th>
                    <th>Time Finish</th>
                    <th>Machine</th>
                    <th>Part Name</th>
                    <th>Good</th>
                    <th>Reject</th>
                    <th>Total Output</th>
                    <th>Peformance</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Data Not Found</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              <?php } ?>
            </table>
          </div>
        </div>
    </div>
</div>

<!-- Modal -->
  <div class="modal fade" id="filterdata" aria-hidden="true" aria-labelledby="filterdata" role="dialog">
    <div class="modal-dialog modal-simple modal-center">
      <div class="modal-content">
         <?= form_open(base_url('frontend/production/filter_report_p'),  'id="login_validation" enctype="multipart/form-data"') ?>
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="icon md-close-circle" aria-hidden="true" style="color: red;"></i>
          </button>
        </div>
        <h4 class="modal-title" style="text-align: center; color: #00bfff;"><b>FILTER DATA</b></h4>
        <div class="modal-body">
          <div class="example-grid">
            <div class="row">
              <!-- <div class="col-lg-6" style="text-align: center;"> -->
              <!--   <div class="col-md-12">
                  <h5 style="text-align: left;">Employee ID :</h5>
                  <select class="form-control" required="required" data-plugin="select2" id="UserOpt" name="employee_no" data-placeholder="Select your ID">
                    <?php foreach ($data_opt as $val) { ?>
                      <option value=""></option>
                      <option class="nameOption" value="<?php echo $val->employee_no?>">
                       <?php echo "$val->employee_no" ?>
                      </option>
                    <?php } ?>
                   </select>
                </div> -->
              <!-- </div> -->
           <!--    <div class="col-lg-6" style="text-align: center;">
                <div class="col-md-12" id="tes">
                  <h5 style="text-align: left;">Your Name :</h5>
                  <input type="text" class="form-control" name="name_opt" required placeholder="xxx" readonly> 
                </div>
              </div> -->

              <!-- <br><br> -->

              <div class="col-lg-6" style="text-align: center;">
                <div class="col-md-12">
                  <h5 style="text-align: left;">From :</h5>
                  <input id="IDStart" type="date" class="form-control" name="form_date" required="required" placeholder="--:--" autocomplete="off" style="background-color: #efc15d; padding:0px;"/>
                </div>
              </div>

              <div class="col-lg-6" style="text-align: center;">
                <div class="col-md-12">
                  <h5 style="text-align: left;">To :</h5>
                  <input id="IDStart" type="date" class="form-control" name="to_date" required="required" placeholder="--:--" autocomplete="off" style="background-color: #efc15d;"/>
                </div>
              </div>

            </div>
          </div>

          <br>
          <!-- <button type="button" class="btn btn-icon btn-primary waves-effect waves-light waves-round checkall"><i class="icon md-apps" aria-hidden="true"></i> Select </button> -->
          <div class="example-grid">
            <div class="row">
              <div class="col-lg-4">
                <div class="form-group row form-material">
                  <div class="col-xl-12 col-md-9">
                    <div class="d-flex flex-column">
                      <!-- <div class="checkbox-custom checkbox-primary">
                        <input class="checkhour" type="checkbox" id="tooling_name" name="tooling_name" value="tooling_name">
                        <label for="tooling_name">Tooling Name</label>
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-group row form-material">
                  <div class="col-xl-12 col-md-9">
                    <div class="d-flex flex-column">
                      <!-- <div class="checkbox-custom checkbox-primary">
                        <input class="checkhour" type="checkbox" id="drawing_no" name="drawing_no" value="drawing_no">
                        <label for="drawing_no">Drawing No</label>
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-group row form-material">
                  <div class="col-xl-12 col-md-9">
                    <div class="d-flex flex-column">
                      <!-- <div class="checkbox-custom checkbox-primary">
                        <input class="checkhour" type="checkbox" id="customers" name="customers" value="customers">
                        <label for="customers">Customers (Short Text)</label>
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <div class="modal-footer">
        <button type="Submit" class="btn btn-success btn-sm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Submit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php form_close() ?>
      </div>
    </div>
  </div>
<!-- End Modal -->
<style>
  .select2-search input { background-color: #ccf5ff; }
  .select2-results { background-color: #ccf5ff; }
  .select2-selection{background-color:#99ebff !important;}
  .input-text{background-color:#99ebff;}
</style>

<script>
  var clicked = false;
  $(".checkall").on("click", function() {
    $(".checkhour").prop("checked", !clicked);
    clicked = !clicked;
    this.innerHTML = clicked ? 'Deselect' : 'Select';
  });
</script>

<script>
  $(function () {
    $("#UserOpt").change(function() {
      var val = $(this).val();
      // console.log(val);
      // $('input[name="name_opt"]').val(val);
      $.ajax({
        method: 'POST',
        url: "<?= base_url("frontend/production/get_data_opt/")?>",
        data: {'id': val},
        cache: false,
        async : true,
        dataType : 'json',
        success: function(data){
          // console.log(data);
          // var html = '';
          // var i;
          $('input[name="name_opt"]').val(data.optname);
        }
      });
    });
  });
</script>

<script type="text/javascript">//Change Data
    $(".change").click(function(){
        var id = $(this).attr("data-bind");
    
       swal({
        title: "you want to change the data?",
        text: "",
        type: "info",
        showCancelButton: true,
        confirmButtonClass: "btn-info",
        confirmButtonText: "Yes",
        cancelButtonText: "Cancel",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
             url: '<?= base_url("auth/login")?>',
             type: 'DELETE',
             error: function() {
                alert('Something is wrong');
             },
             success: function(data) {
                  $("#"+id).remove();
                  // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                  window.location.href = '<?= base_url("auth/login")?>';
             }
          });
        } else {
          swal("Cancelled", "Your imaginary file is safe :)", "error");
        }
      });
    });
</script>