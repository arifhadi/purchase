<?php if (!defined('BASEPATH')) { exit ('No Direct Script Allowed'); }

class Currency extends CI_Controller {

	public function __construct(){
		parent::__construct();
	    if(!$this->ion_auth->logged_in()){
	      redirect('auth/login', 'refresh');
	    }
	    date_default_timezone_set('Asia/Jakarta');
        $this->load->model('m_currency');
        $this->load->helper('download');
	}

	public function menu_currency()
	{
		$this->admintemp->view('backend/currency/menu_currency');
	}

	public function list_master_data_currency()
	{
		$this->data['cur'] = $this->m_currency->get_data_currency();
		$this->admintemp->view('backend/currency/list_master_data_currency',$this->data);
	}

	public function create_master_currency()
	{


		// $myXMLData = file_get_contents("https://www.bi.go.id/biwebservice/wskursbi.asmx/getSubKursLokal3?mts=usd&startdate=2023-01-04&enddate=2023-01-05");

		// $myParse = new SimpleXMLElement('https://www.bi.go.id/biwebservice/wskursbi.asmx/getSubKursLokal3?mts=usd&startdate=2023-01-04&enddate=2023-01-04',null,true);

		// // $simpleXml = simplexml_load_string($myXMLData) or die("Error: Cannot create encode data to xml object");

		// $split_string = substr($myXMLData, 1);

		// log_r($myXMLData);

		// log_r($myXMLData);
		// echo $myXMLData;

		// $url ='https://www.bi.go.id/biwebservice/wskursbi.asmx/getSubKursLokal3?mts=usd&startdate=2023-01-04&enddate=2023-01-05';
 	// 	 $tem = file_get_contents($url);
 	// 	 $string = explode(" ", $tem);


		$hariIni        = new DateTime();
        if (empty($this->input->post())) {
        	// $this->data['xml'] = $string;
            $this->admintemp->view('backend/currency/create_currency');
        }elseif (!empty($this->input->post())) {
            $code       = $this->input->post('code');
            $country    = $this->input->post('country');
           
            $data = array(
                'code'       => $code,
                'country'       => $country,
                'create_at'  => $hariIni->format('y-m-d H:i:s'),
                'user_id'    => USER_ID,
            );
            $status = $this->m_currency->insert_currency($data);
            if ($status == 1) {//Jika Success Insert
                $this->session->set_flashdata('success', 'Your data successfully added !');
                redirect('backend/currency/create_master_currency');
            }else if($status == 'error'){
                $this->session->set_flashdata('error', 'Code is available, please make a unique one !');
                redirect('backend/currency/create_master_currency');
            }
        }
	}

}