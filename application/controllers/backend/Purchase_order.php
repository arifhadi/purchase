<?php if (!defined('BASEPATH')) { exit ('No Direct Script Allowed'); }

class Purchase_order extends CI_Controller {

	public function __construct(){

		parent::__construct();
	    if(!$this->ion_auth->logged_in()){
	      redirect('auth/login', 'refresh');
	    }
	    date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('m_po');
        $this->load->model('m_purchase');
        $this->load->model('m_user');
        $this->load->helper('download');
	}

	public function list_po_release()
	{
    	$this->data['po'] = $this->m_po->get_data_all_po();
        // log_r($this->data['po']);
        $get_ttl_po_unrelease = $this->m_po->ttl_unrelease_po();
        $this->data['ttl_po_unrelease'] = $get_ttl_po_unrelease->ttl_po_unrelease;
        // log_r($this->data['ttl_po_unrelease']);
    	$this->admintemp->view('backend/purchaseorder/list_data_po',$this->data);
	}

    public function list_po_unrelease()
    {
        $this->data['po'] = $this->m_po->get_data_all_po_unrelease();
        // log_r($this->data['po']);
        $get_ttl_po_unrelease = $this->m_po->ttl_unrelease_po();
        $this->data['ttl_po_unrelease'] = $get_ttl_po_unrelease->ttl_po_unrelease;
        $this->admintemp->view('backend/purchaseorder/list_data_po_unrelease',$this->data);
    }

	public function release_po()
	{	
		$hariIni        = new DateTime();
		$po_nos = '0';

      

		if (empty($this->input->post())) {

            $this->data['pr_list'] = $this->m_po->get_data_list_pr();
            $this->data['edit_data'] = $this->m_po->get_data_all_po_row($po_nos);
            $this->admintemp->view('backend/purchaseorder/release_po',$this->data);
        }elseif (!empty($this->input->post())) {
            $format_pr =  $hariIni->format('d-m-y');
            $digit_pr = substr($format_pr, -2);
            $ribuan_pr = '000';
            $cek_po = $this->m_purchase->get_last_po_no_detail();

            if(empty($cek_po))
            {   
                $po_no_temps = $digit_pr . $ribuan_pr;
            }
            else
            {
                $last_pr = $cek_po->po_no_temp;
                $digit_last =substr($last_pr, 0,2);
                if($digit_pr !== $digit_last)
                {
                    $po_no_temps = $digit_pr . $ribuan_pr;
                }
                else
                {   
                    $po_no_temps = $cek_po->po_no_temp + 1;
                }
            }
            $po_no_digit_last = substr($po_no_temps, 2);
            $po_no = $digit_pr ."-". $po_no_digit_last;

            $pr_no    =$this->input->post('pr_no');

            $get_id_item = $this->m_po->get_data_id_item($pr_no);
            $id_item = $get_id_item->description;
            $get_data_lo_cd = $this->m_po->get_detail_pr_item($pr_no);


            if(empty($get_data_lo_cd))
            {
                $get_data_pr_ud = $this->m_po->get_data_pr_ud($pr_no);
                $data_lo = array(
                    'id_item' => $id_item,
                    'qty' => $get_data_pr_ud->new_order_qty,
                    'unit_price_last_order' => $get_data_pr_ud->unit_price_pf,
                    'is_last_order'=>1,
                    'currency' => $get_data_pr_ud->currency,
                    'supplier' => $get_data_pr_ud->supplier_name_pf,
                    );
                $status_lo = $this->m_po->update_last_order($id_item,$data_lo);

                $data_history_lo = array(
                        'id'=>$cek_his_lo->id,
                        'qty' => $get_data_pr_ud->new_order_qty,
                        'unit_price' => $get_data_pr_ud->unit_price_pf,
                        'currency' => $get_data_pr_ud->currency,
                        'supplier' => $get_data_pr_ud->supplier_name_pf,
                    );

                    $status_his_lo = $this->m_po->update_last_order_history($cek_his_lo->id,$data_history_lo);

            }
            else{

              if(!empty($get_data_lo_cd->new_order_qty))
            {

                 $data_lo = array(
                    'id_item' => $id_item,
                    'qty' => $get_data_lo_cd->new_order_qty,
                    'unit_price_last_order' => $get_data_lo_cd->unit_price,
                    'is_last_order'=>1,
                    'currency' => $get_data_lo_cd->currency,
                    'supplier' => $get_data_lo_cd->supplier_code,
                    );
                $status_lo = $this->m_po->update_last_order($id_item,$data_lo);
            }

            $cek_his_lo = $this->m_po->cek_his_lo($id_item);

            if(empty($cek_his_lo->unit_price))
                {
                    $data_history_lo = array(
                        'id'=>$cek_his_lo->id,
                        'qty' => $get_data_lo_cd->new_order_qty,
                        'unit_price' => $get_data_lo_cd->unit_price,
                        'currency' => $get_data_lo_cd->currency,
                        'supplier' => $get_data_lo_cd->supplier_code,
                    );

                    $status_his_lo = $this->m_po->update_last_order_history($cek_his_lo->id,$data_history_lo);

                }
            }

            $data = array(
                'pr_no'       =>$pr_no,
                'po_no'       => $po_no,
                'po_no_temp'       => $po_no_temps,
                'create_at'  => $hariIni->format('y-m-d H:i:s'),
                'user_id'    => USER_ID,
            );
            $data_pr = array(
                'pr_no' => $pr_no,
                'is_release_po'=>1,
             );


            

            $this->db->set($data);
            $status = $this->m_po->insert_po($data);
            $status_po = $this->m_po->update_po_release($pr_no,$data_pr);
            
            if ($status == 1) {//Jika Success Insert
                $this->session->set_flashdata('success', 'Your data successfully Created !');
                $this->update_po($pr_no,$po_no,$po_no_temps);
            }else if($status == 'error'){
                $this->session->set_flashdata('error', 'Code is available, please make a unique one !');
                $this->update_po($pr_no,$po_no,$po_no_temps);
            }


        }


	}

    public function update_po($pr_no,$po_no='',$po_no_temps='')
    {
            $this->data['po_no'] = $po_no;
            $this->data['po_no_temps'] = $po_no_temps;
            $this->data['pr_list'] = $this->m_po->get_data_list_pr();
            $this->data['edit_data'] = $this->m_po->get_data_all_po_row($po_no);
            // $this->data['p']
            // $sup_code = $this->m_po->get_same_sup($pr_no);
            $this->admintemp->view('backend/purchaseorder/release_po_update',$this->data);
    }

    public function update_po_no($po_no='',$po_no_temps='')
    {
        $hariIni        = new DateTime();
        $pr_no    =$this->input->post('pr_no');
        $po_no    =$this->input->post('po_no');
        $po_no_temps    =$this->input->post('po_no_temps');
        $data = array(
            'pr_no'       =>$pr_no,
            'po_no'       => $po_no,
            'po_no_temp'  => $po_no_temps,
            'create_at'  => $hariIni->format('y-m-d H:i:s'),
            'user_id'    => USER_ID,
        );

        $get_id_item = $this->m_po->get_data_id_item($pr_no);
        $id_item = $get_id_item->description;
        $get_data_lo_cd = $this->m_po->get_detail_pr_item($pr_no);


        if(!empty($get_data_lo_cd->new_order_qty))
        {

             $data_lo = array(
                'id_item' => $id_item,
                'qty' => $get_data_lo_cd->new_order_qty,
                'unit_price_last_order' => $get_data_lo_cd->unit_price,
                'is_last_order'=>1,
                'currency' => $get_data_lo_cd->currency,
                'supplier' => $get_data_lo_cd->supplier_code,
            );
             $status_lo = $this->m_po->update_last_order($id_item,$data_lo);
        }
       
            $cek_his_lo = $this->m_po->cek_his_lo($id_item);

            if(empty($cek_his_lo->unit_price))
            {
                $data_history_lo = array(
                    'id'=>$cek_his_lo->id,
                    'qty' => $get_data_lo_cd->new_order_qty,
                    'unit_price' => $get_data_lo_cd->unit_price,
                    'currency' => $get_data_lo_cd->currency,
                    'supplier' => $get_data_lo_cd->supplier_code,
                );

                $status_his_lo = $this->m_po->update_last_order_history($cek_his_lo->id,$data_history_lo);

            }

        $data_pr = array(
            'pr_no' => $pr_no,
            'is_release_po'=>1,
        );


        $this->db->set($data);
        $status = $this->m_po->insert_po($data);
        $status_po = $this->m_po->update_po_release($pr_no,$data_pr);
        $this->update_po($po_no,$po_no_temps);
    }

    public function view_print_po($po_no='')
    {
        $get_data_print = $this->m_po->get_data_print_po($po_no);
        $this->data['name_sup'] = $get_data_print[0]->name_sup;
        $this->data['address'] = $get_data_print[0]->address;
        $this->data['address_2'] = $get_data_print[0]->address_2;
        $this->data['email'] = $get_data_print[0]->email;
        $this->data['telpon'] = $get_data_print[0]->telpon;
        $this->data['attn'] = $get_data_print[0]->attn;
        $this->data['po_no'] = $get_data_print[0]->po_no;
        $this->data['payment_term'] = $get_data_print[0]->payment_term;
        $this->data['incoterm'] = $get_data_print[0]->incoterm;


        $this->data['data_po'] = $get_data_print;
        $grand_total = $this->m_po->sum_grand_total_amount_po($po_no);
        $this->data['grand_total'] = $grand_total->total_amount_grand;
        $this->data['grand_total_qty'] = $grand_total->grand_total_qty;
        // log_r($get_data_print);
         $this->data['edit_data'] = $this->m_po->get_data_latter_head();
        $this->usertemp->view('backend/purchaseorder/print_po',$this->data);
    }

    public function export_to_pdf_po($po_no='')
    {   
         $get_data_print = $this->m_po->get_data_print_po($po_no);
        $this->data['name_sup'] = $get_data_print[0]->name_sup;
        $this->data['address'] = $get_data_print[0]->address;
        $this->data['address_2'] = $get_data_print[0]->address_2;
        $this->data['email'] = $get_data_print[0]->email;
        $this->data['telpon'] = $get_data_print[0]->telpon;
        $this->data['attn'] = $get_data_print[0]->attn;
        $this->data['po_no'] = $get_data_print[0]->po_no;
        $this->data['payment_term'] = $get_data_print[0]->payment_term;
        $this->data['incoterm'] = $get_data_print[0]->incoterm;

        $this->data['data_po'] = $get_data_print;
        $grand_total = $this->m_po->sum_grand_total_amount_po($po_no);
        $this->data['grand_total'] = $grand_total->total_amount_grand;
        $this->data['grand_total_qty'] = $grand_total->grand_total_qty;
         $this->data['edit_data'] = $this->m_po->get_data_latter_head();
        // $this->usertemp->view('backend/purchaseorder/print_po_test',$this->data);




        $this->load->library('pdfgenerator');
        
        // title dari pdf
        $this->data['title_pdf'] = 'PO GIH'.'_'.$po_no;
        
        // filename dari pdf ketika didownload
        $file_pdf = 'PO GIH'.'_'.$po_no;
        // setting paper
        $paper = 'A4';
        //orientasi paper potrait / landscape
        $orientation = "portrait";
        
        $html = $this->load->view('backend/purchaseorder/print_po_test',$this->data, true);     
        
        // run dompdf
        $this->pdfgenerator->generate($html, $file_pdf,$paper,$orientation);


    }


    public function export_to_excel_po($po_no='')
    {   
        $get_data_print = $this->m_po->get_data_print_po($po_no);
        $grand_total = $this->m_po->sum_grand_total_amount_po($po_no);
        $content = $this->load->view('backend/purchaseorder/print_po_to_excel', array (
            'name_sup'   => $get_data_print[0]->name_sup,
            'address' => $get_data_print[0]->address,
            'address_2' => $get_data_print[0]->address_2,
            'email' => $get_data_print[0]->email,
            'telpon' => $get_data_print[0]->telpon,
            'attn' =>$get_data_print[0]->attn,
            'po_no' => $get_data_print[0]->po_no,
            'payment_term' =>$get_data_print[0]->payment_term,
            'incoterm' =>$get_data_print[0]->incoterm,
            'data_po' =>$get_data_print,
            'grand_total' =>$grand_total->total_amount_grand,
            'grand_total_qty' =>$grand_total->grand_total_qty,
        ), TRUE);
        excel_header('PO_GIH'.$po_no.'.xls');
        echo $content;
    }


    public function save_head_latter()
    {
            $hariIni        = new DateTime();
        if (empty($this->input->post())) {
            $this->data['edit_data'] = $this->m_po->get_data_latter_head();

            // log_r($this->data['edit_data']);
            $this->admintemp->view('backend/purchaseorder/settingkophead',$this->data);
        }elseif (!empty($this->input->post())) {

            $old_file_head_latter = $this->input->post('old_file_head_latter');



            $config['upload_path'] = './src/assets/images/file_head_latter/';
            $config['allowed_types'] = 'JPEG|jpg|png|jpeg';
            $config['file_name']    = $_FILES['file_head_latter']['name'];
            $config['max_size']         = 2097152;
            $config['max_width']        = 19200;
            $config['max_height']       = 12800;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);



            if ( ! $this->upload->do_upload('file_head_latter')){
                $file_head_latter = $old_file_head_latter;

            }else{
                $file_head_latter = $this->upload->data('file_name');
            }

            $data = array(
                'file_head_latter'       =>$file_head_latter,
                'create_at'  => $hariIni->format('y-m-d H:i:s'),
                'user_id'    => USER_ID,
            );

            
            $this->db->set($data);
            $status = $this->m_po->save_head_kop($data);
            if ($status == 1) {//Jika Success Insert
                $this->session->set_flashdata('success', 'Your data successfully Updated !');
                redirect('backend/purchase_order/save_head_latter');
            }else if($status == 'error'){
                $this->session->set_flashdata('error', 'Code is available, please make a unique one !');
                redirect('backend/purchase_order/save_head_latter');
            }
        }
    }


}
