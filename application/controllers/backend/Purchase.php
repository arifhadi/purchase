<?php if (!defined('BASEPATH')) { exit ('No Direct Script Allowed'); }

class Purchase extends CI_Controller {

	public function __construct(){
		parent::__construct();
	    if(!$this->ion_auth->logged_in()){
	      redirect('auth/login', 'refresh');
	    }
	    date_default_timezone_set('Asia/Jakarta');
        $this->load->model('m_purchase');
        $this->load->model('m_user');
        $this->load->model('m_po');
        $this->load->helper('download');
	}

	public function print_pr()
	{	
        
       	
		$this->usertemp->view('backend/purchasereq/print_pr');
	}

	public function list_my_pr()
	{


        $this->data['data_pr']  = $this->m_purchase->get_data_pr();
        $this->admintemp->view('backend/purchasereq/list_my_pr',$this->data);
	}

    public function list_detail_my_pr($pr_no)
    {
         $get_data_detail_pr = $this->m_purchase->get_data_detail_pr($pr_no);
         
         $this->data['detial_pr'] = $get_data_detail_pr;
         $this->data['pr_no'] = $pr_no;
         
         if($get_data_detail_pr->is_uncurrent_demand==0)
         {

            $this->data['edit_data'] = $this->m_purchase->get_data_pr_row($pr_no);
            $this->admintemp->view('backend/purchasereq/detail_list_my_pr_no_ud',$this->data);

            
         }
         else{
             $this->data['edit_data'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
            $this->admintemp->view('backend/purchasereq/detail_list_my_pr',$this->data);
            
         }

    }


        public function list_detail_my_pr_werehouse($pr_no)
    {
         $get_data_detail_pr = $this->m_purchase->get_data_detail_pr($pr_no);
         
         $this->data['detial_pr'] = $get_data_detail_pr;
         $this->data['pr_no'] = $pr_no;
         
         if($get_data_detail_pr->is_uncurrent_demand==0)
         {

            $this->data['edit_data'] = $this->m_purchase->get_data_pr_row($pr_no);
            $this->admintemp->view('backend/purchasereq/detail_list_my_pr_no_ud',$this->data);

            
         }
         else{
             $this->data['edit_data'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
            $this->admintemp->view('backend/purchasereq/detail_list_pr_werehouse',$this->data);
            
         }

    }

    public function list_detail_my_pr_hod($pr_no)
    {
         $get_data_detail_pr = $this->m_purchase->get_data_detail_pr($pr_no);
         
         $this->data['detial_pr'] = $get_data_detail_pr;
         $this->data['pr_no'] = $pr_no;
         
         if($get_data_detail_pr->is_uncurrent_demand==0)
         {

            $this->data['edit_data'] = $this->m_purchase->get_data_pr_row($pr_no);
            $this->admintemp->view('backend/purchasereq/detail_list_my_pr_no_ud',$this->data);

            
         }
         else{
             $this->data['edit_data'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
            $this->admintemp->view('backend/purchasereq/list_detail_pr_hod_ud',$this->data);
            
         }

    }


     public function list_my_detail_pr($pr_no)
    {
         $get_data_detail_pr = $this->m_purchase->get_data_detail_pr($pr_no);
         $this->data['detial_pr'] = $get_data_detail_pr;
         $this->data['pr_no'] = $pr_no;
          $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
        if($get_data_detail_pr->is_uncurrent_demand==0)
        {
            
            // log_r("tidak ud");
            $this->data['edit_data'] = $this->m_purchase->get_data_pr_row($pr_no);
            $this->admintemp->view('backend/purchasereq/list_my_detail_pr', $this->data, true);//view email
             
        }
        else{

            // log_r("ud");
            $this->data['edit_data'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
            $this->admintemp->view('backend/purchasereq/list_my_detail_pr_no_ud', $this->data, true);//view email

        }

    }



	public function create_pr()
	{
		$hariIni        = new DateTime();
		
       	// log_r($pr_no);
        if (empty($this->input->post())) {
        	$this->data['machine']         = $this->m_purchase->get_data_machine();
        	$this->data['tool']         = $this->m_purchase->get_data_tooling();
        	$employee_detail	= $this->m_user->detail_users(USER_ID);
        	$employee_no = $employee_detail->employee_no;
        	$this->data['department'] = $this->m_purchase->get_department($employee_no);
        	$this->data['pr'] = $this->m_purchase->get_data_pr();
        
            $this->admintemp->view('backend/purchasereq/request_pr',$this->data);
        }elseif (!empty($this->input->post())) {

        	$format_pr =  $hariIni->format('d-m-y');
            $digit_pr = substr($format_pr, -2);
            $ribuan_pr = '000';


			$cek_pr = $this->m_purchase->get_last_pr_no_detail();

            if(empty($cek_pr))
            {   
                $pr_no = $digit_pr . $ribuan_pr;
            }
            else{
                $last_pr = $cek_pr->pr_no;
                $digit_last = substr($last_pr, 0,2);
                
                if($digit_pr !== $digit_last)
                    {
                        $pr_no = $digit_pr . $ribuan_pr;
                    }
                else{

                    $pr_no = $cek_pr->pr_no + 1;
                }
            }


            $urgency                = $this->input->post('urgency');
            $mc_no     = $this->input->post('mc_no');
            $tool_no                 = $this->input->post('tool_no');
            $should_arrive_by            = $this->input->post('should_arrive_by');
            $department     =$this->input->post('department');
            $claimable =$this->input->post('claimable');
            $is_uncurrent_demand =$this->input->post('is_uncurrent_demand');
            $data = array(
            	'pr_no'			=>$pr_no,
                'urgency'       => $urgency,
                'claimable'		=>$claimable,
                'mc_no'                => $mc_no,
                'tool_no'             => $tool_no,
                'should_arrive_by'               => $should_arrive_by,
                'department'                 => $department,
                'is_uncurrent_demand'       =>$is_uncurrent_demand,
                'create_at'                 => $hariIni->format('y-m-d H:i:s'),
                'user_id'                   => USER_ID,
            );

           
            $status = $this->m_purchase->insert_detail_pr($data);
            $this->session->set_flashdata('success', 'Your data successfully added !');
            if($is_uncurrent_demand==0)
            {
                $this->edit_general_pr($pr_no);
            }
            else{
                $this->edit_general_pr_ud($pr_no);
            }
        }
	}

    public function edit_general_pr($pr_no)
    {
            $get_data  = $this->m_purchase->get_data_pr_row($pr_no);
            $this->data['edit_data'] = $get_data;
            $this->data['pr_no'] = $pr_no;
            $this->data['cost_center'] = $this->m_purchase->get_data_cost_center();
            $this->data['category_request'] = $this->m_purchase->get_data_category_request();
            $this->data['list_item'] = $this->m_purchase->get_data_list_item();
            // log_r($this->data['list_item']);
            $this->admintemp->view('backend/purchasereq/edit_detail_pr',$this->data);
    }

    public function edit_general_pr_ud($pr_no)
    {
            $get_data  = $this->m_purchase->get_data_ud_list_des($pr_no);
            $get_data_detail_pr = $this->m_purchase->get_data_detail_pr($pr_no);
            $this->data['edit_data'] = $get_data;

            // log_r($this->data['edit_data']);
            $this->data['pr_no'] = $pr_no;
            $this->data['is_uncurrent_demand'] = $get_data_detail_pr->is_uncurrent_demand;
            $this->data['cost_center'] = $this->m_purchase->get_data_cost_center();
            $this->data['category_request'] = $this->m_purchase->get_data_category_request();
            $this->admintemp->view('backend/purchasereq/edit_detail_pr_un',$this->data);
        
    }

	public function update_general_pr($pr_no='')
	{

		    $hariIni        = new DateTime();
            $pr_no                      =$this->input->post('pr_no');
            $is_uncurrent_demand = $this->input->post('is_uncurrent_demand');
            $description                = $this->input->post('description');
            $purpose     = $this->input->post('purpose');
            $cost_center                 = $this->input->post('cost_center');
            $category_request            = $this->input->post('category_request');
            $new_order_qty     =$this->input->post('new_order_qty');
            $uom =$this->input->post('uom');
      

            $cek_lo = $this->m_purchase->check_last_order($description);


            if(empty($cek_lo))
            {
                $data_lo = array(
                    'id_item' => $description,
                    'date_order' => $hariIni->format('y-m-d H:i:s'),
                 );

                 $status_lo = $this->m_purchase->insert_last_order($data_lo);
            }
            else{
                $cek_date_lo = $this->m_purchase->check_date_lo($description);

                if(empty($cek_date_lo->date_order))
                {
                    $data_lo = array(
                        'id_item' => $description,
                        'date_order' => $hariIni->format('y-m-d H:i:s'),
                    ); 
                }
                else{
                     $data_lo = array(
                        'id_item' => $description,
                        'create_at' => $hariIni->format('y-m-d H:i:s'),
                     ); 
                }
           
                 $status_lo = $this->m_purchase->update_last_order($description,$data_lo);
            }

            $data = array(
                'pr_no'               => $pr_no,
                'description'         =>strtoupper($description),
                'purpose'             =>strtoupper($purpose),
                'cost_center'         =>$cost_center,
                'category_request'    =>$category_request,
                'new_order_qty'       =>$new_order_qty,
                'uom'                 =>$uom,
                'create_at'           => $hariIni->format('y-m-d H:i:s'),
                'user_id'             => USER_ID,
            );
           
            $data_history_lo = array(
                'id_item' => $description,
                'date_order' => $hariIni->format('y-m-d H:i:s'),
            );
            $status = $this->m_purchase->insert_pr($data);
           
            $status_his = $this->m_purchase->insert_history_last_order($data_history_lo);
            $this->session->set_flashdata('success', 'Your data successfully added !');
            if($is_uncurrent_demand==0)
            {
                $this->edit_general_pr($pr_no);
            }
            else{
                $this->edit_general_pr_ud($pr_no);
            }
	}
    function finish_request_pr()
    {

            $get_data_emp = $this->m_purchase->get_data_user_hod();
            $employee_no = $get_data_emp->employee_no;
            // log_r($employee_no);
            $pr_no                      =$this->input->post('pr_no');
            $is_approve_hod =0;
            $is_approve_werehouse=0;
            $is_super_user=0;
            // log_r($pr_no);
            if($employee_no =='0565')
            {
                $user_hod =10;
                $user_werehouse=18;
                $user_finance=19;
                $user_division_head=20;
                $user_pd=25;
                $user_purchase=17;

            };

             if($employee_no =='0650')
            {
                $user_hod =10;
                $user_werehouse=18;
                $user_finance=19;
                $user_division_head=20;
                $user_pd=25;
                $user_purchase=17;
                $is_approve_hod=1;

            };

            if($employee_no =='0685')
            {
                $user_hod =18;
                $user_werehouse=18;
                $user_finance=19;
                $user_division_head=20;
                $user_pd=25;
                $user_purchase=17;
            };
            if($employee_no =='0407')
            {
                $user_hod=16;
                $user_werehouse=18;
                $user_finance=19;
                $user_division_head=20;
                $user_pd=25;
                $user_purchase=17;
            };
            if($employee_no =='0394')
            {
               
                $user_hod=10;
                $user_werehouse=18;
                $user_finance=19;
                $user_division_head=20;
                $user_pd=25;
                $user_purchase=17;
            };
            if($employee_no =='0481')
            {
                $user_hod=19;
                $user_werehouse=18;
                $user_finance=19;
                $user_division_head=21;
                $user_pd=25;
                $user_purchase=17;
            };

            if($employee_no =='0135')
            {
                $user_hod=19;
                $user_werehouse=18;
                $user_finance=19;
                $user_division_head=21;
                $user_pd=25;
                $user_purchase=17;
                $is_approve_hod=1;
                $is_super_user=1;
            };

              if($employee_no =='0488')
            {
                $user_hod=17;
                $user_werehouse=18;
                $user_finance=19;
                $user_division_head=21;
                $user_pd=25;
                $user_purchase=17;
                $is_approve_hod=1;
            };

            if($employee_no =='0322')
            {
                $user_hod=16;
                $user_werehouse=18;
                $user_finance=19;
                $user_division_head=20;
                $user_pd=25;
                $user_purchase=17;
            };

            if($employee_no =='0659')
            {
                $user_hod=21;
                $user_werehouse=18;
                $user_finance=19;
                $user_division_head=21;
                $user_pd=25;
                $user_purchase=17;
            };

            if($employee_no =='0074')
            {
                $user_hod=18;
                $user_werehouse=18;
                $user_finance=19;
                $user_division_head=20;
                $user_pd=25;
                $user_purchase=17;
                $is_approve_hod=1;
            };


          $data_pr = array(
                'pr_no'               =>$pr_no,
                'user_hod'            =>$user_hod,
                'user_werehouse'      =>$user_werehouse,
                'user_finance'        =>$user_finance,
                'user_division_head'  =>$user_division_head,
                'user_pd'             =>$user_pd,
                'user_purchase'      =>$user_purchase,
                'is_approve_hod'    =>$is_approve_hod,
                'is_approve_werehouse' =>$is_approve_werehouse,
                'is_super_user'     =>$is_super_user,
            );

           $status_pr = $this->m_purchase->update_detail_pr($pr_no,$data_pr);
           if($is_approve_hod==1)
            {
                $data_detail_pr = $this->m_purchase->get_data_detail_pr($pr_no);
                $user_req = "";

                if(empty($data_detail_pr->user_id))
                {
                    $user_req = USER_ID;
                }
                else{
                    $user_req = $data_detail_pr->user_id;
                }

                $this->send_create_pr_werehouse($pr_no,$user_werehouse,$user_req);
            }
           else{
            $this->send_create_pr($pr_no,$user_hod);
           }
    }



    function send_create_pr($pr_no='', $user_hod='')
    {
        $this->load->library('mailer');

        $get_req   = $this->m_purchase->get_user_row(USER_ID);//get requestor id
        $email_req = $get_req->email;
        $this->data['name_req']  =  $get_req->first_name;

        $get_to   = $this->m_purchase->get_user_row($user_hod);//get to id "Sajith"
         // log_r($get_to);
        $email_to = $get_to->email;
        // log_r($email_to);
        $this->data['name_to']  =  $get_to->first_name;
        $this->data['approve_to'] = "HOD";
        

        $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);


        

        // log_r();
        // $email_penerima          = "arif@asiagalaxy.com";
        $subjek                     = "PR Request"." ".$pr_no;
        $this->data['pr_no']    = $pr_no;
        $this->data['date']     =$get_data_detail_pr->create_at;
        $this->data['line_mc_no']   = $get_data_detail_pr->line_mc_no;
        $this->data['mc_no']        = $get_data_detail_pr->mc_no;
        $this->data['urgency']        = $get_data_detail_pr->urgency;
        $this->data['claimable']    = $get_data_detail_pr->claimable;
        $this->data['should_arrive_by']     = $get_data_detail_pr->should_arrive_by;
        
       
        
        if($get_data_detail_pr->is_uncurrent_demand==0)
        {
            
            $this->data['pr_data'] = $this->m_purchase->get_data_pr_row($pr_no);
            $content = $this->load->view('backend/purchasereq/mail_create_pr_hod', $this->data, true);//view email
             
        }
        else{

            $this->data['pr_data'] = $this->m_purchase->get_data_ud_list_des($pr_no);
            $content = $this->load->view('backend/purchasereq/mail_create_pr_hod_no_ud', $this->data, true);//view email

        }


        $mailto = array(
            // 'penerima_satu' => $email_req,
            'penerima_satu'  => $email_to,
            // 'penerima_satu' => "arif@asiagalaxy.com",
        );



        $ccmail = array(
            $cc_satu  = 'noreply@asiagalaxy.com',//email requestor
            // $cc_dua   = "iyappan@asiagalaxy.com",//email iyappan
            // $cc_tiga  = "shyju@asiagalaxy.com",//email shyju
            // $cc_empat = "phuspitaa@asiagalaxy.com",//email phuspita
            // $cc_lima  = "dyah@asiagalaxy.com",//email dyah
            // $cc_satu = "windu@asiagalaxy.com",//email windu
        );

        $sendmail = array(
            'email_penerima' => "",
            'subjek'  => $subjek,
            'content' => $content,
        );

        $send = $this->mailer->send($sendmail, $mailto, $ccmail); // jalankan fungsi php mailer
        $status = $send['status'];
        if ($status == 'Sukses') {
            $this->session->set_flashdata('success', 'Sending email was successful !');
            redirect('backend/purchase/list_my_pr');
        }elseif($status == 'Gagal'){
            $this->session->set_flashdata('error', 'Sending email failed !');
            redirect('backend/purchase/list_my_pr');
        }
    }

    public function list_approval_hod()
    {
        $this->data['approval_hod'] = $this->m_purchase->get_data_approval_row();
        $this->admintemp->view('backend/purchasereq/list_approval_hod',$this->data);
    }

    public function list_history_pr_hod()
    {

        $this->data['historyhod'] = $this->m_purchase->get_data_history_hod_pr();
        $this->admintemp->view('backend/purchasereq/history_pr_hod',$this->data);
        
    }

    public function approve_pr_hod($pr_no="")
    {
        $hariIni        = new DateTime();
        $data_pr = array(
                'pr_no'               =>$pr_no,
                'date_approve_hod'    =>$hariIni->format('y-m-d H:i:s'),
                'is_approve_hod'      =>1,
            );
        $data_detail_pr = $this->m_purchase->get_data_detail_pr($pr_no);
        $is_uncurrent_demand = $data_detail_pr->is_uncurrent_demand;
        $user_werehouse = $data_detail_pr->user_werehouse;
        $user_req = $data_detail_pr->user_id;

     $status_pr = $this->m_purchase->update_detail_pr($pr_no,$data_pr);
     $this->session->set_flashdata('success', 'Success Approve PR !');
     $this->send_create_pr_werehouse($pr_no,$user_werehouse,$user_req);

    }

     function send_create_pr_werehouse($pr_no='', $user_werehouse='',$user_req='')
    {
        $this->load->library('mailer');

        $get_req   = $this->m_purchase->get_user_row($user_req);//get requestor id
        $email_req = $get_req->email;
        $this->data['name_req']  =  $get_req->first_name;

        $get_to   = $this->m_purchase->get_user_row($user_werehouse);//get to id "Sajith"
         // log_r($get_to);
        $email_to = $get_to->email;
        // log_r($email_to);
        $this->data['name_to']  =  $get_to->first_name;
        $this->data['approve_to'] = "Werehouse";
        

        $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
        $this->data['pr_data'] = $this->m_purchase->get_data_pr_row($pr_no);

        // $email_penerima          = "arif@asiagalaxy.com";
        $subjek                     = "PR Request"." ".$pr_no;
        $this->data['pr_no']    = $pr_no;
        $this->data['date']     =$get_data_detail_pr->create_at;
        $this->data['line_mc_no']   = $get_data_detail_pr->line_mc_no;
        $this->data['mc_no']        = $get_data_detail_pr->mc_no;
        $this->data['urgency']        = $get_data_detail_pr->urgency;
        $this->data['claimable']    = $get_data_detail_pr->claimable;
        $this->data['should_arrive_by']     = $get_data_detail_pr->should_arrive_by;
        
        
        
        if($get_data_detail_pr->is_uncurrent_demand==0)
        {
            
            $this->data['pr_data'] = $this->m_purchase->get_data_pr_row($pr_no);
            $content = $this->load->view('backend/purchasereq/mail_create_pr_hod', $this->data, true);//view email
             
        }
        else{

            $this->data['pr_data'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
            $content = $this->load->view('backend/purchasereq/mail_create_pr_hod_no_ud', $this->data, true);//view email

        }

        $mailto = array(
            // 'penerima_satu' => $email_req,
            'penerima_satu'  => $email_to,
            // 'penerima_satu' => "arif@asiagalaxy.com",
        );



        $ccmail = array(
            $cc_satu  = 'noreply@asiagalaxy.com',//email requestor
            // $cc_dua   = "iyappan@asiagalaxy.com",//email iyappan
            // $cc_tiga  = "shyju@asiagalaxy.com",//email shyju
            // $cc_empat = "phuspitaa@asiagalaxy.com",//email phuspita
            // $cc_lima  = "dyah@asiagalaxy.com",//email dyah
            // $cc_satu = "windu@asiagalaxy.com",//email windu
        );

        $sendmail = array(
            'email_penerima' => "",
            'subjek'  => $subjek,
            'content' => $content,
        );

        $send = $this->mailer->send($sendmail, $mailto, $ccmail); // jalankan fungsi php mailer
        // log_r($send);
        $status = $send['status'];
        if ($status == 'Sukses') {
            $this->session->set_flashdata('success', 'Sending email was successful !');
            redirect('backend/purchase/list_approval_werehouse');
        }elseif($status == 'Gagal'){
            $this->session->set_flashdata('error', 'Sending email failed !');
            redirect('backend/purchase/list_approval_werehouse');
        }
    }


    public function decline_pr_hod($pr_no="")
    {
         $hariIni        = new DateTime();
         $reason = $this->input->post('reason');

        $data_pr = array(
                'pr_no'               =>$pr_no,
                'date_reject_hod'     =>$hariIni->format('y-m-d H:i:s'),
                'reason_reject_hod'   =>$reason,
                'is_reject_hod'      =>1,
            );

        $status_pr = $this->m_purchase->update_detail_pr($pr_no,$data_pr);
        $this->session->set_flashdata('success', 'Success Decline PR !');
        redirect('backend/purchase/list_approval_hod');
    }

    public function list_approval_werehouse()
    {
        $this->data['appwerehouse'] = $this->m_purchase->get_data_approval_werehouse();
        // log_r($this->data['appwerehouse']);
        $this->admintemp->view('backend/purchasereq/list_approval_werehouse',$this->data);
    }

    public function list_history_pr_werehouse()
    {
        $this->data['historywerehouse'] = $this->m_purchase->get_data_history_werehouse();
        $this->admintemp->view('backend/purchasereq/history_pr_werehouse',$this->data);
    }




     public function approve_pr_werehouse($pr_no="")
    {
        $hariIni        = new DateTime();
        $data_pr = array(
                'pr_no'               =>$pr_no,
                'date_approve_werehouse'    =>$hariIni->format('y-m-d H:i:s'),
                'is_approve_werehouse'      =>1,
            );
        $data_detail_pr = $this->m_purchase->get_data_detail_pr($pr_no);
        $user_purchase = $data_detail_pr->user_purchase;
        $user_req = $data_detail_pr->user_id;

     $status_pr = $this->m_purchase->update_detail_pr($pr_no,$data_pr);
     $this->send_create_pr_purchase($pr_no,$user_purchase,$user_req);

    }

     function send_create_pr_purchase($pr_no='', $user_purchase='',$user_req='')
    {
        $this->load->library('mailer');

        $get_req   = $this->m_purchase->get_user_row($user_req);//get requestor id
        $email_req = $get_req->email;
        $this->data['name_req']  =  $get_req->first_name;

        $get_to   = $this->m_purchase->get_user_row($user_purchase);//get to id "Sajith"
         // log_r($get_to);
        $email_to = $get_to->email;
        // log_r($email_to);
        $this->data['name_to']  =  $get_to->first_name;
        $this->data['approve_to'] = "Purchasing";
        

        $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
        $this->data['pr_data'] = $this->m_purchase->get_data_pr_row($pr_no);

        // log_r($this->data['pr_data']->qty_cb);

        // $email_penerima          = "arif@asiagalaxy.com";
        $subjek                     = "PR Request"." ".$pr_no;
        $this->data['pr_no']    = $pr_no;
        $this->data['date']     =$get_data_detail_pr->create_at;
        $this->data['line_mc_no']   = $get_data_detail_pr->line_mc_no;
        $this->data['mc_no']        = $get_data_detail_pr->mc_no;
        $this->data['urgency']        = $get_data_detail_pr->urgency;
        $this->data['claimable']    = $get_data_detail_pr->claimable;
        $this->data['should_arrive_by']     = $get_data_detail_pr->should_arrive_by;
        
        // $content = $this->load->view('backend/purchasereq/mail_create_pr_werehouse', $this->data, true);//view email
        
          if($get_data_detail_pr->is_uncurrent_demand==0)
        {
            
            $this->data['pr_data'] = $this->m_purchase->get_data_pr_row($pr_no);
            $content = $this->load->view('backend/purchasereq/mail_create_pr_werehouse', $this->data, true);//view email
             
        }
        else{

            $this->data['pr_data'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
            $content = $this->load->view('backend/purchasereq/mail_create_pr_werehouse_no_ud', $this->data, true);//view email

        }



        $mailto = array(
            // 'penerima_satu' => $email_req,
            'penerima_satu'  => $email_to,
            // 'penerima_satu' => "arif@asiagalaxy.com",
        );



        $ccmail = array(
            $cc_satu  = 'noreply@asiagalaxy.com',//email requestor
            // $cc_dua   = "iyappan@asiagalaxy.com",//email iyappan
            // $cc_tiga  = "shyju@asiagalaxy.com",//email shyju
            // $cc_empat = "phuspitaa@asiagalaxy.com",//email phuspita
            // $cc_lima  = "dyah@asiagalaxy.com",//email dyah
            // $cc_satu = "windu@asiagalaxy.com",//email windu
        );

        $sendmail = array(
            'email_penerima' => "",
            'subjek'  => $subjek,
            'content' => $content,
        );

        $send = $this->mailer->send($sendmail, $mailto, $ccmail); // jalankan fungsi php mailer
        $status = $send['status'];
        if ($status == 'Sukses') {
            $this->session->set_flashdata('success', 'Sending email was successful !');
            // redirect('backend/purchase/list_approval_werehouse');
        }elseif($status == 'Gagal'){
            $this->session->set_flashdata('error', 'Sending email failed !');
            // redirect('backend/purchase/list_approval_werehouse');
        }
    }


    public function decline_pr_werehouse($pr_no="",$reason="")
    {
         $hariIni        = new DateTime();
        $data_pr = array(
                'pr_no'               =>$pr_no,
                'date_reject_hod'     =>$hariIni->format('y-m-d H:i:s'),
                'reason_reject_werehouse'   =>$reason,
                'is_reject_werehouse'      =>1,
            );

        $status_pr = $this->m_purchase->update_detail_pr($pr_no,$data_pr);
        $this->session->set_flashdata('success', 'Success Decline PR !');
        redirect('backend/purchase/list_approval_werehouse');
    }

    public function form_fill_werehouse($pr_no="")
    {

        
        $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
       
        $this->data['qty_cb'] = $this->m_purchase->check_fill_werehouse($pr_no);
        $this->data['pr_no'] =$pr_no;

        // log_r($this->data['qty_cb']->qty_cb);
        if($get_data_detail_pr->is_uncurrent_demand==0)
        {

            // log_r("non ud");
            $this->data['list_were'] = $this->m_purchase->get_data_pr_row($pr_no); 
            $this->admintemp->view('backend/purchasereq/form_fill_werehouse',$this->data);
        }
        else{

             $this->data['list_were'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
             $this->admintemp->view('backend/purchasereq/form_fill_werehouse_no_ud',$this->data);
        }
        // log_r($this->data['qty_cb']);
        // form_fill_werehouse_no_ud

        
    }

    public function create_qty_cb_werehouse($pr_no="",$qty_cb="")
    {
        $hariIni = new DateTime();
        $data = array(
            'id_pr'               =>$pr_no,
            'date_cb'     =>$hariIni->format('y-m-d H:i:s'),
            'qty_cb'   =>$qty_cb,
        );
        $status_pr = $this->m_purchase->update_purchase_requisition($pr_no,$data);
        $this->session->set_flashdata('success', 'success Fill The Qty Current Balance !');
        redirect('backend/purchase/form_fill_werehouse/'.$pr_no);
    }

    public function list_apporval_purchasing()
    {
        $this->data['apppurchasing'] = $this->m_purchase->get_data_app_purchasing();

        $this->admintemp->view('backend/purchasereq/list_approval_purchasing',$this->data);

    }

     public function list_history_pr_purchasing()
    {
        $this->data['historypur'] = $this->m_purchase->get_history_purchasing();
        $this->admintemp->view('backend/purchasereq/history_pr_purchasing',$this->data);
    }

    public function list_detail_purchasing($pr_no)
    {

        $this->data['currency'] = $this->m_purchase->get_data_pr_limit_1($pr_no);
         $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
       
         if($get_data_detail_pr->is_uncurrent_demand==0)
        {
            $this->data['list_fin'] = $this->m_purchase->get_data_purchasing_field($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
             $this->data['pr_no'] =$pr_no; 
             $this->admintemp->view('backend/purchasereq/list_detail_pr_purchasing_no_ud',$this->data);
        }
        else{
            $this->data['list_fin'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount_ud($pr_no); 
             $this->data['pr_no'] =$pr_no; 
             $this->admintemp->view('backend/purchasereq/list_detail_pr_purchasing',$this->data);
        }
    }

    public function update_approval_purchasing()
    {
     
        $unit_price     = $this->input->post('unit_price');
        $unit_price_int = str_replace(".","",$unit_price);
        $total_amount     = $this->input->post('total_amount');
        $total_amount_int = str_replace(".","", $total_amount);
        $supplier_lo     = $this->input->post('supplier_lo');
        $qty_lo_pf     = $this->input->post('qty_lo_pf');
        $date_pf     = $this->input->post('date_pf');
        $unit_price_lo     = $this->input->post('unit_price_lo');
        $supplier_name_pf     = $this->input->post('supplier_name_pf');
        $id_pr = $this->input->post('id_pr');
        $pr_no = $this->input->post('pr_no');
        $currency = $this->input->post('currency');
        $part_no = $this->input->post('part_no');
        $expected_material_date_arrive = $this->input->post('expected_material_date_arrive');
        $hariIni = new DateTime();

        $get_check_part_no = $this->m_purchase->check_part_no($part_no);

        if(!empty($get_check_part_no))
        {

            $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
             if($get_data_detail_pr->is_uncurrent_demand==0)
            {
                $data_date_arrive = array(
                    'pr_no'     => $pr_no,
                    'expected_material_date_arrive' =>$expected_material_date_arrive,
                );
                $status_data = $this->m_purchase->update_date_arrive($pr_no,$data_date_arrive);
                $this->session->set_flashdata('success', 'success Fill The Purchasing Field !'); 
                redirect('backend/purchase/form_fill_purchasing/'.$pr_no);
            }
            
            else{
                if($get_check_part_no->part_no == $part_no)
                {
                    $this->session->set_flashdata('error', 'Part No is Duplicate , Please Using Other Part No !');
                    redirect('backend/purchase/form_fill_purchasing/'.$pr_no);
                }
                else{
                    $data = array(
                        'id_pr'               =>$id_pr,
                        'unit_price_pf'  =>$unit_price_int,
                        'total_amount'  => $total_amount_int,
                        'supplier_name_pf'  => $supplier_name_pf,
                        'date_pf'  => $date_pf,
                        'part_no'   => $part_no,
                        'qty_lo_pf'  => $qty_lo_pf,
                        'currency'  =>$currency,
                        'unit_price_lo'  => $unit_price_lo,
                        'supplier_lo'  => $supplier_lo,
                    );
                    $data_date_arrive = array(
                        'pr_no'     => $pr_no,
                        'expected_material_date_arrive' =>$expected_material_date_arrive,
                    );
                    $status_data = $this->m_purchase->update_date_arrive($pr_no,$data_date_arrive);
                    $status_pr = $this->m_purchase->update_purchase_requisition($id_pr,$data);
                    $this->session->set_flashdata('success', 'success Fill The Purchasing Field !'); 
                    redirect('backend/purchase/form_fill_purchasing/'.$pr_no);
                }
            }
        }else{

            $data = array(
                    'id_pr'               =>$id_pr,
                    'unit_price_pf'  =>$unit_price_int,
                    'total_amount'  => $total_amount_int,
                    'supplier_name_pf'  => $supplier_name_pf,
                    'date_pf'  => $date_pf,
                    'part_no'   => $part_no,
                    'qty_lo_pf'  => $qty_lo_pf,
                    'currency'  =>$currency,
                    'unit_price_lo'  => $unit_price_lo,
                    'supplier_lo'  => $supplier_lo,
                );
                $data_date_arrive = array(
                    'pr_no'     => $pr_no,
                    'expected_material_date_arrive' =>$expected_material_date_arrive,
                );
                $status_data = $this->m_purchase->update_date_arrive($pr_no,$data_date_arrive);
                $status_pr = $this->m_purchase->update_purchase_requisition($id_pr,$data);
                $this->session->set_flashdata('success', 'success Fill The Purchasing Field !'); 
                redirect('backend/purchase/form_fill_purchasing/'.$pr_no);
        }

        
    }

    public function form_fill_purchasing($pr_no="")
    {
         $this->data['list_pur'] = $this->m_purchase->get_data_purchasing_field($pr_no);
        $this->data['apppurchasing'] = $this->m_purchase->get_data_app_purchasing();
        $this->data['pr_no'] = $pr_no; 
        $this->data['sup_lo'] = $this->m_purchase->check_fill_purchasing($pr_no);

        
        $this->data['sup'] = $this->m_purchase->get_data_supplier();
        $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);

        // log_r($get_data_detail_pr);
         // log_r($get_data_detail_pr);
        if($get_data_detail_pr->is_uncurrent_demand==0)
        {
            // log_r("non ud");
            $data_list_pur = $this->m_purchase->get_data_purchasing_field($pr_no); 
            $data_currency = $this->m_purchase->get_currency_non_ud($data_list_pur[0]->id_item);
            $this->data['currency'] = $data_currency->currency;
            $this->data['list_pur'] = $data_list_pur;
            $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no); 
            $this->admintemp->view('backend/purchasereq/form_fill_purchasing',$this->data);
        }
        else{

            $data_currency = $this->m_purchase->get_data_pr_limit_1($pr_no);
            if(!empty($data_currency))
            {
                $this->data['currency'] = $data_currency->currency;
            }
            else {
                $this->data['currency'] = "";
            }
              $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount_ud($pr_no);  
             $this->data['list_pur'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no); 
             $this->admintemp->view('backend/purchasereq/form_fill_purchasing_ud',$this->data);
        }

    }

    public function form_fill_revision_purchasing($pr_no)
    {
        $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount_ud($pr_no);  
        $this->data['list_pur'] = $this->m_purchase->get_data_pr_row_no_ud($pr_no);
        // log_r($this->data['list_pur']);
         $data_currency = $this->m_purchase->get_data_pr_limit_1($pr_no);
         $this->data['currency'] = $data_currency->currency;
         $this->data['pr_no'] = $pr_no;
         $this->data['sup'] = $this->m_purchase->get_data_supplier();
        $this->admintemp->view('backend/purchasereq/form_fill_revision_purchasing_ud',$this->data);
    }

    public function update_revision_fill_purchasing()
    {

        $id_pr = $this->input->post('id_pr');
        $pr_no = $this->input->post('pr_no');
        $unit_price = $this->input->post('unit_price');
        $unit_price_int = str_replace(".","",$unit_price);
        $currency = $this->input->post('currency');
        $total_amount = $this->input->post('total_amount');
        $total_amount_int = str_replace(".","",$total_amount);
        $supplier_name_pf = $this->input->post('supplier_name_pf');
        $date_pf = $this->input->post('date_pf');
        $qty_lo_pf = $this->input->post('qty_lo_pf');
        $unit_price_lo = $this->input->post('unit_price_lo');
        $supplier_lo = $this->input->post('supplier_lo');



        $data_ud_pr = array(
                'id_pr' => $id_pr,
                'unit_price_pf' =>$unit_price_int,
                'currency'  =>$currency,
                'total_amount' =>$total_amount_int,
                'supplier_name_pf' =>$supplier_name_pf,
                'date_pf' =>$date_pf,
                'qty_lo_pf' =>$qty_lo_pf,
                'unit_price_lo' =>$unit_price_lo,
                'supplier_lo' => $supplier_lo,
                
        );

        $data_ud_rev_detail = array(
            'is_revision' =>1,
        );
        $this->session->set_flashdata('success', 'success Fill Revision Purchasing Field !');
        $status = $this->m_purchase->update_purchase_requisition($id_pr,$data_ud_pr);
        $status_detail = $this->m_purchase->update_detail_pr($pr_no,$data_ud_rev_detail);
        redirect('backend/purchase/form_fill_revision_purchasing/'.$pr_no);
    }



     public function approve_pr_purchasing($pr_no="")
    {
        $hariIni        = new DateTime();
        $data_detail_pr = $this->m_purchase->get_data_detail_pr($pr_no);
        $is_super_user = $data_detail_pr->is_super_user;
        $is_approve_finance=0;

        
        if($is_super_user==1)
        {
            $is_approve_finance=1;
        }

        $data_pr = array(
                'pr_no'               =>$pr_no,
                'date_approve_purchasing'    =>$hariIni->format('y-m-d H:i:s'),
                'is_approve_purchasing'      =>1,
                'is_approve_finance'        =>$is_approve_finance,
            );
        
        $user_finance = $data_detail_pr->user_finance;
        $user_req = $data_detail_pr->user_id;
        
        $status_pr = $this->m_purchase->update_detail_pr($pr_no,$data_pr);
        // log_r($is_super_user);
        if($is_super_user==1)
        {

            $user_division_head = $data_detail_pr->user_division_head;
            $this-> send_create_pr_head($pr_no,$user_division_head,$user_req);
             $this->session->set_flashdata('success', 'Success Approve PR !');
        }
        else{
             $this->session->set_flashdata('success', 'Success Approve PR !');
             $this->send_create_pr_finance($pr_no,$user_finance,$user_req);
        }
    }

     function send_create_pr_finance($pr_no='', $user_finance='',$user_req='')
    {
        $this->load->library('mailer');

        $get_req   = $this->m_purchase->get_user_row($user_req);//get requestor id
        $email_req = $get_req->email;
        $this->data['name_req']  =  $get_req->first_name;
        // $this->data['currency'] = $this->m_purchase->get_data_pr_limit_1($pr_no);
        $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
        $get_to   = $this->m_purchase->get_user_row($user_finance);//get to id "Sajith"
         // log_r($get_to);
        $email_to = $get_to->email;
        // $email_to = "abdul.razak@asiagalaxy.com";
        // log_r($email_to);
        $this->data['name_to']  =  $get_to->first_name;
        $this->data['approve_to'] = "Finance";
        

        $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
        // $pr_uncurrent_demand = 

        // $email_penerima          = "arif@asiagalaxy.com";
        $subjek                     = "PR Request"." ".$pr_no;
        $this->data['pr_no']    = $pr_no;
        $this->data['date']     =$get_data_detail_pr->create_at;
        $this->data['line_mc_no']   = $get_data_detail_pr->line_mc_no;
        $this->data['mc_no']        = $get_data_detail_pr->mc_no;
        $this->data['urgency']        = $get_data_detail_pr->urgency;
        $this->data['claimable']    = $get_data_detail_pr->claimable;
        $this->data['should_arrive_by']     = $get_data_detail_pr->should_arrive_by;
        $this->data['uncurrent_demand'] = $get_data_detail_pr->is_uncurrent_demand;
        
        // log_r();

        
            

        if($this->data['uncurrent_demand']==0)
        {
            // log_r("no un current");

            $data_list_pur = $this->m_purchase->get_data_purchasing_field($pr_no); 
            $data_currency = $this->m_purchase->get_currency_non_ud($data_list_pur[0]->id_item);
            $this->data['currency'] = $data_currency->currency;
            $this->data['expected_material_date_arrive'] = $data_list_pur[0]->expected_date;
             $this->data['pr_data'] = $data_list_pur;
              $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no); 
            $content = $this->load->view('backend/purchasereq/mail_create_pr_purchasing_no_ud', $this->data, true);//view email
        }
        else {
            // log_r("current");
            $this->data['expected_material_date_arrive'] = $get_data_detail_pr->expected_material_date_arrive;
            $data_currency = $this->m_purchase->get_data_pr_limit_1($pr_no);
            if(!empty($data_currency))
            {
                $this->data['currency'] = $data_currency->currency;
            }
            else {
                $this->data['currency'] = "";
            }
             $this->data['pr_data'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount_ud($pr_no); 
            $content = $this->load->view('backend/purchasereq/mail_create_pr_purchasing', $this->data, true);//view email
        }
        




        $mailto = array(
            // 'penerima_satu' => $email_req,
            'penerima_satu'  => $email_to,
            // 'penerima_satu' => "arif@asiagalaxy.com",
        );



        $ccmail = array(
            $cc_satu  = 'noreply@asiagalaxy.com',//email requestor
            // $cc_dua   = "iyappan@asiagalaxy.com",//email iyappan
            // $cc_tiga  = "shyju@asiagalaxy.com",//email shyju
            // $cc_empat = "phuspitaa@asiagalaxy.com",//email phuspita
            // $cc_lima  = "dyah@asiagalaxy.com",//email dyah
            // $cc_satu = "windu@asiagalaxy.com",//email windu
        );

        $sendmail = array(
            'email_penerima' => "",
            'subjek'  => $subjek,
            'content' => $content,
        );

        $send = $this->mailer->send($sendmail, $mailto, $ccmail); // jalankan fungsi php mailer
        $status = $send['status'];
        if ($status == 'Sukses') {
            $this->session->set_flashdata('success', 'Sending email was successful !');
            // redirect('backend/purchase/list_apporval_purchasing');
        }elseif($status == 'Gagal'){
            $this->session->set_flashdata('error', 'Sending email failed !');
            // redirect('backend/purchase/list_apporval_purchasing');
        }
    }


    public function decline_pr_purchase($pr_no="",$reason="")
    {
         $hariIni        = new DateTime();
        $data_pr = array(
                'pr_no'               =>$pr_no,
                'date_reject_by_purchasing'     =>$hariIni->format('y-m-d H:i:s'),
                'reason_reject_by_purchasing'   =>$reason,
                'is_reject_by_purchasing'      =>1,
            );

        $status_pr = $this->m_purchase->update_detail_pr($pr_no,$data_pr);
        $this->session->set_flashdata('success', 'Success Decline PR !');
        redirect('backend/purchase/list_apporval_purchasing');
    }
    public function get_data_pr_detail_row()
    {
       
        $id_pr    = $this->input->post('id');
        $data           = $this->m_purchase->get_data_pr_detail_row($id_pr);
        echo json_encode($data);
    }

    public function get_all_data_to_revision()
    {
         $id_pr    = $this->input->post('id');
        $data           = $this->m_purchase->get_all_data_to_revision($id_pr);
        echo json_encode($data);
    }

    
    public function list_detail_finance($pr_no="")
    {
        
        
        $this->data['currency'] = $this->m_purchase->get_data_pr_limit_1($pr_no);
         $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
       
         if($get_data_detail_pr->is_uncurrent_demand==0)
        {
            $this->data['list_fin'] = $this->m_purchase->get_data_purchasing_field($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
             $this->data['pr_no'] =$pr_no; 
             $this->admintemp->view('backend/purchasereq/list_detail_pr_finance_no_ud',$this->data);
        }
        else{
            $this->data['list_fin'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount_ud($pr_no); 
             $this->data['pr_no'] =$pr_no; 
             $this->admintemp->view('backend/purchasereq/list_detail_pr_finance',$this->data);
        }


    }


    public function list_approval_finance()
    {
        $this->data['appfinance'] = $this->m_purchase->get_data_app_finance();
        // log_r($this->data['appwerehouse']);
        $this->admintemp->view('backend/purchasereq/list_approval_finance',$this->data);
    }

     public function list_history_pr_finance()
    {
        $this->data['historyfin'] = $this->m_purchase->get_history_finance();
        // log_r($this->data['historyfin']);
        $this->admintemp->view('backend/purchasereq/history_pr_finance',$this->data);
    }

    public function decline_pr_finance($pr_no="",$reason="")
    {
         $hariIni        = new DateTime();
        $data_pr = array(
                'pr_no'               =>$pr_no,
                'date_reject_finance'     =>$hariIni->format('y-m-d H:i:s'),
                'reason_reject_finance'   =>$reason,
                'is_reject_finance'      =>1,
            );

        $status_pr = $this->m_purchase->update_detail_pr($pr_no,$data_pr);
        $this->session->set_flashdata('success', 'Success Decline PR !');
        redirect('backend/purchase/list_approval_finance');
    }

    public function approve_pr_finance($pr_no="")
    {
        $hariIni        = new DateTime();
        $data_pr = array(
                'pr_no'               =>$pr_no,
                'date_approve_finance'    =>$hariIni->format('y-m-d H:i:s'),
                'is_approve_finance'      =>1,
            );
        $data_detail_pr = $this->m_purchase->get_data_detail_pr($pr_no);
        $user_division_head = $data_detail_pr->user_division_head;
        $user_req = $data_detail_pr->user_id;

     $status_pr = $this->m_purchase->update_detail_pr($pr_no,$data_pr);
      $this->send_create_pr_head($pr_no,$user_division_head,$user_req);
     $this->session->set_flashdata('success', 'Success Approve PR !');
    
    }

     function send_create_pr_head($pr_no='', $user_division_head='',$user_req='')
    {
        $this->load->library('mailer');

        $get_req   = $this->m_purchase->get_user_row($user_req);//get requestor id
        $email_req = $get_req->email;
        $this->data['name_req']  =  $get_req->first_name;
        $this->data['currency'] = $this->m_purchase->get_data_pr_limit_1($pr_no);
        $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
        $get_to   = $this->m_purchase->get_user_row($user_division_head);//get to id "Sajith"
         // log_r($get_to);
        $email_to = $get_to->email;
        // $email_to = "abdul.razak@asiagalaxy.com";
        // log_r($email_to);
        $this->data['name_to']  =  $get_to->first_name;
        $this->data['approve_to'] = "Head Division";
        

        $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
        $this->data['pr_data'] = $this->m_purchase->get_data_pr_row($pr_no);
        
        // log_r($this->data['pr_data']->qty_cb);

        // $email_penerima          = "arif@asiagalaxy.com";
        $subjek                     = "PR Request"." ".$pr_no;
        $this->data['pr_no']    = $pr_no;
        $this->data['date']     =$get_data_detail_pr->create_at;
        $this->data['line_mc_no']   = $get_data_detail_pr->line_mc_no;
        $this->data['mc_no']        = $get_data_detail_pr->mc_no;
        $this->data['urgency']        = $get_data_detail_pr->urgency;
        $this->data['claimable']    = $get_data_detail_pr->claimable;
        $this->data['should_arrive_by']     = $get_data_detail_pr->should_arrive_by;
        $this->data['uncurrent_demand'] = $get_data_detail_pr->is_uncurrent_demand;
        $this->data['expected_material_date_arrive'] = $get_data_detail_pr->expected_material_date_arrive;
        // log_r(); 

        if($this->data['uncurrent_demand']==0)
        {
             $data_list_pur = $this->m_purchase->get_data_purchasing_field($pr_no); 
            $data_currency = $this->m_purchase->get_currency_non_ud($data_list_pur[0]->id_item);
            $this->data['currency'] = $data_currency->currency;

             $this->data['pr_data'] =$data_list_pur;
              $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no); 
            $content = $this->load->view('backend/purchasereq/mail_create_pr_purchasing_no_ud', $this->data, true);//view email
        }
        else {
            // log_r("current");
             $this->data['pr_data'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
             $data_currency = $this->m_purchase->get_data_pr_limit_1($pr_no);
            if(!empty($data_currency))
            {
                $this->data['currency'] = $data_currency->currency;
            }
            else {
                $this->data['currency'] = "";
            }
             // log_r($this->data['pr_data']);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount_ud($pr_no); 
            $content = $this->load->view('backend/purchasereq/mail_create_pr_purchasing', $this->data, true);//view email
        }

        // $content = $this->load->view('backend/purchasereq/mail_create_pr_purchasing', $this->data, true);//view email

        $mailto = array(
            // 'penerima_satu' => $email_req,
            'penerima_satu'  => $email_to,
            // 'penerima_satu' => "abdul.razak@asiagalaxy.com",
        );



        $ccmail = array(
            $cc_satu  = 'noreply@asiagalaxy.com',//email requestor
            // $cc_dua   = "iyappan@asiagalaxy.com",//email iyappan
            // $cc_tiga  = "shyju@asiagalaxy.com",//email shyju
            // $cc_empat = "phuspitaa@asiagalaxy.com",//email phuspita
            // $cc_lima  = "dyah@asiagalaxy.com",//email dyah
            // $cc_satu = "windu@asiagalaxy.com",//email windu
        );

        $sendmail = array(
            'email_penerima' => "",
            'subjek'  => $subjek,
            'content' => $content,
        );

        $send = $this->mailer->send($sendmail, $mailto, $ccmail); // jalankan fungsi php mailer
        $status = $send['status'];
        if ($status == 'Sukses') {
            $this->session->set_flashdata('success', 'Sending email was successful !');
            // redirect('backend/purchase/list_approval_finance');
        }elseif($status == 'Gagal'){
            $this->session->set_flashdata('error', 'Sending email failed !');
            // redirect('backend/purchase/list_approval_finance');
        }
    }

    public function list_approval_director()
    {
         $this->data['apphead'] = $this->m_purchase->get_data_app_head();
        $this->admintemp->view('backend/purchasereq/list_approval_head_division',$this->data);
    }

    public function list_detail_pr_director($pr_no)
    {
        //  $this->data['list_dir'] = $this->m_purchase->get_data_pr_row($pr_no);
        // $this->data['pr_no'] = $pr_no; 
        // $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
        // $this->data['currency'] = $this->m_purchase->get_data_pr_limit_1($pr_no);
        // $this->admintemp->view('backend/purchasereq/form_fill_director',$this->data);

        // $this->data['currency'] = $this->m_purchase->get_data_pr_limit_1($pr_no);
         $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
       
         if($get_data_detail_pr->is_uncurrent_demand==0)
        {
            $this->data['list_dir'] = $this->m_purchase->get_data_purchasing_field($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
             $this->data['pr_no'] =$pr_no; 
             $this->admintemp->view('backend/purchasereq/form_fill_director',$this->data);
        }
        else{

            // log_r("ud");
            $this->data['list_dir'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount_ud($pr_no); 
             $this->data['pr_no'] =$pr_no; 
             $this->admintemp->view('backend/purchasereq/form_fill_director_ud',$this->data);
        }


    }
     public function list_history_head()
    {
        $this->data['historyhead'] = $this->m_purchase->get_data_history_head();
        // log_r($this->data['historyhead']);
        $this->admintemp->view('backend/purchasereq/history_pr_head',$this->data);
    }
    public function list_detail_history_director($pr_no)
    {
      
         $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
       
         if($get_data_detail_pr->is_uncurrent_demand==0)
        {


            $this->data['list_dir'] = $this->m_purchase->get_data_purchasing_field($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
             $this->data['pr_no'] =$pr_no; 
             $this->admintemp->view('backend/purchasereq/list_detail_pr_director',$this->data);
        }
        else{

            // log_r("ud");
            $this->data['list_fin'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount_ud($pr_no); 
             $this->data['pr_no'] =$pr_no; 
             $this->admintemp->view('backend/purchasereq/list_detail_pr_director_no_ud',$this->data);
        }

    }

     public function decline_pr_head($pr_no="",$reason="")
    {
         $hariIni        = new DateTime();
        $data_pr = array(
                'pr_no'               =>$pr_no,
                'date_reject_division_head'     =>$hariIni->format('y-m-d H:i:s'),
                'reason_reject_divison_head'   =>$reason,
                'is_reject_division_head'      =>1,
            );

        $status_pr = $this->m_purchase->update_detail_pr($pr_no,$data_pr);
        $this->session->set_flashdata('success', 'Success Decline PR !');
        redirect('backend/purchase/list_approval_director');
    }

    public function approve_pr_head($pr_no="")
    {
        $hariIni        = new DateTime();
        $data_pr = array(
                'pr_no'               =>$pr_no,
                'date_approve_division_head'    =>$hariIni->format('y-m-d H:i:s'),
                'is_approve_division_head'      =>1,
            );
        $data_detail_pr = $this->m_purchase->get_data_detail_pr($pr_no);
        $user_pd = $data_detail_pr->user_pd;
        $user_req = $data_detail_pr->user_id;

     $status_pr = $this->m_purchase->update_detail_pr($pr_no,$data_pr);
     $this->session->set_flashdata('success', 'Success Approve PR !');
     $this->send_create_pr_pd($pr_no,$user_pd,$user_req);
    }

     function send_create_pr_pd($pr_no='', $user_pd='',$user_req='')
    {
        $this->load->library('mailer');

        $get_req   = $this->m_purchase->get_user_row($user_req);//get requestor id
        $email_req = $get_req->email;
        $this->data['name_req']  =  $get_req->first_name;
        $this->data['currency'] = $this->m_purchase->get_data_pr_limit_1($pr_no);
        $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
        $get_to   = $this->m_purchase->get_user_row($user_pd);//get to id "Sajith"
         // log_r($get_to);
        $email_to = $get_to->email;
        // $email_to = "abdul.razak@asiagalaxy.com";
        // log_r($email_to);
        $this->data['name_to']  =  $get_to->first_name;
        $this->data['approve_to'] = "President Director";
        

        $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
        $this->data['pr_data'] = $this->m_purchase->get_data_pr_row($pr_no);
        $pr_uncurrent_demand = $this->m_purchase->get_data_pr_row_no_ud($pr_no);
 
        // log_r($this->data['pr_data']->qty_cb);

        // $email_penerima          = "arif@asiagalaxy.com";
        $subjek                     = "PR Request"." ".$pr_no;
        $this->data['pr_no']    = $pr_no;
        $this->data['date']     =$get_data_detail_pr->create_at;
        $this->data['line_mc_no']   = $get_data_detail_pr->line_mc_no;
        $this->data['mc_no']        = $get_data_detail_pr->mc_no;
        $this->data['urgency']        = $get_data_detail_pr->urgency;
        $this->data['claimable']    = $get_data_detail_pr->claimable;
        $this->data['should_arrive_by']     = $get_data_detail_pr->should_arrive_by;
        $this->data['uncurrent_demand'] = $get_data_detail_pr->is_uncurrent_demand;
        $this->data['expected_material_date_arrive'] = $get_data_detail_pr->expected_material_date_arrive;
        // $content = $this->load->view('backend/purchasereq/mail_create_pr_purchasing', $this->data, true);//view email
        if($this->data['uncurrent_demand']==0)
        {
            // log_r("no un current");
             $this->data['pr_data'] = $this->m_purchase->get_data_purchasing_field($pr_no);
              $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no); 
            $content = $this->load->view('backend/purchasereq/mail_create_pr_purchasing_no_ud', $this->data, true);//view email
        }
        else {
            // log_r("current"); 
             $this->data['pr_data'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no); 
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount_ud($pr_no); 
            $content = $this->load->view('backend/purchasereq/mail_create_pr_purchasing', $this->data, true);//view email
        }
        $mailto = array(
            // 'penerima_satu' => $email_req,
            // 'penerima_satu'  => $email_to,
            'penerima_satu' => "abdul.razak@asiagalaxy.com",
        );



        $ccmail = array(
            $cc_satu  = 'noreply@asiagalaxy.com',//email requestor
            // $cc_dua   = "iyappan@asiagalaxy.com",//email iyappan
            // $cc_tiga  = "shyju@asiagalaxy.com",//email shyju
            // $cc_empat = "phuspitaa@asiagalaxy.com",//email phuspita
            // $cc_lima  = "dyah@asiagalaxy.com",//email dyah
            // $cc_satu = "windu@asiagalaxy.com",//email windu
        );

        $sendmail = array(
            'email_penerima' => "",
            'subjek'  => $subjek,
            'content' => $content,
        );

        $send = $this->mailer->send($sendmail, $mailto, $ccmail); // jalankan fungsi php mailer
        $status = $send['status'];
        if ($status == 'Sukses') {
            $this->session->set_flashdata('success', 'Sending email was successful !');
            redirect('backend/purchase/list_approval_director');
        }elseif($status == 'Gagal'){
            $this->session->set_flashdata('error', 'Sending email failed !');
            redirect('backend/purchase/list_approval_director');
        }
    }


     public function list_approval_pd()
    {
         $this->data['apppd'] = $this->m_purchase->get_data_app_pd();
         $this->admintemp->view('backend/purchasereq/list_approval_pd',$this->data);
    }

    public function list_detail_pr_pd($pr_no)
    {
        //  $this->data['list_dir'] = $this->m_purchase->get_data_purchasing_field($pr_no);
        // $this->data['pr_no'] = $pr_no; 
        // $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
        // $this->data['currency'] = $this->m_purchase->get_data_pr_limit_1($pr_no);
        // $this->admintemp->view('backend/purchasereq/form_fill_pd',$this->data);

        $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
       
         if($get_data_detail_pr->is_uncurrent_demand==0)
        {
            // log_r("tida ud");
            $this->data['list_dir'] = $this->m_purchase->get_data_purchasing_field($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
             $this->data['pr_no'] =$pr_no; 
             $this->admintemp->view('backend/purchasereq/form_fill_pd',$this->data);
        }
        else{

            // log_r("ud");
            $this->data['list_dir'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount_ud($pr_no); 
             $this->data['pr_no'] =$pr_no; 
             $this->admintemp->view('backend/purchasereq/form_fill_pd_ud',$this->data);
        }
    }

    public function list_history_pd()
    {
        $this->data['historypd'] = $this->m_purchase->get_data_history_pd();
        // log_r($this->data['historyhead']);
        $this->admintemp->view('backend/purchasereq/history_pr_pd',$this->data);
    }

     public function list_detail_history_pd($pr_no)
    {
        //  $this->data['list_pd'] = $this->m_purchase->get_data_pr_row($pr_no);
        // $this->data['pr_no'] = $pr_no; 
        // $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
        $this->data['currency'] = $this->m_purchase->get_data_pr_limit_1($pr_no);
        // $this->admintemp->view('backend/purchasereq/list_detail_pr_pd',$this->data);

        $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
       
         if($get_data_detail_pr->is_uncurrent_demand==0)
        {
            // log_r("tida ud");
            $this->data['list_pd'] = $this->m_purchase->get_data_purchasing_field($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
             $this->data['pr_no'] =$pr_no; 
             $this->admintemp->view('backend/purchasereq/list_detail_pr_pd',$this->data);
        }
        else{

            // log_r("ud");
            $this->data['list_pd'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount_ud($pr_no); 
             $this->data['pr_no'] =$pr_no; 
             $this->admintemp->view('backend/purchasereq/list_detail_pr_pd_ud',$this->data);
        }


    }

   

     public function decline_pr_pd($pr_no="",$reason="")
    {
         $hariIni        = new DateTime();
        $data_pr = array(
                'pr_no'               =>$pr_no,
                'date_reject_by_pd'     =>$hariIni->format('y-m-d H:i:s'),
                'reason_reject_by_pd'   =>$reason,
                'is_reject_by_pd'      =>1,
            );

        $status_pr = $this->m_purchase->update_detail_pr($pr_no,$data_pr);
        $this->session->set_flashdata('success', 'Success Decline PR !');
        // redirect('backend/purchase/list_approval_pd');
    }

    public function approve_pr_pd($pr_no="")
    {
        $hariIni        = new DateTime();
        $data_pr = array(
                'pr_no'               =>$pr_no,
                'date_approve_by_pd'    =>$hariIni->format('y-m-d H:i:s'),
                'is_approve_by_pd'      =>1,
            );

        $data_detail_pr = $this->m_purchase->get_data_detail_pr($pr_no);
        $user_purchase = $data_detail_pr->user_purchase;
        $user_req = $data_detail_pr->user_id;

         $status_pr = $this->m_purchase->update_detail_pr($pr_no,$data_pr);
         $this->session->set_flashdata('success', 'Success Approve PR !');

        // redirect('backend/purchase/list_approval_pd');
         $this->send_create_pr_pd_to_purchase($pr_no,$user_purchase,$user_req);
    }


    function send_create_pr_pd_to_purchase($pr_no='', $user_purchase='',$user_req='')
    {
        $this->load->library('mailer');

        $get_req   = $this->m_purchase->get_user_row($user_req);//get requestor id
        $email_req = $get_req->email;
        $this->data['name_req']  =  $get_req->first_name;
        $this->data['currency'] = $this->m_purchase->get_data_pr_limit_1($pr_no);
        $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
        $get_to   = $this->m_purchase->get_user_row($user_purchase);//get to id "Sajith"
         // log_r($get_to);
        $email_to = $get_to->email;
        // $email_to = "abdul.razak@asiagalaxy.com";
        // log_r($email_to);
        $this->data['name_to']  =  $get_to->first_name;
        $this->data['approve_to'] = "Purchasing";
        

        $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);

        // $email_penerima          = "arif@asiagalaxy.com";
        $subjek                     = "PR Approved"." ".$pr_no;
        $this->data['pr_no']    = $pr_no;
        $this->data['date']     =$get_data_detail_pr->create_at;
        $this->data['line_mc_no']   = $get_data_detail_pr->line_mc_no;
        $this->data['mc_no']        = $get_data_detail_pr->mc_no;
        $this->data['urgency']        = $get_data_detail_pr->urgency;
        $this->data['claimable']    = $get_data_detail_pr->claimable;
        $this->data['should_arrive_by']     = $get_data_detail_pr->should_arrive_by;
        $this->data['uncurrent_demand'] = $get_data_detail_pr->is_uncurrent_demand;
        $this->data['expected_material_date_arrive'] = $get_data_detail_pr->expected_material_date_arrive;
        // log_r();

        
            

        if($this->data['uncurrent_demand']==0)
        {
            // log_r("no un current");
             $this->data['pr_data'] = $this->m_purchase->get_data_purchasing_field($pr_no);
              $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no); 
            $content = $this->load->view('backend/purchasereq/mail_create_pr_pd_to_purchase_no_ud', $this->data, true);//view email
        }
        else {
            // log_r("current");
             $this->data['pr_data'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount_ud($pr_no); 
            $content = $this->load->view('backend/purchasereq/mail_create_pr_pd_to_purchase', $this->data, true);//view email
        }
        
        $mailto = array(
            // 'penerima_satu' => $email_req,
            'penerima_satu'  => $email_to,
            // 'penerima_satu' => "arif@asiagalaxy.com",
        );



        $ccmail = array(
            $cc_satu  = 'noreply@asiagalaxy.com',//email requestor
            // $cc_dua   = "iyappan@asiagalaxy.com",//email iyappan
            // $cc_tiga  = "shyju@asiagalaxy.com",//email shyju
            // $cc_empat = "phuspitaa@asiagalaxy.com",//email phuspita
            // $cc_lima  = "dyah@asiagalaxy.com",//email dyah
            // $cc_satu = "windu@asiagalaxy.com",//email windu
        );

        $sendmail = array(
            'email_penerima' => "",
            'subjek'  => $subjek,
            'content' => $content,
        );

        $send = $this->mailer->send($sendmail, $mailto, $ccmail); // jalankan fungsi php mailer
        $status = $send['status'];
        if ($status == 'Sukses') {
            $this->session->set_flashdata('success', 'Sending email was successful !');
            redirect('backend/purchase/list_approval_pd');
        }elseif($status == 'Gagal'){
            $this->session->set_flashdata('error', 'Sending email failed !');
            redirect('backend/purchase/list_approval_pd');
        }
    }



    function send_approved_pr_purchase($pr_no='', $user_purchase='',$user_req='')
    {
        $this->load->library('mailer');

        $get_req   = $this->m_purchase->get_user_row($user_req);//get requestor id
        $email_req = $get_req->email;
        $this->data['name_req']  =  $get_req->first_name;
        $this->data['currency'] = $this->m_purchase->get_data_pr_limit_1($pr_no);
        $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
        $get_to   = $this->m_purchase->get_user_row($user_purchase);//get to id "Sajith"
         // log_r($get_to);
        $email_to = $get_to->email;
        // $email_to = "abdul.razak@asiagalaxy.com";
        // log_r($email_to);
        $this->data['name_to']  =  $get_to->first_name;
        $this->data['approve_to'] = "President Director";
        $this->data['expected_material_date_arrive'] = $get_data_detail_pr->expected_material_date_arrive;

        $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
        $this->data['pr_data'] = $this->m_purchase->get_data_pr_row($pr_no);

        // log_r($this->data['pr_data']->qty_cb);

        // $email_penerima          = "arif@asiagalaxy.com";
        $subjek                     = "PR Request"." ".$pr_no;
        $this->data['pr_no']    = $pr_no;
        $this->data['date']     =$get_data_detail_pr->create_at;
        $this->data['line_mc_no']   = $get_data_detail_pr->line_mc_no;
        $this->data['mc_no']        = $get_data_detail_pr->mc_no;
        $this->data['urgency']        = $get_data_detail_pr->urgency;
        $this->data['claimable']    = $get_data_detail_pr->claimable;
        $this->data['should_arrive_by']     = $get_data_detail_pr->should_arrive_by;
        
        $content = $this->load->view('backend/purchasereq/mail_create_approved_pd', $this->data, true);//view email
        $mailto = array(
            // 'penerima_satu' => $email_req,
            'penerima_satu'  => $email_to,
            // 'penerima_satu' => "arif@asiagalaxy.com",
        );



        $ccmail = array(
            $cc_satu  = 'noreply@asiagalaxy.com',//email requestor
            // $cc_dua   = "iyappan@asiagalaxy.com",//email iyappan
            // $cc_tiga  = "shyju@asiagalaxy.com",//email shyju
            // $cc_empat = "phuspitaa@asiagalaxy.com",//email phuspita
            // $cc_lima  = "dyah@asiagalaxy.com",//email dyah
            // $cc_satu = "windu@asiagalaxy.com",//email windu
        );

        $sendmail = array(
            'email_penerima' => "",
            'subjek'  => $subjek,
            'content' => $content,
        );

        $send = $this->mailer->send($sendmail, $mailto, $ccmail); // jalankan fungsi php mailer
        $status = $send['status'];
        if ($status == 'Sukses') {
            $this->session->set_flashdata('success', 'Sending email was successful !');
            redirect('backend/purchase/list_approval_director');
        }elseif($status == 'Gagal'){
            $this->session->set_flashdata('error', 'Sending email failed !');
            redirect('backend/purchase/list_approval_director');
        }
    }

     public function list_data_all_pr()
    {
        $this->data['allpr'] = $this->m_purchase->get_data_all_pr();
        $this->admintemp->view('backend/purchasereq/list_data_all_pr',$this->data);
    }
    public function view_print_pr($pr_no)
    {
        // $this->data['printpr'] = $this->m_purchase->get_data_pr_row($pr_no);
        // $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
        // $this->data['currency'] = $this->m_purchase->get_data_pr_limit_1($pr_no);
        $this->data['signature'] =$this->m_purchase->get_data_print_pr($pr_no);
        $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
        $this->data['pr_no'] = $pr_no; 
         $this->data['date']     =$get_data_detail_pr->create_at;
        $this->data['line_mc_no']   = $get_data_detail_pr->line_mc_no;
        $this->data['mc_no']        = $get_data_detail_pr->mc_no;
        $this->data['urgency']        = $get_data_detail_pr->urgency;
        $this->data['claimable']    = $get_data_detail_pr->claimable;
        $this->data['department']    = $get_data_detail_pr->department;

        // $this->data['reason']    = $get_data_detail_pr->reason;
        $this->data['should_arrive_by']     = $get_data_detail_pr->should_arrive_by;
        
        if(empty($get_data_detail_pr->po_no))
        {
            $this->data['po_no'] = "";
        }
        else
        {
            $this->data['po_no'] = $get_data_detail_pr->po_no;
        }

         $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);
       
         if($get_data_detail_pr->is_uncurrent_demand==0)
        {
            // log_r("non ud");
            $this->data['printpr'] = $this->m_purchase->get_data_purchasing_field($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
             $this->data['pr_no'] =$pr_no; 

             $this->data['expected_material_date_arrive'] = $get_data_detail_pr->expected_material_date_arrive;
             $this->usertemp->view('backend/purchasereq/print_pr',$this->data);
        }
        else{

            // log_r("ud");
            $this->data['printpr'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount_ud($pr_no); 
             $this->data['pr_no'] =$pr_no; 
             $this->data['expected_material_date_arrive'] = $get_data_detail_pr->expected_material_date_arrive;
             $this->usertemp->view('backend/purchasereq/print_pr_ud',$this->data);
        }

    }
    public function export_to_pdf($pr_no)
    {

        $this->load->library('pdfgenerator');
        
        // title dari pdf
        $this->data['title_pdf'] = 'PR GIH'.'_'.$pr_no;
        
        // filename dari pdf ketika didownload
        $file_pdf = 'PR GIH'.'_'.$pr_no;
        // setting paper
        $paper = 'A4';
        //orientasi paper potrait / landscape
        $orientation = "landscape";

        $this->data['signature'] =$this->m_purchase->get_data_print_pr($pr_no);
        $get_data_detail_pr = $this->m_purchase->get_data_detail_pr_row($pr_no);

        $this->data['edit_data'] = $this->m_po->get_data_latter_head();

         $this->data['date']     =$get_data_detail_pr->create_at;
        $this->data['line_mc_no']   = $get_data_detail_pr->line_mc_no;
        $this->data['mc_no']        = $get_data_detail_pr->mc_no;
        $this->data['urgency']        = $get_data_detail_pr->urgency;
        $this->data['claimable']    = $get_data_detail_pr->claimable;
        $this->data['department']    = $get_data_detail_pr->department;

        
        $this->data['gih_tool_no'] = $get_data_detail_pr->gih_tool_no;
        

        if($get_data_detail_pr->is_uncurrent_demand==0)
        {
            // log_r("non ud");
            $this->data['printpr'] = $this->m_purchase->get_data_purchasing_field($pr_no);
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount($pr_no);
             $this->data['pr_no'] =$pr_no; 
             $this->data['po_no'] =$get_data_detail_pr->po_no_real; 
             $this->data['expected_material_date_arrive'] = $get_data_detail_pr->expected_material_date_arrive;
             $html = $this->load->view('backend/purchasereq/print_pdf_pr',$this->data, true);     
            $this->pdfgenerator->generate($html, $file_pdf,$paper,$orientation);
        }
        else{

            // log_r("ud");
            $po_no = $this->m_purchase->get_po_no_release($pr_no);

            if(!empty($po))
            {
                $this->data['po_no'] = $po_no->po_no;
            }
            else{
                $this->data['po_no'] = "";
            }
            // log_r($po_no);
            
            $this->data['printpr'] = $this->m_purchase->get_data_list_detail_pr_ud($pr_no);
            // log_r($this->data['printpr'])
             $this->data['ttl_sum'] = $this->m_purchase->get_sum_ttl_ammount_ud($pr_no); 
             $this->data['pr_no'] =$pr_no; 
             $this->data['expected_material_date_arrive'] = $get_data_detail_pr->expected_material_date_arrive;
              $html = $this->load->view('backend/purchasereq/print_pdf_pr_ud',$this->data, true);     
                $this->pdfgenerator->generate($html, $file_pdf,$paper,$orientation);
        }


    }

 public function list_cost_center()
 {
    $this->data['cost_center'] = $this->m_purchase->get_data_cost_center();
    $this->admintemp->view('backend/purchasereq/list_cost_center',$this->data);

 }

 public function cost_center_create()
 {
        $hariIni        = new DateTime();
        if (empty($this->input->post())) {
            // $this->data['customer'] = $this->m_purchase->get_customer();
            $this->admintemp->view('backend/purchasereq/cost_center_create');
        }elseif (!empty($this->input->post())) {
            $cost_center = $this->input->post('cost_center');
            $dept        = $this->input->post('departement');
            $remarks        = $this->input->post('remark');
           
            $data = array(
                'cost_center'        => $cost_center,
                'dept' => $dept,
                'remarks'        => $remarks,
                'create_at'   => $hariIni->format('y-m-d H:i:s'),
                'user_id'     => USER_ID,
            );
            $status = $this->m_purchase->insert_cost_center($data);
            if ($status == 1) {//Jika Success Insert
                $this->session->set_flashdata('success', 'Your data successfully added !');
                redirect('backend/purchase/list_cost_center');
            }else if($status == 'error'){
                $this->session->set_flashdata('error', 'Cost Center Code is available !');
                redirect('backend/purchase/list_cost_center');
            }
        }
 }
 public function cost_center_update($id='')
 {
    $hariIni        = new DateTime();

    // log_r($id);
     if (empty($this->input->post())) {
            $this->data['cost'] = $this->m_purchase->get_data_cost_row($id);

            $this->admintemp->view('backend/purchasereq/update_cost_center',$this->data);
        }elseif (!empty($this->input->post())) {

            $id = $this->input->post('id');
            $cost_center = $this->input->post('cost_center');
            $dept        = $this->input->post('departement');
            $remarks        = $this->input->post('remark');
           
           
            $data = array(
                'id' => $id,
                'cost_center'        => $cost_center,
                'dept' => $dept,
                'remarks'        => $remarks,
                'update_at'   => $hariIni->format('y-m-d H:i:s'),
                'user_id'     => USER_ID,
            );

            // log_r($id);
            $status = $this->m_purchase->update_cost_center($id,$data);
            if ($status == 1) {//Jika Success Insert
                $this->session->set_flashdata('success', 'Your data successfully added !');
                redirect('backend/purchase/list_cost_center');
            }else if($status == 'error'){
                $this->session->set_flashdata('error', 'Cost Center Code is available !');
                redirect('backend/purchase/list_cost_center');
            }
        }
 }

 public function template_excel($n_file=''){
        // log_r($n_file);
        $file = './src/assets/template/'.$n_file;
        force_download($file, NULL);
        die();
    }

public function import_excel_cost_center()
    {
            if(isset($_FILES["file"]["name"])){
        // upload
            $hariIni     = new DateTime();
            $file_tmp = $_FILES['file']['tmp_name'];
            $file_name = $_FILES['file']['name'];
            $file_size =$_FILES['file']['size'];
            $file_type=$_FILES['file']['type'];
            $object = PHPExcel_IOFactory::load($file_tmp);
        
            foreach($object->getWorksheetIterator() as $worksheet){
        
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=3; $row<=$highestRow; $row++){
                    $cost_center        = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $dept = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $remarks = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $data[] = array(
                        'cost_center'          =>$cost_center,
                        'dept'          => $dept,
                        'remarks'          => $remarks,
                        'user_id'      => USER_ID,
                        'create_at'     => $hariIni->format('y-m-d H:i:s'),
                    );
                }
            }
            move_uploaded_file($file_tmp,"./src/excelfile/".$hariIni->format('y-m-d')."".$file_name); // simpan filenya di folder uploads
            $status = $this->db->insert_batch('cost_center', $data);
            if ($status = 1) {//Jika Success Insert
                $this->session->set_flashdata('success', 'Your data successfully added !');
                redirect('backend/purchase/list_cost_center');
            }else{
                $this->session->set_flashdata('error', 'data failed to upload !');
                redirect('backend/purchase/list_cost_center');
            }
            
        }else{
           $this->session->set_flashdata('error', 'data failed to upload !');
           redirect('backend/purchase/list_cost_center');
        }
    }

    public function list_category_request()
    {
        $this->data['cat_req'] = $this->m_purchase->get_data_category_request();
        $this->admintemp->view('backend/purchasereq/list_category_request',$this->data);
    }

    public function create_category_request()
    {
            $hariIni        = new DateTime();
     if (empty($this->input->post())) {
            $this->admintemp->view('backend/purchasereq/category_request_create');
        }elseif (!empty($this->input->post())) {
            $category_request = $this->input->post('category_request');
            $code        = $this->input->post('code');
            $remarks        = $this->input->post('remark');
           
           
            $data = array(
                'category_request'        => $category_request,
                'code' => $code,
                'remarks'        => $remarks,
                'create_at'   => $hariIni->format('y-m-d H:i:s'),
                'user_id'     => USER_ID,
            );
            $status = $this->m_purchase->insert_category_request($data);
            if ($status == 1) {//Jika Success Insert
                $this->session->set_flashdata('success', 'Your data successfully added !');
                redirect('backend/purchase/list_category_request');
            }else if($status == 'error'){
                $this->session->set_flashdata('error', 'Cost Center Code is available !');
                redirect('backend/purchase/list_category_request');
            }
        }
    }

    public function import_excel_category_request()
    {
        if(isset($_FILES["file"]["name"])){
        // upload
            $hariIni     = new DateTime();
            $file_tmp = $_FILES['file']['tmp_name'];
            $file_name = $_FILES['file']['name'];
            $file_size =$_FILES['file']['size'];
            $file_type=$_FILES['file']['type'];
            $object = PHPExcel_IOFactory::load($file_tmp);
        
            foreach($object->getWorksheetIterator() as $worksheet){
        
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=3; $row<=$highestRow; $row++){
                    $code        = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $category_request = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $remarks = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $data[] = array(
                        'code'          =>$code,
                        'category_request'          => $category_request,
                        'remarks'          => $remarks,
                        'user_id'      => USER_ID,
                        'create_at'     => $hariIni->format('y-m-d H:i:s'),
                    );
                }
            }
            move_uploaded_file($file_tmp,"./src/excelfile/".$hariIni->format('y-m-d')."".$file_name);
             // simpan filenya di folder uploads
            $status = $this->db->insert_batch('category_request', $data);
            if ($status = 1) {//Jika Success Insert
                $this->session->set_flashdata('success', 'Your data successfully added !');
                redirect('backend/purchase/list_category_request');
            }else{
                $this->session->set_flashdata('error', 'data failed to upload !');
                redirect('backend/purchase/list_category_request');
            }
            
        }else{
           $this->session->set_flashdata('error', 'data failed to upload !');
           redirect('backend/purchase/list_category_request');
        }
    }

 public function category_request_update($id='')
 {
    $hariIni        = new DateTime();

    // log_r($id);
     if (empty($this->input->post())) {
            $this->data['cat_req'] = $this->m_purchase->get_data_category_row($id);
            // log_r($this->data['cat_req']);
            $this->admintemp->view('backend/purchasereq/update_category_request',$this->data);
        }elseif (!empty($this->input->post())) {

            $id = $this->input->post('id');
            $code = $this->input->post('code');
            $category_request        = $this->input->post('category_request');
            $remarks        = $this->input->post('remark');
           
           
            $data = array(
                'id' => $id,
                'code'        => $code,
                'category_request' => $category_request,
                'remarks'        => $remarks,
                'update_at'   => $hariIni->format('y-m-d H:i:s'),
                'user_id'     => USER_ID,
            );

            // log_r($id);
            $status = $this->m_purchase->update_category_request($id,$data);
            if ($status == 1) {//Jika Success Insert
                $this->session->set_flashdata('success', 'Your data successfully added !');
                redirect('backend/purchase/list_category_request');
            }else if($status == 'error'){
                $this->session->set_flashdata('error', 'Cost Center Code is available !');
                redirect('backend/purchase/list_category_request');
            }
        }
 }

 public function list_data_supplier()
 {
    $this->data['sup'] = $this->m_purchase->get_data_supplier();
    $this->admintemp->view('backend/purchasereq/list_supplier',$this->data);
 }

 public function create_data_supplier()
 {
     $hariIni        = new DateTime();
        if (empty($this->input->post())) {
            $this->admintemp->view('backend/purchasereq/supplier_create');
        }elseif (!empty($this->input->post())) {
            $name       = $this->input->post('supplier_name');
            $address    = $this->input->post('address');
            $telpon    = $this->input->post('telpon');
            $address_2    = $this->input->post('address_2');
            $telpon    = $this->input->post('telpon');
            $loc_code    = $this->input->post('loc_code');
            $payment_term    = $this->input->post('payment_term');
            $attn    = $this->input->post('attn');
            $type_of_purchase    = $this->input->post('type_of_purchase');
            $type_of_bussines    = $this->input->post('type_of_bussines');
            $email    = $this->input->post('email');
            $last_code  = $this->m_purchase->last_code_supp();
            if (empty($last_code)) {
                $code = "20001";
            }else{
                $code = $last_code->code + 1;
            }
            $data = array(
                'code'       => $code,
                'name'       => $name,
                'address'    => $address,
                'telpon'     => $telpon,
                'email'     => $email,
                'address_2'     => $address_2,
                'loc_code'     => $loc_code,
                'payment_term'     => $payment_term,
                'attn'     => $attn,
                'type_of_purchase'     => $type_of_purchase,
                'type_of_bussines'     => $type_of_bussines,
                'email'     => $email,
                'create_at'  => $hariIni->format('y-m-d H:i:s'),
                'user_id'    => USER_ID,
            );
            $status = $this->m_purchase->insert_supplier($data);
            if ($status == 1) {//Jika Success Insert
                $this->session->set_flashdata('success', 'Your data successfully added !');
                redirect('backend/purchase/create_data_supplier');
            }else if($status == 'error'){
                $this->session->set_flashdata('error', 'Code is available, please make a unique one !');
                redirect('backend/purchase/create_data_supplier');
            }
        }

 }

  public function update_data_supplier($code='')
 {
     $hariIni        = new DateTime();
     $data_row_supplier = $this->m_purchase->get_data_supplier_row($code);

        if (empty($this->input->post())) {
            $this->data['supplier'] = $data_row_supplier;
            $this->admintemp->view('backend/purchasereq/supplier_update',$this->data);
        }elseif (!empty($this->input->post())) {

 
            // $code       = $data_row_supplier->code;
            $code = $this->input->post('code');
            $name       = $this->input->post('supplier_name');
            $address    = $this->input->post('address');
            $telpon    = $this->input->post('telpon');
            $address_2    = $this->input->post('address_2');
            $telpon    = $this->input->post('telpon');
            $loc_code    = $this->input->post('loc_code');
            $payment_term    = $this->input->post('payment_term');
            $attn    = $this->input->post('attn');
            $type_of_purchase    = $this->input->post('type_of_purchase');
            $type_of_bussines    = $this->input->post('type_of_bussines');
            $email    = $this->input->post('email');

            $data = array(
                'code'       => $code,
                'name'       => $name,
                'address'    => $address,
                'address_2'    => $address_2,
                'telpon'     => $telpon,
                'email'     => $email,
                'loc_code'     => $loc_code,
                'payment_term'     => $payment_term,
                'type_of_bussines'     => $type_of_bussines,
                'attn'     => $attn,
                'type_of_purchase'     => $type_of_purchase,
                'update_at'  => $hariIni->format('y-m-d H:i:s'),
                'user_id'    => USER_ID,
            );
            $status = $this->m_purchase->update_supplier($code,$data);

            // log_r($status);
            if ($status == 1) {//Jika Success Insert
                $this->session->set_flashdata('success', 'Your data successfully Updated !');
                redirect('backend/purchase/update_data_supplier/'.$code);
            }else if($status == 'error'){
                $this->session->set_flashdata('error', 'Code is available, please make a unique one !');
                redirect('backend/purchase/update_data_supplier/'.$code);
            }
        }

 }



 public function list_data_item()
 {
    $this->data['item'] = $this->m_purchase->get_data_item();
    $this->admintemp->view('backend/purchasereq/list_master_item',$this->data);

 }

 public function item_create()
 {
    $hariIni        = new DateTime();
        if (empty($this->input->post())) {
            $this->data['sup'] = $this->m_purchase->get_data_supplier();
            $this->admintemp->view('backend/purchasereq/item_create',$this->data);
        }elseif (!empty($this->input->post())) {

            $description       = $this->input->post('description');
            $item_no       = $this->input->post('item_no');
            $unit_price    = $this->input->post('unit_price');
            $currency    = $this->input->post('currency');
            $supplier_name_pf    = $this->input->post('supplier_name_pf');

            $unit_price_int = str_replace(".","", $unit_price);
            $incoterm = $this->input->post('incoterm');

            $check_part_no = $this->m_purchase->get_check_part_no($item_no);

            if(empty($check_part_no))
            {
                $data = array(
                'item_no'       =>$item_no,
                'description'       => $description,
                'unit_price'       => $unit_price_int,
                'currency'    => $currency,
                'supplier_code'     => $supplier_name_pf,
                'incoterm'      =>$incoterm,
                'create_at'  => $hariIni->format('y-m-d H:i:s'),
                'user_id'    => USER_ID,
            );
                $this->db->set($data);
                $status = $this->m_purchase->insert_item($data);
                if ($status == 1) {//Jika Success Insert
                    $this->session->set_flashdata('success', 'Your data successfully Created !');
                    redirect('backend/purchase/item_create');
                }else if($status == 'error'){
                    $this->session->set_flashdata('error', 'Code is available, please make a unique one !');
                    redirect('backend/purchase/item_create');
                }
            }
            else{
                $this->session->set_flashdata('error', 'Part No is available, please make a unique one !');
                redirect('backend/purchase/item_create');
            }
            

        
        }
 }


  public function item_update($id='')
 {
    $hariIni        = new DateTime();
        if (empty($this->input->post())) {
            $this->data['sup'] = $this->m_purchase->get_data_supplier();
            $this->data['items'] = $this->m_purchase->get_data_items_row($id);
            $this->admintemp->view('backend/purchasereq/update_item',$this->data);
        }elseif (!empty($this->input->post())) {

            $description       = $this->input->post('description');
            $item_no       = $this->input->post('item_no');
            $unit_price    = $this->input->post('unit_price');
            $currency    = $this->input->post('currency');
            $supplier_name_pf    = $this->input->post('supplier_name_pf');
            $id_item    = $this->input->post('id_item');

            // log_r($id_item);
            $unit_price_int = str_replace(".","", $unit_price);
            $incoterm = $this->input->post('incoterm');

            $data = array(
                'id_item'   =>$id_item,
                'item_no'       =>$item_no,
                'description'       => $description,
                'unit_price'       => $unit_price_int,
                'currency'    => $currency,
                'supplier_code'     => $supplier_name_pf,
                'incoterm'      =>$incoterm,
                'create_at'  => $hariIni->format('y-m-d H:i:s'),
                'user_id'    => USER_ID,
            );

            
            $this->db->set($data);
            $status = $this->m_purchase->update_item($id_item,$data);
            if ($status == 1) {//Jika Success Insert
                $this->session->set_flashdata('success', 'Your data successfully Updated !');
                redirect('backend/purchase/item_update/'.$id_item);
            }else if($status == 'error'){
                $this->session->set_flashdata('error', 'Code is available, please make a unique one !');
                redirect('backend/purchase/item_update/'.$id_item);
            }
        }
 }


 



 public function import_excel_supplier()
    {
            if(isset($_FILES["file"]["name"])){
        // upload
            $hariIni     = new DateTime();
            $file_tmp = $_FILES['file']['tmp_name'];
            $file_name = $_FILES['file']['name'];
            $file_size =$_FILES['file']['size'];
            $file_type=$_FILES['file']['type'];
            $object = PHPExcel_IOFactory::load($file_tmp);
        
            foreach($object->getWorksheetIterator() as $worksheet){
        
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=3; $row<=$highestRow; $row++){
                    $code        = $worksheet->getCellByColumnAndRow(0, $row)->getFormattedValue();
                    $name = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $loc_code = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $payment_term = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $type_of_bussines = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $address = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $address_2 = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $email = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                    $telp = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                    $attn = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                    $status = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                    $f1_gk = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                    $type_purchase = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                    $data[] = array(
                        'code'          =>$code,
                        'name'          => $name,
                        'loc_code'          => $loc_code,
                        'payment_term'          => $payment_term,
                        'type_of_bussines'          => $type_of_bussines,
                        'address'          => $address,
                        'address_2'          => $address_2,
                        'email'          => $email,
                        'telpon'          => $telp,
                        'attn'          => $attn,
                        'status'          => $status,
                        'f1_gk'          => $f1_gk,
                        'type_of_purchase'          => $type_purchase,
                        'user_id'      => USER_ID,
                        'create_at'     => $hariIni->format('y-m-d H:i:s'),
                    );
                }
            }
            move_uploaded_file($file_tmp,"./src/excelfile/".$hariIni->format('y-m-d')."".$file_name); // simpan filenya di folder uploads
            $status = $this->db->insert_batch('supplier', $data);
            if ($status = 1) {//Jika Success Insert
                $this->session->set_flashdata('success', 'Your data successfully added !');
                redirect('backend/purchase/list_data_supplier');
            }else{
                $this->session->set_flashdata('error', 'data failed to upload !');
                redirect('backend/purchase/list_data_supplier');
            }
            
        }else{
           $this->session->set_flashdata('error', 'data failed to upload !');
           redirect('backend/purchase/list_data_supplier');
        }
    }

public function non_active_supplier($id='',$reason='')
{
    $data = array(
        'code'=>$id,
        'status'=>'N/A',
    );
    $status =$this->m_purchase->update_supplier($id,$data);
}

public function active_supplier($code='')
{
    $data = array(
        'code'  =>$code,
        'status' =>'A',
    );

    $status = $this->m_purchase->update_supplier($code,$data);

}






}
