<?php if (!defined('BASEPATH')) { exit ('No Direct Script Allowed'); }

class Supplier extends CI_Controller {

	public function __construct(){
		parent::__construct();
	    if(!$this->ion_auth->logged_in()){
	      redirect('auth/login', 'refresh');
	    }
	    date_default_timezone_set('Asia/Jakarta');
        $this->load->model('m_supplier');
        $this->load->helper('download');
	}

	public function index()
	{	
        
        check_permission_page(ID_GROUP,'read','supplier');

        $this->data['supplier'] = $this->m_supplier->get_data_supplier();
		$this->admintemp->view('backend/supplier/supplier_list',$this->data);
	}

	public function supplier_add()
	{
		 $hariIni        = new DateTime();
		 if (empty($this->input->post())) {
		 	$this->admintemp->view('backend/supplier/supplier_add');
		 }

		 elseif(!empty($this->input->post())) {
		 	$name       = $this->input->post('supplier_name');
            $address    = $this->input->post('address');
            $telpon    = $this->input->post('telpon');
            $last_code  = $this->m_supplier->last_code_supp();
            if (empty($last_code)) {
                $code = "10001";
            }else{
                $code = $last_code->code + 1;
            }
            $data = array(
                'code'       => $code,
                'name'       => $name,
                'address'    => $address,
                'telpon'    => $telpon,
                'create_at'  => $hariIni->format('y-m-d H:i:s'),
                'user_id'    => USER_ID,
            );
            $status = $this->m_supplier->insert_supplier($data);
            if ($status == 1) {//Jika Success Insert
                $this->session->set_flashdata('success', 'Your data successfully added !');
                redirect('backend/supplier/supplier_add');
            }else if($status == 'error'){
                $this->session->set_flashdata('error', 'Code is available, please make a unique one !');
                redirect('backend/supplier/supplier_adds');
            }
		 }

	}

	public function supplier_edit($id="")
	{
		$hariIni        = new DateTime();
        if (empty($this->input->post())) {
            $code = $id;
            $this->data['supplier'] = $this->m_supplier->get_row_supplier($code);
            $this->admintemp->view('backend/supplier/supplier_edit', $this->data);
        }elseif (!empty($this->input->post())) {
            $code       = $this->input->post('code');
            $name       = $this->input->post('supplier_name');
             $supplier_code = $this->input->post('supplier_code');
            $address    = $this->input->post('address');
            $data = array(
                'name'       => $name,
                'address'    => $address,
                'supplier_code' => $supplier_code,
                'update_at'  => $hariIni->format('y-m-d H:i:s'),
                'user_id'    => USER_ID,
            );
            $status = $this->m_supplier->update_supplier($code, $data);
            if ($status == 1) {//Jika Success Insert
                $this->session->set_flashdata('success', 'Your data successfully Updated !');
                redirect('backend/bom_injection/supplier_edit/'.$code);
            }else if($status == 'error'){
                $this->session->set_flashdata('error', 'Code is available, please make a unique one !');
                redirect('backend/bom_injection/supplier_edit/'.$code);
            }
        }
	}


  public function template($n_file=''){
        // log_r($n_file);
        $file = './src/assets/template/'.$n_file;
        force_download($file, NULL);
        die();
    }





   

}
