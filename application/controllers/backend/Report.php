<?php if (!defined('BASEPATH')) { exit ('No Direct Script Allowed'); }

class Report extends CI_Controller {

	public function __construct(){
		parent::__construct();
	    if(!$this->ion_auth->logged_in()){
	      redirect('auth/login', 'refresh');
	    }
	    date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('m_report');
        $this->load->model('m_production');
        $this->load->helper('download');
	}

	public function index()
	{	
        $this->admintemp->view('backend/preport/menu');
	}
	
    public function production()
    {
        $this->data['link_cetak_excel'] ='';
        $this->data['date_report']  = "";
        $this->data['basic_menit']  = "";
        // $this->data['data_emp']  = $this->m_report->get_last_data();
        $this->data['data_emp']     = "";
        // log_r($this->data['data_emp']);
        $this->admintemp->view('backend/preport/list_report',$this->data);
    }

    public function filter_report()
    {
        $period = $this->input->post('period');
        $from   = $this->input->post('form_date');
        $to     = $this->input->post('to_date');
        $shift  = $this->input->post('shift');
        $start  = $this->input->post('start');
        $finish = $this->input->post('finish');
        // $from_date  = $from.' 00:00:00';
        // $to_date    = $to.' 23:59:59';
        
        $this->data['data_emp'] = $this->m_report->get_data_report($from, $to, $start, $finish, $shift);
        // log_r($this->data['data_emp']);
        if ($period =="Daily") {
            $basic_menit = "480";
        }elseif($period =="Weekly") {
            $basic_menit = "2400";
        }elseif ($period =="Montly") {
            $basic_menit = "9600";
        }else{
            $basic_menit = "480";
        }
        $this->data['basic_menit'] = $basic_menit;
        
        $DateReport = $period.' : '.$from.' - '.$to;
        $this->data['date_report'] = $DateReport;
        if ($shift =='all') {
           $this->data['link_cetak_excel'] = $from.'/'.$to.'/all';
        }else{
            $this->data['link_cetak_excel'] = $from.'/'.$to.'/'.$start.'/'.$finish;
        }
        $this->admintemp->view('backend/preport/list_report',$this->data);
    }

    public function download_report($from='', $to='', $start='', $finish='')
    {
        $from_date  = $from.' 00:00:00';
        $to_date    = $to.' 23:59:59';
        if ($start =='all') {
           $shift  = $start;
        }else{
            $shift = "";
        }
        $this->data['data_emp'] = $this->m_report->get_data_report($from, $to, $start, $finish, $shift);
        $this->data['from'] = $from;
        $this->data['to']   = $to;

        $content = $this->load->view('backend/preport/list_excel',$this->data, TRUE);
        // log_r($content);
        excel_header('Peformance_report_'.date('Y-m-d').'.xls');
        echo $content;
    }

    public function delete_prod_output($employee_id='', $date_report='', $id_group_master='', $reason='')
    {
        $hariIni        = new DateTime();
        $id_group       = $id_group_master;
        $get_masterdata = $this->m_production->get_masterdata($employee_id, $date_report, $id_group_master);
        foreach ($get_masterdata as $val) {
            $data[] = array(
                'id_group_master'   => $val->id_group_master,
                'id_master'         => $val->id_master,
                'employee_id'       => $val->employee_id,
                'mc_no'             => $val->mc_no,
                'date_report'       => $val->date_report,
                'date_start'        => $val->date_start,
                'date_finish'       => $val->date_finish,
                'datecreate'        => $val->datecreate,
                't_hours'           => $val->t_hours,
                't_minute'          => $val->t_minute,
                't_second'          => $val->t_second,
                'part_name'         => $val->part_name,
                'tl_name_no'        => $val->tl_name_no,
                'peformance_day'    => $val->peformance_day,
                'reason'            => $reason,
                'delete_at'         => $hariIni->format('y-m-d H:i:s'),
                'user_id'           => USER_ID,
            );
        }
        $insert = $this->m_production->insert_delete($data);//insert Delete
        $status = $this->m_production->delete_peformance($id_group);//delete p_peformance
        $delete = $this->m_production->delete_p_masterdata($employee_id, $date_report, $id_group_master);//delete p_masterdata
        echo $delete;
    }

    public function machine_report()
    {
        $this->data['link_cetak_excel'] ='';
        $this->data['date_report']      ='';
        $this->data['basic_menit']      ='';
        $this->data['data_emp']         = $this->m_report->get_machine();
        $this->data['data_prod']        = $this->m_report->get_data_prod();
        // log_r($this->data['data_emp']);
        // log_r($this->data['data_prod']);
        // foreach ($machine as $value) {
        //     if (empty($value->mc_no)) {
        //         $data = array(
        //             'id' => $value->id, 
        //             'id_machine' => $value->id_machine,
        //             'mc_no' => $value->mc_no,
        //             'employee_id' => $value->employee_id, 
        //         );
        //         // $id_machine = $value->id_machine;
        //         // $data_mc_no = $this->m_report->get_mc_no($id_machine);
        //         // $mc_no      = $data_mc_no->line_mc_no."".$data_mc_no->mc_no;
        //         // $this->m_report->update_mc_no($id_machine, $mc_no);
        //     }
        // }
        $this->admintemp->view('backend/preport/machine',$this->data);
    }
   
    // public function delete_prod_output($employee_id='', $date_report='', $id_group_master='', $id_master='')
    // {
    //     $cekdata    = $this->m_production->check_dataold($employee_id, $date_report);//cek data
    //     foreach ($cekdata as $val) {
    //         $peformance_day = $val->peformance_day;
    //     }
    //     $id_group   = $id_group_master;
        
    //     if (!empty($cekdata)) {//cek apakah data sudah ada
    //         $sum_peformance  = $this->m_production->get_sum_peformance($id_group);
    //         $count_idgroup   = $this->m_production->get_count_group($id_group);
    //         $tot_performance = $sum_peformance->peformance_day-$peformance_day;
    //         $tot_count_group = $count_idgroup-1;

    //         if ($tot_count_group != 0) {
    //             $tot_grade   = $tot_performance/$tot_count_group;
    //             $peformance_day  = round($tot_grade);
    //             if ($peformance_day >= 95) {
    //                 $nilaigrade ='A+';
    //             }elseif($peformance_day >= 90 && $peformance_day < 95) {
    //                 $nilaigrade ='A';
    //             }elseif($peformance_day >= 85 && $peformance_day < 90) {
    //                 $nilaigrade ='B';
    //             }elseif($peformance_day >= 80 && $peformance_day < 85) {
    //                 $nilaigrade ='C';
    //             }elseif($peformance_day >= 70 && $peformance_day < 80) {
    //                 $nilaigrade ='D';
    //             }elseif($peformance_day >= 50 && $peformance_day < 70) {
    //                 $nilaigrade ='E';
    //             }elseif($peformance_day < 50) {
    //                 $nilaigrade ='F';
    //             }
    //             $grade = array(
    //                 'id_group_master' => $id_group,
    //                 'peformance'      => $peformance_day,
    //                 'grade'           => $nilaigrade,
    //             );
    //             $status = $this->m_production->update_peformance($id_group, $grade);//update peformance
    //             if ($status==1) {
    //                 $delete = $this->m_production->delete_p_masterdata($employee_id, $date_report, $id_group_master, $id_master);//delete p_masterdata
    //                 echo $delete;
    //             }
    //         }elseif($tot_count_group == 0){
    //             $status = $this->m_production->delete_peformance($id_group);//delete p_peformance
    //             if ($status==1) {
    //                 $delete = $this->m_production->delete_p_masterdata($employee_id, $date_report, $id_group_master, $id_master);//delete p_masterdata
    //                 echo $delete;
    //             }
    //         }
    //     }
    // }

}
