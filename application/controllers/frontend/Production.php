<?php if (!defined('BASEPATH')) { exit ('No Direct Script Allowed'); }

class Production extends CI_Controller {

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('m_production');
	}

	public function index($id='', $nilaigrade='', $peformance_day='')
	{
		$this->data['data_opt'] 	= $this->m_production->get_opt();
		$this->data['datamachine'] 	= $this->m_production->get_machine();
		$this->data['datatooling'] 	= $this->m_production->get_tooling();
		if ($id =="finish") {
			$this->data['gif_score'] 	  = $this->m_production->get_gif_score($nilaigrade);
			$this->data['peformance_day'] = $peformance_day;
			$this->session->set_flashdata('finish', 'finish');
			$this->usertemp->view('production/form', $this->data);
        }else{
        	$this->usertemp->view('production/form', $this->data);
        }
	}

	public function finish()
	{
		$id ="finish";
		$this->index($id);
	}

	function get_data_opt()
	{
		$employee_id 	= $this->input->post('id');
		$data 			= $this->m_production->get_data_opt($employee_id);
		echo json_encode($data);
	}

	function get_data_machine()
	{
		$data 	= $this->m_production->get_data_machine();
		echo json_encode($data);
	}

	function get_data_mc()
	{
		$machine_no 	= $this->input->post('id');
		$data 			= $this->m_production->get_data_mc($machine_no);
		echo json_encode($data);
	}

	function data_tooling()
	{
		$id_tool 	= $this->input->post('id');
		$data 		= $this->m_production->get_data_tool($id_tool);
		echo json_encode($data);
	}

	// function tes()
	// {
	// 	$hariIni = new DateTime();
	// 	$date 	 = $hariIni->format('ymdHis');
	// 	$tes ='210818153028';
	// 	$s = substr($tes,10,2);
	// 	log_r($s);
	// }

	public function save_form()
	{
		$act_form     	 = $this->input->post('act_form');//finish OR addmore

		$date_report     = $this->input->post('date_report');
		$shift_start     = $this->input->post('shift_start');
		$shift_finish    = $this->input->post('shift_finish');
		$employee_no     = $this->input->post('employee_no');
        $peformance_day  = $this->input->post('peformance_day');
        $machine_no      = $this->input->post('machine_no');
        $mc_no      	 = $this->input->post('mc_no');
        $brand     		 = $this->input->post('brand');
        $tonnage     	 = $this->input->post('tonnage');
        $date_start      = $this->input->post('date_start');
        $date_finish     = $this->input->post('date_finish');
        $time_start      = $this->input->post('time_start');
        $time_finish     = $this->input->post('time_finish');
        $t_hours     	 = $this->input->post('t_hours');
        $t_minute     	 = $this->input->post('t_minute');
        $t_second     	 = $this->input->post('t_second');
        $part_name     	 = $this->input->post('part_name');
        $tl_name_no      = $this->input->post('tl_name_no');
        $no_of_cavities  = $this->input->post('no_of_cavities');
        $cycle_time_pcs  = $this->input->post('cycle_time_pcs');
        $part_weight     = $this->input->post('part_weight');
        $runner_weight   = $this->input->post('runner_weight');
        $target     	 = $this->input->post('target');
        $good     		 = $this->input->post('good');
        $reject     	 = $this->input->post('reject');
        $tot_output      = $this->input->post('tot_output');
        ## Target ##
        $tar_lumps      = $this->input->post('tar_lumps');
        $tar_runner     = $this->input->post('tar_runner');
        $tar_prod_qty   = $this->input->post('tar_prod_qty');
        $tot_target     = $this->input->post('tot_target');
        ## End Target ##
        ## Actual ##
		$act_lumps      = $this->input->post('act_lumps');
        $act_runner     = $this->input->post('act_runner');
        $act_prod_qty   = $this->input->post('act_prod_qty');
        $tot_actual     = $this->input->post('tot_actual');
        ## End Actual ##
        ## Disscrapancy ##
		$diss_lumps      = $this->input->post('diss_lumps');
        $diss_runner     = $this->input->post('diss_runner');
        $diss_prod_qty   = $this->input->post('diss_prod_qty');
        $tot_dissrapancy = $this->input->post('tot_dissrapancy');
        ## End Disscrapancy ##
        
        // if (date('H', $str_finish_time) < date('H', $str_start_time)) {
        // 	$new_date = date('Y-m-d', strtotime($date_create. ' + 1 days'));
        // 	log_r($new_date);
        // }

  		// $date_report  = date("Y-m-d", strtotime($date_start));
		// $newtimestart = date("H:i:s", strtotime($date_start));
		// if ($newtimestart >= "00:01:00" && $newtimestart <= "06:00:00") {
		// 	$newstartdate = date('Y-m-d', strtotime($date_start . ' -1 day'));
		// }else{
		// 	$newstartdate = date("Y-m-d", strtotime($date_start));
		// }

		$cekdata = $this->m_production->check_dataold($employee_no, $date_report);//cek data
		// log_r($cekdata);
		if (!empty($cekdata)) {//jika data id_group ada
			foreach ($cekdata as $key => $value) {
				$indexIdGroup[]     = $value->id_group_master;
				$indexStartDate[]   = $value->date_report;
				$indexShiftStart[]  = $value->shift_start;
				$indexShiftFinish[] = $value->shift_finish;
			}
			$id_group 	  = $indexIdGroup[0];
			$date_report  = $indexStartDate[0];
			$shift_start  = $indexShiftStart[0];
			$shift_finish = $indexShiftFinish[0];
		}else{//jika data baru ("id_group belum ada")
			## **Generate ID Group** ##
			$i = 1;
			do
			{
				$hariIni 	 = new DateTime();
				$datetime 	 = $hariIni->format('ymdHis');
				$id_group 	 = $datetime;
				$group_id    = $this->m_production->check_group_master($id_group);
			$i++;
			} while($group_id > 0);
			## **End Generate ID Group** ##
		}

        ## **Generate ID Master** ##
		$i = 1;
		do
		{
			$s = substr($id_group,10,2);//cut ambil secod dari id_group
			$hariIni 	 = new DateTime();
			$datetime 	 = $hariIni->format('ymdHis');
			$generate_id = $s."".$datetime;
			$id_master   = $this->m_production->check_idmaster($generate_id);
		$i++;
		} while($id_master > 0);
		## **End Generate ID Master** ##

		// log_r("ID Group: ".$id_group." ID Master: ".$generate_id);
        $data = array(
        	'id_group_master' => $id_group,
        	'id_master' 	  => $generate_id,
          	'employee_id'     => $employee_no,
	        'peformance_day'  => $peformance_day,
	        'id_machine'      => $machine_no,
	        'mc_no'		      => $mc_no,
	        'brand'     	  => $brand,
	        'tonnage'     	  => $tonnage,
	        'date_start'	  => $date_start,
	        'shift_start'     => $shift_start,
	        'shift_finish'    => $shift_finish,
	        'time_start'      => $time_start,
	        'date_finish'	  => $date_finish,
	        'time_finish'     => $time_finish,
	        'datecreate'      => $hariIni->format('y-m-d H:i:s'),
	        't_hours'     	  => $t_hours,
	        't_minute'     	  => $t_minute,
	        't_second'     	  => $t_second,
	        'part_name'       => $part_name,
	        'tl_name_no'      => $tl_name_no,
	        'no_of_cavities'  => $no_of_cavities,
	        'cycle_time_pcs'  => $cycle_time_pcs,
	        'part_weight'     => $part_weight,
	        'runner_weight'   => $runner_weight,
	        'target'     	  => $target,
	        'good'    		  => $good,
	        'reject'     	  => $reject,
	        'tot_output'      => $tot_output,
	        ## Target ##
	        'tar_lumps'      => $tar_lumps,
	        'tar_runner'     => $tar_runner,
	        'tar_prod_qty'   => $tar_prod_qty,
	        'tot_target'     => $tot_target,
	        ## End Target ##
	        ## Actual ##
			'act_lumps'      => $act_lumps,
	        'act_runner'     => $act_runner,
	        'act_prod_qty'   => $act_prod_qty,
	        'tot_actual'     => $tot_actual,
	        ## End Actual ##
	        ## Disscrapancy ##
			'diss_lumps'      => $diss_lumps,
	        'diss_runner'     => $diss_runner,
	        'diss_prod_qty'   => $diss_prod_qty,
	        'tot_dissrapancy' => $tot_dissrapancy,
	        ## End Disscrapancy ##
	        'date_report'	  => $date_report,
	        'shift_start'	  => $shift_start,
	        'shift_finish'	  => $shift_finish,
        );
       	// log_r($data);
       	if (!empty($cekdata)) {//cek apakah data sudah ada
       		$sum_peformance  = $this->m_production->get_sum_peformance($id_group);
			$count_idgroup   = $this->m_production->get_count_group($id_group);
			$tot_performance = $sum_peformance->peformance_day+$peformance_day;
			$tot_count_group = $count_idgroup+1;
			$tot_grade 		 = $tot_performance/$tot_count_group;
			$peformance_day  = round($tot_grade);
       	}

       	if ($peformance_day >= 95) {
       		$nilaigrade ='A+';
       	}elseif($peformance_day >= 90 && $peformance_day < 95) {
       		$nilaigrade ='A';
       	}elseif($peformance_day >= 85 && $peformance_day < 90) {
       		$nilaigrade ='B';
       	}elseif($peformance_day >= 80 && $peformance_day < 85) {
       		$nilaigrade ='C';
       	}elseif($peformance_day >= 70 && $peformance_day < 80) {
       		$nilaigrade ='D';
       	}elseif($peformance_day >= 50 && $peformance_day < 70) {
       		$nilaigrade ='E';
       	}elseif($peformance_day < 50) {
       		$nilaigrade ='F';
       	}

       	$grade = array(
       		'id_group_master' => $id_group,
			'peformance' 	  => $peformance_day,
			'grade' 	  	  => $nilaigrade,
       	);
        $insert  = $this->m_production->insert_data_form($data);
        if ($insert == 1) {
        	if (!empty($cekdata)) {//jika input data lama
        		$this->m_production->update_peformance($id_group, $grade);//update peformance
        		$this->session->set_flashdata('success', 'Your data successfully added !');
        		redirect('frontend/production/add_more_page/'.$id_group);
        	}else{//jika data baru
        		$this->m_production->insert_grade($grade);
        		if ($act_form =="finish") {
					$this->index($act_form, $nilaigrade, $peformance_day);
	        	}elseif($act_form =="addmore"){
	        		$this->session->set_flashdata('success', 'Your data successfully added !');
	        		redirect('frontend/production/add_more_page/'.$id_group);
	        	}
        	}//end
        }
	}

	public function add_more_page($id_group='')
	{
		$this->data['data_opt'] 	= $this->m_production->get_opt();
		$this->data['datamachine'] 	= $this->m_production->get_machine();
		$this->data['datatooling'] 	= $this->m_production->get_tooling();
		// $id_group='210818154344';
		$get_peformance 			= $this->m_production->get_peformance_data($id_group);
		$this->data['peformance']  	= $get_peformance->peformance;
		$this->data['grade']  		= $get_peformance->grade;
		$get_master_data 			= $this->m_production->get_master_data($id_group);
		$result_master_data 		= $this->m_production->loop_master_data($id_group);
		$this->data['result_master_data']  = $result_master_data;
		$this->data['master_data']  = $get_master_data;
		$this->data['employee_id']  = $get_master_data->employee_id;
		// log_r($result_master_data);
		$this->usertemp->view('production/form_add_more', $this->data);
	}

	public function save_more_page()
	{
		$id_group_master = $this->input->post('id_group_master');
		$machine_no      = $this->input->post('machine_no');
		$mc_no      	 = $this->input->post('mc_no');
		$peformance_day  = $this->input->post('peformance_day');
        $brand     		 = $this->input->post('brand');
        $tonnage     	 = $this->input->post('tonnage');
        $date_start      = $this->input->post('date_start');
        $date_finish     = $this->input->post('date_finish');
        $time_start      = $this->input->post('time_start');
        $time_finish     = $this->input->post('time_finish');
        $t_hours     	 = $this->input->post('t_hours');
        $t_minute     	 = $this->input->post('t_minute');
        $t_second     	 = $this->input->post('t_second');
        $part_name     	 = $this->input->post('part_name');
        $tl_name_no      = $this->input->post('tl_name_no');
        $no_of_cavities  = $this->input->post('no_of_cavities');
        $cycle_time_pcs = $this->input->post('cycle_time_pcs');
        $part_weight     = $this->input->post('part_weight');
        $runner_weight   = $this->input->post('runner_weight');
        $target     	 = $this->input->post('target');
        $good     		 = $this->input->post('good');
        $reject     	 = $this->input->post('reject');
        $tot_output      = $this->input->post('tot_output');
        ## Target ##
        $tar_lumps       = $this->input->post('tar_lumps');
        $tar_runner      = $this->input->post('tar_runner');
        $tar_prod_qty    = $this->input->post('tar_prod_qty');
        $tot_target      = $this->input->post('tot_target');
        ## End Target ##
        ## Actual ##
		$act_lumps       = $this->input->post('act_lumps');
        $act_runner      = $this->input->post('act_runner');
        $act_prod_qty    = $this->input->post('act_prod_qty');
        $tot_actual      = $this->input->post('tot_actual');
        ## End Actual ##
        ## Disscrapancy ##
		$diss_lumps      = $this->input->post('diss_lumps');
        $diss_runner     = $this->input->post('diss_runner');
        $diss_prod_qty   = $this->input->post('diss_prod_qty');
        $tot_dissrapancy = $this->input->post('tot_dissrapancy');
        ## End Disscrapancy ##

        $start_date  = $date_start." ".$time_start.":00";
        $finish_date = $date_finish." ".$time_finish.":00";

        ## **Generate ID Master** ##
        $id_group 		 = $id_group_master;
        $get_master_data = $this->m_production->get_master_data($id_group);
		$i = 1;
		do
		{
			$s = substr($id_group,10,2);//cut ambil secod dari id_group
			$hariIni 	 = new DateTime();
			$datetime 	 = $hariIni->format('ymdHis');
			$generate_id = $s."".$datetime;
			$id_master   = $this->m_production->check_idmaster($generate_id);
		$i++;
		} while($id_master > 0);
		## **End Generate ID Master** ##
		## Formula Peformance & Grade ##
		$sum_peformance  = $this->m_production->get_sum_peformance($id_group);
		$count_idgroup   = $this->m_production->get_count_group($id_group);
		$tot_performance = $sum_peformance->peformance_day+$peformance_day;
		$tot_count_group = $count_idgroup+1;
		$tot_grade 		 = $tot_performance/$tot_count_group;
		$round_grade 	 = round($tot_grade);
		// log_r($tot_grade." ".$round_grade);
		$data = array(
			'id_group_master' => $id_group,
        	'id_master' 	  => $generate_id,
          	'employee_id'     => $get_master_data->employee_id,
	        'peformance_day'  => $peformance_day,
	        'id_machine'      => $machine_no,
	        'mc_no'		      => $mc_no,
	        'brand'     	  => $brand,
	        'tonnage'     	  => $tonnage,
	       	'date_start'	  => $date_start,
	        'time_start'      => $time_start,
	        'date_finish'	  => $date_finish,
	        'time_finish'     => $time_finish,
	        'datecreate'      => $hariIni->format('y-m-d H:i:s'),
	        't_hours'     	  => $t_hours,
	        't_minute'     	  => $t_minute,
	        't_second'     	  => $t_second,
	        'part_name'       => $part_name,
	        'tl_name_no'      => $tl_name_no,
	        'no_of_cavities'  => $no_of_cavities,
	        'cycle_time_pcs'  => $cycle_time_pcs,
	        'part_weight'     => $part_weight,
	        'runner_weight'   => $runner_weight,
	        'target'     	  => $target,
	        'good'    		  => $good,
	        'reject'     	  => $reject,
	        'tot_output'      => $tot_output,
	        ## Target ##
	        'tar_lumps'       => $tar_lumps,
	        'tar_runner'      => $tar_runner,
	        'tar_prod_qty'    => $tar_prod_qty,
	        'tot_target'      => $tot_target,
	        ## End Target ##
	        ## Actual ##
			'act_lumps'       => $act_lumps,
	        'act_runner'      => $act_runner,
	        'act_prod_qty'    => $act_prod_qty,
	        'tot_actual'      => $tot_actual,
	        ## End Actual ##
	        ## Disscrapancy ##
			'diss_lumps'      => $diss_lumps,
	        'diss_runner'     => $diss_runner,
	        'diss_prod_qty'   => $diss_prod_qty,
	        'tot_dissrapancy' => $tot_dissrapancy,
	        ## End Disscrapancy ##
	        'date_report'	  => $get_master_data->date_report,
	        'shift_start'	  => $get_master_data->shift_start,
	        'shift_finish'	  => $get_master_data->shift_finish,
        );
		
        if ($round_grade >= 95) {
       		$nilaigrade ='A+';
       	}elseif($round_grade >= 90 && $round_grade < 95) {
       		$nilaigrade ='A';
       	}elseif($round_grade >= 85 && $round_grade < 90) {
       		$nilaigrade ='B';
       	}elseif($round_grade >= 80 && $round_grade < 85) {
       		$nilaigrade ='C';
       	}elseif($round_grade >= 70 && $round_grade < 80) {
       		$nilaigrade ='D';
       	}elseif($round_grade >= 50 && $round_grade < 70) {
       		$nilaigrade ='E';
       	}elseif($round_grade < 50) {
       		$nilaigrade ='F';
       	}
       	
       	$grade = array(
       		'id_group_master' => $id_group,
			'peformance' 	  => $round_grade,
			'grade' 	  	  => $nilaigrade,
       	);
        // log_r($grade);
        $insert  = $this->m_production->insert_data_form($data);
        if ($insert == 1) {
        	$this->m_production->update_peformance($id_group_master, $grade);//update peformance
        	$this->session->set_flashdata('success', 'Your data successfully added !');
        	redirect('frontend/production/add_more_page/'.$id_group);
        }

	}

	public function edit_form($id_group='', $id_master='')
	{
		$this->data['data_opt'] 	= $this->m_production->get_opt();
		$this->data['datamachine'] 	= $this->m_production->get_machine();
		$this->data['datatooling'] 	= $this->m_production->get_tooling();
		
		$get_peformance 			= $this->m_production->get_peformance_data($id_group);
		$this->data['peformance']  	= $get_peformance->peformance;
		$this->data['grade']  		= $get_peformance->grade;
		// $get_master_data 			= $this->m_production->get_master_data($id_group);
		$get_edit_data				= $this->m_production->get_edit_data($id_group, $id_master);
		$result_master_data 		= $this->m_production->loop_master_data($id_group);
		$this->data['result_master_data']  = $result_master_data;
		$this->data['edit_data']  	= $get_edit_data;
		$this->data['employee_id']  = $get_edit_data->employee_id;
		// log_r($get_edit_data);
		$this->usertemp->view('production/form_edit', $this->data);
	}

	public function save_edit_form()
	{
		$hariIni 	 	 = new DateTime();
		$id_group_master = $this->input->post('id_group');
		$id_master 		 = $this->input->post('id_master');
		$employee_id 	 = $this->input->post('employee_id');
		$machine_no      = $this->input->post('machine_no');
		$peformance_day  = $this->input->post('peformance_day');
        $brand     		 = $this->input->post('brand');
        $tonnage     	 = $this->input->post('tonnage');
        $date_create     = $this->input->post('date_create');
        $date_start      = $this->input->post('date_start');
        $date_finish     = $this->input->post('date_finish');
        $time_start      = $this->input->post('time_start');
        $time_finish     = $this->input->post('time_finish');
        $t_hours     	 = $this->input->post('t_hours');
        $t_minute     	 = $this->input->post('t_minute');
        $t_second     	 = $this->input->post('t_second');
        $part_name     	 = $this->input->post('part_name');
        $no_of_cavities  = $this->input->post('no_of_cavities');
        $cycle_time_shot = $this->input->post('cycle_time_shot');
        $part_weight     = $this->input->post('part_weight');
        $runner_weight   = $this->input->post('runner_weight');
        $target     	 = $this->input->post('target');
        $good     		 = $this->input->post('good');
        $reject     	 = $this->input->post('reject');
        $tot_output      = $this->input->post('tot_output');
        ## Target ##
        $tar_lumps      = $this->input->post('tar_lumps');
        $tar_runner     = $this->input->post('tar_runner');
        $tar_prod_qty   = $this->input->post('tar_prod_qty');
        $tot_target     = $this->input->post('tot_target');
        ## End Target ##
        ## Actual ##
		$act_lumps      = $this->input->post('act_lumps');
        $act_runner     = $this->input->post('act_runner');
        $act_prod_qty   = $this->input->post('act_prod_qty');
        $tot_actual     = $this->input->post('tot_actual');
        ## End Actual ##
        ## Disscrapancy ##
		$diss_lumps      = $this->input->post('diss_lumps');
        $diss_runner     = $this->input->post('diss_runner');
        $diss_prod_qty   = $this->input->post('diss_prod_qty');
        $tot_dissrapancy = $this->input->post('tot_dissrapancy');
        ## End Disscrapancy ##

        $get_master_data = $this->m_production->get_master_data($id_group_master);
        ## Formula Peformance & Grade ##
		$sum_peformance  = $this->m_production->get_sum_peformance($id_group_master);
		$count_idgroup   = $this->m_production->get_count_group($id_group_master);
		$tot_performance = $sum_peformance->peformance_day+$peformance_day;
		$tot_count_group = $count_idgroup;
		$tot_grade 		 = $tot_performance/$tot_count_group;
		$round_grade 	 = round($tot_grade);
		## End Formula ##

        $data = array(
			'id_group_master' => $id_group_master,
        	'id_master' 	  => $id_master,
          	'employee_id'     => $employee_id,
	        'peformance_day'  => $peformance_day,
	        'id_machine'      => $machine_no,
	        'brand'     	  => $brand,
	        'tonnage'     	  => $tonnage,
	       	'date_start'	  => $date_start,
	        'time_start'      => $time_start,
	        'date_finish'	  => $date_finish,
	        'time_finish'     => $time_finish,
	        'datecreate'      => $hariIni->format('y-m-d H:i:s'),
	        // 'datecreate'      => $date_create,
	        't_hours'     	  => $t_hours,
	        't_minute'     	  => $t_minute,
	        't_second'     	  => $t_second,
	        'part_name'       => $part_name,
	        'no_of_cavities'  => $no_of_cavities,
	        'cycle_time_shot' => $cycle_time_shot,
	        'part_weight'     => $part_weight,
	        'runner_weight'   => $runner_weight,
	        'target'     	  => $target,
	        'good'    		  => $good,
	        'reject'     	  => $reject,
	        'tot_output'      => $tot_output,
	        ## Target ##
	        'tar_lumps'      => $tar_lumps,
	        'tar_runner'     => $tar_runner,
	        'tar_prod_qty'   => $tar_prod_qty,
	        'tot_target'     => $tot_target,
	        ## End Target ##
	        ## Actual ##
			'act_lumps'      => $act_lumps,
	        'act_runner'     => $act_runner,
	        'act_prod_qty'   => $act_prod_qty,
	        'tot_actual'     => $tot_actual,
	        ## End Actual ##
	        ## Disscrapancy ##
			'diss_lumps'      => $diss_lumps,
	        'diss_runner'     => $diss_runner,
	        'diss_prod_qty'   => $diss_prod_qty,
	        'tot_dissrapancy' => $tot_dissrapancy,
	        ## End Disscrapancy ##
	        'date_report'	  => $get_master_data->date_report,
	        'shift_start'	  => $get_master_data->shift_start,
	        'shift_finish'	  => $get_master_data->shift_finish,
        );
        
        if ($round_grade >= 95) {
       		$nilaigrade ='A+';
       	}elseif($round_grade >= 90 && $round_grade < 95) {
       		$nilaigrade ='A';
       	}elseif($round_grade >= 85 && $round_grade < 90) {
       		$nilaigrade ='B';
       	}elseif($round_grade >= 80 && $round_grade < 85) {
       		$nilaigrade ='C';
       	}elseif($round_grade >= 70 && $round_grade < 80) {
       		$nilaigrade ='D';
       	}elseif($round_grade >= 50 && $round_grade < 70) {
       		$nilaigrade ='E';
       	}elseif($round_grade < 50) {
       		$nilaigrade ='F';
       	}

       	$grade = array(
       		'id_group_master' => $id_group_master,
			'peformance' 	  => $round_grade,
			'grade' 	  	  => $nilaigrade,
       	);
       	// log_r($grade);
        $insert  = $this->m_production->update_master_data($data, $id_group_master, $id_master);
        if ($insert == 1) {
        	$this->m_production->update_peformance($id_group_master, $grade);//update peformance
        	$this->session->set_flashdata('success', 'Your data has been successfully updated !');
        	redirect('frontend/production/edit_form/'.$id_group_master.'/'.$id_master);
        }
	}


	public function report_peformance()
	{
		$this->data['link_cetak_excel'] ='';
		$this->data['data_emp'] ='';
		$this->data['data_opt'] = $this->m_production->get_opt();
		$this->data['last_update'] = $this->m_production->get_last_update_daily_prod();
		// log_r($this->data['last_update']);
		$this->usertemp->view('production/master_list',$this->data);
		
	}

	public function filter_report_p()
	{
		// $employee_no 	= $this->input->post('employee_no');
		$from_date		= $this->input->post('form_date');
		$to_date		= $this->input->post('to_date');
		// $from_date_new  = $from_date.' 00:00:00';
		// $to_date_new	= $to_date.' 23:59:59';

		// $this->data['link_cetak_excel'] = $employee_no.'/'.$from_date.'/'.$to_date;
		// $this->data['data_emp'] = $this->m_production->row_emp_report($from_date, $to_date);
		$this->data['data_sum'] = $this->m_production->row_sum_report($from_date,$to_date);
		// log_r($this->data['data_sum']);
		$this->data['data_emp'] = $this->m_production->row_get_report($from_date,$to_date);
		$this->data['last_update'] = $this->m_production->get_last_update_daily_prod();

		// log_r($this->data['data_sum'][0]->output_tot);
		$this->data['data_opt'] = $this->m_production->get_opt();
		$this->usertemp->view('production/master_list',$this->data);
	}

	public function display($id_group='', $id_master='')
	{
		$this->data['data_opt'] 	= $this->m_production->get_opt();
		$this->data['datamachine'] 	= $this->m_production->get_machine();
		$this->data['datatooling'] 	= $this->m_production->get_tooling();
		
		$get_peformance 			= $this->m_production->get_peformance_data($id_group);
		$this->data['peformance']  	= $get_peformance->peformance;
		$this->data['grade']  		= $get_peformance->grade;
		
		$get_edit_data				= $this->m_production->get_edit_data($id_group, $id_master);
		$result_master_data 		= $this->m_production->loop_master_data($id_group);
		$this->data['result_master_data']  = $result_master_data;
		$this->data['edit_data']  	= $get_edit_data;
		$this->data['employee_id']  = $get_edit_data->employee_id;
		// log_r($get_edit_data);
		$this->usertemp->view('production/display', $this->data);
	}

	  public function daily_chart()
    {
      
        $this->data['year']     = $this->m_production->get_year_daily();
        // log_r($this->data['year']);
        $this->usertemp->view('production/chart_daily',$this->data);
    }

    public function get_value_chart()
    {
    	$year    = $this->input->post('id');
        $data1   = $this->m_production->get_chart_1($year);
        $data2   = $this->m_production->get_chart_2($year);
        $data3   = $this->m_production->get_chart_3($year);
        $data4   = $this->m_production->get_chart_4($year);
        $data5   = $this->m_production->get_chart_5($year);
        $data6   = $this->m_production->get_chart_6($year);
        $data7   = $this->m_production->get_chart_7($year);
        $data8   = $this->m_production->get_chart_8($year);
        $data9   = $this->m_production->get_chart_9($year);
        $data10   = $this->m_production->get_chart_10($year);
        $data11   = $this->m_production->get_chart_11($year);
        $data12   = $this->m_production->get_chart_12($year);


        $merged = (object)array_merge_recursive((array)$data1, (array)$data2,(array)$data3,(array)$data4,(array)$data5,(array)$data6,(array)$data7,(array)$data8,(array)$data9,(array)$data10,(array)$data11,(array)$data12);

        echo json_encode($merged) ;
    }
	
}
