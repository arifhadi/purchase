<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Model Register
 */
class M_production extends CI_Model{

	// private $moulding_machine = $this->load->database('machine', true)->get('moulding_machine');
	// private $table2 = 'users';
	// const MAX_PASSWORD_SIZE_BYTES = 4096;

	public function __construct()
	{
		$this->db2  = $this->load->database('tooling', true);
		$this->db1  = $this->load->database('machine', true);
		// $db_machine = $this->load->database('machine', true);
		// private $moulding_machine = $this->db1->get('moulding_machine');
	}

	public function get_opt()
	{
		$this->db->select('*');
		$this->db->from('employees');
		$this->db->where('id_group =', 4);
		return $this->db->get()->result();
	}

	public function get_last_update_daily_prod()
	{
		$this->db->select('MAX(date_report) as last');
		$this->db->from('p_masterdata');
		return $this->db->get()->result();
	}

	public function get_data_opt($employee_id)
	{
		$this->db->select('employees.name as optname');
		$this->db->from('employees');
		$this->db->where('employee_no ', $employee_id);
		return $this->db->get()->row();
	}

	public function get_data_machine()
	{
		$this->db1->select('*');
		$this->db1->from('moulding_machine');
		return $this->db1->get()->result();
	}

	public function get_data_mc($machine_no)
	{
		$this->db1->select('*');
		$this->db1->from('moulding_machine');
		$this->db1->where('id_machine ', $machine_no);
		return $this->db1->get()->row();
	}

	public function get_machine()
	{
		$this->db1->select('*');
		$this->db1->from('moulding_machine');
		// $this->db1->where('status ', 1);
		return $this->db1->get()->result();
	}

	public function get_tooling()
	{
		$this->db2->select('*');
		$this->db2->from('mold_basic_details');
		$this->db2->where_in('status ', [1,4]);//active and temporary
		return $this->db2->get()->result();
	}

	public function get_gif_score($nilaigrade)
	{
		$this->db->select('*');
		$this->db->from('gif_score');
		$this->db->where('grade', $nilaigrade);
		return $this->db->get()->row();
	}

	public function get_data_tool($id_tool)
	{
		$this->db2->select('mold_basic_details.*, part_specifications.mold_cycle_pcs, part_specifications.lowest_ct,  part_specifications.part_weight, part_specifications.runner_weight, mold_specifications.no_of_cavities');
		$this->db2->from('mold_basic_details');
		$this->db2->join('part_specifications','part_specifications.id_master = mold_basic_details.id');
		$this->db2->join('mold_specifications','mold_specifications.id_master = mold_basic_details.id');
		$this->db2->where('mold_basic_details.gih_tool_no', $id_tool);
		return $this->db2->get()->row();
	}

	public function check_group_master($id_group)
	{
		return $this->db->get_where('p_masterdata', array('id_group_master' => $id_group))->num_rows();
	}

	public function check_idmaster($generate_id)
	{
		return $this->db->get_where('p_masterdata', array('id_master' => $generate_id))->num_rows();
	}

	public function check_dataold($employee_no, $date_report)
	{
		// $startDate 	= $newstartdate." 00:00:00";
		// $endDate 	= $newstartdate." 23:59:59";
		$this->db->select('*');
		$this->db->from('p_masterdata');
		$this->db->where('employee_id', $employee_no);
		$this->db->where('date_report', $date_report);
		// $this->db->where('date_start >=', $startDate);
		// $this->db->where('date_start <=', $endDate);
		return $this->db->get()->result();
	}

	public function insert_data_form($data)
	{
		return $this->db->insert('p_masterdata', $data);
	}

	public function insert_grade($grade)
	{
		return $this->db->insert('p_peformance', $grade);
	}

	public function update_peformance($id_group_master, $grade)
	{
		$this->db->where('id_group_master', $id_group_master);
		return $this->db->update('p_peformance', $grade);
	}

	public function get_masterdata($employee_id, $date_report, $id_group_master)
	{
		$this->db->select('*');
		$this->db->from('p_masterdata');
		$this->db->where('employee_id', $employee_id);
		$this->db->where('date_report', $date_report);
		$this->db->where('id_group_master', $id_group_master);
		return $this->db->get()->result();
	}

	public function insert_delete($data)
	{
		return $this->db->insert_batch('p_masterdata_delete', $data); 
	}

	public function delete_peformance($id_group)
	{
		$this->db->where('id_group_master', $id_group);
		return $this->db->delete('p_peformance');
	}

	public function delete_p_masterdata($employee_id, $date_report, $id_group_master)
	{
		return $this->db->delete('p_masterdata', array('employee_id' => $employee_id, 'date_report' => $date_report, 'id_group_master' => $id_group_master));
	}

	public function get_peformance_data($id_group)
	{
		$this->db->select('*');
		$this->db->from('p_peformance');
		$this->db->where('p_peformance.id_group_master', $id_group);
		return $this->db->get()->row();
	}

	public function get_sum_peformance($id_group)
	{
		$this->db->select_sum('peformance_day');
		$this->db->from('p_masterdata');
		$this->db->where('p_masterdata.id_group_master', $id_group);
		return $this->db->get()->row();
	}

	public function get_count_group($id_group)
	{
		$this->db->select('*');
		$this->db->from('p_masterdata');
		$this->db->where('p_masterdata.id_group_master', $id_group);
		return $this->db->count_all_results();
	}

	public function get_master_data($id_group)
	{
		$this->db->select('p_masterdata.*, employees.name as name_emp');
		$this->db->from('p_masterdata');
		$this->db->where('p_masterdata.id_group_master', $id_group);
		$this->db->join('employees','p_masterdata.employee_id = employees.employee_no');
		$this->db->group_by('p_masterdata.id_group_master'); 
		return $this->db->get()->row();
	}

	public function loop_master_data($id_group)
	{
		$this->db->select('p_masterdata.*');
		$this->db->from('p_masterdata');
		$this->db->where('p_masterdata.id_group_master', $id_group);
		return $this->db->get()->result();
	}

	public function get_edit_data($id_group, $id_master)
	{
		$this->db->select('p_masterdata.*, employees.name as name_emp');
		$this->db->from('p_masterdata');
		$this->db->where('p_masterdata.id_group_master', $id_group);
		$this->db->join('employees','p_masterdata.employee_id = employees.employee_no');
		$this->db->where('p_masterdata.id_master', $id_master);
		return $this->db->get()->row();
	}

	public function update_master_data($data, $id_group_master, $id_master)
	{
		$this->db->where('id_group_master', $id_group_master);
		$this->db->where('id_master', $id_master);
		return $this->db->update('p_masterdata', $data);
	}

	public function row_emp_report($from_date='', $to_date='')
	{	
		$this->db->select('count(p_masterdata.id) as tot_row, employees.name as name_emp, p_masterdata.date_report, min(p_masterdata.time_start) as start_time, max(p_masterdata.time_finish) as finish_time, p_masterdata.brand, p_masterdata.part_name, p_masterdata.tonnage, sum(p_masterdata.good) as tot_good, sum(p_masterdata.reject) as tot_reject, p_masterdata.id_group_master, p_masterdata.id_master, sum(p_masterdata.tot_output) as output_tot, p_masterdata.employee_id, p_peformance.peformance');
		$this->db->from('p_masterdata');
		$this->db->join('employees','p_masterdata.employee_id = employees.employee_no');
		$this->db->join('p_peformance','p_peformance.id_group_master = p_masterdata.id_group_master');
		$this->db->where('p_masterdata.date_report >=', $from_date);
		$this->db->where('p_masterdata.date_report <=', $to_date);
		// $this->db->where('p_masterdata.employee_id', 0622);
		$this->db->group_by('p_masterdata.id_group_master');
		// log_r($this->db->get()->result());
		return $this->db->get()->result();
	}


	public function row_sum_report($from_date='', $to_date='')
	{	
		$this->db->select('sum(p_masterdata.good) as good,sum(p_masterdata.target) as target,sum(p_masterdata.tot_output) as output_tot,sum(p_masterdata.reject) as tot_reject');
		$this->db->from('p_masterdata');
		// $this->db->join('employees','p_masterdata.employee_id = employees.employee_no');
		$this->db->join('p_peformance','p_peformance.id_group_master = p_masterdata.id_group_master','right');
		$this->db->where('p_masterdata.date_report >=', $from_date);
		$this->db->where('p_masterdata.date_report <=', $to_date);
		// $this->db->where('p_masterdata.employee_id', $employee_no);
		// $this->db->group_by('p_masterdata.id_group_master');
		// log_r($this->db->get()->result());
		return $this->db->get()->result();
	}

	public function row_get_report($from_date='', $to_date='')
	{
		$this->db->select('a.target,a.id as tot_row, a.date_report, a.time_start as start_time, a.time_finish as finish_time, a.brand, a.part_name, a.tonnage, a.good as tot_good, a.reject as tot_reject, a.id_group_master, a.id_master, a.tot_output as output_tot, a.employee_id, b.peformance,c.name as name_emp');
		$this->db->from('p_masterdata as a');
		$this->db->where('a.date_report >=', $from_date);
		$this->db->where('a.date_report <=', $to_date);
		$this->db->join('p_peformance as b','b.id_group_master = a.id_group_master','right');
		$this->db->join('employees as c','c.employee_no = a.employee_id','left');
		// $this->db->group_by('a.id_group_master');
		return $this->db->get()->result();

	}

	public function get_chart_1($year)
	{
		$this->db->select('CASE WHEN sum(p_masterdata.good) IS NULL THEN 0 WHEN sum(p_masterdata.good)>0 THEN sum(p_masterdata.good) END as good, CASE WHEN sum(p_masterdata.target) IS NULL THEN 0 WHEN sum(p_masterdata.target)>0 THEN sum(p_masterdata.target) END as target ,CASE WHEN sum(p_masterdata.tot_output) IS NULL THEN 0 WHEN sum(p_masterdata.tot_output)>0 THEN sum(p_masterdata.tot_output) END as output_tot,CASE WHEN sum(p_masterdata.reject) IS NULL THEN 0 WHEN sum(p_masterdata.reject)>0 THEN sum(p_masterdata.reject) END as tot_reject');
		$this->db->from('p_masterdata');
		$this->db->where('MONTH(date_report)',1);
		$this->db->where('YEAR(date_report)',$year);
		return $this->db->get()->result();
	}
	public function get_chart_2($year)
	{
		$this->db->select('CASE WHEN sum(p_masterdata.good) IS NULL THEN 0 WHEN sum(p_masterdata.good)>0 THEN sum(p_masterdata.good) END as good, CASE WHEN sum(p_masterdata.target) IS NULL THEN 0 WHEN sum(p_masterdata.target)>0 THEN sum(p_masterdata.target) END as target ,CASE WHEN sum(p_masterdata.tot_output) IS NULL THEN 0 WHEN sum(p_masterdata.tot_output)>0 THEN sum(p_masterdata.tot_output) END as output_tot,CASE WHEN sum(p_masterdata.reject) IS NULL THEN 0 WHEN sum(p_masterdata.reject)>0 THEN sum(p_masterdata.reject) END as tot_reject');
		$this->db->from('p_masterdata');
		$this->db->where('MONTH(date_report)',2);
		$this->db->where('YEAR(date_report)',$year);
		return $this->db->get()->result();
	}

	public function get_chart_3($year)
	{
		$this->db->select('CASE WHEN sum(p_masterdata.good) IS NULL THEN 0 WHEN sum(p_masterdata.good)>0 THEN sum(p_masterdata.good) END as good, CASE WHEN sum(p_masterdata.target) IS NULL THEN 0 WHEN sum(p_masterdata.target)>0 THEN sum(p_masterdata.target) END as target ,CASE WHEN sum(p_masterdata.tot_output) IS NULL THEN 0 WHEN sum(p_masterdata.tot_output)>0 THEN sum(p_masterdata.tot_output) END as output_tot,CASE WHEN sum(p_masterdata.reject) IS NULL THEN 0 WHEN sum(p_masterdata.reject)>0 THEN sum(p_masterdata.reject) END as tot_reject');
		$this->db->from('p_masterdata');
		$this->db->where('MONTH(date_report)',3);
		$this->db->where('YEAR(date_report)',$year);
		return $this->db->get()->result();
	}

	public function get_chart_4($year)
	{
		$this->db->select('CASE WHEN sum(p_masterdata.good) IS NULL THEN 0 WHEN sum(p_masterdata.good)>0 THEN sum(p_masterdata.good) END as good, CASE WHEN sum(p_masterdata.target) IS NULL THEN 0 WHEN sum(p_masterdata.target)>0 THEN sum(p_masterdata.target) END as target ,CASE WHEN sum(p_masterdata.tot_output) IS NULL THEN 0 WHEN sum(p_masterdata.tot_output)>0 THEN sum(p_masterdata.tot_output) END as output_tot,CASE WHEN sum(p_masterdata.reject) IS NULL THEN 0 WHEN sum(p_masterdata.reject)>0 THEN sum(p_masterdata.reject) END as tot_reject');
		$this->db->from('p_masterdata');
		$this->db->where('MONTH(date_report)',4);
		$this->db->where('YEAR(date_report)',$year);
		return $this->db->get()->result();
	}

		public function get_chart_5($year)
	{
		$this->db->select('CASE WHEN sum(p_masterdata.good) IS NULL THEN 0 WHEN sum(p_masterdata.good)>0 THEN sum(p_masterdata.good) END as good, CASE WHEN sum(p_masterdata.target) IS NULL THEN 0 WHEN sum(p_masterdata.target)>0 THEN sum(p_masterdata.target) END as target ,CASE WHEN sum(p_masterdata.tot_output) IS NULL THEN 0 WHEN sum(p_masterdata.tot_output)>0 THEN sum(p_masterdata.tot_output) END as output_tot,CASE WHEN sum(p_masterdata.reject) IS NULL THEN 0 WHEN sum(p_masterdata.reject)>0 THEN sum(p_masterdata.reject) END as tot_reject');
		$this->db->from('p_masterdata');
		$this->db->where('MONTH(date_report)',5);
		$this->db->where('YEAR(date_report)',$year);
		return $this->db->get()->result();
	}

		public function get_chart_6($year)
	{
		$this->db->select('CASE WHEN sum(p_masterdata.good) IS NULL THEN 0 WHEN sum(p_masterdata.good)>0 THEN sum(p_masterdata.good) END as good, CASE WHEN sum(p_masterdata.target) IS NULL THEN 0 WHEN sum(p_masterdata.target)>0 THEN sum(p_masterdata.target) END as target ,CASE WHEN sum(p_masterdata.tot_output) IS NULL THEN 0 WHEN sum(p_masterdata.tot_output)>0 THEN sum(p_masterdata.tot_output) END as output_tot,CASE WHEN sum(p_masterdata.reject) IS NULL THEN 0 WHEN sum(p_masterdata.reject)>0 THEN sum(p_masterdata.reject) END as tot_reject');
		$this->db->from('p_masterdata');
		$this->db->where('MONTH(date_report)',6);
		$this->db->where('YEAR(date_report)',$year);
		return $this->db->get()->result();
	}

	public function get_chart_7($year)
	{
		$this->db->select('CASE WHEN sum(p_masterdata.good) IS NULL THEN 0 WHEN sum(p_masterdata.good)>0 THEN sum(p_masterdata.good) END as good, CASE WHEN sum(p_masterdata.target) IS NULL THEN 0 WHEN sum(p_masterdata.target)>0 THEN sum(p_masterdata.target) END as target ,CASE WHEN sum(p_masterdata.tot_output) IS NULL THEN 0 WHEN sum(p_masterdata.tot_output)>0 THEN sum(p_masterdata.tot_output) END as output_tot,CASE WHEN sum(p_masterdata.reject) IS NULL THEN 0 WHEN sum(p_masterdata.reject)>0 THEN sum(p_masterdata.reject) END as tot_reject');
		$this->db->from('p_masterdata');
		$this->db->where('MONTH(date_report)',7);
		$this->db->where('YEAR(date_report)',$year);
		return $this->db->get()->result();
	}

	public function get_chart_8($year)
	{
		$this->db->select('CASE WHEN sum(p_masterdata.good) IS NULL THEN 0 WHEN sum(p_masterdata.good)>0 THEN sum(p_masterdata.good) END as good, CASE WHEN sum(p_masterdata.target) IS NULL THEN 0 WHEN sum(p_masterdata.target)>0 THEN sum(p_masterdata.target) END as target ,CASE WHEN sum(p_masterdata.tot_output) IS NULL THEN 0 WHEN sum(p_masterdata.tot_output)>0 THEN sum(p_masterdata.tot_output) END as output_tot,CASE WHEN sum(p_masterdata.reject) IS NULL THEN 0 WHEN sum(p_masterdata.reject)>0 THEN sum(p_masterdata.reject) END as tot_reject');
		$this->db->from('p_masterdata');
		$this->db->where('MONTH(date_report)',8);
		$this->db->where('YEAR(date_report)',$year);
		return $this->db->get()->result();
	}

	public function get_chart_9($year)
	{
		$this->db->select('CASE WHEN sum(p_masterdata.good) IS NULL THEN 0 WHEN sum(p_masterdata.good)>0 THEN sum(p_masterdata.good) END as good, CASE WHEN sum(p_masterdata.target) IS NULL THEN 0 WHEN sum(p_masterdata.target)>0 THEN sum(p_masterdata.target) END as target ,CASE WHEN sum(p_masterdata.tot_output) IS NULL THEN 0 WHEN sum(p_masterdata.tot_output)>0 THEN sum(p_masterdata.tot_output) END as output_tot,CASE WHEN sum(p_masterdata.reject) IS NULL THEN 0 WHEN sum(p_masterdata.reject)>0 THEN sum(p_masterdata.reject) END as tot_reject');
		$this->db->from('p_masterdata');
		$this->db->where('MONTH(date_report)',9);
		$this->db->where('YEAR(date_report)',$year);
		return $this->db->get()->result();
	}

	public function get_chart_10($year)
	{
		$this->db->select('CASE WHEN sum(p_masterdata.good) IS NULL THEN 0 WHEN sum(p_masterdata.good)>0 THEN sum(p_masterdata.good) END as good, CASE WHEN sum(p_masterdata.target) IS NULL THEN 0 WHEN sum(p_masterdata.target)>0 THEN sum(p_masterdata.target) END as target ,CASE WHEN sum(p_masterdata.tot_output) IS NULL THEN 0 WHEN sum(p_masterdata.tot_output)>0 THEN sum(p_masterdata.tot_output) END as output_tot,CASE WHEN sum(p_masterdata.reject) IS NULL THEN 0 WHEN sum(p_masterdata.reject)>0 THEN sum(p_masterdata.reject) END as tot_reject');
		$this->db->from('p_masterdata');
		$this->db->where('MONTH(date_report)',10);
		$this->db->where('YEAR(date_report)',$year);
		return $this->db->get()->result();
	}

	public function get_chart_11($year)
	{
		$this->db->select('CASE WHEN sum(p_masterdata.good) IS NULL THEN 0 WHEN sum(p_masterdata.good)>0 THEN sum(p_masterdata.good) END as good, CASE WHEN sum(p_masterdata.target) IS NULL THEN 0 WHEN sum(p_masterdata.target)>0 THEN sum(p_masterdata.target) END as target ,CASE WHEN sum(p_masterdata.tot_output) IS NULL THEN 0 WHEN sum(p_masterdata.tot_output)>0 THEN sum(p_masterdata.tot_output) END as output_tot,CASE WHEN sum(p_masterdata.reject) IS NULL THEN 0 WHEN sum(p_masterdata.reject)>0 THEN sum(p_masterdata.reject) END as tot_reject');
		$this->db->from('p_masterdata');
		$this->db->where('MONTH(date_report)',11);
		$this->db->where('YEAR(date_report)',$year);
		return $this->db->get()->result();
	}

	public function get_chart_12($year)
	{
		$this->db->select('CASE WHEN sum(p_masterdata.good) IS NULL THEN 0 WHEN sum(p_masterdata.good)>0 THEN sum(p_masterdata.good) END as good, CASE WHEN sum(p_masterdata.target) IS NULL THEN 0 WHEN sum(p_masterdata.target)>0 THEN sum(p_masterdata.target) END as target ,CASE WHEN sum(p_masterdata.tot_output) IS NULL THEN 0 WHEN sum(p_masterdata.tot_output)>0 THEN sum(p_masterdata.tot_output) END as output_tot,CASE WHEN sum(p_masterdata.reject) IS NULL THEN 0 WHEN sum(p_masterdata.reject)>0 THEN sum(p_masterdata.reject) END as tot_reject');
		$this->db->from('p_masterdata');
		$this->db->where('MONTH(date_report)',12);
		$this->db->where('YEAR(date_report)',$year);
		return $this->db->get()->result();
	}



	public function get_year_daily()
	{
		$this->db->select('DISTINCT YEAR(date_report) as date_report');
		$this->db->from('p_masterdata');
		$this->db->order_by('YEAR(date_report)');
		return $this->db->get()->result();
	}

	// $this->db->select('p_masterdata.employee_id, p.masterdata.date_report, p.masterdata.time_start, p.masterdata.time_finish, p.masterdata.brand, p.masterdata.tonnage, p.masterdata.part_name, sum(p_masterdata.reject) as tot_reject, sum(p_masterdata.tot_output) as output_tot, employees.name as name_emp, sum(p_masterdata.good) as tot_good');
	// 	$this->db->from('p_masterdata');
	// 	$this->db->join('employees','p_masterdata.employee_id = employees.employee_no');
	// 	$this->db->where('p_masterdata.date_report >=', $from_date);
	// 	$this->db->where('p_masterdata.date_report <=', $to_date);
	// 	$this->db->where('p_masterdata.employee_id', $employee_no);
	// 	$this->db->group_by('p_masterdata.id_group_master');
	// 	return $this->db->get()->result();

} ?>