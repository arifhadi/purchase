<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_currency extends CI_Model{

	public function get_data_currency()
	{
		$this->db->select('*');
		$this->db->from('currency');
		return $this->db->get()->result();
	}

	public function insert_currency($data)
	{
		return $this->db->insert('currency',$data);
	}
}