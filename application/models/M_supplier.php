<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Model Register
 */
class M_supplier extends CI_Model{

	// private $moulding_machine = $this->load->database('machine', true)->get('moulding_machine');
	// private $table2 = 'users';
	// const MAX_PASSWORD_SIZE_BYTES = 4096;

	public function __construct()
	{
		$this->db2  = $this->load->database('tooling', true);
		$this->db1  = $this->load->database('machine', true);
		// $db_machine = $this->load->database('machine', true);
		// private $moulding_machine = $this->db1->get('moulding_machine');
	}

	public function get_data_supplier()
	{

		$this->db->select('*');
		$this->db->from('supplier');
		return $this->db->get()->result();
	}

	public function last_code_supp()
	{
		$this->db->select('code');
		$this->db->from('supplier');
		$this->db->order_by('code','desc');
		$this->db->limit(1);
		return $this->db->get()->row();
	}


	public function insert_supplier($data)
	{
		$code = $data['code'];
		$cek_code = $this->db->get_where('supplier', array('code' => $code))->num_rows();
		if ($cek_code == 1) {
			return 'error';
		}else if($cek_code !== 1){
			return $this->db->insert('supplier', $data);
		}
		
	}

	public function get_row_supplier($code)
	{
		$this->db->select('*');
		$this->db->from('supplier');
		$this->db->where('code', $code);
		return $this->db->get()->row();
	}

	public function update_supplier($code, $data)
	{
		$this->db->where('code', $code);
		return $this->db->update('supplier', $data);
	}

} ?>