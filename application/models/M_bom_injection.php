<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Model Register
 */
class M_bom_injection extends CI_Model{

	// private $table1 = 'u_user_register';
	// private $table2 = 'users';
	// const MAX_PASSWORD_SIZE_BYTES = 4096;

	public function __construct()
	{
		date_default_timezone_set('Asia/Jakarta');
		$this->db2  = $this->load->database('tooling', true);
		$this->db1  = $this->load->database('machine', true);
	}

	public function get_data_tooling()
	{
		$this->db2->select('gih_tool_no,tooling_no');
		$this->db2->from('mold_basic_details');
		return $this->db2->get()->result();
	}

	public function insert_mold_receive($data)
	{

		$part_no = $data['part_no'];
		$cek_code = $this->db->get_where('ijm_mold_approve', array('part_no' => $part_no))->num_rows();
		if ($cek_code == 1) {
			return 'error';
		}else if($cek_code !== 1){
			return $this->db->insert('ijm_mold_approve', $data);
		}
	}

	public function update_mold_receive($part_no,$data)
	{
		$this->db->where('part_no', $part_no);
		return $this->db->update('ijm_mold_approve', $data);
	}

	public function get_mold_approve($part_no)
	{
		
		$this->db->select('is_receive,is_return');
		$this->db->from('ijm_mold_approve');
		$this->db->where('part_no',$part_no);
		return $this->db->get()->result();
	}

	public function get_order_qty_30k()
	{
		$this->db->select('*');
		$this->db->from('ijm_order_30k');
		return $this->db->get()->result();
	}

	public function get_order_qty_30k_row($part_no)
	{
		$this->db->select('*');
		$this->db->from('ijm_order_30k');
		$this->db->where('part_no',$part_no);
		return $this->db->get()->result();
	}

	public function insert_order_qty($data)
	{
		$part_no = $data['part_no'];
		$cek_code = $this->db->get_where('ijm_order_30k', array('part_no' => $part_no))->num_rows();
		if ($cek_code == 1) {
			return 'error';
		}else if($cek_code !== 1){
			return $this->db->insert('ijm_order_30k', $data);
		}

	}

	public function update_order_qty($part_no, $data)
	{
		$this->db->where('part_no', $part_no);
		return $this->db->update('ijm_order_30k', $data);
	}

	public function delete__order_qty($part_no)
	{
	
	
		$this->db->where('part_no',$part_no);
		return $this->db->delete('ijm_order_30k');
		
	}

	public function get_tote_bin()
	{
		$this->db->select('*');
		$this->db->from('ijm_tote_bin');
		return $this->db->get()->result();
	}

	public function insert_tote_bin($data)
	{
		$part_no = $data['part_no'];
		$cek_code = $this->db->get_where('ijm_tote_bin', array('part_no' => $part_no))->num_rows();
		if ($cek_code == 1) {
			return 'error';
		}else if($cek_code !== 1){
			return $this->db->insert('ijm_tote_bin', $data);
		}

	}

	public function update_tote_bin($part_no, $data)
	{
		$this->db->where('part_no', $part_no);
		return $this->db->update('ijm_tote_bin', $data);
	}

		public function get_tote_bin_rows($part_no)
	{
		$this->db->select('*');
		$this->db->from('ijm_tote_bin');
		$this->db->where('part_no',$part_no);
		return $this->db->get()->result();
	}

	public function delete_tote_bin($part_no)
	{
	
	
		$this->db->where('part_no',$part_no);
		return $this->db->delete('ijm_tote_bin');
		
	}

	public function get_spq_details()
	{
		$this->db->select('*');
		$this->db->from('ijm_spq_details');
		return $this->db->get()->result();
	}
	public function get_spq_details_rows($part_no)
	{
		$this->db->select('*');
		$this->db->from('ijm_spq_details');
		$this->db->where('part_no',$part_no);
		return $this->db->get()->result();
	}

	public function insert_spq_details($data)
	{
		$part_no = $data['part_no'];
		$cek_code = $this->db->get_where('ijm_spq_details', array('part_no' => $part_no))->num_rows();
		if ($cek_code == 1) {
			return 'error';
		}else if($cek_code !== 1){
			return $this->db->insert('ijm_spq_details', $data);
		}

	}
	public function delete_spq_details($part_no)
	{
	
	
		$this->db->where('part_no',$part_no);
		return $this->db->delete('ijm_spq_details');
		
	}

	public function get_packing_material()
	{
		$this->db->select('*');
		$this->db->from('ijm_packing_material_requirement');
		return $this->db->get()->result();
	}

	public function insert_packing_material($data)
	{
		$part_no = $data['part_no'];
		$cek_code = $this->db->get_where('ijm_packing_material_requirement', array('part_no' => $part_no))->num_rows();
		if ($cek_code == 1) {
			return 'error';
		}else if($cek_code !== 1){
			return $this->db->insert('ijm_packing_material_requirement', $data);
		}

	}
	public function get_packing_material_row($part_no)
	{
		$this->db->select('*');
		$this->db->from('ijm_packing_material_requirement');
		$this->db->where('part_no',$part_no);
		return $this->db->get()->result();
	}
	public function update_packing_material($part_no, $data)
	{
		$this->db->where('part_no', $part_no);
		return $this->db->update('ijm_packing_material_requirement', $data);
	}
	public function delete_packing_material($part_no)
	{
	
	
		$this->db->where('part_no',$part_no);
		return $this->db->delete('ijm_packing_material_requirement');
		
	}

	public function get_packaging_material()
	{
		$this->db->select('*');
		$this->db->from('ijm_packaging_material');
		return $this->db->get()->result();
	}
	public function get_packaging_material_row($part_no)
	{
		$this->db->select('*');
		$this->db->from('ijm_packaging_material');
		$this->db->where('part_no',$part_no);
		return $this->db->get()->result();
	}

	public function insert_packaging_material($data)
	{
		$part_no = $data['part_no'];
		$cek_code = $this->db->get_where('ijm_packaging_material', array('part_no' => $part_no))->num_rows();
		if ($cek_code == 1) {
			return 'error';
		}else if($cek_code !== 1){
			return $this->db->insert('ijm_packaging_material', $data);
		}

	}
	public function update_packaging_material($part_no, $data)
	{
		$this->db->where('part_no', $part_no);
		return $this->db->update('ijm_packaging_material', $data);
	}

	public function delete_packaging_material($part_no)
	{
	
	
		$this->db->where('part_no',$part_no);
		return $this->db->delete('ijm_packaging_material');
		
	}


	public function get_part()
	{
		$this->db->select('p.part_no,p.colour,p.manufacturing_part_number,p.isPackagingMaterial,p.isPackingMaterialRequirement,p.isSPQ,p.isToteBin,p.isOrderQty30k,p.isMasterBatch,p.ink_type,p.fixture,p.isRequirement,p.isActualRunning, p.part_name, p.status, cs.name as name_cust, pj.name as name_project, p.resin_code,  m.name as name_resin, m.material_base as m_base_resin, m.color as color_resin, b.name as brand_resin, s.name as supllier_resin, p.masterbatch_code, mt.name as masterbatch_name, mt.color as masterbatch_color, p.dossage, sp.name as masterbatch_supplier,ap.is_receive as is_receive,ap.is_return as is_return,ap.date_receive as date_receive,ap.date_return as date_return');
		$this->db->from('ijm_material_part as p');
		$this->db->join('ijm_material_customer as cs','cs.short_name = p.id_customer','left');
		$this->db->join('ijm_material_project as pj','pj.code = p.project_code','left');
		$this->db->join('ijm_material as m','m.code = p.resin_code','left');
		$this->db->join('ijm_material as mt','mt.code = p.masterbatch_code','left');
		$this->db->join('ijm_material_brand as b','b.code = m.brand_code','left');
		$this->db->join('ijm_material_brand as br','br.code = mt.brand_code','left');
		$this->db->join('ijm_material_supplier as s','s.code = m.supplier_code','left');
		$this->db->join('ijm_material_supplier as sp','sp.code = mt.supplier_code','left');
		$this->db->join('ijm_mold_approve as ap','ap.part_no = p.part_no','left');
		// log_r($this->db->get()->result());
		return $this->db->get()->result();
	}

	public function get_cust_requirement()
	{
		$this->db->select('*');
		$this->db->from('ijm_requirement_customer');
		return $this->db->get()->result();
	}
	public function get_cust_requirement_row($part_no)
	{
		$this->db->select('*');
		$this->db->from('ijm_requirement_customer');
		$this->db->where('part_no',$part_no);
		return $this->db->get()->result();
	}

	public function insert_cust_requirement($data)
	{
		$part_no = $data['part_no'];
		$cek_code = $this->db->get_where('ijm_requirement_customer', array('part_no' => $part_no))->num_rows();
		if ($cek_code == 1) {
			return 'error';
		}else if($cek_code !== 1){
			return $this->db->insert('ijm_requirement_customer', $data);
		}

	}

	public function update_cust_requirement($part_no, $data)
	{
		
		$this->db->where('part_no', $part_no);
		return $this->db->update('ijm_requirement_customer', $data);
	}

	public function delete_customer_requirement($part_no)
	{
	
	
		$this->db->where('part_no',$part_no);
		return $this->db->delete('ijm_requirement_customer');
		
	}

	public function get_actual_material()
	{
		$this->db->select('*');
		$this->db->from('ijm_material_actual_running');
		return $this->db->get()->result();
	}

	public function export_excel_part() 
	{
            $this->db->select(array('part_no', 'manufacturing_part_number', 'part_name', 'id_customer', 'project_code'));
            $this->db->from('ijm_material_part');
            $query = $this->db->get();
            return $query->result();
       }
	public function insert_material_part($part_no, $data)
	{
		$part_no = $data['part_no'];
		$cek_code = $this->db->get_where('ijm_material_part', array('part_no' => $part_no))->num_rows();
		if ($cek_code == 1) {
			return 'error';
		}else if($cek_code !== 1){
			return $this->db->insert('ijm_material_part', $data);
		}

	}

	public function update_material_part($part_no, $data)
	{
		$this->db->where('part_no', $part_no);
		return $this->db->update('ijm_material_part', $data);
	}

	public function get_part_row($part_no)
	{
		$this->db->select('p.part_no,p.colour,p.manufacturing_part_number,p.isPackagingMaterial,p.isPackingMaterialRequirement,p.isSPQ,p.isToteBin,p.isOrderQty30k,p.isMasterBatch,p.ink_type,p.fixture,p.isRequirement, p.isActualRunning, p.part_name, p.id_customer, p.project_code, p.status, cs.name as name_cust, pj.name as name_project, p.resin_code,  m.name as name_resin, m.material_base as m_base_resin, m.color as color_resin, b.name as brand_resin, s.name as supllier_resin, p.masterbatch_code, mt.name as masterbatch_name, mt.color as masterbatch_color, p.dossage, sp.name as masterbatch_supplier, p.n_process');
		$this->db->from('ijm_material_part as p');
		$this->db->join('ijm_material_customer as cs','cs.short_name = p.id_customer','left');
		$this->db->join('ijm_material_project as pj','pj.code = p.project_code','left');
		$this->db->join('ijm_material as m','m.code = p.resin_code','left');
		$this->db->join('ijm_material as mt','mt.code = p.masterbatch_code','left');
		$this->db->join('ijm_material_brand as b','b.code = m.brand_code','left');
		$this->db->join('ijm_material_brand as br','br.code = mt.brand_code','left');
		$this->db->join('ijm_material_supplier as s','s.code = m.supplier_code','left');
		$this->db->join('ijm_material_supplier as sp','sp.code = mt.supplier_code','left');
		$this->db->where('p.part_no', $part_no);
		return $this->db->get()->row();
	}


	public function get_material()
	{
		$this->db->select('m.code, m.name, m.color, m.material_base, c.name as name_category, b.name as name_brand, s.name as name_supplier');
		$this->db->from('ijm_material as m');
		$this->db->join('ijm_material_category as c','c.code = m.category_code','left');
		$this->db->join('ijm_material_brand as b','b.code = m.brand_code','left');
		$this->db->join('ijm_material_supplier as s','s.code = m.supplier_code','left');
		return $this->db->get()->result();
	}


	public function get_material_category($category)
	{
		$this->db->select('m.code, m.name, m.color, m.material_base, c.name as name_category, b.name as name_brand, s.name as name_supplier');
		$this->db->from('ijm_material as m');
		$this->db->join('ijm_material_category as c','c.code = m.category_code','left');
		$this->db->join('ijm_material_brand as b','b.code = m.brand_code','left');
		$this->db->join('ijm_material_supplier as s','s.code = m.supplier_code','left');
		$this->db->where('m.category_code', $category);
		return $this->db->get()->result();
	}


	


	public function insert_material($data)
	{
		$code = $data['code'];
		$cek_code = $this->db->get_where('ijm_material', array('code' => $code))->num_rows();
		if ($cek_code == 1) {
			return 'error';
		}else if($cek_code !== 1){
			return $this->db->insert('ijm_material', $data);
		}
	}

	public function get_detail_material($code)
	{
		$this->db->select('m.name as m_name, m.material_base, m.color as color_material, b.name as brand_name, s.name as supplier_name');
		$this->db->from('ijm_material as m');
		$this->db->join('ijm_material_category as c','c.code = m.category_code','left');
		$this->db->join('ijm_material_brand as b','b.code = m.brand_code','left');
		$this->db->join('ijm_material_supplier as s','s.code = m.supplier_code','left');
		$this->db->where('m.code', $code);
		return $this->db->get()->row();
	}

	public function get_detail_machine($code)
	{
		$this->db1->select('tonnage');
		$this->db1->from('moulding_machine');
		$this->db1->where('id_machine',$code);
		return $this->db1->get()->row();
	}
	function get_m_category()
	{
		$this->db->select('*');
		$this->db->from('ijm_material_category');
		return $this->db->get()->result();
	}

	public function get_row_material($code)
	{
		$this->db->select('*');
		$this->db->from('ijm_material');
		$this->db->where('code', $code);
		return $this->db->get()->row();
	}

	public function update_material($code, $data)
	{
		$this->db->where('code', $code);
		return $this->db->update('ijm_material', $data);
	}

	public function get_customer()
	{
		$this->db->select('*');
		$this->db->from('ijm_material_customer');
		return $this->db->get()->result();
		// log_r($this->db->get()->result());
	}

	public function get_data_machine_ijm()
	{
		$this->db1->select('mc_no,id_machine,line_mc_no');
		$this->db1->from('moulding_machine');
		return $this->db1->get()->result();
	}

	public function get_data_machine_smt()
	{
		$this->db1->select('mc_no,id_machine,line_mc_no');
		$this->db1->from('smt_machine');
		return $this->db1->get()->result();
	}


	public function insert_customer($data)
	{
		$short_name = $data['short_name'];
		$cek_short_name = $this->db->get_where('ijm_material_customer', array('short_name' => $short_name))->num_rows();
		if ($cek_short_name == 1) {
			return 'error';
		}else if($cek_short_name !== 1){
			return $this->db->insert('ijm_material_customer', $data);
		}
		
	}


	public function get_data_actual_rows($part_no)
	{
		$this->db->select('*');
		$this->db->from('ijm_material_actual_running');
		$this->db->where('part_no', $part_no);
		return $this->db->get()->row();
	}
	public function insert_actual_planning_material($data)
	{
		$part_no = $data['part_no'];
		$cek_part_no = $this->db->get_where('ijm_material_actual_running', array('part_no' => $part_no))->num_rows();
		if ($cek_part_no == 1) {
			return 'error';
		}else if($cek_part_no !== 1){
			return $this->db->insert('ijm_material_actual_running', $data);
		}
		
	}

	public function update_part_actual($data_part,$part_no)
	{
		$this->db->where('part_no', $part_no);
		return $this->db->update('ijm_material_part', $data_part);
	}


	public function update_actual_planning($data,$part_no)
	{
		$this->db->where('part_no',$part_no);
		return $this->db->update('ijm_material_actual_running',$data);
	}

		public function delete_actual_planning($part_no)
	{
	
	
		$this->db->where('part_no',$part_no);
		return $this->db->delete('ijm_material_actual_running');
		
	}
	public function get_row_customer($short_name)
	{
		$this->db->select('*');
		$this->db->from('ijm_material_customer');
		$this->db->where('short_name', $short_name);
		return $this->db->get()->row();
	}

	public function update_customer($short_name, $data)
	{
		$this->db->where('short_name', $short_name);
		return $this->db->update('ijm_material_customer', $data);
	}

	public function get_supplier_code()
	{
		$this->db->select('*');
		$this->db->from('ijm_material_supplier_code');
		return $this->db->get()->result();
	}

		public function get_supplier_code_row($code)
	{
		$this->db->select('a.supplier_code,a.cc_code');
		$this->db->from('ijm_material_supplier_code as a');
		$this->db->join('ijm_material_supplier as b','b.supplier_code = a.cc_code','left');
		$this->db->where('b.code',$code);
		return $this->db->get()->result();
	}

		public function insert_supplier_code($data)
	{
		$code = $data['cc_code'];
		$cek_code = $this->db->get_where('ijm_material_supplier_code', array('cc_code' => $cc_code))->num_rows();
		if ($cek_code == 1) {
			return 'error';
		}else if($cek_code !== 1){
			return $this->db->insert('ijm_material_supplier_code', $data);
		}
		
	}

	public function get_supplier()
	{
		$this->db->select('*');
		$this->db->from('ijm_material_supplier');
		return $this->db->get()->result();
	}

	

	public function insert_supplier($data)
	{
		$code = $data['code'];
		$cek_code = $this->db->get_where('ijm_material_supplier', array('code' => $code))->num_rows();
		if ($cek_code == 1) {
			return 'error';
		}else if($cek_code !== 1){
			return $this->db->insert('ijm_material_supplier', $data);
		}
		
	}

	public function get_row_supplier($code)
	{
		$this->db->select('*');
		$this->db->from('ijm_material_supplier');
		$this->db->where('code', $code);
		return $this->db->get()->row();
	}

	public function update_supplier($code, $data)
	{
		$this->db->where('code', $code);
		return $this->db->update('ijm_material_supplier', $data);
	}

	public function get_project()
	{
		$this->db->select('p.code, p.name, p.description, c.name as nama_cust');
		$this->db->from('ijm_material_project as p');
		$this->db->join('ijm_material_customer as c','c.short_name = p.id_customer','left');
		return $this->db->get()->result();
	}

	function random_str($length, $keyspace = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')
	{
	    $pieces = [];
	    $max = mb_strlen($keyspace, '8bit') - 1;
	    for ($i = 0; $i < $length; ++$i) {
	        $pieces []= $keyspace[rand(0, $max)];
	    }
	    return implode('', $pieces);
	}

	public function insert_project($data)
	{
		$this->db->select('id_customer, name');
		$this->db->from('ijm_material_project');
		$this->db->where('id_customer', $data['id_customer']);
		$this->db->where('name', $data['name']);
		$data_check  = $this->db->get()->row();
		if (!empty($data_check)) {
			$id_customer = $data_check->id_customer;
			$name 		 = $data_check->name;
			if ($id_customer == $data['id_customer'] AND $name == $data['name']) {
				return "error";
			}else{
				return $this->db->insert('ijm_material_project', $data);
			}
		}elseif (empty($data_check)) {
			return $this->db->insert('ijm_material_project', $data);
		}
	}

	public function get_row_project($code)
	{
		$this->db->select('*');
		$this->db->from('ijm_material_project');
		$this->db->where('code', $code);
		return $this->db->get()->row();
	}

	public function update_project($code, $data)
	{
		$this->db->where('code', $code);
		return $this->db->update('ijm_material_project', $data);
	}

	public function get_brand()
	{
		$this->db->select('*');
		$this->db->from('ijm_material_brand');
		return $this->db->get()->result();
	}

	public function insert_brand($data)
	{
		$this->db->select('code');
		$this->db->from('ijm_material_brand');
		$this->db->where('code', $data['code']);
		$data_check  = $this->db->get()->row();
		if (!empty($data_check)) {
			$code = $data_check->code;
			if ($code == $data['code']) {
				return "error";
			}else{
				return $this->db->insert('ijm_material_brand', $data);
			}
		}elseif (empty($data_check)) {
			return $this->db->insert('ijm_material_brand', $data);
		}
	}

	public function get_row_brand($code)
	{
		$this->db->select('*');
		$this->db->from('ijm_material_brand');
		$this->db->where('code', $code);
		return $this->db->get()->row();
	}

	public function update_brand($code, $data)
	{
		$this->db->where('code', $code);
		return $this->db->update('ijm_material_brand', $data);
	}


	public function get_list_part_master()
	{
		$this->db->select('*');
		$this->db->from('smt_bom_part_master');
		return $this->db->get()->result();
	}

	public function delete_part_master($part_no)
	{
		$this->db->where('id',$part_no);
		return $this->db->delete('smt_bom_part_master');
	}
	public function delete_part_code($part_code)
	{
		$this->db->where('part_code',$part_code);
		return $this->db->delete('smt_bom_part_code');
	}

	public function insert_part_master_smt($data)
	{
		return $this->db->insert('smt_bom_part_master', $data);
	}
	public function data_part_master_row($id)
	{
		$this->db->select('*');
		$this->db->from('smt_bom_part_master');
		$this->db->where('id',$id);
		return $this->db->get()->result();
	}


	public function data_part_code_row($id)
	{
		$this->db->select('*');
		$this->db->from('smt_bom_part_code');
		$this->db->where('part_code',$id);
		return $this->db->get()->result();
	}


	public function update_part_master($data, $id)
	{
		
		$this->db->where('id', $id);
		return $this->db->update('smt_bom_part_master', $data);

	}

	public function update_part_code($data, $part_code)
	{
		
		$this->db->where('part_code', $part_code);
		return $this->db->update('smt_bom_part_code', $data);

	}

	public function get_data_part_code()
	{
		$this->db->select('*');
		$this->db->from('smt_bom_part_code');
		$this->db->where('is_plan',0);
		return $this->db->get()->result();
	}


	public function insert_part_code_smt($data)
	{
		$this->db->select('part_code');
		$this->db->from('smt_bom_part_code');
		$this->db->where('part_code', $data['part_code']);
		$data_check  = $this->db->get()->row();

		// log_r($data_check);
		if (!empty($data_check)) {
			$code = $data_check->part_code;
			if ($code == $data['part_code']) {
				return "error";
			}else{
				return $this->db->insert('smt_bom_part_code', $data);
			}
		}elseif (empty($data_check)) {
			return $this->db->insert('smt_bom_part_code', $data);
		}
	}


} ?>