<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Model Register
 */
class M_po extends CI_Model{

	// private $moulding_machine = $this->load->database('machine', true)->get('moulding_machine');
	// private $table2 = 'users';
	// const MAX_PASSWORD_SIZE_BYTES = 4096;

	public function __construct()
	{
		$this->db2  = $this->load->database('tooling', true);
		$this->db1  = $this->load->database('machine', true);
		$this->db3  = $this->load->database('hrms', true);
	}

	public function get_data_all_po()
	{
		$this->db->select('a.create_at po_date,a.po_no,a.pr_no,b.create_at as date_request,c.cost_center,c.purpose,CASE WHEN c.description >=1 THEN e.description ELSE c.description END as description,c.category_request,d.first_name as user_req,e.supplier_code,CASE WHEN f.name IS NULL THEN h.name ELSE f.name END as name_sup ,e.item_no as part_no,c.new_order_qty,c.uom,CASE WHEN c.unit_price_pf IS NULL THEN e.unit_price ELSE c.unit_price_pf END as unit_price_ttl,CASE WHEN c.unit_price_pf IS NULL THEN (e.unit_price * c.new_order_qty) ELSE (c.unit_price_pf * c.new_order_qty) END as total_amount,CASE WHEN c.currency IS NULL THEN e.currency ELSE c.currency END as currency,e.incoterm,b.date_approve_by_pd as date_pr_approve,f.payment_term,f.loc_code,g.department',FALSE);
		$this->db->from('release_po as a');
		$this->db->join('detail_pr as b','b.pr_no=a.pr_no');
		$this->db->join('purchase_requisition as c','c.pr_no=b.pr_no','left');
		$this->db->join('users as d','d.id = b.user_id');
		$this->db->join('item as e','e.id_item = c.description','left');
		$this->db->join('supplier as f','f.code = e.supplier_code','left');
		$this->db->join('supplier as h','h.code=c.supplier_name_pf','left');
		$this->db->join('u1632880_hrms.employees as g','g.employee_no=d.employee_no','left');
		return $this->db->get()->result();
	}

	public function get_data_all_po_unrelease()
	{
		$this->db->select('b.pr_no,b.create_at as date_request,c.cost_center,c.purpose,CASE WHEN c.description >=1 THEN e.description ELSE c.description END as description,c.category_request,d.first_name as user_req,e.supplier_code,e.item_no as part_no,f.name as name_sup,c.new_order_qty,c.uom,CASE WHEN c.unit_price_pf IS NULL THEN e.unit_price ELSE c.unit_price_pf END as unit_price_ttl,CASE WHEN c.unit_price_pf IS NULL THEN (e.unit_price * c.new_order_qty) ELSE (c.unit_price_pf * c.new_order_qty) END as total_amount,CASE WHEN c.currency IS NULL THEN e.currency ELSE c.currency END as currency,e.incoterm,b.date_approve_by_pd as date_pr_approve,f.payment_term,f.loc_code,g.department',FALSE);
		$this->db->from('detail_pr as b');
		$this->db->join('purchase_requisition as c','c.pr_no=b.pr_no','left');
		$this->db->join('users as d','d.id = b.user_id');
		$this->db->join('item as e','e.id_item = c.description','left');
		$this->db->join('supplier as f','f.code = e.supplier_code','left');
		$this->db->join('u1632880_hrms.employees as g','g.employee_no=d.employee_no','left');
		$this->db->where('is_approve_hod',1);
		$this->db->where('is_approve_werehouse',1);
		$this->db->where('is_approve_finance',1);
		$this->db->where('is_approve_division_head',1);
		$this->db->where('is_approve_by_pd',1);
		$this->db->where('is_approve_purchasing',1);
		$this->db->where('is_release_po',0);
		return $this->db->get()->result();
	}

	public function get_data_all_po_row($po_no)
	{
		$this->db->select('a.create_at po_date,a.po_no,a.pr_no,b.create_at as date_request,c.cost_center,c.purpose,c.description,c.category_request,d.first_name as user_req,e.supplier_code,e.item_no as part_no,CASE WHEN f.name IS NULL THEN h.name ELSE f.name END as name_sup,c.new_order_qty,c.uom,CASE WHEN c.unit_price_pf IS NULL THEN e.unit_price ELSE c.unit_price_pf END as unit_price_ttl,CASE WHEN c.unit_price_pf IS NULL THEN (e.unit_price * c.new_order_qty) ELSE (c.unit_price_pf * c.new_order_qty) END as total_amount,CASE WHEN c.currency IS NULL THEN e.currency ELSE c.currency END as currency,e.incoterm,b.date_approve_by_pd as date_pr_approve,f.payment_term,f.loc_code,g.department',FALSE);
		$this->db->from('release_po as a');
		$this->db->join('detail_pr as b','b.pr_no=a.pr_no');
		$this->db->join('purchase_requisition as c','c.pr_no=b.pr_no','left');
		$this->db->join('users as d','d.id = b.user_id');
		$this->db->join('item as e','e.id_item = c.description','left');
		$this->db->join('supplier as f','f.code = e.supplier_code','left');
		$this->db->join('supplier as h','h.code=c.supplier_name_pf','left');
		$this->db->join('u1632880_hrms.employees as g','g.employee_no=d.employee_no','left');
		$this->db->where('a.po_no',$po_no);
		return $this->db->get()->result();
	}


	public function get_data_print_po($po_no)
	{
		$this->db->select('f.telpon,f.attn,f.email,a.create_at po_date,a.po_no,a.pr_no,b.create_at as date_request,c.cost_center,c.purpose,CASE WHEN c.description>1 THEN e.description ELSE c.description END as description,c.category_request,d.first_name as user_req,e.supplier_code,e.item_no as part_no,CASE WHEN f.name IS NULL THEN h.name ELSE f.name END as name_sup,c.new_order_qty,c.uom,CASE WHEN c.unit_price_pf IS NULL THEN e.unit_price ELSE c.unit_price_pf END as unit_price_ttl,CASE WHEN c.unit_price_pf IS NULL THEN (e.unit_price * c.new_order_qty) ELSE (c.unit_price_pf * c.new_order_qty) END as total_amount,CASE WHEN c.currency IS NULL THEN e.currency ELSE c.currency END as currency,e.incoterm,b.date_approve_by_pd as date_pr_approve,e.incoterm,CASE WHEN f.payment_term IS NULL THEN h.payment_term ELSE f.payment_term END as payment_term,CASE WHEN f.address IS NULL THEN h.address ELSE f.address END as address,CASE WHEN f.address_2 IS NULL THEN h.address_2 ELSE f.address_2 END as address_2,CASE WHEN f.loc_code IS NULL THEN h.loc_code ELSE f.loc_code END as loc_code,g.department',
			FALSE);
		$this->db->from('release_po as a');
		$this->db->join('detail_pr as b','b.pr_no=a.pr_no');
		$this->db->join('purchase_requisition as c','c.pr_no=b.pr_no','left');
		$this->db->join('users as d','d.id = b.user_id');
		$this->db->join('item as e','e.id_item = c.description','left');
		$this->db->join('supplier as f','f.code = e.supplier_code','left');
		$this->db->join('supplier as h','h.code=c.supplier_name_pf','left');
		$this->db->join('u1632880_hrms.employees as g','g.employee_no=d.employee_no','left');
		$this->db->where('a.po_no',$po_no);
		return $this->db->get()->result();
	}

	public function sum_grand_total_amount_po($po_no)
	{
		$this->db->select('CASE WHEN c.unit_price_pf IS NULL THEN SUM(e.unit_price * c.new_order_qty) ELSE SUM(c.unit_price_pf * c.new_order_qty) END as total_amount_grand,SUM(c.new_order_qty) as grand_total_qty',
			FALSE);
		$this->db->from('release_po as a');
		$this->db->join('detail_pr as b','b.pr_no=a.pr_no');
		$this->db->join('purchase_requisition as c','c.pr_no=b.pr_no','left');
		$this->db->join('users as d','d.id = b.user_id');
		$this->db->join('item as e','e.id_item = c.description','left');
		$this->db->join('supplier as f','f.code = e.supplier_code','left');
		$this->db->join('u1632880_hrms.employees as g','g.employee_no=d.employee_no','left');
		$this->db->where('a.po_no',$po_no);
		return $this->db->get()->row();
	}




	public function get_data_list_pr()
	{
		$this->db->select('a.pr_no,CASE WHEN a.description>1 THEN c.description ELSE a.description END as description,b.is_release_po,CASE WHEN a.supplier_name_pf IS NULL THEN c.supplier_code ELSE a.supplier_name_pf END as supplier_code',FALSE);
		$this->db->from('purchase_requisition as a');
		$this->db->join('detail_pr as b','b.pr_no=a.pr_no','left');
		$this->db->join('item as c','c.id_item=a.description','left');
		$this->db->where('is_approve_hod',1);
		$this->db->where('is_approve_werehouse',1);
		$this->db->where('is_approve_finance',1);
		$this->db->where('is_approve_division_head',1);
		$this->db->where('is_approve_by_pd',1);
		$this->db->where('is_approve_purchasing',1);
		$this->db->where('is_release_po',0);
		return $this->db->get()->result();
	}




	public function insert_po($data)
	{
		return $this->db->insert('release_po',$data);  
	}

	public function update_po_release($pr_no,$data)
	{
		$this->db->where('pr_no',$pr_no);
		$this->db->update('detail_pr',$data);
	}
	public function get_data_id_item($pr_no)
	{
		$this->db->select('description');
		$this->db->from('purchase_requisition');
		return $this->db->get()->row();
	}

	public function get_data_pr_ud($pr_no)
	{
		$this->db->select('*');
		$this->db->from('purchase_requisition');
		return $this->db->get()->row();
	}

	public function update_last_order($id_item,$data)
	{
		$this->db->where('id_item',$id_item);
		return $this->db->update('last_order_detail',$data);
	}

	public function get_detail_pr_item($pr_no)
	{
		$this->db->select('a.new_order_qty,b.unit_price,b.currency,b.supplier_code');
		$this->db->from('purchase_requisition as a');
		$this->db->join('item as b','b.id_item=a.description');
		$this->db->where('pr_no',$pr_no);
		return $this->db->get()->row();
	}

	public function update_last_order_history($id_item,$data)
	{
		$this->db->where('id_item',$id_item);
		return $this->db->update('history_last_order_detail',$data);
	}

	public function cek_his_lo($id_item)
	{
		$this->db->select('id,unit_price');
		$this->db->from('history_last_order_detail');
		$this->db->where('id_item',$id_item);
		$this->db->order_by('id DESC');
		$this->db->limit(1);
		return $this->db->get()->row();
	}

	public function ttl_unrelease_po()
	{
		$this->db->select('COUNT(*) as ttl_po_unrelease');
		$this->db->from('detail_pr');
		$this->db->where('is_approve_hod',1);
		$this->db->where('is_approve_werehouse',1);
		$this->db->where('is_approve_finance',1);
		$this->db->where('is_approve_division_head',1);
		$this->db->where('is_approve_by_pd',1);
		$this->db->where('is_approve_purchasing',1);
		$this->db->where('is_release_po',0);
		return $this->db->get()->row();
	}

	public function get_data_latter_head()
	{
		$this->db->select('file_head_latter');
		$this->db->from('head_latter');
		$this->db->order_by('create_at DESC');
		$this->db->limit(1);
		// log_r($this->db->get()->row());
		return $this->db->get()->row();
	}

	public function save_head_kop($data)
	{
		return $this->db->insert('head_latter');
	}




} ?>