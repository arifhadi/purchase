<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Model Register
 */
class M_employee extends CI_Model{

	// private $table1 = 'u_user_register';
	// private $table2 = 'users';
	// const MAX_PASSWORD_SIZE_BYTES = 4096;

	public function __construct()
	{
		date_default_timezone_set('Asia/Jakarta');
		$this->db2  = $this->load->database('tooling', true);
		$this->db1  = $this->load->database('machine', true);
	}

	public function get_mahasiswa()
	{
		$this->db->select('*');
		$this->db->from('mahasiswa');
		return $this->db->get()->result();
	}

	public function get_employee()
	{
		$this->db->select('*');
		$this->db->from('employees');
		$this->db->where('id_group', 4);
		return $this->db->get()->result();
	}

	public function insert_data_form($data)
	{
		return $this->db->insert('p_masterdata', $data);
	}


	public function update_peformance($id_group_master, $grade)
	{
		$this->db->where('id_group_master', $id_group_master);
		return $this->db->update('p_peformance', $grade);
	}

	public function get_group()
	{
		$this->db->select('*');
		$this->db->from('groups');
		return $this->db->get()->result();
	}

	public function insert_employee($data)
	{
		return $this->db->insert('employees', $data);
	}

	public function get_date_end_contract($employee_no)
	{
		$this->db->select('date_end_contract, desc_emp_end');
		$this->db->from('employees');
		$this->db->where('employee_no', $employee_no);
		return $this->db->get()->row();
	}

	public function update_emp_nonactive($data, $employee_no)
	{
		$this->db->where('employee_no', $employee_no);
		return $this->db->update('employees', $data);
	}

	public function check_emp_no($employee_no)
	{
		return $this->db->get_where('employees', array('employee_no' => $employee_no))->num_rows();
	}

	public function get_edit_emp($emp_no)
	{
		$this->db->select('*');
		$this->db->from('employees');
		$this->db->where('employee_no', $emp_no);
		return $this->db->get()->row();
	}

	public function update_emp($employee_no, $data)
	{
		$this->db->where('employee_no', $employee_no);
		return $this->db->update('employees', $data);
	}

	public function update_status_emp()
	{
		$datenow = date("Y-m-d");
		$this->db->select('emp.employee_no, emp.date_end_contract');
		$this->db->from('employees as emp');
		$this->db->where('emp.date_end_contract <', $datenow);
		$query = $this->db->get()->result();
		foreach ($query as $value) {
			$this->db->set('status', '0');
			$this->db->where('employees.employee_no', $value->employee_no);
			$this->db->update('employees');
		}
	}

	public function get_select_emp($id_filter)
	{
		$this->db->select($id_filter);
		$this->db->from('employees');
		$this->db->group_by($id_filter);
		return $this->db->get()->result();
	}

	public function get_filter_emp($name_filter, $data_filter)
	{
		$this->db->select('*');
		$this->db->from('employees');
		if ($data_filter !="all") {
			$this->db->where($name_filter, $data_filter);
		}
		return $this->db->get()->result();
	}

} ?>