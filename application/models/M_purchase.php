<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Model Register
 */
class M_purchase extends CI_Model{

	// private $moulding_machine = $this->load->database('machine', true)->get('moulding_machine');
	// private $table2 = 'users';
	// const MAX_PASSWORD_SIZE_BYTES = 4096;

	public function __construct()
	{
		$this->db2  = $this->load->database('tooling', true);
		$this->db1  = $this->load->database('machine', true);
		$this->db3  = $this->load->database('hrms', true);
		// $db_machine = $this->load->database('machine', true);
		// private $moulding_machine = $this->db1->get('moulding_machine');
	}

	public function get_data_user_hod()
	{
		$this->db->select('employee_no');
		$this->db->from('users');
		$this->db->where('id',USER_ID);
		return $this->db->get()->row();
	}

	public function get_check_part_no($part_no)
	{
		$this->db->select('item_no');
		$this->db->from('item');
		$this->db->where('item_no',$part_no);
		return $this->db->get()->row();
	}

	public function get_data_pr()
	{
		$this->db->select('a.*,b.mc_no,b.line_mc_no');
		$this->db->from('detail_pr as a');
		$this->db->join('u1632880_machinemanagement.moulding_machine as b','b.id_machine=a.mc_no','left');
		$this->db->where('a.user_id',USER_ID);
		$this->db->order_by('pr_no DESC');
		return $this->db->get()->result();
	}

	public function get_data_history_werehouse()
	{
		$this->db->select('a.*,b.line_mc_no,b.mc_no,c.first_name,d.first_name as user_hod');
		$this->db->from('detail_pr as a');
		$this->db->join('u1632880_machinemanagement.moulding_machine as b','b.id_machine=a.mc_no','left');
		$this->db->join('users as c','c.id=a.user_id');
		$this->db->join('users as d','d.id=a.user_hod');
		$this->db->where('a.user_werehouse',USER_ID);
		$this->db->where('a.is_approve_hod',1);
		// $this->db->where('a.is_approve_werehouse',0);
		return $this->db->get()->result();
	}

	public function get_data_approval_werehouse()
	{
		$this->db->select('a.*,b.mc_no,b.line_mc_no,c.first_name,d.first_name as user_hod');
		$this->db->from('detail_pr as a');
		$this->db->join('u1632880_machinemanagement.moulding_machine as b','b.id_machine=a.mc_no','left');
		$this->db->join('users as c','c.id=a.user_id');
		$this->db->join('users as d','d.id=a.user_hod');
		$this->db->where('a.user_werehouse',USER_ID);
		$this->db->where('is_approve_hod',1);
		$this->db->where('is_approve_werehouse',0);
		$this->db->order_by('pr_no DESC');
		return $this->db->get()->result();
	}

	public function check_fill_werehouse($pr_no)
	{
		$this->db->select('qty_cb');
		$this->db->from('purchase_requisition');
		$this->db->where('pr_no',$pr_no);
		$this->db->order_by('id_pr','desc');
		$this->db->limit(1);
		return $this->db->get()->row();
	}

	public function get_data_pr_detail_row($id_pr)
	{
		$this->db->select('new_order_qty');
		$this->db->from('purchase_requisition');
		$this->db->where('id_pr',$id_pr);
		return $this->db->get()->row();
	}

	public function get_all_data_to_revision($id_pr)
	{
		$this->db->select('*');
		$this->db->from('purchase_requisition');
		$this->db->where('id_pr',$id_pr);
		return $this->db->get()->row();
	}

	public function check_fill_purchasing($pr_no)
	{
		$this->db->select('supplier_name_pf');
		$this->db->from('purchase_requisition');
		$this->db->where('pr_no',$pr_no);
		$this->db->order_by('id_pr','desc');
		$this->db->limit(1);
		return $this->db->get()->row();
	}

	public function update_purchase_requisition($pr_no,$data)
	{
		$this->db->where('id_pr', $pr_no);
		return $this->db->update('purchase_requisition', $data);
	}

	public function get_data_pr_row($pr_no)
	{
		$this->db->select('a.*,b.description as desc_item,b.item_no');
		$this->db->from('purchase_requisition as a');
		$this->db->join('item as b','b.id_item = a.description');
		$this->db->where('pr_no',$pr_no);
		return $this->db->get()->result();
	}

	public function get_currency_non_ud($id_item)
	{
		$this->db->select('currency');
		$this->db->from('item');
		$this->db->where('id_item',$id_item);
		return $this->db->get()->row();
	}

	public function get_data_pr_row_no_ud($pr_no)
	{
		$this->db->select('a.id_pr,a.pr_no,a.po_no,a.description,a.purpose,a.cost_center,a.category_request,a.new_order_qty,a.uom,a.qty_cb,a.date_cb,a.unit_price_pf,a.currency,a.total_amount,a.date_pf,a.qty_lo_pf,a.unit_price_lo,b.name as supplier_name_pf,a.supplier_lo');
		$this->db->from('purchase_requisition as a');
		$this->db->join('supplier as b','b.code=a.supplier_name_pf');
		// $this->db->join('supplier as c','c.code=a.supplier_lo');
		$this->db->where('pr_no',$pr_no);
		return $this->db->get()->result();
	}

	public function check_part_no($part_no)
	{
		$this->db->select('part_no');
		$this->db->from('purchase_requisition');
		$this->db->where('part_no',$part_no);
		$this->db->order_by('part_no DESC');
		$this->db->limit(1);
		return $this->db->get()->row();
	}

	public function get_data_list_detail_pr_ud($pr_no)
	{
		$this->db->select('a.*,b.expected_material_date_arrive');
		$this->db->from('purchase_requisition as a');
		$this->db->join('detail_pr as b','b.pr_no=a.pr_no','left');
		$this->db->where('a.pr_no',$pr_no);
		return $this->db->get()->result();
	}

	public function get_data_ud_list_des($pr_no)
	{
		$this->db->select('*');
		$this->db->from('purchase_requisition');
		$this->db->where('pr_no',$pr_no);
		return $this->db->get()->result();
	}

	public function get_data_purchasing_field($pr_no)
	{

	$this->db->select("a.id_pr,a.pr_no,a.po_no,a.purpose,a.cost_center,a.category_request,a.qty_cb,a.date_cb,a.new_order_qty as new_order_qty,a.uom,a.unit_price_pf,a.qty_lo_pf,a.unit_price_lo,a.date_pf,a.supplier_lo,b.item_no,b.id_item,b.unit_price as unit_price,b.description as desc_item,b.currency as item_curr,c.name as sup_name,(new_order_qty*unit_price) as total_ammount_item,CASE WHEN d.is_last_order>0 THEN b.unit_price ELSE NULL END AS unit_price_new_lo,CASE WHEN d.is_last_order>0 THEN a.new_order_qty ELSE NULL END AS qty_lo,CASE WHEN d.is_last_order>0 THEN c.name ELSE NULL END AS sup_lo_his,CASE WHEN d.is_last_order>0 THEN d.date_order ELSE NULL END AS date_order_lo,e.expected_material_date_arrive as expected_date",FALSE);
		$this->db->from('purchase_requisition as a');
		$this->db->join('detail_pr as e','e.pr_no=a.pr_no','left');
		$this->db->join('item as b','b.id_item = a.description','left');
		$this->db->join('supplier as c','c.code = b.supplier_code','left');
		$this->db->join('last_order_detail as d','d.id_item = b.id_item');
		$this->db->where('a.pr_no',$pr_no);
		return $this->db->get()->result();
	}

	public function get_data_pr_limit_1($pr_no)
	{
		$this->db->select('currency');
		$this->db->from('purchase_requisition');
		$this->db->where('pr_no',$pr_no);
		$this->db->where('currency IS NOT NULL');
		$this->db->limit(1);
		return $this->db->get()->row();
	}

	public function get_data_detail_pr($pr_no)
	{
		$this->db->select('*');
		$this->db->from('detail_pr');
		$this->db->where('pr_no',$pr_no);
		return $this->db->get()->row();
	}

	public function get_data_detail_pr_row($pr_no)
	{
		$this->db->select('a.*,b.mc_no,b.line_mc_no,c.po_no as po_no_real,d.gih_tool_no');
		$this->db->from('detail_pr as a');
		$this->db->join('u1632880_machinemanagement.moulding_machine as b','b.id_machine=a.mc_no','left');
		$this->db->join('release_po as c','c.pr_no=a.pr_no','left');
		$this->db->join('u1632880_tool_management.mold_basic_details as d','d.id=a.tool_no','left');
		$this->db->where('a.pr_no',$pr_no);
		return $this->db->get()->row();
	}

	public function get_po_no_release($pr_no)
	{
		$this->db->select('po_no');
		$this->db->from('release_po');
		$this->db->where('pr_no',$pr_no);
		return $this->db->get()->row();
	}


	public function get_data_machine()
	{
		$this->db1->select('id_machine,mc_no,line_mc_no');
		$this->db1->from('moulding_machine');
		return $this->db1->get()->result();
	}

	public function get_data_tooling()
	{
		$this->db2->select('id,mc_no,line_mc_no');
		$this->db2->from('moulding_machine');
		return $this->db2->get()->result();
	}

	public function get_department($employee_no)
	{
		$this->db3->select('department');
		$this->db3->from('employees');
		$this->db3->where('employee_no',$employee_no);
		return $this->db3->get()->row();
	}

	public function insert_detail_pr($data)
	{
		return $this->db->insert('detail_pr',$data);
	}
	public function insert_pr($data)
	{
		return $this->db->insert('purchase_requisition',$data);
	}

	public function update_detail_pr($pr_no, $data)
	{
		$this->db->where('pr_no', $pr_no);
		return $this->db->update('detail_pr', $data);
	}

	public function get_user_row($user_id='')//ambil data user yg create TT
	{
		$this->db->select('users.id as user_id, users.email, users.employee_no, users.first_name');
		$this->db->from('users');
		$this->db->where('users.id', $user_id);
		$this->db->where('users.active !=', 0);
		return $this->db->get()->row();
	}

	public function get_last_pr_no()
	{
		$this->db->select('pr_no');
		$this->db->from('purchase_requisition');
		$this->db->order_by('pr_no desc');
		$this->db->limit(1);
		return $this->db->get()->row();
	}

	public function get_last_pr_no_detail()
	{
		$this->db->select('pr_no');
		$this->db->from('detail_pr');
		$this->db->order_by('pr_no desc');
		$this->db->limit(1);
		return $this->db->get()->row();
	}

	public function get_last_po_no_detail()
	{
		$this->db->select('po_no_temp');
		$this->db->from('release_po');
		$this->db->order_by('po_no_temp desc');
		$this->db->limit(1);
		return $this->db->get()->row();
	}



	public function get_data_approval_row()
	{
		$this->db->select('a.*,b.line_mc_no,b.mc_no,c.first_name');
		$this->db->from('detail_pr as a');
		$this->db->join('u1632880_machinemanagement.moulding_machine as b','b.id_machine=a.mc_no','left');
		$this->db->join('users as c','c.id=a.user_id');
		$this->db->where('a.user_hod',USER_ID);
		$this->db->where('a.is_approve_hod',0);
		$this->db->where('a.is_reject_hod',0);
		return $this->db->get()->result();
	}

	public function get_data_history_hod_pr()
	{
		$this->db->select('a.*,b.line_mc_no,b.mc_no,c.first_name');
		$this->db->from('detail_pr as a');
		$this->db->join('u1632880_machinemanagement.moulding_machine as b','b.id_machine=a.mc_no','left');
		$this->db->join('users as c','c.id=a.user_id');
		$this->db->where('a.user_hod',USER_ID);
		return $this->db->get()->result();
	}

	public function get_data_app_purchasing()
	{
		
		$this->db->select('a.*,b.mc_no,b.line_mc_no,c.first_name,d.first_name as user_hod,e.first_name as user_werehouse');
		$this->db->from('detail_pr as a');
		$this->db->join('u1632880_machinemanagement.moulding_machine as b','b.id_machine=a.mc_no','left');
		$this->db->join('users as c','c.id=a.user_id');
		$this->db->join('users as d','d.id=a.user_hod');
		$this->db->join('users as e','e.id=a.user_werehouse');
		$this->db->where('a.user_purchase',USER_ID);
		$this->db->where('is_approve_hod',1);
		$this->db->where('is_approve_werehouse',1);
		$this->db->where('is_approve_purchasing',0);
		$this->db->where('is_reject_by_purchasing',0);
		$this->db->order_by('pr_no DESC');
		return $this->db->get()->result();
	}

		public function get_history_purchasing()
	{
		
		$this->db->select('a.*,b.mc_no,b.line_mc_no,c.first_name,d.first_name as user_hod,e.first_name as user_werehouse');
		$this->db->from('detail_pr as a');
		$this->db->join('u1632880_machinemanagement.moulding_machine as b','b.id_machine=a.mc_no','left');
		$this->db->join('users as c','c.id=a.user_id');
		$this->db->join('users as d','d.id=a.user_hod');
		$this->db->join('users as e','e.id=a.user_werehouse');
		$this->db->where('a.user_purchase',USER_ID);
		$this->db->where('is_approve_hod',1);
		$this->db->where('is_approve_werehouse',1);
		return $this->db->get()->result();
	}

	public function update_date_arrive($pr_no,$data)
	{
		$this->db->where('pr_no', $pr_no);
		return $this->db->update('detail_pr', $data);

	}

	public function get_data_app_finance()
	{

		$this->db->select('a.*,b.mc_no,b.line_mc_no,c.first_name,d.first_name as user_hod,e.first_name as user_werehouse,f.first_name as user_purchase');
		$this->db->from('detail_pr as a');
		$this->db->join('u1632880_machinemanagement.moulding_machine as b','b.id_machine=a.mc_no','left');
		$this->db->join('users as c','c.id=a.user_id');
		$this->db->join('users as d','d.id=a.user_hod');
		$this->db->join('users as e','e.id=a.user_werehouse');
		$this->db->join('users as f','f.id=a.user_purchase');
		$this->db->where('a.user_finance',USER_ID);
		$this->db->where('is_approve_hod',1);
		$this->db->where('is_approve_werehouse',1);
		$this->db->where('is_approve_purchasing',1);
		$this->db->where('is_approve_finance',0);
		$this->db->where('is_reject_finance',0);
		$this->db->order_by('pr_no DESC');
		return $this->db->get()->result();
	}




	public function get_sum_ttl_ammount($pr_no)
	{
		$this->db->select('SUM(new_order_qty*unit_price) as ttl_amount');
		$this->db->from('purchase_requisition as a');
		$this->db->join('item as b','b.id_item=a.description');
		$this->db->where('pr_no',$pr_no);
		return $this->db->get()->row();
	}

	public function get_sum_ttl_ammount_ud($pr_no)
	{
		$this->db->select('SUM(total_amount) as ttl_amount');
		$this->db->from('purchase_requisition');
		$this->db->where('pr_no',$pr_no);
		return $this->db->get()->row();
	}

	public function get_history_finance()
	{
		
		$this->db->select('a.*,b.mc_no,b.line_mc_no,c.first_name,d.first_name as user_hod,e.first_name as user_werehouse,f.first_name as user_purchasing');
		$this->db->from('detail_pr as a');
		$this->db->join('u1632880_machinemanagement.moulding_machine as b','b.id_machine=a.mc_no','left');
		$this->db->join('users as c','c.id=a.user_id');
		$this->db->join('users as d','d.id=a.user_hod');
		$this->db->join('users as e','e.id=a.user_werehouse');
		$this->db->join('users as f','f.id=a.user_purchase');
		$this->db->where('a.user_finance',USER_ID);
		$this->db->where('is_approve_hod',1);
		$this->db->where('is_approve_werehouse',1);
		$this->db->where('is_approve_purchasing',1);
		return $this->db->get()->result();
	}
	public function get_data_history_head()
	{
		
		$this->db->select('a.*,b.mc_no,b.line_mc_no,c.first_name,d.first_name as user_hod,e.first_name as user_werehouse,f.first_name as user_purchasing,g.first_name as user_finance');
		$this->db->from('detail_pr as a');
		$this->db->join('u1632880_machinemanagement.moulding_machine as b','b.id_machine=a.mc_no','left');
		$this->db->join('users as c','c.id=a.user_id');
		$this->db->join('users as d','d.id=a.user_hod');
		$this->db->join('users as e','e.id=a.user_werehouse');
		$this->db->join('users as f','f.id=a.user_purchase');
		$this->db->join('users as g','g.id=a.user_finance');
		$this->db->where('a.user_division_head',USER_ID);
		$this->db->where('is_approve_hod',1);
		$this->db->where('is_approve_werehouse',1);
		$this->db->where('is_approve_purchasing',1);
		$this->db->where('is_approve_finance',1);
		return $this->db->get()->result();
	}


		public function get_data_history_pd()
	{
		
		$this->db->select('a.*,b.mc_no,b.line_mc_no,c.first_name,d.first_name as user_hod,e.first_name as user_werehouse,f.first_name as user_purchasing,g.first_name as user_finance,h.first_name as user_division_head');
		$this->db->from('detail_pr as a');
		$this->db->join('u1632880_machinemanagement.moulding_machine as b','b.id_machine=a.mc_no','left');
		$this->db->join('users as c','c.id=a.user_id');
		$this->db->join('users as d','d.id=a.user_hod');
		$this->db->join('users as e','e.id=a.user_werehouse');
		$this->db->join('users as f','f.id=a.user_purchase');
		$this->db->join('users as g','g.id=a.user_finance');
		$this->db->join('users as h','h.id=a.user_division_head');
		$this->db->where('a.user_pd',USER_ID);
		$this->db->where('is_approve_hod',1);
		$this->db->where('is_approve_werehouse',1);
		$this->db->where('is_approve_purchasing',1);
		$this->db->where('is_approve_finance',1);
		$this->db->where('is_approve_division_head',1);
		$this->db->order_by('pr_no DESC');
		return $this->db->get()->result();
	}

	


	public function get_data_app_head()
	{

		$this->db->select('a.*,b.mc_no,b.line_mc_no,c.first_name,d.first_name as user_hod,e.first_name as user_werehouse,f.first_name as user_purchase,g.first_name as user_finance');
		$this->db->from('detail_pr as a');
		$this->db->join('u1632880_machinemanagement.moulding_machine as b','b.id_machine=a.mc_no','left');
		$this->db->join('users as c','c.id=a.user_id');
		$this->db->join('users as d','d.id=a.user_hod');
		$this->db->join('users as e','e.id=a.user_werehouse');
		$this->db->join('users as f','f.id=a.user_purchase');
		$this->db->join('users as g','g.id=a.user_finance');
		$this->db->where('a.user_division_head',USER_ID);
		$this->db->where('is_approve_hod',1);
		$this->db->where('is_approve_werehouse',1);
		$this->db->where('is_approve_purchasing',1);
		$this->db->where('is_approve_finance',1);
		$this->db->where('is_approve_division_head',0);
		$this->db->where('is_reject_division_head',0);
		return $this->db->get()->result();
	}


	public function get_data_app_pd()
	{

		$this->db->select('a.*,b.mc_no,b.line_mc_no,c.first_name,d.first_name as user_hod,e.first_name as user_werehouse,f.first_name as user_purchase,g.first_name as user_finance,h.first_name as user_division_head');
		$this->db->from('detail_pr as a');
		$this->db->join('u1632880_machinemanagement.moulding_machine as b','b.id_machine=a.mc_no','left');
		$this->db->join('users as c','c.id=a.user_id');
		$this->db->join('users as d','d.id=a.user_hod');
		$this->db->join('users as e','e.id=a.user_werehouse');
		$this->db->join('users as f','f.id=a.user_purchase');
		$this->db->join('users as g','g.id=a.user_finance');
		$this->db->join('users as h','h.id=a.user_division_head');
		$this->db->where('a.user_pd',USER_ID);
		$this->db->where('is_approve_hod',1);
		$this->db->where('is_approve_werehouse',1);
		$this->db->where('is_approve_purchasing',1);
		$this->db->where('is_approve_finance',1);
		$this->db->where('is_approve_division_head',1);
		$this->db->where('is_approve_by_pd',0);
		$this->db->where('is_reject_by_pd',0);
		$this->db->order_by('pr_no DESC');
		return $this->db->get()->result();
	}

		public function get_data_all_pr()
	{
		$this->db->select('a.*,b.mc_no,b.line_mc_no,c.first_name');
		$this->db->from('detail_pr as a');
		$this->db->join('u1632880_machinemanagement.moulding_machine as b','b.id_machine=a.mc_no','left');
		$this->db->join('users as c','c.id=a.user_id','left');
		$this->db->order_by('pr_no desc');
		return $this->db->get()->result();
	}

	public function get_data_print_pr($pr_no)
	{

		$this->db->select('a.*,b.mc_no,b.line_mc_no,c.first_name,d.first_name as user_hod,e.first_name as user_werehouse,f.first_name as user_purchase,g.first_name as user_finance,h.first_name as user_division_head,i.first_name as user_pd');
		$this->db->from('detail_pr as a');
		$this->db->join('u1632880_machinemanagement.moulding_machine as b','b.id_machine=a.mc_no','left');
		$this->db->join('users as c','c.id=a.user_id');
		$this->db->join('users as d','d.id=a.user_hod');
		$this->db->join('users as e','e.id=a.user_werehouse');
		$this->db->join('users as f','f.id=a.user_purchase');
		$this->db->join('users as g','g.id=a.user_finance');
		$this->db->join('users as h','h.id=a.user_division_head');
		$this->db->join('users as i','i.id=a.user_pd');
		$this->db->where('a.pr_no',$pr_no);
		$this->db->where('is_approve_hod',1);
		$this->db->where('is_approve_werehouse',1);
		$this->db->where('is_approve_purchasing',1);
		$this->db->where('is_approve_finance',1);
		$this->db->where('is_approve_division_head',1);
		$this->db->where('is_approve_by_pd',1);
		return $this->db->get()->row();
	}

	public function get_data_cost_center()
	{
		$this->db->select('*');
		$this->db->from('cost_center');
		return $this->db->get()->result();
	}

	public function get_data_cost_row($id)
	{
		$this->db->select('*');
		$this->db->from('cost_center');
		$this->db->where('id',$id);
		return $this->db->get()->row();
	}

	public function insert_cost_center($data)
	{
		return $this->db->insert('cost_center',$data); 
	}

	public function update_cost_center($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('cost_center', $data);
	}

	public function get_data_category_request()
	{
		$this->db->select('*');
		$this->db->from('category_request');
		return $this->db->get()->result();
	}

	public function insert_category_request($data)
	{
		return $this->db->insert('category_request',$data);
	}
	public function get_data_category_row($id)
	{
		$this->db->select('*');
		$this->db->from('category_request');
		$this->db->where('id',$id);
		return $this->db->get()->row();
	}
	public function update_category_request($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('category_request', $data);
	}

	public function get_data_supplier()
	{
		$this->db->select('*');
		$this->db->from('supplier');
		return $this->db->get()->result();
	}

	public function last_code_supp()
	{
		$this->db->select('code');
		$this->db->from('supplier');
		$this->db->order_by('code','desc');
		$this->db->limit(1);
		return $this->db->get()->row();
	}

	public function insert_supplier($data)
	{
		return $this->db->insert('supplier',$data);
	}

	public function get_data_supplier_row($code)
	{
		$this->db->select('*');
		$this->db->from('supplier');
		$this->db->where('code',$code);
		return $this->db->get()->row();
	}

	public function update_supplier($code,$data)
	{
		$this->db->where('code', $code);
		return $this->db->update('supplier', $data);
	}

	public function get_data_item()
	{
		$this->db->select('a.*,b.name as supplier_name');
		$this->db->from('item as a');
		$this->db->join('supplier as b','b.code=a.supplier_code');
		return $this->db->get()->result();
	}
	public function insert_item($data)
	{
		return $this->db->insert('item');
	}

	public function update_item($id_item,$data)
	{
		$this->db->where('id_item',$id_item);
		return $this->db->update('item',$data);
	}

	public function get_data_items_row($id)
	{
		$this->db->select('*');
		$this->db->from('item');
		$this->db->where('id_item',$id);
		return $this->db->get()->row();

	}

	public function get_data_list_item()
	{
		$this->db->distinct()->select('description,MAX(item_no) as item_no,MAX(id_item) as id_item');
		$this->db->from('item');
		$this->db->group_by('description');
		$this->db->order_by('id_item ASC');
		return $this->db->get()->result();
	}

	public function insert_last_order($data)
	{
		return $this->db->insert('last_order_detail',$data); 
	}

	public function insert_history_last_order($data)
	{
		return $this->db->insert('history_last_order_detail',$data); 
	}

	public function check_last_order($id)
	{
		$this->db->select('*');
		$this->db->from('last_order_detail');
		$this->db->where('id_item',$id);
		$this->db->limit(1);
		return $this->db->get()->row();
	}

	public function update_last_order($id,$data)
	{
		$this->db->where('id_item', $id);
		return $this->db->update('last_order_detail', $data);
	}

	public function update_detail_pr_po($pr_no,$data)
	{
		$this->db->where('pr_no',$pr_no);
		return $this->db->update('detail_pr', $data);
	}

	public function update_pr_requi($pr_no,$data)
	{
		$this->db->where('pr_no',$pr_no);
		return $this->db->update('purchase_requisition', $data);
	}

	public function check_date_lo($id_item)
	{
		$this->db->select('date_order');
		$this->db->from('last_order_detail');
		$this->db->where('id_item',$id_item);
		$this->db->limit(1);
		return $this->db->get()->row();
	}

	public function get_data_all_po()
	{
		$this->db->select('*');
		$this->db->from('release_po');
		return $this->db->get()->result();
	}



} ?>