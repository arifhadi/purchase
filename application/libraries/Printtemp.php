<?php

	/**
	 * 
	 */
	class Printtemp {

		protected $_CI;

		function __construct(){
			$this->_CI = &get_instance();
		}

		function view($printtemp,$data=null){
			$this->_CI->load->view('template/back_header');
			$this->_CI->load->view($printtemp,$data);
			$this->_CI->load->view('template/back_footer');
		}


	}

?>